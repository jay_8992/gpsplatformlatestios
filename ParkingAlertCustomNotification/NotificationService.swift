//
//  NotificationService.swift
//  ParkingAlertCustomNotification
//
//  Created by Georadius on 27/08/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UserNotifications


class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

  /*  override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
            
            contentHandler(bestAttemptContent)
        }
    }
    
*/

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let content = bestAttemptContent {
            func failEarly() {
                contentHandler(request.content)
            }
            guard let apnsData = content.userInfo["data"] as? [String: Any] else {
                return failEarly()
            }
            guard let mediaURLString = apnsData["media-url"] as? String else {
                return failEarly()
            }
            guard let mediaURL = URL(string: mediaURLString) else {
                return failEarly()
            }
            
            URLSession.shared.downloadTask(with: mediaURL, completionHandler: { (location, response, error) in
                if let downloadedURL = location {
                    let tempDirectory = NSTemporaryDirectory()
                    let tempFile = "file://".appending(tempDirectory).appending( mediaURL.lastPathComponent)
                    let tempURL = URL(string: tempFile)
                    if let tmpUrl = tempURL {
                        try? FileManager.default.moveItem( at: downloadedURL, to: tmpUrl)
                        if let attachment = try? UNNotificationAttachment( identifier: "image.png", url: tmpUrl) {
                            self.bestAttemptContent?.attachments = [attachment]
                        }
                    }
                }
                self.contentHandler!(self.bestAttemptContent!)
            }).resume()
            
            let firstAction = UNNotificationAction( identifier: "VIEW", title: "View", options: [.foreground])

            let secondAction = UNNotificationAction( identifier: "IGNORE", title: "Ignore", options: [.foreground])

            let category = UNNotificationCategory( identifier: "Parking_Alert", actions: [firstAction, secondAction], intentIdentifiers: [], options: [])

            UNUserNotificationCenter.current().setNotificationCategories([category])
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
