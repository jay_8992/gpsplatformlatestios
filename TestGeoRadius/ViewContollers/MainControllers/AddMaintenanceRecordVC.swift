//
//  AddMaintenanceRecordVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import EzPopup

class AddMaintenanceRecordVC: UIViewController,DataDelegate,PopUpDataDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var scrollOffset : CGFloat = 0
    var distance : CGFloat = 0
    @IBOutlet weak var vehicleTF: UITextField!
    @IBOutlet weak var activityNameTF: UITextField!
    @IBOutlet weak var reminderTypeTF: UITextField!
    @IBOutlet weak var notificationTF: UITextField!
    @IBOutlet weak var alertNotifTF: UITextField!
    var activeField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    var selectedDeviceId = [String]()
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var labels: Array<UILabel>!
    @IBOutlet weak var btn_addMaintenance: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setLanguageInControls()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setLanguageInControls()
    {
        print("labels count :\(labels.count)")
        if labels.count == 5
        {
            for index in 0...4
            {
                switch index {
                case 0:
                    labels[index].text = LanguageHelperClass().vehicleTxt
                case 1:
                    labels[index].text = LanguageHelperClass().activityText
                case 2:
                    labels[index].text = LanguageHelperClass().reminderTypeTxt
                case 3:
                    labels[index].text = LanguageHelperClass().notifTitleTxt
                case 4:
                    labels[index].text = LanguageHelperClass().alertNotifTxt
                default:
                    print("done")
                }
            }
        }
        titleLbl.text = LanguageHelperClass().addMaintenanceTxt
        btn_addMaintenance.setTitle(LanguageHelperClass().maintenanceSubText, for: .normal)
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var safeArea = self.view.frame
            safeArea.size.height += scrollView.contentOffset.y
            safeArea.size.height -= keyboardSize.height + (UIScreen.main.bounds.height*0.04) // Adjust buffer to your liking
            // determine which UIView was selected and if it is covered by keyboard
            let activeField: UIView? = [vehicleTF, activityNameTF, reminderTypeTF,notificationTF,alertNotifTF].first { $0.isFirstResponder }
            if let activeField = activeField {
                if safeArea.contains(CGPoint(x: 0, y: activeField.frame.maxY)) {
                    print("No need to Scroll")
                    return
                } else {
                    distance = activeField.frame.maxY - safeArea.size.height
                    scrollOffset = scrollView.contentOffset.y
                    self.scrollView.setContentOffset(CGPoint(x: 0, y: scrollOffset + distance), animated: true)
                }
            }
            // prevent scrolling while typing
            scrollView.isScrollEnabled = false
        }
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if distance == 0 {
            return
        }
        // return to origin scrollOffset
        self.scrollView.setContentOffset(CGPoint(x: 0, y: scrollOffset), animated: true)
        scrollOffset = 0
        distance = 0
        scrollView.isScrollEnabled = true
    }
    
    
    @IBAction func textFieldSelected(_ sender: UITextField) {
        print("sender.tag:\(sender.tag)")
        switch sender.tag {
        case 0:
            sender.resignFirstResponder()
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
            contentVC.delegate = self
            // Init popup view controller with content is your content view controller
            let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        case 1:
            print("case 1")
            
        case 2:
            print("case 2")
            sender.resignFirstResponder()
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            contentVC.titleArr = ["Days","Km"]
            contentVC.popUpType = "ReminderType"
            // Init popup view controller with content is your content view controller
            let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
            let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize) , popupWidth: contentVC.view.frame.size.width, popupHeight: 150.0)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        case 3:
            print("case 2")
            sender.resignFirstResponder()
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            contentVC.titleArr = ["Email","SMS","Both"]
            contentVC.popUpType = "NotificationType"
            // Init popup view controller wth content is your content view controller
            let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
            let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize) , popupWidth: contentVC.view.frame.size.width, popupHeight: 200.0)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        default:
            print("default")
        }
    }
    
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
        print("device id:\(deviceId)")
        selectedDeviceId = deviceId
        if self.selectedDeviceId.count > 0
        {
            self.vehicleTF.text = "\(self.selectedDeviceId.count) selected"
        }
    }
    
    
    func sendData(sender: PopUpVC, selectedType: String, popUpType: String) {
        if popUpType == "ReminderType"
        {
            self.reminderTypeTF.text = selectedType
        }
        else
        {
            self.notificationTF.text = selectedType
        }
    }
    
    
    @IBAction func submitMaintenanceRecord(_ sender: UIButton) {
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AddMaintenanceRecordVC: UITextFieldDelegate {
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //        activeField = textField
    //        lastOffset = self.scrollView.contentOffset
    //        return true
    //    }
    //
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //        activeField?.resignFirstResponder()
    //        activeField = nil
    //        return true
    //    }
}

// MARK: Keyboard Handling




