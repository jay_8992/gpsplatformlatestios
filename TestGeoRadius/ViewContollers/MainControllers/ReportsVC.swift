//
//  ReportsVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit



class ReportsVC: UIViewController {
    
    @IBOutlet weak var tbl_report_items: UITableView!
    @IBOutlet weak var reportCollectionView: UICollectionView!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var leftSideBarBtn: UIButton!
    
    var name = [String]()
    var images = ["alert.svg", "fleetusage_icon.svg", "overspeed_icon.svg", "history_icon.svg", "trip_icon.svg", "distance_icon.svg", "fuel_icon.svg", "device_command_icon.svg", "camera_icon.svg","temperature_icon.svg"]
    let pink_color = UIColor.init(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    let skyblue_color = UIColor.init(red: 135/255, green: 206/255, blue: 250/255, alpha: 1)
    let vividred_color = UIColor.init(red: 247/255, green: 13/255, blue: 26/255, alpha: 1)
    let darkorange_color = UIColor.init(red: 255/255, green: 140/255, blue: 0/255, alpha: 1)
    let green_color = UIColor.init(red: 0/255, green: 128/255, blue: 0/255, alpha: 1)
    var colors = [UIColor]()
    var language = ""
    @IBOutlet weak var titleLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = getTargetName()
        colors = [UIColor.red,Global_Blue_Color,darkorange_color,vividred_color,skyblue_color,UIColor.purple,green_color,UIColor.black,UIColor.darkText,pink_color]
        setListTitles()
        header_view.dropShadow(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        SideMenu()
        // Do any additional setup after loading the view.
        // self.tbl_report_items.register(UINib(nibName: "Report_Billing_Cell", bundle: nil), forCellReuseIdentifier: "Report_Billing_Cell")
        self.reportCollectionView.delegate = self
        self.reportCollectionView.dataSource = self
    }
    
    func setTabBarItems(){
          
         let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
         print("domain name in reports:\(domain_name)")
        
        if domain_name == "https://doordrishti.co"
        
        {
            name.append(LanguageHelperClass().customRptTxt)
            images.append("custom_report.svg")
            colors.append(UIColor.darkGray)
        }
        
        if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String]{
            print("arr:\(arr), arr count:\(arr.count)")
            if !arr.contains(where: {$0 == "76"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().alertsTxt) {
                    name.remove(at: index)
                    colors.remove(object: UIColor.red)
                    images.remove(object: "alert.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "32"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().fleetUSageTxt) {
                    name.remove(at: index)
                    colors.remove(object: Global_Blue_Color)
                    images.remove(object: "fleetusage_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "45"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().overspeedTxt) {
                    name.remove(at: index)
                    colors.remove(object: darkorange_color)
                    images.remove(object: "overspeed_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "30"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().mapHistoryTxt) {
                    name.remove(at: index)
                    colors.remove(object: vividred_color)
                    images.remove(object: "history_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "42"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().tripsTxt) {
                    name.remove(at: index)
                    colors.remove(object: skyblue_color)
                    images.remove(object: "trip_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "31"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().vehicleDistanceTxt) {
                    name.remove(at: index)
                    colors.remove(object: UIColor.purple)
                    images.remove(object: "distance_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "142"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().fuelTxt) {
                    name.remove(at: index)
                    colors.remove(object: green_color)
                    images.remove(object: "fuel_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            if !arr.contains(where: {$0 == "228"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().demindCommandTxt) {
                    name.remove(at: index)
                    colors.remove(object: UIColor.black)
                    images.remove(object: "device_command_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            if !arr.contains(where: {$0 == "200"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().cameraTxt) {
                    name.remove(at: index)
                    colors.remove(object: UIColor.darkText)
                    images.remove(object: "camera_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            if !arr.contains(where: {$0 == "63"})
            {
                if let index = name.firstIndex(of: LanguageHelperClass().temperatureTxt) {
                    name.remove(at: index)
                    colors.remove(object: pink_color)
                    images.remove(object: "temperature_icon.svg")
                } else {
                    // not found
                    print("arr not found")
                }
            }
            print("reports arr:\(name)")
        }
    }
    
    
    func setListTitles()
    {
       // language = UserDefaults.standard.value(forKey: "language") as! String
      //  let bd = setLanguage(lang : language)
        name.append(LanguageHelperClass().alertsTxt)
        name.append(LanguageHelperClass().fleetUSageTxt)
        name.append(LanguageHelperClass().overspeedTxt)
        name.append(LanguageHelperClass().mapHistoryTxt)
        name.append(LanguageHelperClass().tripsTxt)
        name.append(LanguageHelperClass().vehicleDistanceTxt)
        name.append(LanguageHelperClass().fuelTxt)
        name.append(LanguageHelperClass().demindCommandTxt)
        name.append(LanguageHelperClass().cameraTxt)
        name.append(LanguageHelperClass().temperatureTxt)
       // name.append(LanguageHelperClass().customRptTxt)
        self.setTabBarItems()
    }
    
    
    @IBAction func notificationBtnClicked(_ sender: UIButton) {
        //is_queue = false
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "alert_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AlertReportVC") as! AlertReportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "fleet_usage"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FleetUsageVC") as! FleetUsageVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "overspeed"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OverspeedVC") as! OverspeedVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "map_history"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapHistoryVC") as! MapHistoryVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "trip_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TripReportVC") as! TripReportVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "fuel_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FuelReportVC") as! FuelReportVC
            vc.fromPage = "FuelReport"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "vehicle_distance"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleDistanceVC") as! VehicleDistanceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "CameraVC"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "device_command"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeviceCommandVC") as! DeviceCommandVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "temp_report"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FuelReportVC") as! FuelReportVC
            vc.fromPage = "Temperature"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if identifier == "customrpt"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomReportVC") as! CustomReportViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
}

extension ReportsVC : UITableViewDelegate, UITableViewDataSource,UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report_items.dequeueReusableCell(withIdentifier: "Report_Billing_Cell") as! Report_Billing_Cell
        cell.lbl_report_name.text = name[indexPath.row]
        //cell.img_report.image = UIImage(named: images[indexPath.row])
        let namSvgImgVar: SVGKImage = SVGKImage(named: images[indexPath.row])
        //let namSvgImgVyuVar = SVGKImageView(svgkImage: namSvgImgVar)
        let namImjVar: UIImage = namSvgImgVar.uiImage
        cell.img_report.image = namImjVar
        
        
        // cell.img_report.image = namImjVar
        // UIImage(named: namImjVar)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report_items.frame.size.height / 7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected report:\(name[indexPath.row])")
        switch name[indexPath.row] {
        case LanguageHelperClass().alertsTxt:
            self.performSegue(withIdentifier: "alert_report", sender: self)
        case LanguageHelperClass().fleetUSageTxt:
            self.performSegue(withIdentifier: "fleet_usage", sender: self)
        case LanguageHelperClass().overspeedTxt:
            self.performSegue(withIdentifier: "overspeed", sender: self)
        case LanguageHelperClass().mapHistoryTxt:
            self.performSegue(withIdentifier: "map_history", sender: self)
        case LanguageHelperClass().tripsTxt:
            self.performSegue(withIdentifier: "trip_report", sender: self)
        case LanguageHelperClass().vehicleDistanceTxt:
             self.performSegue(withIdentifier: "vehicle_distance", sender: self)
        case LanguageHelperClass().fuelTxt:
             self.performSegue(withIdentifier: "fuel_report", sender: self)
        case LanguageHelperClass().demindCommandTxt:
             self.performSegue(withIdentifier: "device_command", sender: self)
        case LanguageHelperClass().cameraTxt:
            self.performSegue(withIdentifier: "CameraVC", sender: self)
        case LanguageHelperClass().temperatureTxt:
             self.performSegue(withIdentifier: "temp_report", sender: self)
        case LanguageHelperClass().customRptTxt:
            self.performSegue(withIdentifier: "customrpt", sender: self)
        default:
            print("out of index")
        }
        
       /*
        if indexPath.row == 0{
            self.performSegue(withIdentifier: "alert_report", sender: self)
        }
        if indexPath.row == 1{
            self.performSegue(withIdentifier: "fleet_usage", sender: self)
        }
        if indexPath.row == 2{
            //
            self.performSegue(withIdentifier: "overspeed", sender: self)
        }
        if indexPath.row == 3{
            self.performSegue(withIdentifier: "map_history", sender: self)
        }
        if indexPath.row == 4{
            self.performSegue(withIdentifier: "trip_report", sender: self)
        }
        if indexPath.row == 5{
            self.performSegue(withIdentifier: "vehicle_distance", sender: self)
        }
        if indexPath.row == 6{
            self.performSegue(withIdentifier: "fuel_report", sender: self)
        }
        if indexPath.row == 7{
            self.performSegue(withIdentifier: "device_command", sender: self)
        }
        if indexPath.row == 8{
            self.performSegue(withIdentifier: "CameraVC", sender: self)
        }
       */
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return name.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = reportCollectionView.dequeueReusableCell(withReuseIdentifier: "ReportCell", for: indexPath) as! ReportPageCollectionViewCell
        cell.layer.cornerRadius = 20.0
        cell.titleLbl.text = name[indexPath.item]
        let namSvgImgVar: SVGKImage = SVGKImage(named: images[indexPath.item])
        let namImjVar: UIImage = namSvgImgVar.uiImage
        cell.iconView.image = namImjVar
        cell.titleLbl.textColor = colors[indexPath.item]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var noOfCellsInRow = 0
        var margin:CGFloat = 0.0
        
        if (UIDevice.current.userInterfaceIdiom == .pad){
            print("iPad")
            noOfCellsInRow = 3
            margin = 40.0
        }
        else
        {
            noOfCellsInRow = 2
            margin = 20.0
        }
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch name[indexPath.item] {
        case LanguageHelperClass().alertsTxt:
            self.performSegue(withIdentifier: "alert_report", sender: self)
        case LanguageHelperClass().fleetUSageTxt:
            self.performSegue(withIdentifier: "fleet_usage", sender: self)
        case LanguageHelperClass().overspeedTxt:
            self.performSegue(withIdentifier: "overspeed", sender: self)
        case LanguageHelperClass().mapHistoryTxt:
            self.performSegue(withIdentifier: "map_history", sender: self)
        case LanguageHelperClass().tripsTxt:
            self.performSegue(withIdentifier: "trip_report", sender: self)
        case LanguageHelperClass().vehicleDistanceTxt:
             self.performSegue(withIdentifier: "vehicle_distance", sender: self)
        case LanguageHelperClass().fuelTxt:
             self.performSegue(withIdentifier: "fuel_report", sender: self)
        case LanguageHelperClass().demindCommandTxt:
             self.performSegue(withIdentifier: "device_command", sender: self)
        case LanguageHelperClass().cameraTxt:
            self.performSegue(withIdentifier: "CameraVC", sender: self)
        case LanguageHelperClass().temperatureTxt:
             self.performSegue(withIdentifier: "temp_report", sender: self)
        case LanguageHelperClass().customRptTxt:
             self.performSegue(withIdentifier: "customrpt", sender: self)
        default:
            print("out of index")
        }
        
    }
    
    
    func SideMenu(){
        NetworkManager().StopAllThreads()
        leftSideBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
}


