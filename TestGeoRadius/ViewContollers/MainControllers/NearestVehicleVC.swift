//
//  NearestVehicleVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 01/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class NearestVehicleVC: UIViewController {
    
    @IBOutlet weak var vehicleListView: UITableView!
    var deviceId: String!
    var coordinate:String!
    var alert_view = AlertView.instanceFromNib()
    var cellSpacingHeight:CGFloat = 10.0
    var data : [NearestVehicleModel] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        CallVehicleFromServer()
        // Do any additional setup after loading the view.
    }
    
//    https://track.georadius.in/vehicle_result.php?action=nearest_vehicles&device_id=12211&latitude=28.647699&longitude=77.19366&user_name=apps@nms.co.in&hash_key=KSTFMRJZQIBVNIDA
    
    
    func CallVehicleFromServer(){
           
           if !NetworkAvailability.isConnectedToNetwork() {
                      
                      showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
                      self.vehicleListView.isHidden = true
                      self.alert_view.removeFromSuperview()
                      return
                  }
                 
           self.view.addSubview(alert_view)
           self.data.removeAll()
          
           let coordinateArr = coordinate.filter { !$0.isWhitespace }.components(separatedBy: ",")
           let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
           let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
           let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
           
           let urlString = domain_name + Nearest_VehicleList + deviceId + "&latitude=" + coordinateArr[0] + "&longitude=" + coordinateArr[1] + "&user_name=" + user_name + "&hash_key=" + hash_key
        
           print("url : \(urlString)")
           
           NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
               if isNetwork && data != nil{
                   //self.tbl_reports.isHidden = false
                  let response =  data![K_Data] as? [[String:Any]]
                   for name in response! {
                       let v_name = name
                       let registration_no = v_name["registration_no"] as? String ?? "---"
                       let duration = v_name["time_duration"] as? String ?? "---"
                       let driverName = v_name["driver_name"] as? String ?? "---"
                       let driver_phone_number = v_name["driver_phone_number"] as? String ?? "---"
                    
                       let newData = NearestVehicleModel(vehicle_no: registration_no, vehicle_duration: duration, driver_name: driverName, driver_no: driver_phone_number)
                       self.data.append(newData)
                   }
                   
                   //self.filterdData = self.vehicle_data
                   self.vehicleListView.delegate = self
                   self.vehicleListView.dataSource = self
                   self.vehicleListView.reloadData()
               }else{
                   
                   //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                   self.vehicleListView.isHidden = true
                   self.alert_view.removeFromSuperview()
                   print("ERROR FOUND")
               }
               
               if r_error != nil{
                   print("sdfsdlklk \(String(describing: r_error))")
               }
               self.alert_view.removeFromSuperview()
           })
       }
    
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NearestVehicleVC:UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 3.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = vehicleListView.dequeueReusableCell(withIdentifier: "NearVehiclesCell") as! NearestVehiclesTableViewCell
         cell.vehicleLbl.text = data[indexPath.section].vehicle_no
        // cell.durationLbl.text = data[indexPath.section].vehicle_duration
         cell.driverNameLbl.text = data[indexPath.section].driver_name
         cell.phoneNameLbl.text = data[indexPath.section].driver_no
         cell.layer.borderColor = UIColor.lightGray.cgColor
         cell.layer.borderWidth = 0.5
         cell.layer.cornerRadius = 8
        return cell
    }
    
  
}
