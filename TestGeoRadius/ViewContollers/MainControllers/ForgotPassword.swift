//
//  ForgotPassword.swift
//  GeoTrack
//
//  Created by Georadius on 20/09/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ForgotPassword: UIViewController {
    
    @IBOutlet weak var btn_submit: UIButton!
    
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var view_login_card: UIView!
    @IBOutlet weak var lbl_main_error: UILabel!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    let alert_view = AlertView.instanceFromNib()
    var language = ""
    var bd:Bundle!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        language = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : language)
        SetTextFieldLeftSide(imageName : "email", txt_field: txt_email)
        SetTextFieldLeftSide(imageName : "driver_mobile", txt_field: txt_phone)
        img_icon.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        view_login_card.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        SetLanguage()
        Set_Layer()
    }
    
    @IBAction func pressed_back(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func pressed_submit(_ sender: Any) {
        
        if txt_email.text!.count < 1 && txt_phone.text!.count < 1{
        lbl_main_error.isHidden = false
            return
        }
        
        if  txt_phone.text!.count < 10 || isValidEmail(candidate: txt_email.text!) == false || isValidEmail(candidate: txt_phone.text!) == false
        {
            lbl_main_error.text = "Please provide valid email id or phone no."
            lbl_main_error.isHidden = false
            return
        }
        
        lbl_main_error.isHidden = true
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let urlString = domain_name + Forgot_Password + "action=change_password&data_format=1&phone=" + txt_phone.text! + "&email=" + txt_email.text!
   
        NetworkManager().CallForgotPasswordAPI(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{

                showToast(controller: self, message : data!, seconds: 2.0)
                
            }else{
                
                showToast(controller: self, message : self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
    func Set_Layer(){
        Set_Card_View(Card: view_login_card)
        Set_Login_Radius(Button: btn_submit)
    }

    func SetLanguage()
      {
        
        txt_email.placeholder = self.bd.localizedString(forKey: "EMAIL", value: nil, table: nil)
        txt_phone.placeholder = self.bd.localizedString(forKey: "PHONE", value: nil, table: nil)
        let name = self.bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
         btn_submit.setTitle(name, for: .normal)
        titleLbl.text = bd.localizedString(forKey: "FORGET_PASSWORD", value: nil, table: nil)
        lbl_main_error.text = self.bd.localizedString(forKey: "FILL_FIELD_DATA", value: nil, table: nil)
     }
    
    func isValidPhone(phone: String) -> Bool {

        let phoneRegex = "^((0091)|(\\+91)|0?)[6789]{1}\\d{9}$";
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }

    //For email validation
    func isValidEmail(candidate: String) -> Bool {

        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
        if valid {
            valid = !candidate.contains("..")
        }
        return valid
    }

}
