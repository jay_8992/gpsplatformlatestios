//
//  ParkingAlertNotifVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 27/08/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import AVFoundation

class ParkingAlertNotifVC: UIViewController {
    @IBOutlet weak var msgLbl: UILabel!
    var bombSoundEffect: AVAudioPlayer?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var viewBtn: UIButton!
    @IBOutlet weak var exitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let path = Bundle.main.path(forResource: "security_tone.aiff", ofType:nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            bombSoundEffect = try AVAudioPlayer(contentsOf: url)
            bombSoundEffect?.play()
        } catch {
            // couldn't load file :(
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func viewAlertPage(_ sender: Any) {
        setParkingAlertOff()
    }
    
    @IBAction func exitFromApp(_ sender: Any) {
        bombSoundEffect?.stop()
    }
  
   
    func setParkingAlertOff() {
           
           if let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as? String, let device_id = UserDefaults.standard.value(forKey: "deviceID") as? String
           {
               let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
               let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
               let url = domain_name + "/geofence.php"
               let parameters : Dictionary<String, Any> = [
                   "action" : "parking",
                   "user_name" : user_name,
                   "hash_key" : hash_key,
                   "parking_status": "0",
                   "latitude":0,
                   "longitude" : 0,
                   "device_id": device_id
               ]
               print("parking set URL: \(url), parameters:\(parameters)")
               NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler: {data, r_error, isNetwork in
                   if isNetwork{
                       let result = data?[K_Result] as? Int ?? 100
                       switch (result){
                       case 0 :
                           if let message = data?[K_Message] as? String
                           {
                               print("parking message is :\(message)")
                           }
                           break
                       case _ where result > 0 :
                           let message = data?[K_Message] as! String
                           print(message)
                           break
                       default:
                           print("Default Case")
                       }
                   }else{
                       //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                       print("ERROR FOUND")
                   }
               })
           }
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
