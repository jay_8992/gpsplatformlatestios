//
//  SearchResultsVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 06/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit


protocol DataDelegate: class {
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String])
}


class SearchResultsVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var vehiclesList = ["D1","D2","D3","D4"]
    var cellSpacingHeight:CGFloat = 15.0
    var filterdData : [FuelReportModel] = []
    var data : [FuelReportModel] = []
    var isSelectAll = false
    var alert_view = AlertView.instanceFromNib()
    var searchActive : Bool = false
    var single_selection_index = [Int]()
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    @IBOutlet weak var selectAllBtn: UIButton!
    weak var delegate: DataDelegate?
    var urlString = ""
    @IBOutlet weak var all_select_view: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        self.tableView.tableFooterView = UIView()
        CallVehicleFromServer()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectAllDevices(_ sender: Any) {
        
        let senderBtn: UIButton = sender as! UIButton
        single_selection_index.removeAll()
        print(isSelectAll)
        if isSelectAll == true {
            isSelectAll = false
            senderBtn.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            single_selection_index.removeAll()
            selected_devie_id.removeAll()
        }
        else{
            isSelectAll = true
            if (searchActive)
            {
                
                for (index,_) in filterdData.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(filterdData[index].device_id)
                }}
            else
            {
                for (index,_) in data.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(all_selected_device_id[index])
                }
            }
            senderBtn.setBackgroundImage(UIImage(named: "check"), for: .normal)
        }
        
        print("selected device id arr;\(selected_devie_id)")
        tableView.reloadData()
    }
    
    func CallVehicleFromServer(){
        
        
        if !NetworkAvailability.isConnectedToNetwork() {
                   
                   showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
                   self.tableView.isHidden = true
                   self.alert_view.removeFromSuperview()
                   return
               }
              
        self.view.addSubview(alert_view)
        self.data.removeAll()
        self.selected_devie_id.removeAll()
        self.filterdData.removeAll()
        
        var selectedURL = ""
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        if urlString.count > 2
        {
            selectedURL = self.urlString
            
        }
        else
        {
            selectedURL = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        }
        
        print("selected url:\(selectedURL)")
        NetworkManager().CallVehicleData(urlString: selectedURL, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    let registration_no = v_name["registration_no"] as! String
                    let device_id = v_name["device_id"] as! String
                    self.all_selected_device_id.append(device_id)
                    let newData = FuelReportModel(registration_no: registration_no, device_id: device_id)
                    self.data.append(newData)
                }
                
                print("all device id:\(self.all_selected_device_id)")
                self.filterdData = self.data
                //self.filterdData = self.vehicle_data
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.searchBar.delegate = self
                self.tableView.reloadData()
            }else{
                
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tableView.isHidden = true
                self.alert_view.removeFromSuperview()
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    
    
    
    @IBAction func CloseView(_ sender: UIButton) {
        delegate?.sendDeviceData(sender: self, deviceId: self.selected_devie_id)
        dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SearchResultsVC: UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return filterdData.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        print("selected devices :\(self.selected_devie_id) , filtered devices :\(filterdData))")
        if searchActive && filterdData.count > 0 {
            if selected_devie_id.contains(filterdData[indexPath.section].device_id)
            {
                print("hide uncheck image")
                cell.img_uncheck.isHidden = true
                cell.img_check.isHidden = false
            }
            else
            {
                print("show uncheck image")
                cell.img_uncheck.isHidden = false
            }
            cell.lbl_item_name.text = filterdData[indexPath.section].registration_no
            
        }
        else{
            for val in single_selection_index{
                if indexPath.section == val{
                    if cell.img_uncheck.isHidden{
                        cell.img_uncheck.isHidden = false
                    }else{
                        cell.img_uncheck.isHidden = true
                        cell.img_check.isHidden = false
                    }
                }
            }
            cell.lbl_item_name.text = data[indexPath.section].registration_no
            
        }
        
      /*  if searchActive{
            cell.lbl_item_name.text = filterdData[indexPath.section].registration_no
        }
        else{
            cell.lbl_item_name.text = data[indexPath.section].registration_no
        }
     */
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! VehicleCell
        if  cell.img_uncheck.isHidden{
            if (searchActive)
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != filterdData[indexPath.section].device_id}
            }
            else
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]
            }
                single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
            }}
            
        else{
            if (searchActive)
            {
                self.selected_devie_id.append(filterdData[indexPath.section].device_id)
            }
            else
            {
                self.selected_devie_id.append(self.all_selected_device_id[indexPath.section])
            }
            
            self.single_selection_index.append(indexPath.section)
        }
        print("selected device id:\(self.selected_devie_id)")
    
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filterdData.count
        } else {
            count = data.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          self.searchBar.endEditing(true)
          self.searchBar.text = ""
          self.searchBar.showsCancelButton = false
          self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterdData = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        print("search data:\(filterdData)")
        if(filterdData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tableView.reloadData()
    }
}


