//
//  SetParkingAlertVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 26/08/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

protocol ParkingAlertDelegate: class {
    func sendParkingAlertStatus(status: String)
}
class SetParkingAlertVC: UIViewController {
    
    @IBOutlet weak var parkingStatusLbl: UILabel!
    var device_id: String!
    var lat:Double!
    var long:Double!
    var parking_status:String!
    var parking_mode_val:String!
    var vehicle_status:Int!
    weak var delegate: ParkingAlertDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if parking_status == "ON"
        {
            self.parking_mode_val = "0"
            self.parkingStatusLbl.text = "Disable Parking Mode"
        }
        else
        {
            self.parking_mode_val = "1"
            self.parkingStatusLbl.text = "Enable Parking Mode"
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closePopVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitParkingAlert(_ sender: Any) {
        
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let url = domain_name + "/geofence.php"
        print("device id for parking is :\(device_id)")
        let parameters : Dictionary<String, Any> = [
                 "action" : "parking",
                 "user_name" : user_name,
                 "hash_key" : hash_key,
                 "parking_status": self.parking_mode_val!,
                 "latitude": self.lat!,
                 "longitude" : self.long!,
                 "device_id": self.device_id!
        ]
        
             print("parking set URL: \(url), parameters:\(parameters)")
             NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler: {data, r_error, isNetwork in
                 if isNetwork{
                     let result = data?[K_Result] as? Int ?? 100
                     switch (result){
                     case 0 :
                         if let message = data?[K_Message] as? String
                         {
                            if message == "Parking added successfully."
                            {
                                self.parking_mode_val = "1"
                                self.closePopViewController(message: message)
                            }
                            else if message == "Parking is turned off."
                            {
                                self.parking_mode_val = "0"
                                self.closePopViewController(message: message)
                            }
                            else
                            {
                                showToast(controller: self, message: message, seconds: 3.0)
                            }
                            self.delegate?.sendParkingAlertStatus(status: self.parking_mode_val)
                         }
                         //self.SetAllData()
                         break
                         
                     case _ where result > 0 :
                         let message = data?[K_Message] as! String
                         print(message)
                         break
                     default:
                         print("Default Case")
                     }
                     
                 }else{
                     //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                     print("ERROR FOUND")
                 }
             })
    }
    
    func closePopViewController(message:String) {
        self.presentingViewController?.dismiss(animated: true, completion: {
            let alertMessage = UIAlertController(title: message, message: "", preferredStyle: .alert)
               let alertButton = UIAlertAction(title: "OK", style: UIAlertAction.Style.default)
               alertMessage.addAction(alertButton)
            UIApplication.getTopMostViewController()?.present(alertMessage, animated: true, completion: nil)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
