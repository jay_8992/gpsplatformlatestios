//
//  GeofenceListViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 06/05/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import EzPopup
import RSSelectionMenu

class GeofenceListViewController: UIViewController,DataDelegate {
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
        print("device id:\(deviceId)")
        if selectedDeviceId.count > 0
        {
            selectedDeviceId = []
        }
        selectedDeviceId = deviceId
        dismiss(animated: true) {
            if deviceId.count > 0
            {
                if deviceId.count == 0
                {
                     self.vehicleTF.text = "1 vehicle selected"
                }
                else
                {
                     self.vehicleTF.text = " \(deviceId.count) selected"
                }
            }
        }
    }
    
    var selectedDeviceId = [String]()
    let alert_view = AlertView.instanceFromNib()
    var geofencingData = NSArray()
    var cellSpacingHeight:CGFloat = 15.0
    @IBOutlet weak var tableView: UITableView!
    var noDataLabel: UILabel!
    var regNoArr = [String]()
    var selectedDataArray = [String]()
    var simpleSelectedArray = [String]()
    var vehicle_type_id = [String]()
    var cellSelectionStyle: CellSelectionStyle = .tickmark
    var device_id_arr = [String]()
    var val_vehicle_type_id : String!
    var geofence_id_val : String!
    var geofence_name_arr = [String]()
    var geofence_id_arr = [String]()

    @IBOutlet weak var geofenceNameTF: UITextField!
    @IBOutlet weak var vehicleTF: UITextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        noDataLabel = UILabel(frame: CGRect(x: 0, y: self.tableView.frame.origin.y, width: tableView.bounds.size.width, height: 30))
        noDataLabel.translatesAutoresizingMaskIntoConstraints = false
        noDataLabel.numberOfLines = 0
        noDataLabel.text = "No Records Found"
        noDataLabel.textColor = UIColor.darkText
        noDataLabel.textAlignment = .center
        self.view.addSubview(noDataLabel)
        noDataLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        noDataLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noDataLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        noDataLabel.isHidden = true
        tableView.tableFooterView  = UIView()
       // titleLbl.text = LanguageHelperClass().geofencingTxt
       // getAllVehiclesGeofenceData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectGeofenceName(_ sender: UITextField) {
        
        let senderTF: UITextField = sender
        senderTF.resignFirstResponder()
        self.view.addSubview(alert_view)
        self.vehicle_type_id.removeAll()
        self.device_id_arr.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + Geofence_List
        
        print("geofence list url \(urlString)")
        
        let parameters_new : Dictionary<String, Any> = [
            "action" : "geofence_list",
            "user_name" : user_name,
            "hash_key" : hash_key,
            "user_id" : user_id
        ]
        
        
        NetworkManager().CallNewTrackResult(urlString: urlString, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
            
            let result = data?[K_Result] as! Int
            print("result:\(result)")
            
            switch (result){
                
            case 0 :
                
                let c_data = data?[K_Data] as! [Dictionary<String, Any>]

                if c_data.count == 0
                {
                    DispatchQueue.main.async {
                        showToast(controller: self, message: "Reloading Data..", seconds: 1.0)
                    }
                }
                    
                else
                {
                   self.geofence_name_arr = c_data.map({$0 ["geofence_name"] as? String ?? "" })
                   self.geofence_id_arr = c_data.map({$0 ["geofence_id"] as? String ?? "" })
                    DispatchQueue.main.async
                    {
                    UIView.animate(withDuration: 0.8, animations: {
                        self.showSingleSelectionMenu(style: .present)
                    })
                    }
                }
                
                break
            case 2 :
                let message = data?[K_Message] as! String
                print(message)
                break
            default:
                print("Default Case")
            }
            
            self.alert_view.removeFromSuperview()
            
        })
        
    }
    
    
    @IBAction func selectVehicleName(_ sender: UITextField) {
        let senderTF: UITextField = sender
        senderTF.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
    }
    
    @IBAction func submitGeofenceSearchData(_ sender: UIButton) {
        
        if geofenceNameTF.text!.count < 1 {
            showToast(controller: self, message: "Please select geofence name", seconds: 2.0)
            return
        }
        else
        {
            getVehiclesData()
        }
    }
    
    
    func getVehiclesData()
       {
           if !NetworkAvailability.isConnectedToNetwork() {
            
               showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
               self.tableView.isHidden = true
               self.alert_view.removeFromSuperview()
               return
           }
        
        self.view.addSubview(alert_view)
        let device_ids = selectedDeviceId.joined(separator: ",")
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
           
        let urlString = domain_name + Geofence_Devices_Search + "device_id=" + device_ids + "&geofence_id=" + geofence_id_val + "&user_name=" + user_name + "&hash_key=" + hash_key + "&user_id=" + user_id
        
         print("url string:\(urlString)")
        
        if geofencingData.count > 0
        {
            geofencingData = []
        }
           
        NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
               if isNetwork{
                   let result = data?[K_Result] as! Int
                   print("result:\(result)")
                   switch (result){
                   case 0 :
                       let c_data = data?[K_Data] as! [Dictionary<String, Any>]
                       let child_data = c_data.map({$0 ["child_data"] as? [Dictionary<String, Any>]})
                       if child_data.count > 0
                       {
                         self.geofencingData = child_data[0]! as NSArray
                       }
                     
                       self.tableView.delegate = self
                       self.tableView.dataSource = self
                       
                       if self.geofencingData.count == 0
                       {
                           DispatchQueue.main.async
                               {
                               self.noDataLabel.isHidden = false
                               self.tableView.reloadData()
                           }
                       }
                           
                       else
                       {
                       DispatchQueue.main.async {
                           self.noDataLabel.isHidden = true
                           self.tableView.reloadData()
                       }
                     }
                       break
                    
                    case 1 :
                        DispatchQueue.main.async
                            {
                                self.noDataLabel.isHidden = false
                                self.tableView.reloadData()
                        }
                    break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       DispatchQueue.main.async
                       {
                           showToast(controller: self, message: "No Geofence Data Available", seconds: 2.0)
                       }
                       print("Default Case")
                   }
                   
               }else{
                   print("ERROR FOUND")
               }
              
               self.alert_view.removeFromSuperview()
           })
    }
    
    func showSingleSelectionMenu(style: PresentationStyle) {
           // Here you'll get cell configuration where you'll get array item for each index
           // Cell configuration following parameters.
           // 1. UITableViewCell   2. Item of type T  3.IndexPath
           let selectionMenu = RSSelectionMenu(dataSource: self.geofence_name_arr) { (cell, item, indexPath) in
               cell.textLabel?.text = item
           }
           // set default selected items when menu present on screen.
           // here you'll get handler each time you select a row
           // 1. Selected Item  2. Index of Selected Item  3. Selected or Deselected  4. All Selected Items
           selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (text, index, isSelected, selectedItems) in
               // update your existing array with updated selected items, so when menu show menu next time, updated items will be default selected.
               self?.simpleSelectedArray = selectedItems
               let selectedIndex = self?.geofence_name_arr.indices.filter {self!.geofence_name_arr[$0] == text}
               print("selected index:\(String(describing: selectedIndex))")
               switch style {
               case .push:
                  print("Push view")
               case .present:
                   self?.geofenceNameTF.text = text
                   if selectedIndex?.count == 1
                   {
                    self!.geofence_id_val = self?.geofence_id_arr [selectedIndex![0]]
                   }
                  // self?.tbl_sort_item.isHidden = true
                  // print("vehicle type id:\(String(describing: self?.val_vehicle_type_id))")
               default:
                   break
               }
              // self?.tbl_sort_item.reloadData()
           }
           
           selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in
               print("vehicle name arr:\(String(describing: self?.geofence_id_arr)), geofence id:\(String(describing: self?.geofence_id_val)) ")
               return self?.geofence_name_arr.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
           }
           
           selectionMenu.onDismiss = { [weak self] selectedItems in
               self?.selectedDataArray = selectedItems
               /// do some stuff when menu is dismssed
             //  self?.tbl_sort_item.isHidden = true
           }
           /// set cell selection style - Default is 'tickmark'
           /// (Optional)
           selectionMenu.cellSelectionStyle = self.cellSelectionStyle
           /// Customization
           /// set navigationBar title, attributes and colors
           selectionMenu.setNavigationBar(title: "Select Vehicle", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white], barTintColor: #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), tintColor: UIColor.white)
           
           // show menu as (push or present)
           selectionMenu.show(style: style, from: self)
       }
       
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}

extension GeofenceListViewController:UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.geofencingData.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 3.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeofencingCell") as! GeofencingHomeCell
        let data = self.geofencingData[indexPath.section] as! Dictionary<String, Any>
        cell.locationLbl.text = data["geofence_name"] as? String
        if let checkType = data["check_type"] as? String
        {
            if checkType == "2"
            {
                cell.geofenceStatusLbl.text = "IN & OUT"
                cell.geofenceStatusLbl.textColor = UIColor.systemGreen
            }
        }
        
        cell.vehicleTitleLbl.text = data["registration_no"] as? String
        cell.dateFrmLbl.text = data["start_date"] as? String
        cell.dateToLbl.text = data["end_date"] as? String
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 12
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GeofenceMapVC") as! GeofenceLocationMapVC
        let data = self.geofencingData[indexPath.section] as! Dictionary<String, Any>
        vc.geofence_id = data["geofence_id"] as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
