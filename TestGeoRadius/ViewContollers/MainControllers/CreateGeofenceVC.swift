//
//  CreateGeofenceVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 07/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import EzPopup


// MARK: - State

private enum State {
    case closed
    case open
}


extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

class CreateGeofenceVC: UIViewController,DataDelegate {
    
    @IBOutlet weak var geofenceMapView: GMSMapView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var dateAndTimeView: UIView!
    private var bottomConstraint = NSLayoutConstraint()
    private let popupOffset: CGFloat = 370
    var coordinates: [CLLocationCoordinate2D] = []
    var polygonPath = GMSPolyline()
    var polylineArr = [GMSPolyline]()
    var markerArr = [GMSMarker]()
    let toolBar = UIToolbar()
    @IBOutlet weak var geofenceNameTF: UITextField!
    @IBOutlet weak var geofenceCheckTF: UITextField!
    @IBOutlet weak var vehicleTF: UITextField!
    @IBOutlet weak var overstayTF: UITextField!
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    @IBOutlet weak var timeErrLbl: UILabel!
    let locationPicker = UIPickerView()
    let datePicker = UIDatePicker()
    
    var locationPickArr = ["In","Out","In & Out"]
    var selectedTextField: UITextField = UITextField()
    let dropDownImg = UIImage(named: "downarrow")
    @IBOutlet weak var overlayView: UIView!
    var selectedDeviceId = [String]()
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet var labelsArr: Array<UILabel>!
    
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_ok: UIButton!
    @IBOutlet weak var lbl_selectDateTime: UILabel!
    var coordinateArr = [String]()
    var latArr = [Double]()
    var langTime = [Double]()
    var geofenceType = ""
    @IBOutlet weak var btn_date_select: UIButton!
    var fromDate = ""
    var toDate = ""
    var timeOverride = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timeErrLbl.isHidden = true
        setLanguageInControls()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.layout()
        overlayView.addGestureRecognizer(panRecognizer)
        self.geofenceMapView.delegate = self
        
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CreateGeofenceVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        self.geofenceMapView.animate(toLocation: CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773))
        let camera = GMSCameraPosition.camera(withLatitude: 28.6127, longitude: 77.2773, zoom: 8)
        self.geofenceMapView!.camera = camera
        
        SetTextFieldRightSide(imageName: "downarrow", txt_field: geofenceCheckTF)
        geofenceCheckTF.inputView = locationPicker
        geofenceCheckTF.inputAccessoryView = toolBar
      
        SetTextFieldRightSide(imageName: "downarrow", txt_field: vehicleTF)
        
        fromDateTF.tintColor = UIColor.clear
        toDateTF.tintColor = UIColor.clear
        
        geofenceCheckTF.delegate = self
        vehicleTF.delegate = self
        locationPicker.delegate = self
        toDateTF.delegate = self
        fromDateTF.delegate = self
        showDatePicker()
        // Do any additional setup after loading the view.
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .dateAndTime
        datePicker.minimumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CreateGeofenceVC.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CreateGeofenceVC.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        // add toolbar to textField
        fromDateTF.inputAccessoryView = toolbar
        toDateTF.inputAccessoryView = toolbar
        // add datepicker to textField
        fromDateTF.inputView = datePicker
        toDateTF.inputView = datePicker
    }
    
    
    func setLanguageInControls()
    {
        print("labels count :\(labelsArr.count)")
        if labelsArr.count == 5
        {
            for index in 0...4
            {
                switch index {
                case 0:
                    labelsArr[index].text = LanguageHelperClass().geofenceNameTxt
                case 1:
                    labelsArr[index].text = LanguageHelperClass().geofenceCheckTxt
                case 2:
                    labelsArr[index].text = LanguageHelperClass().vehicleTxt
                case 3:
                    labelsArr[index].text = LanguageHelperClass().overstayTxt
                case 4:
                    labelsArr[index].text = LanguageHelperClass().restrictionTxt
                default:
                    print("done")
                }
            }
        }
        
        // titleLbl.text = LanguageHelperClass().createGeofenceTxt
        lbl_selectDateTime.text = LanguageHelperClass().selectDateTimeTxt
        fromDateTF.placeholder = LanguageHelperClass().fromDateTimeTxt
        toDateTF.placeholder = LanguageHelperClass().toDateTimeTxt
        btn_ok.setTitle(LanguageHelperClass().ok_DoneTxt, for: .normal)
        btn_cancel.setTitle(LanguageHelperClass().cancelTxt, for: .normal)
        btn_submit.setTitle(LanguageHelperClass().submitTxt, for: .normal)
    }
    
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if self.selectedTextField == self.fromDateTF {
            fromDateTF.text = formatter.string(from: datePicker.date)
            fromDate = fromDateTF.text!
        }
        else if self.selectedTextField == self.toDateTF
        {
            toDateTF.text = formatter.string(from: datePicker.date)
            toDate = toDateTF.text!
        }
        //dismiss date picker dialog
        self.dateAndTimeView.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.dateAndTimeView.endEditing(true)
    }
    
    
    private var currentState: State = .closed
    
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        return recognizer
    }()
    
    
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
                self.bottomView.layer.cornerRadius = 10
                
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                self.bottomView.layer.cornerRadius = 0
                
            }
            self.view.layoutIfNeeded()
        })
        
        transitionAnimator.addCompletion { position in
            
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.popupOffset
            }
            
            self.runningAnimators.removeAll()
            
        }
        
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        
        
        inTitleAnimator.scrubsLinearly = false
        
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
            
            runningAnimators.forEach { $0.pauseAnimation() }
            
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            let translation = recognizer.translation(in: bottomView)
            var fraction = -translation.y / popupOffset
            
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            
            let yVelocity = recognizer.velocity(in: bottomView).y
            let shouldClose = yVelocity > 0
            
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
    
    
    func layout(){
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bottomView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = bottomView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        bottomView.heightAnchor.constraint(equalToConstant: 420).isActive = true
    }
    
    
    @IBAction func lastPointDelete(_ sender: UIButton) {
        print("coordinates before removing\(coordinates)")
        if let polyline = polylineArr.last, let marker = markerArr.last
        {
            if coordinates.count > 2
            {
                polyline.map = nil
                marker.map = nil
                polylineArr.removeLast()
                coordinates.removeLast()
                markerArr.removeLast()
            }
        }
    }
    
    @IBAction func deleteAllPoints(_ sender: UIButton) {
        coordinates = []
        geofenceMapView.clear()
    }
    
    @IBAction func dissmissVC(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func openDatePopUp(_ sender: UIButton) {
        
        if (sender.isSelected == true)
        {
            timeOverride = "false"
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            sender.isSelected = false
        }
        else
        {
            timeOverride = "true"
            sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
            sender.isSelected = true
        }
        
        let popUp = PopupViewController(contentView: self.dateAndTimeView, popupWidth: self.dateAndTimeView.frame.size.width, popupHeight: self.dateAndTimeView.frame.size.height)
        popUp.backgroundAlpha = 0.7
        popUp.canTapOutsideToDismiss = true
        popUp.cornerRadius = 10
        popUp.shadowEnabled = true
        DispatchQueue.main.async {
            //View related code
            self.present(popUp, animated: false)
        }
    }
    
    @IBAction func submitGeofenceData(_ sender: UIButton) {
        
        if geofenceNameTF.text!.count < 1 || geofenceCheckTF.text!.count < 1 || vehicleTF.text!.count < 1 {
            showToast(controller: self, message : "Geofence name, Geofence check, vehicle name are mandatory", seconds: 2.0)
            return
        }
        
        if coordinates.count == 0
        {
            showToast(controller: self, message : "Please provide geofence for selected vehicle", seconds: 2.0)
            return
        }
        
        if coordinateArr.count > 0
        {
            coordinateArr = []
        }
        
        let device_ids = selectedDeviceId.joined(separator: ",")
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        

        for i in 0..<coordinates.count
        {
           
            coordinateArr.append(String(coordinates[i].latitude.truncate(to: 6)) + " " + String(coordinates[i].longitude.truncate(to: 6)))
        }
        
        let firstCoordinateData = coordinateArr[0]
        let geofenceArr = coordinateArr.map { String($0) }.joined(separator: ",") + "," + firstCoordinateData
        let overstayVal = self.overstayTF.text ?? "0"
        
        let baseURL =  Geofence_Add + "geofence_name=" + geofenceNameTF.text! + "&geofence_data=" + geofenceArr + "&geofence_type=2" + "&geofence_source=0"
        let deviceId = "&checkpoint_array=" + "&device_id=" + device_ids + "&check_type=" + self.geofenceType
        let geofencePoints = "&geofence_point_count=" + String(coordinates.count) + "&device_command_data="
        let dateParameters = "&start_date=" + fromDate + "&end_date=" + toDate
        let timeParemeters =  "&time_override=" + self.timeOverride + "&overstay_value=" + overstayVal
        let userParamters =  "&user_name=" + user_name + "&hash_key=" + hash_key
        
        let mainUrl = baseURL + deviceId + geofencePoints + dateParameters + timeParemeters + userParamters
                
        let encodedUrl = domain_name + mainUrl.addingPercentEncoding(withAllowedCharacters:.rfc3986Unreserved)!

        print("encodedUrl:\(encodedUrl)")
        
        NetworkManager().CallUpdateDataOnServer(urlString: encodedUrl, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                print("data:\(data)")
                if let message = data
                {
                    if message == "Geofence added Successfully."
                    {
                        let alert = UIAlertController(title: "", message: "Geofence created successfully.", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
               // showToast(controller: self, message : "Geofence created successfully", seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
        })
        
    }
    
    @IBAction func submitDateInterval(_ sender: UIButton) {
        
        if fromDateTF.text?.count == 0 || toDateTF.text?.count == 0
        {
            timeErrLbl.isHidden = false
            timeErrLbl.text = "*Please select both fields"
            return
        }
        
        if toDateTF.text! <  fromDateTF.text!{
            timeErrLbl.isHidden = false
            timeErrLbl.text = "*End Date should be greater then Start Date."
            return
        }
        
        if toDateTF.text! ==  fromDateTF.text!{
            timeErrLbl.isHidden = false
            timeErrLbl.text = "*End Date should be greater then Start Date."
            return
        }
        
        timeErrLbl.isHidden = true
        dismiss(animated: true, completion: nil)
               
    }
    
    
    @IBAction func dissmissPopupVIew(_ sender: UIButton) {
        timeOverride = "false"
        btn_date_select.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
        btn_date_select.isSelected = false
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func openVehiclesList(_ sender: UITextField) {
        sender.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
 }
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
        print("device id:\(deviceId)")
        selectedDeviceId = deviceId
        vehicleTF.text = " \(selectedDeviceId.count) selected "
    }
    
    /*
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CreateGeofenceVC:GMSMapViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        let destination = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        coordinates.append(destination)
        createMarker(point: coordinate)
    }
    
    func createMarker(point: CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude)
        marker.icon = UIImage(named: "dotIcon1")
        markerArr.append(marker)
        marker.map = geofenceMapView
        createPolygon()
    }
    
    func createPolygon(){
        let path = GMSMutablePath()
        for coordinate in coordinates{
            path.add(CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
        }
        path.add(CLLocationCoordinate2D(latitude: coordinates[0].latitude, longitude: coordinates[0].longitude))
        polygonPath = GMSPolyline(path: path)
        polylineArr.append(polygonPath)
        polygonPath.strokeColor = UIColor.red
        polygonPath.strokeWidth = 2.0
        polygonPath.map = geofenceMapView
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("did begin editing")
        self.selectedTextField = textField
    }
    
    @objc func donePicker()
    {
        if self.selectedTextField == self.geofenceCheckTF
        {
            DispatchQueue.main.async
                {
                    self.geofenceCheckTF.resignFirstResponder()
            }
        }
        else if self.selectedTextField == self.vehicleTF
        {
            DispatchQueue.main.async
                {
                    self.vehicleTF.resignFirstResponder()
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if self.selectedTextField == self.geofenceCheckTF {
            return locationPickArr.count
        }
        
        return 0
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("title for row in picker")
        
        //Set text for countryField
        
        if self.selectedTextField == self.geofenceCheckTF {
            return locationPickArr[row]
        }
        
        return nil
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Set text for countryField
        if self.selectedTextField == self.geofenceCheckTF {
            self.geofenceCheckTF?.text = locationPickArr[row]
        }
        
        if locationPickArr[row] == "In"
        {
            self.geofenceType = "0"
        }
        
        else if locationPickArr[row] == "Out"
        {
            self.geofenceType = "1"
        }
        else
        {
            self.geofenceType = "2"
        }
    }
    
}

