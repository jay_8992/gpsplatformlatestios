//
//  ServiceProvider.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ServiceProvider: UIViewController {

    @IBOutlet weak var view_heaeder: UIView!
    @IBOutlet weak var txt_demo: UITextField!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var txt_company_name: UITextField!
    @IBOutlet weak var txt_contact: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    let alert_view = AlertView.instanceFromNib()
    var titleArr = [String]()
    var imageTitleArr = ["com_name","comp_add","comp_phone","comp_email"]
    let cellSpacingHeight:CGFloat = 15.0
    
    //ServicePvdCell

    override func viewDidLoad() {
        
        super.viewDidLoad()
        SideMenu()
        // Do any additional setup after loading the view.
        view_heaeder.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tableView.frame
        tempImageView.contentMode = .scaleAspectFill
        self.tableView.backgroundView = tempImageView
        self.tableView.tableFooterView = UIView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        txt_demo.addTarget(self, action: #selector(tap_demo), for: .editingDidBegin)
        txt_contact.addTarget(self, action: #selector(tap_contact), for: .editingDidBegin)
        txt_email.addTarget(self, action: #selector(tap_email), for: .editingDidBegin)
        txt_company_name.addTarget(self, action: #selector(tap_company), for: .editingDidBegin)
        CallAPIToUploadData()
    }

    
    func SideMenu(){
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
    func CallAPIToUploadData(){
        
        self.view.addSubview(alert_view)
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        
        if titleArr.count > 0 {
            titleArr.removeAll()
        }
        
        let urlString = domain_name + "/group_result.php?&action=search&group_name=&company_name=&group_id=" + group_id + "&user_name=" + user_name + "&hash_key=" + hash_key + "&data_format=1"
        
        NetworkManager().CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil {
              
                for val in data! {
                    let val_data = val as! Dictionary<String, Any>
                    self.titleArr.append(val_data["company_name"] as? String ?? "")
                    self.titleArr.append(val_data["company_address"] as? String ?? "")
                    self.titleArr.append(val_data["company_phone"] as? String ?? "")
                    self.titleArr.append(val_data["company_email"] as? String ?? "")
                    
                    DispatchQueue.main.async {
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                    }
                }
                }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
               self.alert_view.removeFromSuperview()
        })
    }

}


extension ServiceProvider:UITableViewDelegate,UITableViewDataSource{
    
    @objc func tap_demo(){
        txt_demo.resignFirstResponder()
    }
    
    @objc func tap_contact(){
        txt_contact.resignFirstResponder()
    }
    
    @objc func tap_email(){
        txt_email.resignFirstResponder()
    }
    
    @objc func tap_company(){
        txt_company_name.resignFirstResponder()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return self.titleArr.count
       }
       
       // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 1
       }
       
       // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           if section == 0
           {
               return 3.0
           }
           return cellSpacingHeight
       }
       
       // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
           headerView.backgroundColor = UIColor.clear
           return headerView
       }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          print("image arr:\(imageTitleArr), title arr:\(titleArr)")
          let cell = tableView.dequeueReusableCell(withIdentifier: "ServicePvdCell") as! ServiceProviderTableViewCell
          cell.imgIcon.image = UIImage(named:imageTitleArr[indexPath.section])
          cell.titleLbl.text = titleArr[indexPath.section]
          cell.layer.borderColor = UIColor.lightGray.cgColor
          cell.layer.borderWidth = 0.5
          cell.layer.cornerRadius = 10
          return cell
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
      }
      
}
