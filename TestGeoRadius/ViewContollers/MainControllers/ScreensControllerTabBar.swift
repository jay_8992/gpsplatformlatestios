//
//  ScreensControllerTabBar.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
//import SOTabBar

class ScreensControllerTabBar: UITabBarController {
    
    @IBOutlet weak var screensTabBar: UITabBar!
    let alert_view = AlertView.instanceFromNib()
    var all_data : Dictionary<String, Any>?
    
    var isDidLoad = true
    var timer = Timer()
    var menuIdArr: [String] = []
    var viewControllersArr : [UIViewController] = []
    
    var dashVC : UIViewController!
    var reportsVC : UIViewController!
    var billingVC : UIViewController!
    var userAddVC : UIViewController!
    var vehicleEditVC : UIViewController!
    
    var navigationController2 : UINavigationController!
    var navigationController3 : UINavigationController!
    var navigationController4 : UINavigationController!
    var navigationController5 : UINavigationController!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let login_first = UserDefaults.standard.value(forKey: "login_first") as? Bool ?? false
        
        if login_first == true
        {
            UserDefaults.standard.set(false, forKey: "login_first")
            CallTrakingDataFromServer()
        }
        else
        {
            SetTabBarViewContollers()
        }
        
      //  CallTrakingDataFromServer()
    }
    
   
    func SetTabBarViewContollers()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        dashVC = storyboard.instantiateViewController(withIdentifier: "DashboardVC")
        dashVC.title = LanguageHelperClass().homeTxt
    
        reportsVC = storyboard.instantiateViewController(withIdentifier: "ReportsVC")
        reportsVC.title = LanguageHelperClass().reportsTxt
        navigationController2 = UINavigationController(rootViewController: reportsVC)
        navigationController2.setNavigationBarHidden(true, animated: true)
    
        billingVC = storyboard.instantiateViewController(withIdentifier: "BillingVC")
        billingVC.title = LanguageHelperClass().billingsTxt
        navigationController3 = UINavigationController(rootViewController: billingVC)
       
        userAddVC = storyboard.instantiateViewController(withIdentifier: "UserAddList")
        userAddVC.title = LanguageHelperClass().userTxt
        navigationController4 = UINavigationController(rootViewController: userAddVC)
      
        vehicleEditVC = storyboard.instantiateViewController(withIdentifier: "VehiclesListVC")
        vehicleEditVC.title = LanguageHelperClass().vehicleTxt
        navigationController5 = UINavigationController(rootViewController: vehicleEditVC)
        //  navigationController5.tabBarItem.image = UIImage.init(named: "reports_0")
        
        viewControllersArr = [dashVC,navigationController2,navigationController3,navigationController4,navigationController5]
        
        setTabBarItems()
    }
    
    func CallTrakingDataFromServer(){
        
        //showActivityIndicator()
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_type_id = UserDefaults.standard.value(forKey: USER_TYPE_ID) as? String ?? "0"
        let urlString = domain_name + Menu_Id + "&user_type_id=" + user_type_id + "&user_name=" + user_name + "&hash_key=" + hash_key
        NetworkManager().GetMenuReportsFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            print("menu response :\(String(describing: data))")
            if isNetwork && data != nil {
                if let c_data = data as? [Dictionary<String, Any>]
                {
                    self.menuIdArr = c_data.map({$0 ["menu_id"] as! String})
                    UserDefaults.standard.set(self.menuIdArr, forKey: "MenuPermArr")
                    DispatchQueue.main.async {
                        self.SetTabBarViewContollers()
                    }
                }
                else
                {
                   DispatchQueue.main.async {
                        self.SetTabBarViewContollers()
                    }
                }
            }
                
            else{
                if NetworkAvailability.isConnectedToNetwork() {
                    print("Internet connection available")
                }
                else{
                    //  showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
                }
               // showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                print("ERROR FOUND:\(String(describing: r_error))")
                DispatchQueue.main.async {
                   /* let alert = UIAlertController(title: "Error", message: "Please Check Your Internet Connection", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] _ in
                        alert?.dismiss(animated: true, completion: nil)
                    }))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                  */
                    self.SetTabBarViewContollers()
                }
            }
            
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("in tab bar vcr")
        super.viewWillAppear(animated)// this line need
       // ConnectionManager.sharedInstance.addListener(listener: self)
       // SetTabBarViewContollers()
       // CallTrakingDataFromServer()
    }
    
    
    func setTabBarItems() {
      // hideActivityIndicator()
        if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String]{
            print("arr:\(arr), arr count:\(arr.count)")
            if !arr.contains(where: {$0 == "182"})
            {
                if let index = viewControllersArr.firstIndex(of: navigationController3) {
                    viewControllersArr.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "3"})
            {
                if let index = viewControllersArr.firstIndex(of: navigationController2) {
                    viewControllersArr.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "16"})
            {
                if let index = viewControllersArr.firstIndex(of: navigationController4) {
                    viewControllersArr.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            
            if !arr.contains(where: {$0 == "17"})
            {
                if let index = viewControllersArr.firstIndex(of: navigationController5) {
                    viewControllersArr.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            print("view controllers arr:\(viewControllersArr)")
            self.setViewControllers(viewControllersArr, animated: true)
        }
        else
        {
            self.setViewControllers(viewControllersArr, animated: true)
        }
        
    }
}

/* extension ScreensControllerTabBar: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.Connection) {
        
        switch status {
        case .unavailable :
            debugPrint("ViewController: Network became unreachable")
        case .wifi:
            debugPrint("ViewController: Network reachable through WiFi")
            CallTrakingDataFromServer()
        case .cellular:
            debugPrint("ViewController: Network reachable through Cellular Data")
            CallTrakingDataFromServer()
        case .none:
            debugPrint("ViewController: Network reachable through WAN")
        }
    }
    
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.backgroundColor = UIColor.white
        activityIndicator.layer.cornerRadius = 6
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.large
        activityIndicator.startAnimating()
        //UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicator.tag = 100 // 100 for example
        // before adding it, you need to check if it is already has been added:
        for subview in view.subviews {
            if subview.tag == 100 {
                print("already added")
                return
            }}
        view.addSubview(activityIndicator)
    }

    func hideActivityIndicator() {
        let activityIndicator = view.viewWithTag(100) as? UIActivityIndicatorView
        activityIndicator?.stopAnimating()
        // I think you forgot to remove it?
        activityIndicator?.removeFromSuperview()
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
}
*/
