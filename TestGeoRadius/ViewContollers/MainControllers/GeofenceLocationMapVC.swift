//
//  GeofenceLocationMapVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 07/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps


class GeofenceLocationMapVC: UIViewController {
    var latitudeArr = [Double]()
    var longitudeArr = [Double]()
    var geofence_id = ""
    let alert_view = AlertView.instanceFromNib()
    
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        getDeviceGeofenceData()
        // Do any additional setup after loading the view.
    }
    
    
    func getDeviceGeofenceData()
       {
           if !NetworkAvailability.isConnectedToNetwork() {
                            
               showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
               self.alert_view.removeFromSuperview()
               return
           }
           self.view.addSubview(alert_view)
          
           let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
           let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
           let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
           
           let urlString = domain_name + Geofence_IndividualDevice_Search + "geofencedevice_id=" + geofence_id + "&user_name=" + user_name + "&hash_key=" + hash_key
           print("url string:\(urlString)")
           NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
               if isNetwork{
                   let result = data?[K_Result] as! Int
                   print("result:\(result)")
                   switch (result){
                   case 0 :
                       let c_data = data?[K_Data] as! Array<Any>
                       for val in c_data
                       {
                        let val_data = val as! Dictionary<String,Any>
                        if let latStr = val_data["st_x"] as? String , let longStr = val_data["st_y"] as? String
                        {
                            self.latitudeArr.append(Double(latStr)!)
                            self.longitudeArr.append(Double(longStr)!)
                        }
                        else
                        {
                           self.latitudeArr.append(val_data["st_x"] as! Double)
                           self.longitudeArr.append(val_data["st_y"] as! Double)
                        }
                       }
                       
                       print("geofencing data:\(c_data)")
                       DispatchQueue.main.async {
                        self.showDataOnMap()
                       }
                     
                       break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       print("Default Case")
                   }
                   
               }else{
                   print("ERROR FOUND")
               }
              
               self.alert_view.removeFromSuperview()
           })
       }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showDataOnMap()
    {
        let path = GMSMutablePath()
       // var marker = GMSMarker()
        self.mapView.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitudeArr[0], longitude: self.longitudeArr[0]))
        let camera = GMSCameraPosition.camera(withLatitude: self.latitudeArr[0], longitude: self.longitudeArr[0], zoom: 15)
        self.mapView!.camera = camera
        
        for (index, _) in self.latitudeArr.enumerated()
        {
            path.add(CLLocationCoordinate2DMake(latitudeArr[index], longitudeArr[index]))
        }
        let polygon = GMSPolygon(path: path)
        polygon.fillColor = UIColor(red: 0.25, green: 0, blue: 0, alpha: 0.1);
        polygon.strokeColor = UIColor.init(hue: 210, saturation: 88, brightness: 84, alpha: 1)
        //polygon.strokeColor = .black
        polygon.strokeWidth = 4
        polygon.map = mapView
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
