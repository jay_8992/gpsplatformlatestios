//
//  GeofencingViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 06/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import EzPopup

class GeofencingViewController: UIViewController,DataDelegate {
   
    var container: ContainerViewController!
    @IBOutlet weak var selectVehicleTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var selectedDeviceId = [String]()
    let alert_view = AlertView.instanceFromNib()
    var geofencingData = NSArray()
    var cellSpacingHeight:CGFloat = 15.0
    @IBOutlet weak var leftBarBtn: UIButton!
    var noDataLabel: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var geofence_segment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenu()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        geofence_segment.selectedSegmentIndex = 0
        segmentSelected(geofence_segment)
        /* self.tableView.tableFooterView = UIView()
        noDataLabel = UILabel(frame: CGRect(x: 0, y: self.tableView.frame.origin.y, width: tableView.bounds.size.width, height: 30))
        noDataLabel.translatesAutoresizingMaskIntoConstraints = false
        noDataLabel.numberOfLines = 0
        noDataLabel.text = "No Records Found"
        noDataLabel.textColor = UIColor.darkText
        noDataLabel.textAlignment = .center
        self.view.addSubview(noDataLabel)
        noDataLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        noDataLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noDataLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        noDataLabel.isHidden = true
        titleLbl.text = LanguageHelperClass().geofencingTxt
        getAllVehiclesGeofenceData()
        */
        // Do any additional setup after loading the view.
    }
    
    func getAllVehiclesGeofenceData()
    {
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tableView.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        self.selectedDeviceId.removeAll()
        var selectedURL = ""
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        selectedURL = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        print("selected url:\(selectedURL)")
        NetworkManager().CallVehicleData(urlString: selectedURL, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    let device_id = v_name["device_id"] as! String
                    self.selectedDeviceId.append(device_id)
                }
                if self.selectedDeviceId.count > 1
                {
                    self.getVehiclesData()
                }
                //self.filterdData = self.vehicle_data
                
            }else{
                
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tableView.isHidden = true
                self.alert_view.removeFromSuperview()
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
    }
 
    
    func getVehiclesData()
    {
        if !NetworkAvailability.isConnectedToNetwork() {
                         
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.tableView.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        self.view.addSubview(alert_view)
        let device_ids = selectedDeviceId.joined(separator: ",")
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        let urlString = domain_name + Geofence_Devices_Search + "device_id=" + device_ids + "&geofence_name=" + "&user_name=" + user_name + "&hash_key=" + hash_key
        print("url string:\(urlString)")
        
     NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                print("result:\(result)")
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    self.geofencingData = c_data as NSArray
                    print("geofencing data:\(c_data)")
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    if self.geofencingData.count == 0
                    {
                        DispatchQueue.main.async
                            {
                            self.noDataLabel.isHidden = false
                            self.tableView.reloadData()
                        }
                    }
                        
                    else
                    {
                    DispatchQueue.main.async {
                        self.noDataLabel.isHidden = true
                        self.tableView.reloadData()
                    }
                  }
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    DispatchQueue.main.async
                    {
                        showToast(controller: self, message: "No Geofence Data Available", seconds: 2.0)
                    }
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
           
            self.alert_view.removeFromSuperview()
        })
    }
    
    @IBAction func selectVehicleTF(_ sender: Any) {
        let senderTF: UITextField = sender as! UITextField
        senderTF.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
             {
             case 0:
                 container!.segueIdentifierReceivedFromParent("AddGeofence", "")
                 break
             case 1:
                 container!.segueIdentifierReceivedFromParent("SearchGeofence", "")
                 break
             default:
                 break;
            }
    }
    
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
        print("device id:\(deviceId)")
        if selectedDeviceId.count > 0
        {
           selectedDeviceId = []
        }
        selectedDeviceId = deviceId
        dismiss(animated: true) {
            if deviceId.count > 0
            {
              self.getVehiclesData()
            }
        }
    }
    
    func SideMenu()
    {
           NetworkManager().StopAllThreads()
           leftBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
           revealViewController()?.rearViewRevealWidth = 250
    }
    
    @IBAction func openCreateGeofencePage(_ sender: UIButton) {
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateGeofenceVC") as! CreateGeofenceVC
        self.navigationController?.pushViewController(contentVC, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
           if segue.identifier == "container" {
               container = segue.destination as! ContainerViewController
               container.animationDurationWithOptions = (0.5, .transitionCrossDissolve)
           }
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension GeofencingViewController:UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.geofencingData.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 3.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeofencingCell") as! GeofencingHomeCell
        let data = self.geofencingData[indexPath.section] as! Dictionary<String, Any>
        cell.locationLbl.text = data["geofence_name"] as? String
        if let checkType = data["check_type"] as? String
        {
            if checkType == "2"
            {
                cell.geofenceStatusLbl.text = "IN & OUT"
                cell.geofenceStatusLbl.textColor = UIColor.systemGreen
            }
        }
        
        cell.vehicleTitleLbl.text = data["registration_no"] as? String
        cell.dateFrmLbl.text = data["start_date"] as? String
        cell.dateToLbl.text = data["end_date"] as? String
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 12
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GeofenceMapVC") as! GeofenceLocationMapVC
        let data = self.geofencingData[indexPath.section] as! Dictionary<String, Any>
        vc.geofence_id = data["geofence_id"] as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
