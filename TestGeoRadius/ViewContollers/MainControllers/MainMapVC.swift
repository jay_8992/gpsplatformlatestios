//
//  MainMapVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 08/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import Mapbox

class MyCustomPointAnnotation: MGLPointAnnotation {
    var willUseImage: Bool = false
}

class MainMapVC: UIViewController, GMUClusterManagerDelegate, GMSMapViewDelegate, GMUClusterRendererDelegate{
    
    
    @IBOutlet weak var btn_zoom_out: UIButton!
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var btn_traffic: UIButton!
    @IBOutlet weak var map_show: UIView!
    @IBOutlet weak var map_view: MGLMapView!
    @IBOutlet weak var map_apple: MKMapView!
    @IBOutlet weak var showMap: GMSMapView!
    @IBOutlet weak var btn_google_map: UIButton!
    @IBOutlet weak var btn_apple_map: UIButton!
    
    var mapView: MGLMapView!
    var vehicals : Array<Any>?
    var latitude = [Double]()
    var registration = [String]()
    var vehicle_type_id = [String]()
    var longitude = [Double]()
    var direction = [Double]()
    var device_id = [String]()
    var rasterLayer: MGLRasterStyleLayer?
    var count = 0
    var imageName : String = STOP_CAR
    var status = [Int]()
    var clusterManager: GMUClusterManager!
    var moving = false
    var idle = false
    var stopped = false
    var no_data = false
    var all = true
    var is_queue = false
    let alert_view = AlertView.instanceFromNib()
    var vehicals_s : Array<Any>?
    var vehicle_data1: Array<Any>!
    
    var moving_c = 00
    var stopped_c = 00
    var unreach_c = 00
    var idle_c = 00
    
    var all_vehicles_count = 0
    
    var timeInterval = 50.0
    
    var locationArr: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        mapView = MGLMapView(frame: view.bounds, styleURL: MGLStyle.lightStyleURL)
//        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        mapView.tintColor = .darkGray
//        map_show.addSubview(mapView)
        //mapView.delegate = self
        
        btn_traffic.layer.cornerRadius = btn_traffic.frame.size.height / 10
        btn_traffic.layer.borderColor = UIColor.darkGray.cgColor
        btn_traffic.layer.borderWidth = 1
        
        btn_zoom_in.layer.cornerRadius = btn_zoom_in.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)
        
        btn_zoom_out.layer.cornerRadius = btn_zoom_out.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
        
        
//        mapView.setCenter(CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773), zoomLevel: 10, animated: false)
//        let point = MyCustomPointAnnotation()
//        point.coordinate = CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773)
//
//        let myPlaces = [point]
//        mapView.removeAnnotations(myPlaces)
//        mapView.addAnnotations(myPlaces)
        
        // -------------------------------------SetGoogleMap-----------------------------------//
        
       // self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773))
       // let camera = GMSCameraPosition.camera(withLatitude: 28.6127, longitude: 77.2773, zoom: 10)
      //  self.showMap!.camera = camera
        
    }
    


    override func viewWillAppear(_ animated: Bool) {
        let is_global_selection = UserDefaults.standard.value(forKey: "is_global_selection") as? Bool ?? false
        print("is global selection:\(is_global_selection)")
    }
    
    
    func CheckIsHidden(isHidden_V: Bool){
        if isHidden_V{
            is_queue = false
            NetworkManager().StopAllThreads()
        }
    }
    
    func Check(Moving: Bool, Idle: Bool, Stopped: Bool, No_data: Bool, All: Bool, select: Bool, vehicle_data: Array<Any>){
        print("in main map vc")
        let indexPage = UserDefaults.standard.value(forKey: "select_home_page") as? Int ?? 0
        let is_global_selection = UserDefaults.standard.value(forKey: "is_global_selection") as? Bool ?? false
        
        if indexPage != 0 && is_global_selection  {
            print("index page:\(indexPage)")
            showMap.isHidden = false
            self.moving = Moving
            self.idle = Idle
            self.stopped = Stopped
            self.no_data = No_data
            self.all = All
           // CallTrakingDataFromServer()
            CallTrakingDataFromServer_Map_Page()
        }
        else
        {
            
          /*  if vehicle_data.count < 1 {
                showMap.isHidden = true
                return
            }
          */
            showMap.isHidden = false
            alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.view.addSubview(self.alert_view)
            self.vehicals = vehicle_data
            self.vehicals_s = vehicle_data
            self.latitude.removeAll()
            self.longitude.removeAll()
            self.status.removeAll()
            self.registration.removeAll()
            self.device_id.removeAll()
            
            UIView.animate(withDuration: 0.0, animations: {
                // self.SetDataOnMAp()
            }, completion: { _ in
                
                self.moving = Moving
                self.idle = Idle
                self.stopped = Stopped
                self.no_data = No_data
                self.all = All
                
                for (_, loc) in vehicle_data.enumerated(){
                    let val_data = loc as! Dictionary<String, Any>
                    let status = val_data["device_status"] as! Int
                    if self.moving{
                        if status == 2{
                            self.SetAllData(val_data: val_data)
                        }
                    }else if self.idle{
                        if status == 1{
                            self.SetAllData(val_data: val_data)
                        }
                    }else if self.stopped{
                        if status == 0{
                            self.SetAllData(val_data: val_data)
                        }
                        
                    }else if self.no_data{
                        if status == 4{
                            //if index < 200{
                            self.SetAllData(val_data: val_data)
                            
                        }
                    }else if self.all{
                        //if index < 200{
                        self.SetAllData(val_data: val_data)
                        
                    }
                }
            })
        }
    }
    
    
    @IBAction func btn_traffic_pressed(_ sender: Any) {
        if showMap.isTrafficEnabled{
            showMap.isTrafficEnabled = false
            btn_traffic.setImage(UIImage(named: "no_traffic"), for: .normal)
        }else{
            showMap.isTrafficEnabled = true
            btn_traffic.setImage(UIImage(named: "traffic"), for: .normal)
        }
    }
    
    
    @IBAction func select_map_type(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            showMap.mapType = .normal
            
        case 1:
            showMap.mapType = .hybrid
            
        default:
            showMap.mapType = .satellite
        }
    }
    
    
    func SetAllData(val_data: Dictionary<String, Any>){

        print("val data:\(val_data)")
        
        if let location = val_data["location"] as? String
        {
            if location == "[GPS not available]"
            {
                return
            }
        }

        self.latitude.append(GetLatitudeFromData(Vehicals : val_data))
        self.longitude.append(GetLonitudeFromData(Vehicals: val_data))
        status.append(GetDeviceStatus(Vehicals: val_data))
        vehicle_type_id.append(GetVehicleTypeId(Vehicals: val_data))
        registration.append(GetRegistrationNumber(Vehicals: val_data))
        device_id.append(GetDeviceID(Vehicals: val_data))
        let angle = Double(val_data["direction"] as! String)
        self.direction.append(angle!)
        
        CATransaction.begin()
        CATransaction.setValue(Int(3.0), forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock({() -> Void in
            self.alert_view.removeFromSuperview()
        })
               
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
       
        let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 20, 50, 100, 500, 1000])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: self.showMap,
                                                 clusterIconGenerator: iconGenerator)
        self.clusterManager = GMUClusterManager(map: self.showMap, algorithm: algorithm,
                                                renderer: renderer)
        // Generate and add random items to the cluster manager.
        //self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
        //let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
       // self.showMap!.camera = camera
        DispatchQueue.main.async {
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
        self.showMap!.camera = camera

        }
       //
        renderer.delegate = self
        renderer.update()
        self.showMap.delegate = self
        self.generateClusterItems()
        // Call cluster() after items have been added to perform the clustering
        // and rendering on map.
        self.clusterManager.cluster()
        //self.mapView.delegate = self
        self.clusterManager.setDelegate(self, mapDelegate: self)
        CATransaction.commit()

        
      /*  DispatchQueue.main.async {
            UIView.animate(withDuration: 2.0, animations: {
                //Time consuming task here
                let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 20, 50, 100, 500, 1000])
                let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
                let renderer = GMUDefaultClusterRenderer(mapView: self.showMap,
                                                         clusterIconGenerator: iconGenerator)
                self.clusterManager = GMUClusterManager(map: self.showMap, algorithm: algorithm,
                                                        renderer: renderer)
                // Generate and add random items to the cluster manager.
                self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
                let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
                self.showMap!.camera = camera
                renderer.delegate = self
                renderer.update()
                self.showMap.delegate = self
                self.generateClusterItems()
                // Call cluster() after items have been added to perform the clustering
                // and rendering on map.
                self.clusterManager.cluster()
                //self.mapView.delegate = self
                self.clusterManager.setDelegate(self, mapDelegate: self)
                // self.SetDataOnMAp()
            }, completion: { _ in
                self.alert_view.removeFromSuperview()
            })
        }
      */
    }
    
    
    
    func SetAllData_New(val_data: Dictionary<String, Any> , locArr:[String]) {

          print("val data:\(val_data)")
          
          let location = locArr[0]
          if location == "[GPS not available]"
          {
            return
          }

          self.latitude.append(GetLatitudeFromData(Vehicals : val_data))
          self.longitude.append(GetLonitudeFromData(Vehicals: val_data))
          status.append(GetDeviceStatus(Vehicals: val_data))
          vehicle_type_id.append(GetVehicleTypeId(Vehicals: val_data))
          registration.append(GetRegistrationNumber(Vehicals: val_data))
          device_id.append(GetDeviceID(Vehicals: val_data))
          let angle = Double(val_data["direction"] as! String)
          self.direction.append(angle!)
          
          CATransaction.begin()
          CATransaction.setValue(Int(3.0), forKey: kCATransactionAnimationDuration)
          CATransaction.setCompletionBlock({() -> Void in
              self.alert_view.removeFromSuperview()
          })
                 
          CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
         
          let iconGenerator = GMUDefaultClusterIconGenerator(buckets: [10, 20, 50, 100, 500, 1000])
          let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
          let renderer = GMUDefaultClusterRenderer(mapView: self.showMap,
                                                   clusterIconGenerator: iconGenerator)
          self.clusterManager = GMUClusterManager(map: self.showMap, algorithm: algorithm,
                                                  renderer: renderer)
          // Generate and add random items to the cluster manager.
          //self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
          //let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
         // self.showMap!.camera = camera
          DispatchQueue.main.async {
          self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
          let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
          self.showMap!.camera = camera

          }
         //
          renderer.delegate = self
          renderer.update()
          self.showMap.delegate = self
          self.generateClusterItems()
          // Call cluster() after items have been added to perform the clustering
          // and rendering on map.
          self.clusterManager.cluster()
          //self.mapView.delegate = self
          self.clusterManager.setDelegate(self, mapDelegate: self)
          CATransaction.commit()
      }
    
    func CallTrakingDataFromServer(){
        print("calling api service")
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.showMap.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.alert_view)
        
        
        vehicals?.removeAll()
        vehicle_data1.removeAll()
        latitude.removeAll()
        longitude.removeAll()
        status.removeAll()
        registration.removeAll()
        device_id.removeAll()
        vehicals_s?.removeAll()
        direction.removeAll()
        vehicle_type_id.removeAll()
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async{
            //Time consuming task here
            NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                if isNetwork{
                    let result = data?[K_Result] as! Int
                    switch (result){
                    case 0 :
                        let c_data = data?[K_Data] as! Dictionary<String,Any>
                        self.vehicals = (c_data[VEHICALS] as! Array<Any>)
                        self.vehicals_s = (c_data[VEHICALS] as! Array<Any>)
                        if self.vehicals!.count > 210{
                            self.vehicle_data1 = SplitArray(into: 210, yourArray: self.vehicals!)
                        }
                        else
                        {
                            self.vehicle_data1 = self.vehicals!
                        }
                        self.moving_c = c_data["moving_count"] as! Int
                        self.stopped_c =  c_data["stopped_count"] as! Int
                        self.unreach_c =  c_data["unreachable_count"] as! Int
                        self.idle_c =  c_data["idling_count"] as! Int
                        
                        for loc in self.vehicle_data1 {
                            let val_data = loc as! Dictionary<String, Any>
                            let status = val_data["device_status"] as! Int
                            if self.moving{
                                if status == 2{
                                    self.SetAllData(val_data: val_data)
                                }
                            }else if self.idle{
                                if status == 1{
                                    self.SetAllData(val_data: val_data)
                                }
                            }else if self.stopped{
                                if status == 0{
                                    self.SetAllData(val_data: val_data)
                                }
                            } else if self.no_data{
                                if status == 4{
                                    self.SetAllData(val_data: val_data)
                                }
                            }else{
                                self.SetAllData(val_data: val_data)
                            }
                        }
                        
                    /*    let allData : Dictionary<String, Any> = ["moving_count" : self.moving_c,
                                                                 "stopped_count" : self.stopped_c,
                                                                 "unreachable_count" : self.unreach_c,
                                                                 "idling_count" : self.idle_c
                        ]
                     */
                        NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                        NotificationCenter.default.post(name: Notification.Name("TrackingDataForDashboard"), object: nil, userInfo: c_data)
                        break
                    case 2 :
                        let message = data?[K_Message] as! String
                        
                        print(message)
                        break
                    default:
                        print("Default Case")
                    }
                }else{
                    //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                    print("ERROR FOUND")
                }
                if self.is_queue{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 50.0) {
                        // print("Timer fired! ---------")
                        [weak self] in
                        if self?.is_queue != nil && (self?.is_queue)! {
                            self!.CallTrakingDataFromServer()
                        }
                    }
                }
                self.alert_view.removeFromSuperview()
            })
        }}
    
    
    
    func CallTrakingDataFromServer_Map_Page() {
        print("new api map page")

        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.showMap.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(self.alert_view)
        
        
        vehicals?.removeAll()
        vehicle_data1 = []
        latitude.removeAll()
        longitude.removeAll()
        status.removeAll()
        registration.removeAll()
        device_id.removeAll()
        vehicals_s?.removeAll()
        direction.removeAll()
        vehicle_type_id.removeAll()
             
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let startingPoint = Date()

        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
              
        let url = domain_name + "/tracking_api.php"
        
        let parameters : Dictionary<String, Any> = [
            "action" : "track",
            "user_name" : user_name,
            "hash_key" : hash_key,
            
        ]
        print("Tracking URL DashBoard: \(url)")
        NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler: {data, r_error, isNetwork in
               
               if isNetwork {
                   
                   let result = data?[K_Result] as! Int
                   
                   switch (result){

                   case 0 :
                       let c_data = data?[K_Data] as! Dictionary<String,Any>
                       self.moving_c = c_data["moving_count"] as? Int ?? 0
                       self.stopped_c =  c_data["stopped_count"] as? Int ?? 0
                       self.unreach_c =  c_data["unreachable_count"] as? Int ?? 0
                       self.idle_c =  c_data["idling_count"] as? Int ?? 0
                       self.all_vehicles_count = self.moving_c + self.idle_c +  self.stopped_c + self.unreach_c
                       self.vehicals = (c_data[VEHICALS] as! Array<Any>)
                       self.vehicals_s = (c_data[VEHICALS] as! Array<Any>)

                       if self.vehicals!.count > 210{
                        self.vehicle_data1 = SplitArray(into: 210, yourArray: self.vehicals!)
                       }
                       else
                       {
                        self.vehicle_data1 = self.vehicals!
                       }
                       // Broadcast Data by Notification
                       NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                       NotificationCenter.default.post(name: Notification.Name("TrackingDataForDashboard"), object: nil, userInfo: c_data)
                   break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       print("Default Case")
                   }
               }
                   
               else{
                   if NetworkAvailability.isConnectedToNetwork() {
                       print("Internet connection available")
                   }
                   else{
                    showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
                   }
                   //showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                   print("ERROR FOUND")
               }
               
            DispatchQueue.main.async {
            dispatchGroup.leave()   // <<----
            }
            
               if self.all_vehicles_count > 1000
               {
                   self.timeInterval = 60.0
               }
               else if self.all_vehicles_count > 500
               {
                   self.timeInterval = 50.0
               }
               else if self.all_vehicles_count > 200
               {
                   self.timeInterval = 25.0
               }
             
               if self.is_queue {
                   DispatchQueue.main.asyncAfter(deadline: .now() + self.timeInterval) {
                       // print("Timer fired! ---------")
                       [weak self] in
                       if self?.is_queue != nil && (self?.is_queue)!{
                           self!.CallTrakingDataFromServer_Map_Page()
                       }
                   }
               }
              // self.alert_view.removeFromSuperview()
              // self.progressView?.hide()
           })
        
               dispatchGroup.enter()   // <<---
               
               let url_location = domain_name + "/tracking_api.php"
               
               let parameters_new : Dictionary<String, Any> = [
                                "action" : "extra",
                                "user_name" : user_name,
                                "hash_key" : hash_key,
               ]
               
               locationArr.removeAll()

               NetworkManager().CallNewTrackResult(urlString: url_location, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
                   
                   let result = data?[K_Result] as! Int
                   print("result:\(result)")
                   
                   switch (result){
                       
                   case 0 :
                       
                    let c_data = data?[K_Data] as!  Dictionary<String,Any>
                    
                    print("location data:\(c_data)")
                    
                    if let loc_data = c_data["vehicle_data"] as? [Dictionary<String, Any>]
                    {
                        self.locationArr = loc_data.map({$0 ["place"] as! String})
                        //self.distanceArr = loc_data.map({$0 ["today_distance"] as? Double ?? 0.0})
                    }
                    
                    print("loc arr:\(self.locationArr)")
                    
                    if self.vehicals?.count == 0
                    {
                        DispatchQueue.main.async {
                            showToast(controller: self, message: "Reloading Data..", seconds: 1.0)
                        }
                    }
                        
                    else
                    {
                        DispatchQueue.main.async {
                            NotificationCenter.default.post(name: Notification.Name("TrackingDataForAlertCount"), object: nil, userInfo: c_data)
                        }
                    }
                       break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       print("Default Case")
                   }
                
                    DispatchQueue.main.async {
                    dispatchGroup.leave()
                }
                   self.alert_view.removeFromSuperview()
                   
               })
               
               dispatchGroup.notify(queue: .main) {
                      print("both api done in dashboard")
                      print("time consumed \(startingPoint.timeIntervalSinceNow * -1) seconds elapsed")
                      for i in 0..<self.vehicle_data1.count {
                        let val_data = self.vehicle_data1[i] as! Dictionary<String,Any>
                          let status = val_data["device_status"] as! Int
                          if self.moving{
                              if status == 2{
                                self.SetAllData_New(val_data: val_data, locArr: [self.locationArr[i]])
                              }
                          }else if self.idle{
                              if status == 1{
                                self.SetAllData_New(val_data: val_data, locArr: [self.locationArr[i]])
                              }
                          }else if self.stopped{
                              if status == 0{
                                self.SetAllData_New(val_data: val_data, locArr: [self.locationArr[i]])
                              }
                          } else if self.no_data{
                              if status == 4{
                                self.SetAllData_New(val_data: val_data, locArr: [self.locationArr[i]])
                              }
                          }else{
                            self.SetAllData_New(val_data: val_data, locArr: [self.locationArr[i]])
                          }
                      }
                     // self.setVehicleCountData()
                     // self.SetAllDataToTable(vehicle_data: self.all_vehicals)
                   
               }
       }
    
    @objc func GetTrackingData(notification: Notification) {
        // print("got tracking data")
        let c_data = notification.userInfo
        vehicals?.removeAll()
        latitude.removeAll()
        longitude.removeAll()
        status.removeAll()
        registration.removeAll()
        device_id.removeAll()
        vehicals = (c_data![VEHICALS] as! Array<Any>)
        for loc in vehicals! {
            let val_data = loc as! Dictionary<String, Any>
            let status = val_data["device_status"] as! Int
            if moving{
                if status == 2{
                    SetAllData(val_data: val_data)
                }
            } else if idle {
                if status == 1{
                    SetAllData(val_data: val_data)
                }
            } else if stopped {
                if status == 0{
                    SetAllData(val_data: val_data)
                }
            } else if no_data {
                if status == 4{
                    SetAllData(val_data: val_data)
                }
            } else{
                SetAllData(val_data: val_data)
            }
        }
    }
    

    @objc func pressed_zoom_in(){
        
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom + 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
        
    }
    
    @objc func pressed_zoom_out(){
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    @objc func showGooleMap(){
        showMap.isHidden = false
        map_apple.isHidden = true
    }
    
    @objc func showAppleMap(){
        showMap.isHidden = true
        map_apple.isHidden = false
    }

}

extension MainMapVC{
    
    func SetDataOnMAp(){
        for (index, _) in self.registration.enumerated(){
            let point = MyCustomPointAnnotation()
            point.coordinate = CLLocationCoordinate2D(latitude: self.latitude[index], longitude: self.longitude[index])
            
            var vehicle_name = "car"
            if vehicle_type_id[index] == "56"{
                vehicle_name = "police"
            }else if vehicle_type_id[index] == "1"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "2"{
                vehicle_name = "truck"
            }else if vehicle_type_id[index] == "3"{
                vehicle_name = "bus"
            }else if vehicle_type_id[index] == "4"{
                vehicle_name = "van"
            }else if vehicle_type_id[index] == "5"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "6"{
                vehicle_name = "bike"
            }else if vehicle_type_id[index] == "7"{
                vehicle_name = "scooty"
            }else{
                vehicle_name = "car"
            }
            
            if status[index] == 0{
                imageName = vehicle_name + "_red"
            }
            if status[index] == 1{
                imageName = vehicle_name + "_yellow"
            }
            if status[index] == 2{
                imageName = vehicle_name + "_green"
            }
            if status[index] == 4{
                imageName = vehicle_name + "_black"
            }
            
            point.title = self.registration[index]
            let myPlaces = [point]
            self.mapView.addAnnotations(myPlaces)
        }
    }
    
    // -------------------------------------------------- Google Map -----------------------------------//
    
    func SetGoogleDataOnMap(){
        
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: self.latitude[0], longitude: self.longitude[0]))
        let camera = GMSCameraPosition.camera(withLatitude: self.latitude[0], longitude: self.longitude[0], zoom: 10)
        self.showMap!.camera = camera
        
        var marker = GMSMarker()
        for (index, _) in self.registration.enumerated()
        {
            let initialLocation = CLLocationCoordinate2DMake(self.latitude[index], self.longitude[index])
            marker = GMSMarker(position: initialLocation)
            
            var vehicle_name = "car"
            if vehicle_type_id[index] == "56"{
                vehicle_name = "police"
            }else if vehicle_type_id[index] == "1"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "2"{
                vehicle_name = "truck"
            }else if vehicle_type_id[index] == "3"{
                vehicle_name = "bus"
            }else if vehicle_type_id[index] == "4"{
                vehicle_name = "van"
            }else if vehicle_type_id[index] == "5"{
                vehicle_name = "car"
            }else if vehicle_type_id[index] == "6"{
                vehicle_name = "bike"
            }else if vehicle_type_id[index] == "7"{
                vehicle_name = "scooty"
            }else{
                vehicle_name = "car"
            }
            
            if status[index] == 0{
                imageName = vehicle_name + "_red"
            }
            if status[index] == 1{
                imageName = vehicle_name + "_yellow"
            }
            if status[index] == 2{
                imageName = vehicle_name + "_green"
            }
            if status[index] == 4{
                imageName = vehicle_name + "_black"
            }
            
            marker.icon = UIImage(named: imageName)
            marker.map = self.showMap
        }
    }
    
    func generateClusterItems() {
        
        for index in 0...self.latitude.count - 1 {
            let lat = self.latitude[index]
            let lng = self.longitude[index]
            let name = self.registration[index]
            let item =
                POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name)
            
            clusterManager.add(item)
        }
    }
    
    
    func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    // MARK: - GMUClusterManagerDelegate
    
    func clusterManager(clusterManager: GMUClusterManager, didTapCluster cluster: GMUCluster) {
        
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position, zoom: showMap.camera.zoom + 1)
        let update = GMSCameraUpdate.setCamera(newCamera)
        showMap.moveCamera(update)
    }
    
    
    // MARK: - GMUMapViewDelegate
    
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let poiItem = marker.userData as? POIItem {
            marker.title = poiItem.name!
            
        } else {
            NSLog("Did tap a normal marker")
        }
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        self.is_queue = false
        var index_val : Int!
        var deviceID = [String]()
        for (index, loc) in (vehicals_s?.enumerated())!{
            let val_data = loc as! Dictionary<String, Any>
            deviceID.append(GetDeviceID(Vehicals: val_data))
            if marker.title == GetRegistrationNumber(Vehicals: val_data){
                index_val = index
            }
        }
        
        //UserDefaults.standard.set(false, forKey: "is_global_selection")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowTrackingInfo") as! ShowTrackingInfo
        vc.index = index_val
        vc.vehicals = vehicals_s
        vc.fromVC = "MapPage"
        vc.device_id = deviceID[index_val]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func renderer(_ renderer: GMUClusterRenderer, didRenderMarker marker: GMSMarker) {
        
        var image_name = STOP_CAR
        var direction : Double = 0.0
        print("latitude arr count:\(self.latitude.count)")
        for (index, _) in self.latitude.enumerated(){
            if marker.position.latitude == latitude[index]{
                var vehicle_name = "car"
                if vehicle_type_id[index] == "73"{
                    vehicle_name = "tractor"
                }
                else if vehicle_type_id[index] == "74"{
                    vehicle_name = "roadroller"
                }
                else if vehicle_type_id[index] == "75"{
                    vehicle_name = "cementmixer"
                }
                else if vehicle_type_id[index] == "56"{
                    vehicle_name = "pcr"
                }else if vehicle_type_id[index] == "1"{
                    vehicle_name = "car"
                }else if vehicle_type_id[index] == "2"{
                    vehicle_name = "truck"
                }else if vehicle_type_id[index] == "3"{
                    vehicle_name = "bus"
                }else if vehicle_type_id[index] == "4"{
                    vehicle_name = "van"
                }else if vehicle_type_id[index] == "5"{
                    vehicle_name = "car"
                }else if vehicle_type_id[index] == "6"{
                    vehicle_name = "bike"
                }else if vehicle_type_id[index] == "7"{
                    vehicle_name = "scooty"
                }
                else if vehicle_type_id[index] == "58"{
                    vehicle_name = "ambulance"
                }else{
                    vehicle_name = "car"
                }
                
                if status[index] == 0 {
                    image_name = vehicle_name + "_red_map"
                    direction = self.direction[index]
                }
                if status[index] == 1{
                    
                    image_name = vehicle_name + "_yellow_map"
                    direction = self.direction[index]
                }
                if status[index] == 2 {
                    direction = self.direction[index]
                    image_name = vehicle_name + "_green_map"
                }
                
                if status[index] == 4{
                    direction = self.direction[index]
                    image_name = vehicle_name + "_black_map"
                }
            }
            
            marker.groundAnchor = CGPoint(x: 0.5, y: 1)
            
            if (marker.userData as? POIItem) != nil {
                let vehicle = UIImage(named: image_name)!
                marker.icon = vehicle
                marker.setIconSize(scaledToSize: .init(width: 25, height: 40))
                marker.tracksViewChanges = false
                marker.rotation = direction
            }
        }
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        let camera = GMSCameraPosition.camera(withLatitude: latitude[0], longitude: longitude[0], zoom: 12)
        showMap.animate(to: camera)
        //self.showMap!.camera = camera
        return true
    }
    
    
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
    
    }
    
}

class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String?
    
    
    init(position: CLLocationCoordinate2D, name: String?) {
        self.position = position
        self.name = name
    }
}

class MapClusterIconGenerator: GMUDefaultClusterIconGenerator {

    override func icon(forSize size: UInt) -> UIImage {
        let image = textToImage(drawText: String(size) as NSString,
                                inImage: UIImage(named: "cluster")!,
                                font: UIFont.systemFont(ofSize: 12))
        return image
    }

    private func textToImage(drawText text: NSString, inImage image: UIImage, font: UIFont) -> UIImage {

        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))

        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = NSTextAlignment.center
        let textColor = UIColor.black
        let attributes=[
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: textStyle,
            NSAttributedString.Key.foregroundColor: textColor]

        // vertically center (depending on font)
        let textH = font.lineHeight
        let textY = (image.size.height-textH)/2
        let textRect = CGRect(x: 0, y: textY, width: image.size.width, height: textH)
        text.draw(in: textRect.integral, withAttributes: attributes)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }

}
