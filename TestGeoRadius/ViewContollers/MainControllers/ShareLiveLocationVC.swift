//
//  ShareLiveLocationVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class ShareLiveLocationVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var titleArr = ["2 Hrs","24 Hrs","7 Days","Custom"]
    var cellSpacingHeight:CGFloat = 10.0
    var selectedImage:UIImage!
    var deselectedImage:UIImage!
    var datePicker : UIDatePicker!
    let toolBar = UIToolbar()
    var inputView1 : UIView!
    var device_id:String!
    var date_time:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            selectedImage = UIImage(systemName:"largecircle.fill.circle")?.withTintColor(UIColor.systemGreen, renderingMode: .alwaysTemplate)
        } else {
            // Fallback on earlier versions
            selectedImage = UIImage.init(named: "circle_filled")?.withRenderingMode(.alwaysTemplate)
        }
        
        if #available(iOS 13.0, *) {
            deselectedImage = UIImage(systemName:"circle")?.withTintColor(UIColor.clear, renderingMode: .alwaysTemplate)
        } else {
            // Fallback on earlier versions
            deselectedImage = UIImage.init(named: "circle_empty")?.withRenderingMode(.alwaysOriginal)
        }
        
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
            
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareLocation(_ sender: UIButton) {
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "NO INTERNET CONNECTION", seconds: 1.5)
                  // self.tbl_report.isHidden = true
                   return
               }
               let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
               let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
               let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
               let urlString1 = domain_name + "/vehicle_result.php?action=temp_access&device_id=" + device_id!
               let urlString = urlString1 + "&phone_val=&email_val=&data_format=1&user_name=" + user_name + "&hash_key=" + hash_key + "&validity_date=" + date_time
               let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
              print("share location api:\(encodedString), time :\(date_time)")
        
               NetworkManager().CallShareDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
                   if isNetwork && data != nil{
                       let url = data!["tempurl"] as! String
                   
                       let items = [url];
                       let activity = UIActivityViewController(activityItems: items, applicationActivities: nil);
                       self.present(activity, animated: true, completion: nil)
                       
                   }else{
                        showToast(controller: self, message : "Something went wrong." , seconds: 2.0)
                       print("ERROR FOUND")
                   }
                   if r_error != nil{
                      showToast(controller: self, message : r_error!, seconds: 2.0)
                   }
                   
               })
    }
    
    
    func doDatePicker(sender:UIView){
        
        // DatePicker
        inputView1 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250))
        if #available(iOS 13.0, *) {
            inputView1.backgroundColor = UIColor.systemGroupedBackground
        } else {
            // Fallback on earlier versions
             inputView1.backgroundColor = UIColor.groupTableViewBackground
        }
        
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: 100, height: 30))
        titleLabel.text = "Valid Till:"
        
        let doneButton = UIButton(frame: CGRect(x: self.view.frame.size.width-80, y: 200, width: 70, height: 35))
        doneButton.setTitle("Done", for: .normal)
        doneButton.layer.cornerRadius = 10.0
        doneButton.backgroundColor = UIColor.darkGray
        doneButton.setTitleColor(.white, for: .normal)
        inputView1.addSubview(doneButton) // add Button to UIView
        doneButton.addTarget(self, action: #selector(ShareLiveLocationVC.doneClick), for: UIControl.Event.touchUpInside)
        
        let cancelButton = UIButton(frame: CGRect(x: self.view.frame.size.width-160, y:200, width: 70, height: 35))
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.layer.cornerRadius = 10.0
        cancelButton.backgroundColor = UIColor.darkGray
        cancelButton.setTitleColor(.white, for: .normal)
        inputView1.addSubview(cancelButton) // add Button to UIView
        cancelButton.addTarget(self, action: #selector(ShareLiveLocationVC.cancelClick), for: UIControl.Event.touchUpInside)
              
        
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 40, width: self.view.frame.size.width, height: 150))
        self.datePicker?.backgroundColor = UIColor.white
        self.datePicker?.minimumDate = Date()
        self.datePicker?.datePickerMode = .dateAndTime
        
        
        inputView1.addSubview(titleLabel)
        inputView1.addSubview(doneButton)
        inputView1.addSubview(self.datePicker)

        self.view.addSubview(inputView1)
        // ToolBar
    }

 
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.calendar = Calendar(identifier: .gregorian)
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        date_time = dateFormatter1.string(from: datePicker.date)
        self.datePicker.resignFirstResponder()
        inputView1.removeFromSuperview()
    }

    @objc func cancelClick() {
        inputView1.removeFromSuperview()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ShareLiveLocationVC: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
            return self.titleArr.count
        }
        
        // There is just one row in every section
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        // Set the spacing between sections
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if section == 0
            {
                return 0.0
            }
            return cellSpacingHeight
        }
        
        // Make the background color show through
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            return headerView
        }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
           let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityCell") as! ActivityTypeCell
           cell.activityTitleLbl.text = titleArr[indexPath.section]
           cell.selectionStyle = .none
           return cell
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath) as! ActivityTypeCell
        cell.iconView.setImage(selectedImage, for: .normal)
        if indexPath.section == 0
        {
            date_time = Date.getFutureTime(hours: 2)
        }
        else if  indexPath.section == 1
        {
            date_time = Date.getFutureTime(hours: 24)
        }
        else if indexPath.section == 2
        {
            date_time = Date.getPastDate(days: 6, from: "ShareLoc")
        }
        else if indexPath.section == 3
        {
            DispatchQueue.main.async {
                self.doDatePicker(sender: cell)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath) as! ActivityTypeCell
        cell.iconView.setImage(deselectedImage, for: .normal)
    }
}


