//
//  CriticalAlertsVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import QuickLook
import PDFGenerator

class CriticalAlertsVC: UIViewController {

    var viewModel: APIDemoViewModelProtocol?
    var alertsList : [CriticalAlertsModel] = []
    var filteredAlertsList : [CriticalAlertsModel] = []
    @IBOutlet weak var tableView: UITableView!
    let cellSpacingHeight: CGFloat = 15.0
    lazy var previewItem = NSURL()
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    @IBOutlet weak var lbl_title: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
       // self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.startShimmerAnimation(withIdentifier: "CriticalAlertCell", numberOfRows: 1, numberOfSections: 8)
        self.searchBar.tintColor = UIColor.systemBlue
        showVehiclesList()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func downloadCriticalAlertsReport(_ sender: UIButton) {
        
        let pdfFilePath = self.tableView.exportAsPdfFromTable()
        let previewController = QLPreviewController()
        if let pathURL = NSURL(fileURLWithPath: pdfFilePath) as? NSURL
        {
        self.previewItem = pathURL
        previewController.dataSource = self
        self.present(previewController, animated: true, completion: nil)
        }
    }
    
    
    func showVehiclesList()
    {
        self.viewModel = APIDemoViewModel()
        self.bindUI()
        self.viewModel!.getCriticalAlertsList()
    }
   
    private func bindUI() {
        
        self.viewModel?.isLoaderHidden.bind({ [weak self] in
               self?.shouldHideLoader(isHidden: $0)
           })
        
           self.viewModel?.alertMessage.bind({ [weak self] in
               self?.showAlertWith(message: $0)
           })
        
           self.viewModel?.criticalAlerts.bind({ [weak self] in
               if let alerts = $0 {
                  self?.alertsList.removeAll()
                  self?.filteredAlertsList.removeAll()
                  
                   for vehicleDetails in alerts.data
                   {
                       let vehicleDetail = CriticalAlertsModel(alert_id: vehicleDetails.alert_id ?? "", alert_time: vehicleDetails.alert_time ?? "", registration_no: vehicleDetails.registration_no ?? "", alert_type_name: vehicleDetails.alert_type_name ?? "")
                       
                       self?.alertsList.append(vehicleDetail)
                   }
               }
               
               DispatchQueue.main.async {
                   self?.filteredAlertsList = self?.alertsList ?? []
                   self?.stopShimmerAnimation()
                   self?.tableView.delegate = self
                   self?.tableView.dataSource = self
                   self?.searchBar.delegate = self
                   self?.tableView.reloadData()
               }
           })}
    
    
    @IBAction func back_btn_pressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension CriticalAlertsVC:UITableViewDataSource,UITableViewDelegate,QLPreviewControllerDataSource,UISearchBarDelegate
{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem as QLPreviewItem
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = self.filteredAlertsList.count
        } else {
            count = self.alertsList.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CriticalAlertCell") as! CriticalAlertsCell
        if searchActive{
            cell.lbl_registration_no.text = self.filteredAlertsList[indexPath.row].registration_no
            cell.lbl_alert_time.text = self.filteredAlertsList[indexPath.row].alert_time
            cell.lbl_alert_type.text = self.filteredAlertsList[indexPath.row].alert_type_name
        }
        else{
            cell.lbl_registration_no.text = self.alertsList[indexPath.row].registration_no
            cell.lbl_alert_time.text = self.alertsList[indexPath.row].alert_time
            cell.lbl_alert_type.text = self.alertsList[indexPath.row].alert_type_name
        }
        
        cell.selectionStyle = .none
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 12
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
          searchActive = true;
          self.searchBar.showsCancelButton = true
      }
      
      func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
          searchActive = false;
          self.searchBar.endEditing(true)
      }
      
      func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          self.searchBar.endEditing(true)
          self.searchBar.text = ""
          self.searchBar.showsCancelButton = false
          self.tableView.reloadData()
      }
      
      func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          self.searchBar.endEditing(true)
      }
      
      func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            searchActive = false;
        }
        else
        {
          searchActive = true;
          filteredAlertsList = alertsList.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
          print("search data:\(filteredAlertsList)")
        }
          self.tableView.reloadData()
      }
}

extension UITableView {
    
    // Export pdf from UITableView and save pdf in drectory and return pdf file path
    func exportAsPdfFromTable() -> String {
        self.showsVerticalScrollIndicator = false
        let originalFrame = self.frame
        self.frame = CGRect(x:originalFrame.origin.x, y: originalFrame.origin.y, width: self.contentSize.width, height: self.contentSize.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, self.bounds, nil)
        UIGraphicsBeginPDFPage()
        guard let pdfContext = UIGraphicsGetCurrentContext() else { return "" }
        self.layer.render(in: pdfContext)
        UIGraphicsEndPDFContext()
        // Save pdf data
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
               let docDirectoryPath = paths[0]
               let pdfPath = docDirectoryPath.appendingPathComponent("criticalAlerts1.pdf")
               if pdfData.write(to: pdfPath, atomically: true) {
                   self.frame = originalFrame
                   self.showsVerticalScrollIndicator = true
                   return pdfPath.path
               } else {
                   return ""
               }
    }
    
}
