//
//  SettingsViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import EzPopup

protocol PopupViewControllerDelegate: class {
    /// It is called when pop up is dismissed by tap outside
    func popupViewControllerDidDismiss(sender: SettingsViewController)
}


class SettingsViewController: UIViewController {
    
    
    @IBOutlet weak var dashboardPopUp: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btn_map_page_select: UIButton!
    @IBOutlet weak var btn_live_page_select: UIButton!
    @IBOutlet weak var btn_dashboard_select: UIButton!
    @IBOutlet weak var pswdTF: UITextField!
    @IBOutlet weak var cnfmPswdTF: UITextField!
    @IBOutlet weak var errorLbl: UILabel!
    var titleArr = [String]()
    var imageTitleArr = ["dashboard_setting","notif_settings","reset_pswd","changeLang"]
    let cellSpacingHeight : CGFloat = 15.0
    var uncheckImage = UIImage()
    var checkImage = UIImage()
    let selectedColor = UIColor(red: 42.0/255.0, green: 165.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    let alert_view = AlertView.instanceFromNib()
    @IBOutlet weak var resetPswdPopUp: UIView!
    @IBOutlet weak var languageSelectionPopUp: UIView!
    @IBOutlet weak var englishSelectionBtn: UIButton!
    @IBOutlet weak var thaiSelectionBtn: UIButton!
    var currentVC = UIViewController()
    weak var delegate: PopupViewControllerDelegate?
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet var labels: Array<UILabel>! //6
    @IBOutlet weak var btn_reset: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            uncheckImage = UIImage(systemName: "circle")!
        } else {
            // Fallback on earlier versions
            uncheckImage = UIImage.init(named: "circle_empty")!
        }
        
        if #available(iOS 13.0, *) {
            checkImage = UIImage(systemName: "largecircle.fill.circle")!
        } else {
            // Fallback on earlier versions
            checkImage = UIImage.init(named: "circle_filled")!
        }
        setListTitles()
        setLanguageInControls()
        titleLbl.text = LanguageHelperClass().settingsTxt
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tableView.frame
        tempImageView.contentMode = .scaleAspectFill
        self.tableView.backgroundView = tempImageView
        self.tableView.tableFooterView = UIView()
        
        btn_dashboard_select.addTarget(self, action: #selector(pressed_dashboard_select_btn), for: .touchUpInside)
        btn_live_page_select.addTarget(self, action: #selector(pressed_live_select_btn), for: .touchUpInside)
        btn_map_page_select.addTarget(self, action: #selector(pressed_map_select_btn), for: .touchUpInside)
        englishSelectionBtn.addTarget(self, action: #selector(pressed_english_select_btn), for: .touchUpInside)
        thaiSelectionBtn.addTarget(self, action: #selector(pressed_thai_select_btn), for: .touchUpInside)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        
        if let selectedPage = UserDefaults.standard.value(forKey: "select_home_page") as? Int
        {
            if selectedPage == 0
            {
                selectedDashboardPage()
            }
            else if selectedPage == 1
            {
                selectedLivePage()
            }
            else
            {
                selectedMapPage()
            }
        }
        
        if let language = UserDefaults.standard.value(forKey: "language") as? String
        {
            if language == "en"
            {
                selectedEnglishLanguage()
            }
            else if language == "th-TH"
            {
                selectedThaiLanguage()
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    func setLanguageInControls() {
        
        for index in 0...5
        {
            switch index {
            case 0:
                labels[index].text = LanguageHelperClass().selDefPageTxt
            case 1:
                labels[index].text = LanguageHelperClass().dashPageTxt
            case 2:
                labels[index].text = LanguageHelperClass().livePageTxt
            case 3:
                labels[index].text = LanguageHelperClass().mapPageTxt
            case 4:
                labels[index].text = LanguageHelperClass().resetPasswordTxt
            case 5:
                labels[index].text = LanguageHelperClass().selDefLangTxt
            default:
                print("done")
            }
        }
        
        pswdTF.placeholder = LanguageHelperClass().passwordTxt
        cnfmPswdTF.placeholder = LanguageHelperClass().confirmPswdTxt
        btn_reset.setTitle(LanguageHelperClass().resetTxt, for: .normal)
    }
    
    
    func setListTitles() {
        
        if titleArr.count > 0
        {
            titleArr = []
        }
        titleArr.append(LanguageHelperClass().dashboardTxt)
        titleArr.append(LanguageHelperClass().notificationsTxt)
        titleArr.append(LanguageHelperClass().resetPasswordTxt)
        titleArr.append(LanguageHelperClass().changeLangTxt)
    }

    
    @IBAction func sideMenuBtnClicked(_ sender: UIButton) {
        SideMenu(sender: sender)
    }
    
    func SideMenu(sender:UIButton){
        sender.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    
    
    @objc func pressed_map_select_btn(){
        UserDefaults.standard.set(2, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")
        selectedMapPage()
        dismissView()
    }
    
    @objc func pressed_live_select_btn(){
        UserDefaults.standard.set(1, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")
        selectedLivePage()
        dismissView()
    }
    
    @objc func pressed_dashboard_select_btn(){
        UserDefaults.standard.set(0, forKey: "select_home_page")
        UserDefaults.standard.set(true, forKey: "is_global_selection")
        selectedDashboardPage()
        dismissView()
    }
    
    @objc func pressed_english_select_btn() {
        UserDefaults.standard.set("en", forKey: "language")
        englishSelectionBtn.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        englishSelectionBtn.tintColor = selectedColor
        thaiSelectionBtn.setBackgroundImage(uncheckImage, for: .normal)
        titleLbl.text = LanguageHelperClass().settingsTxt
        setLanguageInControls()
        setListTitles()
        self.tableView.reloadData()
        dismissView()
    }
    
    @objc func pressed_thai_select_btn(){
        UserDefaults.standard.set("th-TH", forKey: "language")
        thaiSelectionBtn.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        thaiSelectionBtn.tintColor = selectedColor
        englishSelectionBtn.setBackgroundImage(uncheckImage, for: .normal)
        titleLbl.text = LanguageHelperClass().settingsTxt
        setLanguageInControls()
        setListTitles()
        self.tableView.reloadData()
        dismissView()
    }
    
    
    func selectedLivePage()
    {
        btn_map_page_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_map_page_select.tintColor = .darkGray
        btn_dashboard_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_dashboard_select.tintColor = .darkGray
        btn_live_page_select.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        btn_live_page_select.tintColor = selectedColor
    }
    
    func selectedMapPage()
    {
        btn_map_page_select.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        btn_map_page_select.tintColor = selectedColor
        btn_dashboard_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_dashboard_select.tintColor = .darkGray
        btn_live_page_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_live_page_select.tintColor = .darkGray
    }
    
    func selectedDashboardPage()
    {
        btn_map_page_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_map_page_select.tintColor = .darkGray
        btn_dashboard_select.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        btn_dashboard_select.tintColor = selectedColor
        btn_live_page_select.setBackgroundImage(uncheckImage, for: .normal)
        btn_live_page_select.tintColor = .darkGray
    }
    
    func selectedEnglishLanguage()
    {
        thaiSelectionBtn.setBackgroundImage(uncheckImage, for: .normal)
        thaiSelectionBtn.tintColor = .darkGray
        englishSelectionBtn.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        englishSelectionBtn.tintColor = selectedColor
    }
    
     func selectedThaiLanguage()
     {
        englishSelectionBtn.setBackgroundImage(uncheckImage, for: .normal)
        englishSelectionBtn.tintColor = .darkGray
        thaiSelectionBtn.setBackgroundImage(checkImage.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        thaiSelectionBtn.tintColor = selectedColor
    }
    
    @IBAction func resetPassword(_ sender: Any) {
        if pswdTF.text!.count < 1 || cnfmPswdTF.text!.count < 1{
            errorLbl.text = " *Please fill all the fields"
            return
        }
        
        if pswdTF.text != cnfmPswdTF.text{
            errorLbl.text = " *Passwords do not match"
            return
        }
        
        errorLbl.text = ""
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + Reset_Password + "user_name=" + user_name + "&" + "hash_key=" + hash_key + "&" + "user_id=" + user_id + "&" + "password=" + cnfmPswdTF.text!
        
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(alert_view)
        
        NetworkManager().CallForgotPasswordAPI(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
                
            }else{
                // print("sdfkllsdk \(isNetwork) \(String(describing: data))")
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
            self.LogoutFromServer()
        })
        
    }
    
    func LogoutFromServer(){
        print("Logout")
        
        UIApplication.shared.keyWindow!.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as? String ?? ""
        //https://track.georadius.in/login_result.php?action=logoutApp&user_id=2212&user_app_id=4ccafb1053b6983e34d572d75388d920a5cb9ce8676577975d7a62cc8a3ceb81
        let urlString = domain_name + "/login_result.php?action=logoutApp&user_id=" + user_id + "&user_app_id=" + device_token
        
        NetworkManager().CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //showToast(controller: self, message : "Logout", seconds: 2.0)
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.alert_view.removeFromSuperview()
            SaveLoginKey(key : "")
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController = loginVC
        })
    }
    
    
    @objc func dismissView() {
        dismiss(animated: true) {
            self.delegate?.popupViewControllerDidDismiss(sender: self)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SettingsViewController: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.titleArr.count
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 10.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("image arr:\(imageTitleArr), title arr:\(titleArr)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! ServiceProviderTableViewCell
        cell.imgIcon.image = UIImage(named:imageTitleArr[indexPath.section])
        cell.titleLbl.text = titleArr[indexPath.section]
        cell.selectionStyle = .none
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 10
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height/10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0
        {
            let popUp = PopupViewController(contentView: self.dashboardPopUp, popupWidth: self.dashboardPopUp.frame.size.width, popupHeight: self.dashboardPopUp.frame.size.height)
            popUp.backgroundAlpha = 0.7
            popUp.canTapOutsideToDismiss = true
            popUp.cornerRadius = 10
            popUp.shadowEnabled = true
            DispatchQueue.main.async {
                //View related code
                self.present(popUp, animated: false)
            }
        }
        else if indexPath.section == 1
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.section == 2
        {
            let popUp = PopupViewController(contentView: self.resetPswdPopUp, popupWidth: self.resetPswdPopUp.frame.size.width, popupHeight: self.resetPswdPopUp.frame.size.height)
            popUp.backgroundAlpha = 0.7
            popUp.canTapOutsideToDismiss = true
            popUp.cornerRadius = 10
            popUp.shadowEnabled = true
            DispatchQueue.main.async {
                //View related code
                self.present(popUp, animated: false)
            }
        }
            
        else if indexPath.section == 3
        {
            let popUp = PopupViewController(contentView: self.languageSelectionPopUp, popupWidth: self.languageSelectionPopUp.frame.size.width, popupHeight: self.languageSelectionPopUp.frame.size.height)
            popUp.backgroundAlpha = 0.7
            popUp.canTapOutsideToDismiss = true
            popUp.cornerRadius = 10
            popUp.shadowEnabled = true
            DispatchQueue.main.async {
                //View related code
                self.present(popUp, animated: false)
            }
        }
    }
}

