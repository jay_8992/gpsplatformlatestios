//
//  PopUpVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

protocol PopUpDataDelegate: class {
    func sendData(sender: PopUpVC, selectedType:String,popUpType:String)
}

class PopUpVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var titleArr = [String]()
    var cellSpacingHeight:CGFloat = 10.0
     weak var delegate: PopUpDataDelegate?
    var selectedType = ""
    var popUpType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
extension PopUpVC: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
            return self.titleArr.count
        }
        
        // There is just one row in every section
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        // Set the spacing between sections
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            if section == 0
            {
                return 10.0
            }
            return cellSpacingHeight
        }
        
        // Make the background color show through
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.clear
            return headerView
        }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
           let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityCell") as! ActivityTypeCell
           cell.activityTitleLbl.text = titleArr[indexPath.section]
           cell.selectionStyle = .none
           return cell
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedType = titleArr[indexPath.section]
        delegate?.sendData(sender: self, selectedType: self.selectedType, popUpType: self.popUpType)
        dismiss(animated: true, completion: nil)
        
    }
}



