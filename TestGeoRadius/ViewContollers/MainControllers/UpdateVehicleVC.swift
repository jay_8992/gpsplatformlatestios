//
//  UpdateVehicleVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 10/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import RSSelectionMenu

class UpdateVehicleVC: UIViewController {
    @IBOutlet weak var lbl_registration: UILabel!
    @IBOutlet weak var txt_registration: UITextField!
    @IBOutlet weak var lbl_device_tag: UILabel!
    @IBOutlet weak var txt_device_tag: UITextField!
    @IBOutlet weak var btn_update: UIButton!
    @IBOutlet weak var lbl_vehicleType: UILabel!
    @IBOutlet weak var txt_vehicle_type: UITextField!
    @IBOutlet weak var txt_device_serial: UITextField!
    @IBOutlet weak var lbl_device_serial: UILabel!
    @IBOutlet weak var tbl_show_items: UITableView!
    let alert_view = AlertView.instanceFromNib()
    @IBOutlet weak var lbl_driver_name: UILabel!
    @IBOutlet weak var txt_driver_name: UITextField!
    @IBOutlet weak var lbl_driver_no: UILabel!
    @IBOutlet weak var txt_driver_no: UITextField!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var lbl_vehicle_type: UILabel!
    @IBOutlet weak var view_vehicle_type: UIView!
    @IBOutlet weak var lbl_voiceNo: UILabel!
    var data = [String]()
    var filterdData : [String]!
    var device_id : String!
    var vehicle_type_id = [String]()
    var vehicle_typeid : String!
    var simpleSelectedArray = [String]()
    var data1 : [VehicleTypeModel] = []
    var filteredData1 : [VehicleTypeModel] = []
    var cellSpacingHeight:CGFloat = 15.0
    var searchActive : Bool = false
    var language:String!
    var alertText : String!
    var bd:Bundle!
    var cellSelectionStyle: CellSelectionStyle = .tickmark
    var vehicleDataName = [String]()
    var selectedDataArray = [String]()
    var driverName = ""
    var driverNo = ""
    var device_serial = ""
    var driver_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        language = UserDefaults.standard.value(forKey: "language") as? String
        setLanguageInControls()
       
       // txt_voice.addTarget(self, action: #selector(Tap_voice_no), for: .editingDidBegin)
        txt_device_serial.addTarget(self, action: #selector(Tap_device_serial), for: .editingDidBegin)
        txt_vehicle_type.addTarget(self, action: #selector(Tap_vehicle_type), for: .editingDidBegin)
        btn_update.addTarget(self, action: #selector(pressed_btn_update), for: .touchUpInside)
        
       // SetTextFieldBorder(textField: txt_registration)
       // SetTextFieldBorder(textField: txt_device_serial)
      //  SetTextFieldBorder(textField: txt_vehicle_type)
      // SetTextFieldBorder(textField: txt_device_tag)
      // SetTextFieldBorder(textField: txt_voice)

        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_vehicle_type)
        self.tbl_show_items.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        
        view_vehicle_type.layer.cornerRadius = 5
        view_vehicle_type.layer.masksToBounds = true
        view_vehicle_type.layer.borderColor = UIColor.lightGray.cgColor
        view_vehicle_type.layer.borderWidth = 0.3
        
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(Tap_vehicle_type))
        lbl_vehicle_type.isUserInteractionEnabled = true
        lbl_vehicle_type.addGestureRecognizer(tapgesture)
    }
    
    func setLanguageInControls()
    {
        bd = setLanguage(lang : language)
        lbl_registration.text = bd.localizedString(forKey: "REGISTRATION_NO", value: nil, table: nil)
        lbl_vehicleType.text = bd.localizedString(forKey: "VEHICLE_TYPE", value: nil, table: nil)
        lbl_device_tag.text = bd.localizedString(forKey: "DEVICE_TAG", value: nil, table: nil)
        lbl_driver_name.text = bd.localizedString(forKey: "DRIVER_NAME", value: nil, table: nil)
        lbl_driver_no.text = bd.localizedString(forKey: "DRIVER_NO", value: nil, table: nil)
        alertText = bd.localizedString(forKey: "PLEASE_WAIT", value: nil, table: nil)
    }
    
    @objc func Tap_vehicle_type() {
        txt_vehicle_type.resignFirstResponder()
        tbl_show_items.isHidden = true
        //isSelect_Vehicle = false
      //   CallVehicleType()
        UIView.animate(withDuration: 0.8, animations: {
          self.showSingleSelectionMenu(style: .present)
        })
//        UIView.animate(withDuration: 0.8, animations: {
//            self.tbl_show_items.frame.origin.y = 0
//        })
    }
    
    @objc func Tap_voice_no(){
       // txt_voice.resignFirstResponder()
    }
    
    @objc func Tap_device_serial(){
        txt_device_serial.resignFirstResponder()
    }
    
    @objc func pressed_btn_update(){
        UpdateDataToServer()
    }
    
    func SetDataOnFields(registration_no: String, voice_no: String, device_tag: String, device_serial: String, device_id: String, vehicle_type_id: String , driver_name: String, driver_no:String, driver_id:String){
        txt_registration.text = registration_no
        //txt_voice.text = voice_no
        txt_device_tag.text = device_tag
       // txt_device_serial.text = device_serial
        txt_driver_name.text = driver_name
        txt_driver_no.text = driver_no
        self.device_serial = device_serial
        self.device_id = device_id
        self.vehicle_typeid = vehicle_type_id
        self.driver_id = driver_id
        CallVehicleType()
    }
    
    func UpdateDataToServer(){
        if lbl_vehicle_type.text!.count < 1{
            showToast(controller: self, message: "Vehicle type can't be nil", seconds: 0.9)
            return
        }
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        driverName = txt_driver_name.text ?? ""
        driverNo = txt_driver_no.text ?? ""
        
        let urlString = Update_Vehicle + self.device_serial + "&device_id=" + self.device_id! + "&vehicle_type=" + self.vehicle_typeid + "&registration_no=" + txt_registration.text! + "&tag=" + txt_device_tag.text! + "&driver_name=" + driverName + "&driver_no=" + driverNo + "&driver_id=" + self.driver_id +  "&user_name=" + user_name + "&hash_key=" + hash_key
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        print("url string:\(encodedString)")

        NetworkManager().CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : LanguageHelperClass.getSeverError() , seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : LanguageHelperClass.getSeverError() , seconds: 2.0)
            }
        })
    }
    
    
    func CallVehicleType(){
        
        data.removeAll()
        self.vehicleDataName.removeAll()
        self.vehicle_type_id.removeAll()
        self.data1.removeAll()
        self.filteredData1.removeAll()
        self.view.addSubview(self.alert_view)
        self.alert_view.backgroundColor = UIColor.clear
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let vehicle_type_url = domain_name + Vehicle_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        NetworkManager().CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.data.append(v_val["vehicle_type_name"] as! String)
                        self.vehicle_type_id.append(GetVehicleTypeId(Vehicals: v_val))
                        let type_id = String(GetVehicleTypeId(Vehicals: v_val))
                        
                        if type_id == self.vehicle_typeid{
                            self.lbl_vehicle_type.textColor = UIColor.black
                            self.lbl_vehicle_type.text = (v_val["vehicle_type_name"] as? String ?? "")
                        }
                        
                        let vehicle_type_name = v_val["vehicle_type_name"] as? String ?? ""
                        self.vehicleDataName.append(vehicle_type_name)
                    
                        let newData = VehicleTypeModel(vehicle_type_name: vehicle_type_name, vehicle_type_id: type_id)
                        self.data1.append(newData)
                    }
                    
                    // self.filterdData = self.data
                    self.filteredData1 = self.data1
                   // self.showSingleSelectionMenu(style: .present)
                    self.tbl_show_items.delegate = self
                    self.searchBar.delegate = self
                    self.tbl_show_items.dataSource = self
                    self.tbl_show_items.reloadData()
                    //self.search_items.delegate = self
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
}

extension UpdateVehicleVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_show_items.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        cell.layer.cornerRadius = 10.0
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.img_check.isHidden = true
        cell.img_uncheck.isHidden = true
       // cell.lbl_item_name.text = vehicleDataName[indexPath.section]
        if searchActive{
            cell.lbl_item_name.text = filteredData1[indexPath.section].vehicle_type_name
        }
        else{
            cell.lbl_item_name.text = data1[indexPath.section].vehicle_type_name
        }

        //  cell.lbl_item_name.text = filterdData[indexPath.section]
        return cell
    }


    func showSingleSelectionMenu(style: PresentationStyle) {
        // Here you'll get cell configuration where you'll get array item for each index
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Item of type T  3.IndexPath
        let selectionMenu = RSSelectionMenu(dataSource: self.vehicleDataName) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        // set default selected items when menu present on screen.
        // here you'll get handler each time you select a row
        // 1. Selected Item  2. Index of Selected Item  3. Selected or Deselected  4. All Selected Items
        
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (text, index, isSelected, selectedItems) in
            // update your existing array with updated selected items, so when menu show menu next time, updated items will be default selected.
            self?.simpleSelectedArray = selectedItems
            let selectedIndex = self?.vehicleDataName.indices.filter {self!.vehicleDataName[$0] == text}
            print("selected index:\(String(describing: selectedIndex))")
            switch style {
            case .push:
               print("Push view")
            case .present:
                self?.lbl_vehicle_type.text = text
                if selectedIndex?.count == 1
                {
                 self?.vehicle_typeid = self?.vehicle_type_id[selectedIndex![0]]
                }
             //   self?.tbl_show_items.isHidden = true
                print("vehicle type id:\(String(describing: self?.vehicle_typeid))")
            default:
                break
            }
            // self?.tbl_show_items.reloadData()
        }
        
        selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in
            print("vehicle name arr:\(String(describing: self?.vehicleDataName)), vehicle id arr:\(String(describing: self?.vehicle_type_id)) ")
            return self?.vehicleDataName.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        
        selectionMenu.onDismiss = { [weak self] selectedItems in
            print("dissmissed view")
            self?.selectedDataArray = selectedItems
            /// do some stuff when menu is dismssed
          //  self?.tbl_show_items.isHidden = true
        }
        /// set cell selection style - Default is 'tickmark'
        /// (Optional)
        selectionMenu.cellSelectionStyle = self.cellSelectionStyle
        /// Customization
        /// set navigationBar title, attributes and colors
        selectionMenu.setNavigationBar(title: "Select Vehicle Type", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white], barTintColor: #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), tintColor: UIColor.white)
        
        // show menu as (push or present)
        selectionMenu.show(style: style, from: self)
    }
    
    // MARK:- Formsheet & SearchBar
    
    func showAsFormsheet() {
        
        /// You can also set selection style - while creating menu instance
        
        let menu = RSSelectionMenu(selectionStyle: .single, dataSource: self.vehicleDataName) { (cell, name, indexPath) in
            
            cell.textLabel?.text = name
            
            // cell customization
            // set tint color
            cell.tintColor = UIColor.orange
        }
        
        // provide - selected items and selection delegate
        
        menu.setSelectedItems(items: simpleSelectedArray) { [weak self] (name, index, selected, selectedItems) in
            self?.selectedDataArray = selectedItems
            
            /// do some stuff...
            
            self?.txt_vehicle_type.text = name
            self?.tbl_show_items.reloadData()
        }
        
        // show with search bar
        
        menu.showSearchBar { [weak self] (searchText) -> ([String]) in
            
            // Filter your result from data source based on any condition
            // Here data is filtered by name that starts with the search text
            
            return self?.vehicleDataName.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        
        // cell selection style
        menu.cellSelectionStyle = self.cellSelectionStyle
        
        // show empty data label - if needed
        // Note: Default text is 'No data found'
        
        menu.showEmptyDataLabel()
        
        // show as formsheet
        menu.show(style: .present, from: self)
    }
    

    // MARK:- Popover with cell style - subTitle & get onDismiss Handler
    
  
    // MARK:- Extra Header Row
    
   
    
}


