//
//  VehicleBasedDistanceVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class VehicleBasedDistanceVC: UIViewController {
    var viewModel: APIDemoViewModelProtocol?
    var vehiclesDistanceList : [VehiclesDistanceModel] = []
    var filteredList : [VehiclesDistanceModel] = []
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var tableView: UITableView!
    var today_date = ""
    var seven_days_ago = ""
    let cellSpacingHeight: CGFloat = 0.0
    var apiType = ""
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        if apiType == "today"
        {
            lbl_title.text = "Today"
        }
        else
        {
            lbl_title.text = "This Week"
        }
          // self.navigationController?.isNavigationBarHidden = true
          //header_view.dropShadow(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("api type:\(apiType)")
        today_date = Date.getTodayDate()
        seven_days_ago = Date.getPastDate(days: -6, from: "DistanceReport")
        self.tableView.startShimmerAnimation(withIdentifier: "DistanceRptCell", numberOfRows: 1, numberOfSections: 15)
        showVehiclesDistanceList()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_btn_pressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func showVehiclesDistanceList()
    {
        self.viewModel = APIDemoViewModel()
        self.bindUI()
        if apiType == "today"
        {
            self.viewModel!.getVehiclesDistance(startDate: today_date, endDate: today_date)
        }
        else
        {
            self.viewModel!.getVehiclesDistance(startDate: seven_days_ago, endDate: today_date)
        }
    }
   
    private func bindUI() {
           self.viewModel?.isLoaderHidden.bind({ [weak self] in
               self?.shouldHideLoader(isHidden: $0)
           })
           self.viewModel?.alertMessage.bind({ [weak self] in
               self?.showAlertWith(message: $0)
           })
           self.viewModel?.vehicleDistance.bind({ [weak self] in
               if let alerts = $0 {
                   print("vehicle list:\(alerts)")
                   self?.vehiclesDistanceList.removeAll()
                   self?.filteredList.removeAll()
                   
                   for vehicleDetails in alerts.data
                   {
                       let vehicleDetail = VehiclesDistanceModel(device_id: vehicleDetails.device_id ?? "", registration_number: vehicleDetails.registration_number ?? "", total_distance: vehicleDetails.total_distance ?? "")
                       self?.vehiclesDistanceList.append(vehicleDetail)
                   }
               }
               
               DispatchQueue.main.async {
                   self?.filteredList = self?.vehiclesDistanceList ?? []
                   self?.stopShimmerAnimation()
                   self?.searchBar.delegate = self
                   self?.tableView.delegate = self
                   self?.tableView.dataSource = self
                   self?.tableView.tableFooterView = UIView()
                   self?.tableView.reloadData()
               }
               
           })}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VehicleBasedDistanceVC:UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // There is just one row in every section
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          var count : Int = 0
          if(searchActive) {
              count = self.filteredList.count
          } else {
              count = self.vehiclesDistanceList.count
          }
          return count
      }
    // Make the background color show through
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceRptCell") as! DistanceRptCell
        if searchActive{
            cell.lbl_reg_no.text = self.filteredList [indexPath.row].registration_number
            cell.lbl_distance.text = self.filteredList[indexPath.row].total_distance
        }
        else{
            cell.lbl_reg_no.text = self.vehiclesDistanceList[indexPath.row].registration_number
            cell.lbl_distance.text = self.vehiclesDistanceList[indexPath.row].total_distance
        }
        
        cell.selectionStyle = .none
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        cell.layer.cornerRadius = 6
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
     func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
           searchActive = true;
           self.searchBar.showsCancelButton = true
       }
       
       func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
           searchActive = false;
           self.searchBar.endEditing(true)
       }
       
       func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false;
           self.searchBar.endEditing(true)
           self.searchBar.text = ""
           self.searchBar.showsCancelButton = false
           self.tableView.reloadData()
       }
       
       func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchActive = false;
           self.searchBar.endEditing(true)
       }
       
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         if searchText.isEmpty {
             searchActive = false;
         }
         else
         {
           searchActive = true;
           filteredList = vehiclesDistanceList.filter { $0.registration_number.localizedCaseInsensitiveContains(searchText) }
           print("search data:\(filteredList)")
         }
           self.tableView.reloadData()
       }
    
}
