//
//  ShowDashboardVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 05/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import Charts

class ShowDashboardVC: UIViewController {
  
    var item_name = [String]()
    var item_count = [0, 0.0, 0.0, 0.0]
    private let numEntry = 20
    var count_status = [0, 0, 0, 0, 0]
    var language = ""
    var kmText = ""
    @IBOutlet weak var lbl_critical_alerts: UILabel!
    @IBOutlet weak var lbl_today_distance: UILabel!
    @IBOutlet weak var lbl_week_distance: UILabel!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var line_graph_view: LineChartView!
    @IBOutlet weak var bar_chart_view: BarChartView!
    weak var axisFormatDelegate: IAxisValueFormatter?
    var vehicle_status = ["Moving","Idle","Stopped","No Data","All"]
    
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var content_view: UIView!
    var distanceArr: [Double] = []
    var dateArr : [String] = []
    
    @IBOutlet weak var critical_alert_view: UIView!
    @IBOutlet weak var today_view: UIView!
    @IBOutlet weak var this_week_view: UIView!
    
    let tapRecForCriticalAlertView = UITapGestureRecognizer()
    let tapRecForTodayView = UITapGestureRecognizer()
    let tapRecForThisWeekView = UITapGestureRecognizer()
    
    @IBOutlet var header_labels: [UILabel]!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        tapRecForCriticalAlertView.addTarget(self, action: Selector(("tappedCriticalAlertView")))
        tapRecForTodayView.addTarget(self, action: Selector(("tappedTodayView")))
        tapRecForThisWeekView.addTarget(self, action: Selector(("tappedThisWeekView")))
         
        critical_alert_view.addGestureRecognizer(tapRecForCriticalAlertView)
        critical_alert_view.isUserInteractionEnabled = true
        today_view.addGestureRecognizer(tapRecForTodayView)
        today_view.isUserInteractionEnabled = true
        this_week_view.addGestureRecognizer(tapRecForThisWeekView)
        this_week_view.isUserInteractionEnabled = true
        
      
      
        // Do any additional setup after loading the view.
    }
    
    @objc func tappedCriticalAlertView() {
       print("tapped critical alert")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CriticalAlertsVC") as! CriticalAlertsVC
        self.present(vc, animated: true, completion: nil)
    }

    @objc func tappedTodayView() {
      print("tapped today view")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DistanceReportVC") as! VehicleBasedDistanceVC
        vc.apiType = "today"
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func tappedThisWeekView() {
        print("tapped this week")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DistanceReportVC") as! VehicleBasedDistanceVC
        vc.apiType = "this week"
        self.present(vc, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        header_view.layer.cornerRadius = header_view.frame.size.height / 18
        header_view.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        line_graph_view.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        bar_chart_view.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingData), name: Notification.Name("TrackingData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingAlertData), name: Notification.Name("TrackingDataForAlertCount"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingChartData), name: Notification.Name("TrackingChartData"), object: nil)
        language = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : language)
        item_name.removeAll()
        item_name.append(bd.localizedString(forKey: "CRITICAL_ALERTS", value: nil, table: nil))
        item_name.append(bd.localizedString(forKey: "TODAY_DISTANCE", value: nil, table: nil))
        item_name.append(bd.localizedString(forKey: "THIS_WEEK", value: nil, table: nil))
       // item_name.append(bd.localizedString(forKey: "CRITICAL_ALERTS", value: nil, table: nil))
        kmText = bd.localizedString(forKey: "KM_TEXT", value: nil, table: nil)
        for i in 0..<item_name.count {
            print("item name:\(item_name)")
            header_labels[i].text = item_name[i]
        }
    }
    
 
    @objc func GetTrackingChartData(notification: Notification) {
        print("chart data recieved in dashboard page")
        self.distanceArr.removeAll()
        self.dateArr.removeAll()
        if let arr = notification.userInfo?["avg_dis_data"] as? [Dictionary<String, Any>] {
            self.distanceArr = arr.map({$0 ["distance"] as? Double ?? 0.0})
            self.dateArr = arr.map({$0 ["date"] as? String ?? ""})
            self.updateLineGraph()
        }
    }
    
    
    @objc func GetTrackingData(notification: Notification){
        print("tracking data recieved in dashboard page")
        let c_data = notification.userInfo
        self.count_status.removeAll()
    
        let moving : Int = c_data!["moving_count"] as? Int ?? 0
        let idle : Int =  c_data!["idling_count"] as? Int ?? 0
        let stop : Int = c_data!["stopped_count"] as? Int ?? 0
        let unreach : Int = c_data!["unreachable_count"] as? Int ?? 0
        
        self.count_status.append(moving)
        self.count_status.append(idle)
        self.count_status.append(stop)
        self.count_status.append(unreach)
        
        let all_count =  moving + idle + stop + unreach
        self.count_status.append(all_count)

        self.lbl_critical_alerts.text = String(c_data!["alert"] as? Int ?? 0)
        self.setChart(dataPoints: self.vehicle_status, values: self.count_status)
    }
    
    @objc func GetTrackingAlertData(notification: Notification) {
        
        let c_data = notification.userInfo
        var today : Double = 0
        var week : Double = 0
        
        let t_day = c_data!["total_today_distance"] as? Double ?? 0.0
        today = today + t_day
        let w_day = c_data!["total_week_distance"] as? Double ?? 0.0
        week = week + w_day
        
        self.item_count[0] = Double(c_data!["alert_count"] as? Int ?? 0)
        self.item_count[1] = today.roundedtoPlaces(places: 2)
        self.item_count[2] = week.roundedtoPlaces(places: 2)
        
        print("alerts data:\(self.item_count)")
        
        self.lbl_today_distance.text = String(self.item_count[1]) + kmText
        self.lbl_week_distance.text = String(self.item_count[2]) + kmText
    }
       
    
   func updateLineGraph() {
        var lineChartEntry  = [ChartDataEntry]() //this is the Array that will eventually be displayed on the graph.
        //here is the for loop
         print("distance arr:\(distanceArr)")
        for i in 0..<distanceArr.count {
            let value = ChartDataEntry(x: Double(i), y: Double(distanceArr[i])) // here we set the X and Y status in a data chart entry
            lineChartEntry.append(value) // here we add it to the data set
        }
    
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Last 7 Days Vehicles Distance (in km.)") //Here we convert lineChartEntry to a LineChartDataSet
        line1.colors = [NSUIColor.blue] //Sets the colour to blue
        let gradientColors = [UIColor.cyan.cgColor, UIColor.clear.cgColor] as CFArray // Colors of the gradient
        let colorLocations:[CGFloat] = [1.0, 0.0] // Positioning of the gradient
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations) // Gradient Object
        line1.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0) // Set the Gradient
        line1.drawFilledEnabled = true
    
        let data = LineChartData() //This is the object that will be added to the chart

        data.addDataSet(line1) //Adds the line to the dataSet
        
        line_graph_view.data = data //finally - it adds the chart data to the chart and causes an update
   
        line_graph_view.animate(xAxisDuration: 0.0, yAxisDuration: 2.0, easingOption: .easeInCubic)
        line_graph_view.xAxis.drawGridLinesEnabled = false
        line_graph_view.xAxis.granularity = 1
        line_graph_view.xAxis.granularityEnabled = true
        line_graph_view.xAxis.valueFormatter = IndexAxisValueFormatter(values: dateArr)
        line_graph_view.xAxis.labelPosition = .bottom
        //line_graph_view.xAxis.drawLabelsEnabled = false
        line_graph_view.rightAxis.drawAxisLineEnabled = false
        line_graph_view.rightAxis.drawLabelsEnabled = false
        line_graph_view.rightAxis.drawGridLinesEnabled = false
        line_graph_view.leftAxis.drawGridLinesEnabled = false
        line_graph_view.chartDescription?.text = "" // Here we set the description for the graph

    }
    
     func setChart(dataPoints: [String], values: [Int]) {
        
        bar_chart_view.noDataText = "You need to provide data for the chart."
        // Prevent from setting an empty data set to the chart (crashes)
        guard dataPoints.count > 0 else { return }
        var dataEntries = [BarChartDataEntry]()
        for i in 0..<dataPoints.count{
            // print(forX[i])
            // let dataEntry = BarChartDataEntry(x: (forX[i] as NSString).doubleValue, y: Double(unitsSold[i]))
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]) , data: dataPoints as AnyObject?)
            print(dataEntry)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Vehicles Status")
        chartDataSet.drawValuesEnabled = true
        chartDataSet.colors = [Moving_Color,Idle_Color,Stopped_Color,Unreach_Color,Global_Blue_Color]
        chartDataSet.highlightColor = UIColor.orange.withAlphaComponent(0.3)
        chartDataSet.highlightAlpha = 1
        let chartData = BarChartData(dataSet: chartDataSet)
        bar_chart_view.data = chartData
    
        //Animation bars
        bar_chart_view.animate(xAxisDuration: 0.0, yAxisDuration: 2.0, easingOption: .easeInCubic)
        
        // X axis configurations
        bar_chart_view.xAxis.granularity = 1
        bar_chart_view.xAxis.granularityEnabled = true
        bar_chart_view.xAxis.valueFormatter = IndexAxisValueFormatter(values: vehicle_status)
        bar_chart_view.xAxis.drawAxisLineEnabled = true
        bar_chart_view.xAxis.drawGridLinesEnabled = false
        bar_chart_view.xAxis.labelFont = UIFont.systemFont(ofSize: 9.0)
        bar_chart_view.xAxis.labelTextColor = UIColor.black
        bar_chart_view.xAxis.labelPosition = .bottom
        
        // Right axis configurations
        bar_chart_view.rightAxis.drawAxisLineEnabled = false
        bar_chart_view.rightAxis.drawGridLinesEnabled = false
        bar_chart_view.rightAxis.drawLabelsEnabled = false
        
        // Other configurations
        bar_chart_view.highlightPerDragEnabled = false
        bar_chart_view.chartDescription?.text = ""
        bar_chart_view.legend.enabled = false
        bar_chart_view.pinchZoomEnabled = false
        bar_chart_view.doubleTapToZoomEnabled = false
        bar_chart_view.scaleYEnabled = false
        
        bar_chart_view.drawMarkers = true
        
        let l = bar_chart_view.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4
        
        // On tapped bar marker before adding BalloonMarker.swift
        //let marker =  BalloonMarker(color: UIColor.orange, font: UIFont.boldSystemFont(ofSize: 13), textColor: UIColor.white, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 15.0, right: 7.0))
        //marker.chartView = bar_chart_view
        //bar_chart_view.marker = marker
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scroll_view.contentSize = CGSize(width: content_view.frame.width, height: bar_chart_view.frame.origin.y + bar_chart_view.frame.size.height + 10)
    }
}

extension ShowDashboardVC : IAxisValueFormatter {
func stringForValue(_ value: Double, axis: AxisBase?) -> String {
               return vehicle_status[Int(value)]
}
   
}
