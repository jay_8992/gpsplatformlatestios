//
//  ContainerViewController.swift
//  BudgetEntryManagmentSystem
//
//  Created by BIS MAC 1 on 25/10/18.
//  Copyright © 2018 BIS MAC 1. All rights reserved.
//

import UIKit

open class ContainerViewController: UIViewController {
    //Manipulating container views
    fileprivate weak var viewController : UIViewController!
    //Keeping track of containerViews
    fileprivate var containerViewObjects = Dictionary<String,UIViewController>()
    /** Pass in a tuple of required TimeInterval with UIViewAnimationOptions */
    var animationDurationWithOptions:(TimeInterval, UIView.AnimationOptions) = (0,[])
    
    /** Specifies which ever container view is on the front */
    open var currentViewController : UIViewController{
        get {
            return self.viewController
            
        }
    }
    /** Returns all the embedded controllers **/
    open var viewControllers: [UIViewController] {
        return Array(containerViewObjects.values)
    }
    
    fileprivate var segueIdentifier : String!
    
    /*Identifier For First Container SubView*/
    @IBInspectable internal var firstLinkedSubView : String!
    
    internal var locationID : String!
    
    internal var proposedBgtType : String!

    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
    }
    open override func viewDidAppear(_ animated: Bool) {
//        if let identifier = firstLinkedSubView{
//            segueIdentifierReceivedFromParent(identifier)
//        }
    }
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func segueIdentifierReceivedFromParent(_ identifier: String,_ proposedBudget: String){
        self.segueIdentifier = identifier
        self.proposedBgtType = proposedBudget
        self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
    }
    
    override open func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueIdentifier{
            //Remove Container View
            if viewController != nil{
                viewController.view.removeFromSuperview()
                viewController = nil
            }
            //Add to dictionary if isn't already there
            
//            if let embeddedVC = segue.destination as? AdvanceBudgetReportViewController, segue.identifier == "AdvanceBudget" {
//                print("advance budget segue")
//                embeddedVC.dataFromContainer(locationID: self.locationID, proposedBgtType: self.proposedBgtType)
//               // embeddedVC.proposedBudgetType = self.proposedBgtType
//            }

            if ((self.containerViewObjects[self.segueIdentifier] == nil)){
                viewController = segue.destination
                self.containerViewObjects[self.segueIdentifier] = viewController
                
            }else{
                for (key, value) in self.containerViewObjects{
                    if key == self.segueIdentifier{
                        viewController = value
                    }
                }
            }
            
         /*   if let embeddedVC = segue.destination as? AdvanceBudgetReportViewController, segue.identifier == "AdvanceBudget" {
                print("advance budget segue")
                embeddedVC.dataFromContainer(locationID: self.locationID, proposedBgtType: self.proposedBgtType)
                // embeddedVC.proposedBudgetType = self.proposedBgtType
            }
          */
            
            UIView.transition(with: self.view, duration: animationDurationWithOptions.0, options: animationDurationWithOptions.1, animations: {
                self.addChild(self.viewController)
                self.viewController.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width,height: self.view.frame.height)
                self.view.addSubview(self.viewController.view)
            }, completion: { (complete) in
                self.viewController.didMove(toParent: self)
            })
        }
    }
}

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


