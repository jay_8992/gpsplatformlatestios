//
//  AboutVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var txt_about: UITextView!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenu()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        let data = About_Data.data(using: String.Encoding.unicode)!
        let attrStr = try? NSAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        txt_about.attributedText = attrStr
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func SideMenu(){
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
    

}
