//
//  VehicleAssignment.swift
//  GeoTrack
//
//  Created by Georadius on 21/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import EzPopup

class VehicleAssignment: UIViewController,DataDelegate,PopUpDataDelegate {
    
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var lbl_groupVehicles: UILabel!
    @IBOutlet weak var lbl_userVehicles: UILabel!
    @IBOutlet weak var txt_groupVehicles: UITextField!
    @IBOutlet weak var txt_userVehicles: UITextField!
    @IBOutlet weak var btn_assign: UIButton!
    @IBOutlet weak var btn_unassign: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    var selectedDeviceId = [String]()
    var searchUrl = ""
    var userID = ""
    let alert_view = AlertView.instanceFromNib()

    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguageInControls()
        //setSearchUrl()
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_groupVehicles)
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_userVehicles)
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.btn_back.addTarget(self, action: #selector(pressed_back_btn), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    func setSearchUrl(inverseStr:String)
    {
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        searchUrl = domain_name + Vehicle_Assign_List + "user_name=" + user_name + "&hash_key=" + hash_key + "&data_format=1" + "&inverse_user=" + inverseStr + "&user_id=" + userID
       
        print("searchURL:\(searchUrl)")
    }
    
    func setLanguageInControls()
    {
        titleLbl.text = LanguageHelperClass().vehicleAssignTxt
        lbl_groupVehicles.text = LanguageHelperClass().grpVehicleTxt
        txt_groupVehicles.placeholder = LanguageHelperClass().selectVehicleTxt
        txt_userVehicles.placeholder = LanguageHelperClass().selectVehicleTxt
        lbl_userVehicles.text = LanguageHelperClass().userVehicleTxt
        btn_assign.setTitle(LanguageHelperClass().assignTxt, for: .normal)
        btn_unassign.setTitle(LanguageHelperClass().unAssignTxt, for: .normal)
    }
    
    
    @IBAction func textFieldSelected(_ sender: UITextField) {
        switch sender.tag {
        case 0:
            sender.resignFirstResponder()
            UserDefaults.standard.set(sender.tag, forKey: "selectedTxtTag")
            setSearchUrl(inverseStr: "1")
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
            contentVC.urlString = self.searchUrl
            contentVC.delegate = self
            // Init popup view controller with content is your content view controller
            let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        case 1:
            print("case 1")
            sender.resignFirstResponder()
            UserDefaults.standard.set(sender.tag, forKey: "selectedTxtTag")
            setSearchUrl(inverseStr: "0")
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
            contentVC.urlString = self.searchUrl
            contentVC.delegate = self
            // Init popup view controller with content is your content view controller
            let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
            
        case 2:
            print("case 2")
            sender.resignFirstResponder()
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            contentVC.titleArr = ["Days","Km"]
            contentVC.popUpType = "ReminderType"
            // Init popup view controller with content is your content view controller
            let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
            let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize) , popupWidth: contentVC.view.frame.size.width, popupHeight: 150.0)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        case 3:
            print("case 3")
            sender.resignFirstResponder()
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
            contentVC.titleArr = ["Email","SMS","Both"]
            contentVC.popUpType = "NotificationType"
            // Init popup view controller wth content is your content view controller
            let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
            let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize) , popupWidth: contentVC.view.frame.size.width, popupHeight: 200.0)
            popupVC.cornerRadius = 20
            // show it by call present(_ , animated:) method from a current UIViewController
            present(popupVC, animated: true)
        default:
            print("default")
        }
    }
    
    @IBAction func assignVehicle(_ sender: UIButton) {
        assignUnassignVehicleToUser(status: "1")
    }
    
    @IBAction func unassignVehicle(_ sender: Any) {
        assignUnassignVehicleToUser(status: "0")
    }
    
    func assignUnassignVehicleToUser(status:String)
    {
        alert_view.frame.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.addSubview(alert_view)
        
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        let device_ids = selectedDeviceId.joined(separator: ",")
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        let urlString  =  domain_name + Vehicle_User_Assign + "&status=" + status + "&device_id=" + device_ids + "&user_id=" + userID + "&data_format=1" + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        print("Assign Url: \(urlString)")
        
        NetworkManager().CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            print("response data:\(data)")
            if isNetwork && data != nil{
                if data == "Updated Successfully" || data == "Vehicle Assigned" {
                    showToast(controller: self, message : data!, seconds: 2.0)
                    DispatchQueue.main.async {
                        self.txt_groupVehicles.text = ""
                        self.txt_userVehicles.text = ""
                    }
                }
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.alert_view.removeFromSuperview()
            
        })
        
    }
    
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
        print("device id:\(deviceId)")
        selectedDeviceId = deviceId
        if let selectedTFTag = UserDefaults.standard.value(forKey: "selectedTxtTag") as? Int
        {
            print("selected TFTag:\(selectedTFTag)")
            if self.selectedDeviceId.count > 0
            {
                if selectedTFTag == 0
                {
                self.txt_groupVehicles.text = "\(self.selectedDeviceId.count) selected"
                }
                else
                {
                self.txt_userVehicles.text = "\(self.selectedDeviceId.count) selected"
                }
            }
        }
    }
    
    
    func sendData(sender: PopUpVC, selectedType: String, popUpType: String) {
        //        if popUpType == "ReminderType"
        //        {
        //            self.reminderTypeTF.text = selectedType
        //        }
        //        else
        //        {
        //            self.notificationTF.text = selectedType
        //        }
    }
    
    @objc func pressed_back_btn(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
