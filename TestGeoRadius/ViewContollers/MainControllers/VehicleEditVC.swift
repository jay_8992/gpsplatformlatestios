//
//  VehicleEditVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleEditVC: UIViewController {
    
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var search_items: UISearchBar!
    @IBOutlet weak var tbl_show_items: UITableView!
    @IBOutlet weak var lbl_vehicleName: UILabel!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var txt_device_serial: UITextField!
    @IBOutlet weak var lbl_device_serial: UILabel!
    @IBOutlet weak var lbl_voiceNo: UILabel!
    @IBOutlet weak var txt_voice: UITextField!
    @IBOutlet weak var lbl_vehicle_type: UILabel!
    @IBOutlet weak var lbl_device_tag: UILabel!
    @IBOutlet weak var txt_device_tag: UITextField!
    @IBOutlet weak var txt_vehicle_type: UITextField!
    @IBOutlet weak var txt_device_type: UITextField!
    @IBOutlet weak var txt_registration: UITextField!
    @IBOutlet weak var lbl_regnNo: UILabel!
    
    @IBOutlet weak var lbl_driver_name: UILabel!
    @IBOutlet weak var txt_driver_name: UITextField!
    @IBOutlet weak var lbl_driver_no: UILabel!
    @IBOutlet weak var txt_driver_no: UITextField!
    
    var searchActive : Bool = false
    @IBOutlet weak var btn_update: UIButton!
    var data = [String]()
    
    var filterd_device_tag = [String]()
    // self.device_tag.append(GetDeviceTag(Vehicals: v_name))
    
    var filterd_device_derial = [String]()
    //self.device_serial.append(GetDeviceSerial(Vehicals: v_name))
    var filterd_voice_no = [String]()
    
    //self.voice_no.append(GetVoiceNumber(Vehicals: v_name))
    
    var filterd_vehicle_type = [String]()
    // self.vehicle_type.append(GetVehicleTypeName(Vehicals: v_name))
    
    var filterd_vehicle_type_id = [String]()
    //self.vehicle_type_id.append(GetVehicleTypeID(Vehicals: v_name))
    // self.device_ids.append(GetDeviceID(Vehicals: v_name))
    var filterd_device_ids = [String]()
    
    var device_serial = [String]()
    var voice_no = [String]()
    var device_tag = [String]()
    var filterdData : [String]!
    var select_vehicle : String!
    var vehicle_type = [String]()
    var device_ids = [String]()
    var vehicle_type_id = [String]()
    var vehicle_typeid : String!
    var device_id : String!
    var isSelect_Vehicle = true
    var cellSpacingHeight:CGFloat = 15.0
    var deviceSerialNo:String!
    var selectedVehicleType:String!
    var selectedDeviceTag:String!
    var selectedVoiceNo:String!
    
    var data1 : [VehicleTypeModel] = []
    var filteredData1 : [VehicleTypeModel] = []
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    var driver_name:String!
    var driver_no:String!
    var driver_id:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguageInControl()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        SetTexttFields()
        // Do any additional setup after loading the view.
        txt_voice.isEnabled = false
        //txt_device_tag.isEnabled = false
        // txt_vehicle_type.isEnabled = false
        //txt_registration.isEnabled = false
        // txt_vehicle_type.isEnabled = false
        txt_select_vehicle.isEnabled = false
        txt_device_serial.isEnabled = false
        
        self.txt_select_vehicle.text = self.select_vehicle
        self.txt_registration.text =  self.select_vehicle
        self.txt_device_serial.text = self.deviceSerialNo
        self.txt_vehicle_type.text = self.selectedVehicleType
        self.txt_device_tag.text = self.selectedDeviceTag
        self.txt_voice.text = self.selectedVoiceNo
        self.txt_driver_name.text = self.driver_name
        self.txt_driver_no.text = self.driver_no
        
        let searchBarStyle = search_items.value(forKey: "searchField") as? UITextField
        searchBarStyle?.clearButtonMode = .never
        
        search_items.placeholder = "Search"
        self.tbl_show_items.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        txt_select_vehicle.addTarget(self, action: #selector(tap_select_vehicle), for: .editingDidBegin)
        txt_vehicle_type.addTarget(self, action: #selector(tap_vehicle_type), for: .editingDidBegin)
        btn_update.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
    }
    

    func setLanguageInControl()
    {
        titleLbl.text = LanguageHelperClass().vehicleEditTxt
        lbl_vehicleName.text = LanguageHelperClass().vehicleTxt
        lbl_regnNo.text = LanguageHelperClass().regNoTxt
        lbl_device_serial.text = LanguageHelperClass().deviceSerialTxt
        lbl_vehicle_type.text = LanguageHelperClass().vehicleTypeTxt
        lbl_device_tag.text = LanguageHelperClass().deviceTagTxt
        lbl_voiceNo.text = LanguageHelperClass().voiceNoTxt
    }
    
    
    @objc func tap_select_vehicle(){
        txt_select_vehicle.resignFirstResponder()
    }
    
    @objc func tap_vehicle_type(){
        txt_vehicle_type.resignFirstResponder()
    }
    
    @IBAction func back_pressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func submit_pressed(){
        
        if txt_registration.text!.count < 1 || txt_device_serial.text!.count < 1 || txt_voice.text!.count < 1{
            showToast(controller: self, message : "Please check all fields", seconds: 2.0)
            return
        }
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            return
        }
      
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let driver_name = self.txt_driver_name.text ?? ""
        let driver_no = self.txt_driver_no.text ?? ""
        
        let urlString = Update_Vehicle + txt_device_serial.text! + "&device_id=" + self.device_id! + "&vehicle_type=" + self.vehicle_typeid! + "&registration_no=" + txt_registration.text! + "&tag=" + txt_device_tag.text! + "&user_name=" + user_name + "&hash_key=" + hash_key
        let driverDetails = "&driver_name=" + driver_name + "&driver_no=" + driver_no + "&driver_id=" + self.driver_id
        let finalUrlString = urlString + driverDetails
        
        print("url string:\(finalUrlString)")
        
        let encodedString = finalUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        NetworkManager().CallUpdateDataOnServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                showToast(controller: self, message : data!, seconds: 2.0)
            }else{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
            }
            if r_error != nil{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
            }
        })
    }
    
    func SetTexttFields(){
        SetTextFieldRightSide(imageName: "downarrow", txt_field: txt_vehicle_type)
    }
    
    @IBAction func select_vhicle_tap(_ sender: Any) {
        CallVehicleFromServer()
        tbl_show_items.isHidden = false
        isSelect_Vehicle = true
        self.txt_vehicle_type.isEnabled = true
        
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = 20
        })
    }
    
    @IBAction func select_vehicle_type(_ sender: Any) {
        tbl_show_items.isHidden = false
        isSelect_Vehicle = false
        CallVehicleType()
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = 20
        })
    }
    
    func CallVehicleType(){
        data.removeAll()
        self.data1.removeAll()
        self.filteredData1.removeAll()
        self.vehicle_type_id.removeAll()
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let vehicle_type_url = domain_name + Vehicle_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    print("data :\(c_data)")
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.data.append(v_val["vehicle_type_name"] as! String)
                        self.vehicle_type_id.append(GetVehicleTypeId(Vehicals: v_val))
                        
                        let vehicle_type_name = v_val["vehicle_type_name"] as! String
                        let vehicle_type_id = GetVehicleTypeId(Vehicals: v_val)
                        
                        let newData = VehicleTypeModel(vehicle_type_name: vehicle_type_name, vehicle_type_id: vehicle_type_id)
                        self.data1.append(newData)
                    }
                    
                    self.filteredData1 = self.data1
                    self.filterd_vehicle_type_id = self.vehicle_type_id
                    self.tbl_show_items.delegate = self
                    self.search_items.delegate = self
                    self.tbl_show_items.dataSource = self
                    self.tbl_show_items.reloadData()
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
            
        })
    }
    
    func CallVehicleFromServer(){
        data.removeAll()
        device_tag.removeAll()
        self.device_serial.removeAll()
        self.voice_no.removeAll()
        self.vehicle_type.removeAll()
        self.device_ids.removeAll()
        self.vehicle_type_id.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["return_json"] as! Array<Any>
                    
                    for name in val{
                        let v_name = name as! Dictionary<String, Any>
                        self.data.append(GetRegistrationNumber(Vehicals: v_name))
                        self.device_tag.append(GetDeviceTag(Vehicals: v_name))
                        self.device_serial.append(GetDeviceSerial(Vehicals: v_name))
                        self.voice_no.append(GetVoiceNumber(Vehicals: v_name))
                        self.vehicle_type.append(GetVehicleTypeName(Vehicals: v_name))
                        self.vehicle_type_id.append(GetVehicleTypeID(Vehicals: v_name))
                        self.device_ids.append(GetDeviceID(Vehicals: v_name))
                    }
                    
                    if self.isSelect_Vehicle{
                        self.filterdData = self.data
                        self.filterd_voice_no = self.voice_no
                        self.filterd_device_tag = self.device_tag
                        self.filterd_device_derial = self.device_serial
                        self.filterd_vehicle_type = self.vehicle_type
                        self.filterd_vehicle_type_id = self.vehicle_type_id
                        self.filterd_device_ids = self.device_ids
                        
                        self.tbl_show_items.delegate = self
                        self.tbl_show_items.dataSource = self
                        self.tbl_show_items.reloadData()
                        self.search_items.delegate = self
                    }
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                print("ERROR FOUND")
            }
        })
    }
    
}

extension VehicleEditVC : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return tbl_sort_item.frame.size.height / 8
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filteredData1.count
        } else {
            count = data1.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 10.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl_show_items.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        cell.layer.cornerRadius = 10.0
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.img_check.isHidden = true
        cell.img_uncheck.isHidden = true
        if searchActive{
            cell.lbl_item_name.text = filteredData1[indexPath.section].vehicle_type_name
        }
        else{
            cell.lbl_item_name.text = data1[indexPath.section].vehicle_type_name
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.8, animations: {
            self.tbl_show_items.frame.origin.y = self.tbl_show_items.frame.size.height
               if self.isSelect_Vehicle{
                self.txt_select_vehicle.text = self.filterdData[indexPath.section]
                self.txt_device_serial.text = self.filterd_device_derial[indexPath.section]
                self.txt_voice.text = self.filterd_voice_no[indexPath.section]
                self.txt_device_tag.text = self.filterd_device_tag[indexPath.section]
                self.txt_registration.text = self.filterdData[indexPath.section]
                self.txt_vehicle_type.text = self.filterd_vehicle_type[indexPath.section]
                self.vehicle_typeid = self.filterd_vehicle_type_id[indexPath.section]
            } else{
                if self.searchActive{
                    self.txt_vehicle_type.text = self.filteredData1[indexPath.section].vehicle_type_name
                    self.vehicle_typeid = self.filteredData1[indexPath.section].vehicle_type_id
                }
                else
                {
                    self.txt_vehicle_type.text = self.data1[indexPath.section].vehicle_type_name
                    self.vehicle_typeid = self.data1[indexPath.section].vehicle_type_id
                }
            }
        })
        self.search_items.endEditing(true)
    }
}

extension VehicleEditVC : UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_items.endEditing(true)
        self.tbl_show_items.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_items.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_items.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData1 = data1.filter { $0.vehicle_type_name.localizedCaseInsensitiveContains(searchText) }
        if(filteredData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        
        self.tbl_show_items.reloadData()
    }
}

