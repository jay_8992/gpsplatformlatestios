//
//  MaintenanceRecordVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class MaintenanceRecordVC: UIViewController {
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var searchVehicleTF: UITextField!
    @IBOutlet weak var vehicleTF: UITextField!
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    var isSelectionViewOpen = false
     let datePicker = UIDatePicker()
    let dropDownImg = UIImage(named: "downarrow")
    var selectedTextField: UITextField = UITextField()
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet var labels: Array<UILabel>!
    @IBOutlet weak var leftBarBtn: UIButton!
    @IBOutlet weak var btn_search: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenu()
        setLanguageInControls()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.selectionView.isHidden = true
        
        searchVehicleTF.withImage(direction: .Right, image: dropDownImg!, colorSeparator: .lightGray, colorBorder: .lightGray)
        vehicleTF.withImage(direction: .Right, image: dropDownImg!, colorSeparator: .lightGray, colorBorder: .lightGray)
        fromDateTF.withImage(direction: .Right, image: dropDownImg!, colorSeparator: .lightGray, colorBorder: .lightGray)
        toDateTF.withImage(direction: .Right, image: dropDownImg!, colorSeparator: .lightGray, colorBorder: .lightGray)
        

        fromDateTF.tintColor = UIColor.clear
        toDateTF.tintColor = UIColor.clear
        fromDateTF.delegate = self
        toDateTF.delegate = self
        
        showDatePicker()
        // Do any additional setup after loading the view.
    }
    
    func setLanguageInControls()
       {
           print("labels count :\(labels.count)")
           if labels.count == 3
          {
           for index in 0...2
           {
               switch index {
               case 0:
                   labels[index].text = LanguageHelperClass().geofenceNameTxt
               case 1:
                   labels[index].text = LanguageHelperClass().geofenceCheckTxt
               case 2:
                   labels[index].text = LanguageHelperClass().vehicleTxt
              
               default:
                   print("done")
               }
            }
           }
           titleLbl.text = LanguageHelperClass().createGeofenceTxt
           fromDateTF.placeholder = LanguageHelperClass().fromDateTimeTxt
           toDateTF.placeholder = LanguageHelperClass().toDateTimeTxt
           btn_search.setTitle(LanguageHelperClass().searchTxt, for: .normal)
       }
       
    
    func showDatePicker(){
           //Formate Date
           datePicker.datePickerMode = .date
           datePicker.minimumDate = Date()
           //ToolBar
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           //done button & cancel button
           let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(MaintenanceRecordVC.donedatePicker))
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(MaintenanceRecordVC.cancelDatePicker))
           toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
           // add toolbar to textField
            fromDateTF.inputAccessoryView = toolbar
           toDateTF.inputAccessoryView = toolbar
                  // add datepicker to textField
           fromDateTF.inputView = datePicker
           toDateTF.inputView = datePicker
       }
       
       @objc func donedatePicker(){
             //For date formate
             let formatter = DateFormatter()
             formatter.dateFormat = "MM-dd-yyyy"
             if self.selectedTextField == self.fromDateTF {
                 fromDateTF.text = formatter.string(from: datePicker.date)
             }
             else if self.selectedTextField == self.toDateTF
             {
                 toDateTF.text = formatter.string(from: datePicker.date)
             }
             //dismiss date picker dialog
             self.view.endEditing(true)
         }
         
         @objc func cancelDatePicker(){
             //cancel button dismiss datepicker dialog
             self.view.endEditing(true)
         }
       
    
    @IBAction func openSelectionVIew(_ sender: Any) {
        
        if isSelectionViewOpen
        {
            self.selectionView.isHidden = true
            isSelectionViewOpen = false
        }
            
        else
        {
            self.selectionView.isHidden = false
            isSelectionViewOpen = true
        }
    }
    
    @IBAction func searchMaintenanceRecord(_ sender: UIButton) {
    }
    
    @IBAction func openAddMaintenancePage(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMaintenanceVC") as! AddMaintenanceRecordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func SideMenu()
    {
           NetworkManager().StopAllThreads()
           leftBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
           revealViewController()?.rearViewRevealWidth = 250
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}


extension MaintenanceRecordVC:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
           print("did begin editing")
           self.selectedTextField = textField
       }
}
