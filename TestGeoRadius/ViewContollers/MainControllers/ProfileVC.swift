//
//  ProfileVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/04/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var lbl_groupName: UILabel!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let alert_view = AlertView.instanceFromNib()
    var titleArr = ["Full Name","Phone","Email","Company Name","Company Address"]
    var descArr = [String]()
    let cellSpacingHeight:CGFloat = 15.0

    @IBOutlet weak var btn_menu: UIButton!
    @IBOutlet weak var view_header: UIView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tableView.frame
        tempImageView.contentMode = .scaleAspectFill
        self.tableView.backgroundView = tempImageView
        self.tableView.tableFooterView = UIView()
        SideMenu()
        CallDataFromServer()
        // Do any additional setup after loading the view.
    }

    func SideMenu(){
           btn_menu.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
           revealViewController()?.rearViewRevealWidth = 250
    }
    
    func CallDataFromServer(){
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        if descArr.count > 0
        {
            descArr = []
        }
        
      let urlString = domain_name + "/user_result.php?action=search_user_json&user_name=" + user_name + "&hash_key=" + hash_key + "&userid=" + user_id
        NetworkManager().CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil {
                for val in data! {
                    let val_data = val as! Dictionary<String, Any>
                    self.descArr.append(val_data["username"] as? String ?? "")
                    self.descArr.append(val_data["phone"] as? String ?? "")
                    self.descArr.append(val_data["company_email"] as? String ?? "")
                    self.descArr.append(val_data["company_name"] as? String ?? "")
                    
                    let companyAddress = val_data["company_address"] as? String ?? ""
                    let decodedData = Data(base64Encoded: companyAddress)!
                    let decodedString = String(data: decodedData, encoding: .utf8)!
            
                    self.descArr.append(decodedString)
                    
                    DispatchQueue.main.async {
                        self.lbl_groupName.text = self.descArr[0]
                        self.lbl_phone.text = self.descArr[1]
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.reloadData()
                    }
                }
                
            }else{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
            }
           
            self.alert_view.removeFromSuperview()
        })
    }


}

extension ProfileVC :UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
    }
       
       // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArr.count
       }
       
       // Set the spacing between sections
    
       // Make the background color show through
    
/*  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        let headerView = UIView.init(frame: CGRect.init(x: 5, y: 0, width: tableView.frame.width, height: 40))
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 0, width: headerView.frame.width-10, height: headerView.frame.height-5)
        label.text = "ACCOUNT INFORMATION"
        label.font = UIFont.init(name: "Montserrat-Bold", size: 18.0)
        headerView.addSubview(label)

        return headerView
    }
 */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          print("title arr:\(titleArr), desc arr:\(descArr)")
          let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileNewCell") as! ProfileNewCell
          cell.lbl_title.text = titleArr[indexPath.row]
          cell.lbl_desc.text = descArr[indexPath.row]
         // cell.layer.borderColor = UIColor.lightGray.cgColor
         // cell.layer.borderWidth = 0.5
         // cell.layer.cornerRadius = 10
          return cell
      }
      
      func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
      }
      
}
