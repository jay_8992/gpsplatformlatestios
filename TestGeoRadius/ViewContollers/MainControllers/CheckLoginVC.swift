//
//  CheckLoginVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 27/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class CheckLoginVC: UIViewController {
    
    var menuIdArr: [String] = []
    var progressView: ProgressView?
    var willNavigate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ReachabilityManager.shared.addListener(listener: self)
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let isNotification = UserDefaults.standard.value(forKey: "IsNotification") as! Bool
        NotificationBar.sharedConfig.duration = 5.0
      //  ConnectionManager.sharedInstance.addListener(listener: self)
        if isNotification{
            // print("sdllsdlkf \(isNotification)")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            if IsLoginKey(){
              // ConnectionManager.sharedInstance.addListener(listener: self)
               // ReachabilityManager.shared.addListener(listener: self)
                self.CallTrakingDataFromServer()
               /* DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
                    self.navigationController?.pushViewController(vc, animated: true)
                }
             */
            }
            else{
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLanguageVC") as! SelectLanguageVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //ConnectionManager.sharedInstance.removeListener(listener: self)
        ReachabilityManager.shared.removeListener(listener: self)
    }

    func CallTrakingDataFromServer(){
             
        // self.view.addSubview(alert_view)
        if IsLoginKey()
        {
            showActivityIndicator()
            self.progressView?.show()
            let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
            let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
            let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
            let user_type_id = UserDefaults.standard.value(forKey: USER_TYPE_ID) as? String ?? "0"
            let urlString = domain_name + Menu_Id + "&user_type_id=" + user_type_id + "&user_name=" + user_name + "&hash_key=" + hash_key
            
            print("menu id url:\(urlString)")
            
            NetworkManager().GetMenuReportsFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                print("menu response :\(data)")
                if isNetwork && data != nil {
                    if let c_data = data as? [Dictionary<String, Any>]
                    {
                        self.menuIdArr = c_data.map({$0 ["menu_id"] as! String})
                        UserDefaults.standard.set(self.menuIdArr, forKey: "MenuPermArr")
                        self.hideActivityIndicator()
                        if self.willNavigate
                        {
                            self.willNavigate = false
                            DispatchQueue.main.async {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
                    
                else{
                    if NetworkAvailability.isConnectedToNetwork() {
                        print("Internet connection available")
                    }
                    else{
                        //  showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
                    }
                    
                    //showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                    print("ERROR FOUND")
                    self.hideActivityIndicator()
                    DispatchQueue.main.async {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScreensControllerTabBar") as! ScreensControllerTabBar
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
                
            })
        }
    }
}
extension CheckLoginVC: NetworkStatusListener {
    
    func networkStatusDidChange(status: Reachability.Connection) {
        
        switch status {
        case .unavailable :
           // showToast(controller: self, message: "No Network Connection , Please Connect to Internet", seconds: 3.0)
            NotificationBar(over: self, text: "No Internet Connection", style: .error).show()
            debugPrint("ViewController: Network became unreachable")
        case .wifi:
            debugPrint("ViewController: Network reachable through WiFi")
           // NotificationBar(over: self, text: "Back Online", style: .success).show()
            CallTrakingDataFromServer()
        case .cellular:
            debugPrint("ViewController: Network reachable through Cellular Data")
            CallTrakingDataFromServer()
        case .none:
            debugPrint("ViewController: Network reachable through WAN")
        }
        
    }
    
    func showActivityIndicator() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.backgroundColor = UIColor.white
        activityIndicator.layer.cornerRadius = 6
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            activityIndicator.style = UIActivityIndicatorView.Style.large
        } else {
            // Fallback on earlier versions
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        }
        activityIndicator.startAnimating()
        //UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicator.tag = 100 // 100 for example
        // before adding it, you need to check if it is already has been added:
        for subview in view.subviews {
            if subview.tag == 100 {
                print("already added")
                return
            }}
        view.addSubview(activityIndicator)
    }

    func hideActivityIndicator() {
        let activityIndicator = view.viewWithTag(100) as? UIActivityIndicatorView
        activityIndicator?.stopAnimating()
        // I think you forgot to remove it?
        activityIndicator?.removeFromSuperview()
        //UIApplication.shared.endIgnoringInteractionEvents()
    }
}
