//
//  VehiclesListViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehiclesListViewController: UIViewController{
    var viewModel: APIDemoViewModelProtocol?
    var vehicleList : [VehicleListModal] = []
    @IBOutlet weak var vehicleListTblView: UITableView!
    let cellSpacingHeight: CGFloat = 15.0
    @IBOutlet weak var leftSideBarBtn: UIButton!
    @IBOutlet weak var header_view: UIView!
    var progressView: ProgressView?
    var swipeActionsList = [String]()
    var contextActions = [UIContextualAction]()
    @IBOutlet weak var search_bar: UISearchBar!
    var searchActive : Bool = false
    var filterd_vehicle_list : [VehicleListModal] = []
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        self.navigationController?.isNavigationBarHidden = true
        self.vehicleListTblView.startShimmerAnimation(withIdentifier: "VehiclesListCell", numberOfRows: 1, numberOfSections: 5)
        swipeActionsList = []
        swipeActionsList.append(LanguageHelperClass().resetTxt)
        swipeActionsList.append(LanguageHelperClass().assignTxt)
        swipeActionsList.append(LanguageHelperClass().editTxt)
        setSwipeActions()
        showVehiclesList()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = getTargetName()
        SideMenu()
        header_view.dropShadow(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        // Do any additional setup after loading the view.
    }
    
    
    func setSwipeActions()
    {
        if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String]{
            print("arr:\(arr), arr count:\(arr.count)")
            if !arr.contains(where: {$0 == "122"})
            {
                if let index = swipeActionsList.firstIndex(of: LanguageHelperClass().assignTxt) {
                    swipeActionsList.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }}
            
            if !arr.contains(where: {$0 == "112"})
            {
                if let index = swipeActionsList.firstIndex(of: LanguageHelperClass().editTxt) {
                    swipeActionsList.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
        }
    }
    
    
    func showVehiclesList()
    {
         self.viewModel = APIDemoViewModel()
         self.bindUI()
         self.viewModel!.getAllVehiclesList()
    }
    
    
    private func bindUI() {
        self.viewModel?.isLoaderHidden.bind({ [weak self] in
            self?.shouldHideLoader(isHidden: $0)
        })
        self.viewModel?.alertMessage.bind({ [weak self] in
            self?.showAlertWith(message: $0)
        })
        self.viewModel?.vehicle.bind({ [weak self] in
            if let vehicle = $0 {
                print("vehicle list:\(vehicle)")
                if (self?.vehicleList.count)! > 0
                {
                    self?.vehicleList.removeAll()
                }
                for vehicleDetails in vehicle.data.return_json
                {
                    print("reg no: \(vehicleDetails.driver_id)")
                    let vehicleDetail = VehicleListModal(registrationNo: vehicleDetails.registration_no ?? "", IMEINo: vehicleDetails.serial_no ?? "", voiceNo: vehicleDetails.voice_no ?? "", vehicleType: vehicleDetails.vehicle_type_name ?? "", billingValidity: vehicleDetails.next_billing_date ?? "", device_tag: vehicleDetails.device_tag ?? "", device_id: vehicleDetails.device_id ?? "",vehicle_type_id:vehicleDetails.vehicle_type_id ?? "", driver_name:vehicleDetails.fleet_driver_name ?? "",driver_mobile:vehicleDetails.driver_phone_number ?? "", driver_id: vehicleDetails.driver_id ?? "")
                    
                    self?.vehicleList.append(vehicleDetail)
                }
            }
            
            DispatchQueue.main.async {
                self?.stopShimmerAnimation()
                self?.vehicleListTblView.delegate = self
                self?.vehicleListTblView.dataSource = self
                self?.search_bar.delegate = self
                self?.vehicleListTblView.reloadData()
            }
            
        })}
    
    func getDate(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateStr)!
    }
    
    
    @IBAction func notificationBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}



extension VehiclesListViewController:UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(searchActive) {
            return filterd_vehicle_list.count
        } else {
            return vehicleList.count
        }
    }
    
    // There is just one row in every section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 3.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
      let cell = vehicleListTblView.dequeueReusableCell(withIdentifier: "VehiclesListCell") as! VehiclesListTableViewCell
    
        if searchActive{
            return SetDataInCell(cell: cell, indexPath: indexPath.section, data: filterd_vehicle_list)
        }else{
            return SetDataInCell(cell: cell, indexPath: indexPath.section, data: vehicleList)
        }
        
    }
    
    func SetDataInCell(cell: VehiclesListTableViewCell, indexPath: Int, data: [VehicleListModal]) -> UITableViewCell {

        let cell = vehicleListTblView.dequeueReusableCell(withIdentifier: "VehiclesListCell") as! VehiclesListTableViewCell
        let currentDate = self.getDate(dateStr: Date.getCurrentDate())
        let vehicleBillingDate = self.getDate(dateStr: data[indexPath].billingValidity)
        let numOfDays = currentDate.daysBetweenDate(toDate: vehicleBillingDate)
        print("current date:\(currentDate) , billing date:\(vehicleBillingDate) , no of days:\(numOfDays)")
        
        if currentDate > vehicleBillingDate
        {
            cell.vehicleStatusStack.isHidden = false
            cell.regnStatusLbl.text = "EXPIRED"
            cell.regnStatusLbl.textColor = UIColor.red
            cell.backgroundColor = UIColor.lightGray
        }
            
        else if numOfDays < 7
        {
            cell.vehicleStatusStack.isHidden = false
            cell.regnStatusLbl.text = "EXPIRING SOON"
            cell.regnStatusLbl.textColor = UIColor.systemYellow
            cell.backgroundColor = UIColor.white
        }
            
        else
        {
            cell.vehicleStatusStack.isHidden = true
            cell.backgroundColor = UIColor.white
        }
        
        cell.registrationNoLbl.text = data[indexPath].registrationNo
        cell.IMEINoLbl.text = data[indexPath].IMEINo
        cell.voiceNoLbl.text = data[indexPath].voiceNo
        cell.vehicleTypeLbl.text = data[indexPath].vehicleType
        cell.billingValidityLbl.text = data[indexPath].billingValidity
        cell.selectionStyle = .none
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 12
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        contextActions = []
        
        for action in swipeActionsList
        {
            if action == LanguageHelperClass().editTxt
            {
                let editAction = UIContextualAction(style: .destructive, title: action) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleEditVC") as! VehicleEditVC
                    if self.searchActive {
                        vc.select_vehicle = self.filterd_vehicle_list[indexPath.section].registrationNo
                        vc.deviceSerialNo = self.filterd_vehicle_list[indexPath.section].IMEINo
                        vc.selectedVehicleType = self.filterd_vehicle_list[indexPath.section].vehicleType
                        vc.selectedDeviceTag = self.filterd_vehicle_list[indexPath.section].device_tag
                        vc.selectedVoiceNo = self.filterd_vehicle_list[indexPath.section].voiceNo
                        vc.device_id = self.filterd_vehicle_list[indexPath.section].device_id
                        vc.vehicle_typeid = self.filterd_vehicle_list[indexPath.section].vehicle_type_id
                        vc.driver_name = self.filterd_vehicle_list[indexPath.section].driver_name
                        vc.driver_no = self.filterd_vehicle_list[indexPath.section].driver_mobile
                        vc.driver_id = self.filterd_vehicle_list[indexPath.section].driver_id

                    }
                    else
                    {
                    vc.select_vehicle = self.vehicleList[indexPath.section].registrationNo
                    vc.deviceSerialNo = self.vehicleList[indexPath.section].IMEINo
                    vc.selectedVehicleType = self.vehicleList[indexPath.section].vehicleType
                    vc.selectedDeviceTag = self.vehicleList[indexPath.section].device_tag
                    vc.selectedVoiceNo = self.vehicleList[indexPath.section].voiceNo
                    vc.device_id = self.vehicleList[indexPath.section].device_id
                    vc.vehicle_typeid = self.vehicleList[indexPath.section].vehicle_type_id
                    vc.driver_name = self.vehicleList[indexPath.section].driver_name
                    vc.driver_no = self.vehicleList[indexPath.section].driver_mobile
                    vc.driver_id = self.vehicleList[indexPath.section].driver_id
                    print("driver id:\(self.vehicleList[indexPath.section].driver_id)")
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                editAction.backgroundColor = .systemYellow
                contextActions.append(editAction)
            }
            if action == LanguageHelperClass().assignTxt
            {
                let assignAction = UIContextualAction(style: .destructive, title: LanguageHelperClass().assignTxt) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleListAssignVC") as! VehicleListAssignmentVC
                    if self.searchActive {
                    vc.deviceId = self.filterd_vehicle_list[indexPath.section].device_id
                    }
                    else
                    {
                    vc.deviceId = self.vehicleList[indexPath.section].device_id

                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                assignAction.backgroundColor = .systemGreen
                contextActions.append(assignAction)
            }
            
            if action == LanguageHelperClass().resetTxt
            {
                let passwordAction = UIContextualAction(style: .destructive, title: LanguageHelperClass().resetTxt) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPassword
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                passwordAction.backgroundColor = .systemRed
                contextActions.append(passwordAction)
            }
        }
        
        let configuration = UISwipeActionsConfiguration(actions: contextActions)
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    
    func SideMenu(){
        NetworkManager().StopAllThreads()
        leftSideBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
}

extension VehiclesListViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
       // self.tbl_show_live_track.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterd_vehicle_list = vehicleList.filter { $0.registrationNo.localizedCaseInsensitiveContains(searchText) }
        if searchText.count == 0
        {
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.vehicleListTblView.reloadData()
    }
}
