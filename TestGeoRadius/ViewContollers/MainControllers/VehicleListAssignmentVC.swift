//
//  VehicleListAssignmentVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 31/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleListAssignmentVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var deviceId: String!
    var groupNameArr = [String]()
    var userNameArr = [String]()
    var groupName = ""
    var userName = ""
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.text = LanguageHelperClass().vehicleAssignTxt
        groupName = LanguageHelperClass().groupNameTxt
        userName = LanguageHelperClass().userNameTxt
        
        downloadDetails()
        // Do any additional setup after loading the view.
    }
    
    func downloadDetails(){
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()// <<---
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        let urlString = domain_name + Vehicle_Group_Assignment + "device_id=" + deviceId + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        print("url string \(urlString)")
        
        NetworkManager().CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                   if isNetwork && data != nil{
                       DispatchQueue.main.async {
                           dispatchGroup.leave()   // <<----
                       }
                       for val in data!{
                           let val_data = val as! Dictionary<String, Any>
                           print("valData:\(val_data)")
                        let group_name = val_data["group_name"] as! String
                        self.groupNameArr.append(group_name)
                       }
                       }else{
                    showToast(controller: self, message : LanguageHelperClass().serverErrorText, seconds: 2.0)
                       print("ERROR FOUND")
                   }
                   if r_error != nil{
                       showToast(controller: self, message : LanguageHelperClass().serverErrorText, seconds: 2.0)
                   }
                })
        
        dispatchGroup.enter()   // <<---
        
        let urlString1 = domain_name + Vehicle_User_Asssignment + "device_id=" + deviceId + "&user_name=" + user_name + "&hash_key=" + hash_key
        
       NetworkManager().CallNotificatioSettingDataFromServer(urlString: urlString1, completionHandler: {data, r_error, isNetwork in
                          if isNetwork && data != nil{
                            DispatchQueue.main.async {
                               dispatchGroup.leave()   // <<----
                            }
                              for val in data!{
                                  let val_data1 = val as! Dictionary<String, Any>
                                let user_name = val_data1["username"] as! String
                                self.userNameArr.append(user_name)
                                print("valData1:\(val_data1)")
                              }
                              }else{
                              showToast(controller: self, message : LanguageHelperClass().serverErrorText, seconds: 2.0)
                              print("ERROR FOUND")
                          }
                          if r_error != nil{
                              showToast(controller: self, message : LanguageHelperClass().serverErrorText, seconds: 2.0)
                          }
                    })
            
           dispatchGroup.notify(queue: .main) {
               print("both are done" )
            print("arr1:\(self.groupNameArr), arr2:\(self.userNameArr)")
              self.tableView.isHidden = false
              self.tableView.delegate = self
              self.tableView.dataSource = self
              self.tableView.reloadData()
               // whatever you want to do when both are done
           }
       }
    
    
    @IBAction func closeView(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VehicleListAssignmentVC : UITableViewDelegate, UITableViewDataSource{
    
    
     func numberOfSections(in tableView: UITableView) -> Int  {
           return 2
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return groupNameArr.count
        }
        else if section == 1
        {
            return userNameArr.count
        }
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as UITableViewCell
        
        if indexPath.section == 0
        {
          cell.textLabel?.text = groupNameArr[indexPath.row]
        }
        else if indexPath.section == 1
        {
           cell.textLabel?.text = userNameArr[indexPath.row]
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 1) {
            return self.userName
        }
        else if(section == 0) {
            return self.groupName
        }
        else
        {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height/9
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//          let view = HeaderViewReportAll.instanceFromNib()
//        if(section == 1) {
//             view.lbl_registration.text = "UserName"
//               }
//               else if(section == 0) {
//                    view.lbl_registration.text = "GroupName"
//             }
//
//          return view
//      }
    
   
    
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//           let view = HeaderViewReportAll.instanceFromNib()
//              if(section == 1) {
//                  view.lbl_registration.text = "UserName"
//              } else {
//                  view.lbl_registration.text = "GroupName"
//            }
//           return view
//
//    }
//
    
//    func SideMenu(){
//        StopAllThreads()
//        leftSideBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
//        revealViewController()?.rearViewRevealWidth = 250
//    }
    
    
}
