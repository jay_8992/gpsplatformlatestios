//
//  DashboardVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 28/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit



class DashboardVC: UIViewController,SendingVehiclesActiveCount {
   
    @IBOutlet weak var view_alert_c: UIView!
    @IBOutlet weak var segment_set: UISegmentedControl!
    @IBOutlet weak var btn_menu_pressed: UIButton!
    @IBOutlet weak var collection_item: UICollectionView!
    @IBOutlet weak var Map_View: UIView!
    @IBOutlet weak var Dashboard_View: UIView!
    @IBOutlet weak var Live_View: UIView!
    var timer = Timer()
    let alert_view = AlertView.instanceFromNib()
    var liveStatus : SuccessVC?
    var mapStatus : MainMapVC?
    var all_vehicals : Array<Any> = []
    var moving_vehicals : Array<Any> = []
    var idle_vehicals : Array<Any> = []
    var stopped_vehicals : Array<Any> = []
    var noData_vehicals : Array<Any> = []
    var moving = 00
    var stopped = 00
    var unreach = 00
    var idle = 00
    var selectedPath = 4
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var movingBtn: UIButton!
    @IBOutlet weak var IdleBtn: UIButton!
    @IBOutlet weak var stoppedBtn: UIButton!
    @IBOutlet weak var noDataBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var movingView: UIView!
    @IBOutlet weak var movingCount: UILabel!
    @IBOutlet weak var idleCountlBL: UILabel!
    @IBOutlet weak var stopCntLbl: UILabel!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var allDataLbl: UILabel!
    var is_queue = true
    var progressView: ProgressView?
    var movingText = ""
    var idleText = ""
    var stoppedText = ""
    var noDataText = ""
    var allText = ""
    var language:String!
    var alertText : String!
    var bd:Bundle!
    @IBOutlet weak var header_view: UIView!
    var timeInterval = 30.0
    var all_vehicles_count = 0
   //private lazy var networkManager = ServiceManager ()
    private var loadingNotificationBar: NotificationBar?
    var locationArr: [String] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = getTargetName()
        header_view.dropShadow(color: .darkGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        let isNotification = UserDefaults.standard.value(forKey: "IsNotification") as! Bool
        
        if isNotification{
            print("navigate to notifications page")
           // let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ParkingAlertVC") as! ParkingAlertNotifVC
            //self.navigationController?.pushViewController(vc, animated: true)
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
      
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
              
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        SideMenu()
        view_alert_c.isHidden = true
        is_queue = true
        // print("sdkslklsdks-------------------------------------------------------------------")
        alert_view.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        //alert_view.backgroundColor = UIColor.lightGray
        setVehicleCountData()
        if let indexPage = UserDefaults.standard.value(forKey: "select_home_page") as? Int
         {
             if indexPage == 0
             {
                self.view.addSubview(alert_view)
             }
        }
        else
        {
              self.view.addSubview(alert_view)
        }
     
    
    // Do any additional setup after loading the view.
    }
    
  
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetTrackingData), name: Notification.Name("TrackingDataForDashboard"), object: nil)
        language = UserDefaults.standard.value(forKey: "language") as? String
        setLanguageInControls()
        //ConnectionManager.sharedInstance.addListener(listener: self)
        NotificationBar.sharedConfig.duration = 5.0
        ReachabilityManager.shared.addListener(listener: self)

        if let indexPage = UserDefaults.standard.value(forKey: "select_home_page") as? Int
        {
            print("index page:\(indexPage)")
            if indexPage == 0
            {
                CallTrakingDataFromServer()
                CallVehicleDistanceDataFromServer()
            }
            else
            {
                segment_set.selectedSegmentIndex = indexPage
                SettingPage(selectedSegmentIndex: indexPage)
            }
        }
            
        else
        {
            CallTrakingDataFromServer()
            CallVehicleDistanceDataFromServer()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //ConnectionManager.sharedInstance.removeListener(listener: self)
        ReachabilityManager.shared.removeListener(listener: self)
        is_queue = false
        // self.timer.invalidate()
    }
    
    @objc func didBecomeActive() {
          print("did become active")
          let isNotification = UserDefaults.standard.value(forKey: "ParkingAlertClicked") as? Bool ?? false
          if  isNotification {
              UserDefaults.standard.set(false, forKey: "ParkingAlertClicked")
              let vc = self.storyboard?.instantiateViewController(withIdentifier: "ParkingAlertVC") as! ParkingAlertNotifVC
              vc.modalPresentationStyle = .fullScreen
              self.present(vc, animated: true, completion: nil)
          }
      }

      @objc func willEnterForeground() {
          print("will enter foreground")
      }
      
        
    func SetHomePageOnDashboard(){
        let indexPage = UserDefaults.standard.value(forKey: "select_home_page") as! Int
        if indexPage == 2{
            SettingPage(selectedSegmentIndex: indexPage)
        }else if indexPage == 1{
            SettingPage(selectedSegmentIndex: indexPage)
        }else{
            SettingPage(selectedSegmentIndex: indexPage)
        }
    }
    
    
    func setLanguageInControls()
    {
        bd = setLanguage(lang : language)
        movingText = bd.localizedString(forKey: "MOVING", value: nil, table: nil)
        idleText = bd.localizedString(forKey: "IDLE", value: nil, table: nil)
        stoppedText = bd.localizedString(forKey: "STOPPED", value: nil, table: nil)
        noDataText = bd.localizedString(forKey: "NO_DATA", value: nil, table: nil)
        allText = bd.localizedString(forKey: "ALL", value: nil, table: nil)
        segment_set.setTitle(bd.localizedString(forKey: "DASHBOARD", value: nil, table: nil), forSegmentAt: 0)
        segment_set.setTitle(bd.localizedString(forKey: "LIVE", value: nil, table: nil), forSegmentAt: 1)
        segment_set.setTitle(bd.localizedString(forKey: "MAP", value: nil, table: nil), forSegmentAt: 2)
        alertText = bd.localizedString(forKey: "PLEASE_WAIT", value: nil, table: nil)
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "liveBoard"{
            guard let locationController = children[1] as? SuccessVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
            }
            liveStatus = locationController
            liveStatus?.CheckIsHidden(isHidden_V: true)
        }
        
        if identifier == "mapBoard"{
            
            guard let MaplocationController = children[2] as? MainMapVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
            }
            mapStatus = MaplocationController
            mapStatus?.CheckIsHidden(isHidden_V: true)
        }
    }
    
    
    func SetAllDataToTable(vehicle_data: Array<Any>){
        
        self.moving_vehicals = []
        self.idle_vehicals = []
        self.stopped_vehicals = []
        self.noData_vehicals = []
        
        for val in vehicle_data{
            let val_data = val as! Dictionary<String, Any>
            let status = val_data["device_status"] as? Int ?? 0
            if status == 2{
                self.moving_vehicals.append(val_data)
            }
            if status == 1{
                self.idle_vehicals.append(val_data)
            }
            
            if status == 0{
                self.stopped_vehicals.append(val_data)
            }
            
            if status == 4{
                self.noData_vehicals.append(val_data)
            }
        }
    }
    
    
    func CallTrakingDataFromServer_Old(){
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let urlString = domain_name + Track_Base + "user_name=" + user_name + "&hash_key=" + hash_key
        
        print("Tracking URL DashBoard: \(urlString)")
        
        NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    self.moving = c_data["moving_count"] as! Int
                    self.stopped =  c_data["stopped_count"] as! Int
                    self.unreach =  c_data["unreachable_count"] as! Int
                    self.idle =  c_data["idling_count"] as! Int
                    self.all_vehicles_count = self.moving + self.idle +  self.stopped + self.unreach
                    self.all_vehicals = c_data["vehicles"] as! Array<Any>
                    self.setVehicleCountData()
                    self.SetAllDataToTable(vehicle_data: self.all_vehicals)
                    // Broadcast Data by Notification
                    NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }
                
            else{
                if NetworkAvailability.isConnectedToNetwork() {
                    print("Internet connection available")
                }
                else{
                    showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
                }
                //showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                print("ERROR FOUND")
            }
            
            if self.all_vehicles_count > 1000
            {
                self.timeInterval = 60.0
            }
            else if self.all_vehicles_count > 500
            {
                self.timeInterval = 50.0
            }
            else if self.all_vehicles_count > 200
            {
                self.timeInterval = 25.0
            }
          
            if self.is_queue {
                DispatchQueue.main.asyncAfter(deadline: .now() + self.timeInterval) {
                    // print("Timer fired! ---------")
                    [weak self] in
                    if self?.is_queue != nil && (self?.is_queue)!{
                        self!.CallTrakingDataFromServer()
                    }
                }
            }
            self.alert_view.removeFromSuperview()
           // self.progressView?.hide()
        })
    }
    
    //New Optimized API (14/04/2020)
    func CallTrakingDataFromServer() {
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let startingPoint = Date()

        // Use dispatchGroup for using multiple APIs together
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
              
        let url = domain_name + "/tracking_api.php"
        
        let parameters : Dictionary<String, Any> = [
            "action" : "track",
            "user_name" : user_name,
            "hash_key" : hash_key,
            
        ]
        print("Tracking URL DashBoard: \(url)")
        NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler: {data, r_error, isNetwork in
               
               if isNetwork {
                   
                   let result = data?[K_Result] as! Int
                   
                   switch (result){

                   case 0 :
                       let c_data = data?[K_Data] as! Dictionary<String,Any>
                       self.moving = c_data["moving_count"] as? Int ?? 0
                       self.stopped =  c_data["stopped_count"] as? Int ?? 0
                       self.unreach =  c_data["unreachable_count"] as? Int ?? 0
                       self.idle =  c_data["idling_count"] as? Int ?? 0
                       self.all_vehicles_count = self.moving + self.idle +  self.stopped + self.unreach
                       self.all_vehicals = c_data["vehicles"] as! Array<Any>
                       // Broadcast Data by Notification
                       NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                   break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       print("Default Case")
                   }
               }
                   
               else{
                   if NetworkAvailability.isConnectedToNetwork() {
                       print("Internet connection available")
                   }
                   else{
                       showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
                   }
                   //showToast(controller: self, message: "Please Check Your Internet Connection", seconds: 1.5)
                   print("ERROR FOUND")
               }
               
            DispatchQueue.main.async {
            dispatchGroup.leave()   // <<----
            }
            
               if self.all_vehicles_count > 1000
               {
                   self.timeInterval = 60.0
               }
               else if self.all_vehicles_count > 500
               {
                   self.timeInterval = 50.0
               }
               else if self.all_vehicles_count > 200
               {
                   self.timeInterval = 25.0
               }
             
               if self.is_queue {
                   DispatchQueue.main.asyncAfter(deadline: .now() + self.timeInterval) {
                       // print("Timer fired! ---------")
                       [weak self] in
                       if self?.is_queue != nil && (self?.is_queue)!{
                           self!.CallVehicleDistanceDataFromServer()
                           self!.CallTrakingDataFromServer()
                       }
                   }
               }
              // self.alert_view.removeFromSuperview()
              // self.progressView?.hide()
           })
        
               dispatchGroup.enter()   // <<---
               
               let url_location = domain_name + "/tracking_api.php"
               
               let parameters_new : Dictionary<String, Any> = [
                                "action" : "extra",
                                "user_name" : user_name,
                                "hash_key" : hash_key,
               ]
               
               locationArr.removeAll()

               NetworkManager().CallNewTrackResult(urlString: url_location, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
                   
                   let result = data?[K_Result] as! Int
                   print("result:\(result)")
                   
                   switch (result){
                       
                   case 0 :
                       
                      let c_data = data?[K_Data] as! Dictionary<String,Any>
    
                       if c_data.count == 0
                       {
                           DispatchQueue.main.async {
                               showToast(controller: self, message: "Reloading Data..", seconds: 1.0)
                           }
                       }
                           
                       else
                       {
                           DispatchQueue.main.async {
                             NotificationCenter.default.post(name: Notification.Name("TrackingDataForAlertCount"), object: nil, userInfo: c_data)
                           }
                       }
                       
                       break
                   case 2 :
                       let message = data?[K_Message] as! String
                       print(message)
                       break
                   default:
                       print("Default Case")
                   }
                
                    DispatchQueue.main.async {
                    dispatchGroup.leave()
                }
                   self.alert_view.removeFromSuperview()
                   
               })
               
               dispatchGroup.notify(queue: .main) {
                      print("both api done in dashboard")
                      print("time consumed \(startingPoint.timeIntervalSinceNow * -1) seconds elapsed")
                      self.setVehicleCountData()
                      self.SetAllDataToTable(vehicle_data: self.all_vehicals)
                   
               }
       }
    
    func CallVehicleDistanceDataFromServer(){
        
       // self.view.addSubview(alert_view)
      //  self.progressView?.show()
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id =  UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let url_graph = domain_name + "/tracking_api.php"
        let parameters_new : Dictionary<String, Any> = [
            "action" : "extra",
            "user_name" : user_name,
            "hash_key" : hash_key,
            "graph":"1"
        ]
                                
        NetworkManager().CallNewTrackResult(urlString: url_graph, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
            
            let result = data?[K_Result] as! Int
            print("result:\(result)")
            
            switch (result){
                
            case 0 :
                
                //  let c_data = data?[K_Data] as! Dictionary<String,Any>
                if let c_data = data?[K_Data] as? [Dictionary<String, Any>]
                {
                    var distance_data = [String:[Any]]()
                    distance_data.updateValue(c_data, forKey: "avg_dis_data")
                    NotificationCenter.default.post(name: Notification.Name("TrackingChartData"), object: nil, userInfo: distance_data)
                }
                
                break
            case 2 :
                let message = data?[K_Message] as! String
                print(message)
                break
            default:
                print("Default Case")
            }
            
        })
        
    }
    
    
    @objc func RepeatFunc(){
        CallTrakingDataFromServer()
    }
    
    
    func sendVehiclesData(data: Dictionary<String, Any>) {
           print("count data in dashboard vc:\(data)")
    }
       
    @objc func GetTrackingData(notification: Notification){
       // print("got data in dashboard page : \(notification.userInfo)")
        let c_data = notification.userInfo
        let values = (c_data![VEHICALS] as! Array<Any>)
        self.moving = c_data!["moving_count"] as? Int ?? 0
        self.stopped =  c_data!["stopped_count"] as? Int ?? 0
        self.unreach =  c_data!["unreachable_count"] as? Int ?? 0
        self.idle =  c_data!["idling_count"] as? Int ?? 0
        self.SetAllDataToTable(vehicle_data: values)
        setVehicleCountData()
    }
    
    
    @IBAction func pressed_live_track(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func pressed_segment(_ sender: UISegmentedControl) {
        
        SettingPage(selectedSegmentIndex: sender.selectedSegmentIndex)
    }
    
    
    @IBAction func pressed_notification(_ sender: Any) {
        is_queue = false
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func SettingPage(selectedSegmentIndex : Int) {
        
        if selectedSegmentIndex == 0 {
            is_queue = true
            CallTrakingDataFromServer()
            self.performSegue(withIdentifier: "liveBoard", sender: self)
            self.Dashboard_View.isHidden = false
            self.Live_View.isHidden = true
            self.Map_View.isHidden = true
        }
        
        if selectedSegmentIndex == 1 {
            guard let locationController = children[1] as? SuccessVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
            }
            liveStatus = locationController
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.0, animations: {
                   // showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                }, completion: { _ in
                    self.liveStatus?.StartThread(isStart: true, vehic: self.all_vehicals)
                    
                })
            }
           
            self.Dashboard_View.isHidden = true
            self.Live_View.isHidden = false
            self.Map_View.isHidden = true
            self.alert_view.removeFromSuperview()
            NetworkManager().StopAllThreads()
            is_queue = false
        }
        
        if selectedSegmentIndex == 2 {
            self.is_queue = false
            guard let MaplocationController = children[2] as? MainMapVC else  {
                fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
            }
            mapStatus = MaplocationController
            UIView.animate(withDuration: 0.0, animations: {
                
                switch (self.selectedPath) {
                case 0 :
                    UIView.animate(withDuration: 0.0, animations: {
                       // showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        self.mapStatus?.Check(Moving: true, Idle: false, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.moving_vehicals)
                        
                    })
                    break
                case 1 :
                    UIView.animate(withDuration: 0.0, animations: {
                       // showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        self.mapStatus?.Check(Moving: false, Idle: true, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.idle_vehicals)
                        
                    })
                    break
                case 2 :
                    UIView.animate(withDuration: 0.0, animations: {
                        //showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        if self.all_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.all_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: self.stopped_vehicals)
                        }
                        
                    })
                    break
                case 3 :
                    
                    UIView.animate(withDuration: 0.0, animations: {
                       // showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        if self.noData_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.noData_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: self.noData_vehicals)
                        }
                    })
                    
                    break
                case 4 :
                    
                    UIView.animate(withDuration: 0.0, animations: {
                      //  showToast(controller: self, message: "Please Wait...", seconds: 0.0)
                    }, completion: { _ in
                        if self.all_vehicals.count > 210{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: SplitArray(into: 210, yourArray: self.all_vehicals))
                        }else{
                            self.mapStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: self.all_vehicals)
                        }
                    })
                    
                    break
                default:
                    print("No")
                }
                
                NetworkManager().StopAllThreads()
                self.Dashboard_View.isHidden = true
                self.Live_View.isHidden = true
                self.Map_View.isHidden = false
            }, completion: { _ in
               //  self.performSegue(withIdentifier: "mapBoard", sender: self)
            })
        }
    }
    
    //function to set vehicle count data in header part
    
    func setVehicleCountData()
    {

        DispatchQueue.main.async {
            
            let movingImg: UIImage = UIImage(named: "green")!
            self.movingBtn.setBackgroundImage(movingImg, for: .normal)
            self.movingBtn.applyGradient(colours: [Moving_Color])
            self.movingBtn.setTitle(String(self.moving), for: .normal)
            self.movingCount.text = self.movingText
            self.movingCount.textColor = Moving_Color
            
            let idleImg: UIImage = UIImage(named: "yellow")!
            self.IdleBtn.setBackgroundImage(idleImg, for: .normal)
            self.IdleBtn.applyGradient(colours: [Alert_Color])
            self.IdleBtn.setTitle(String(self.idle), for: .normal)
            self.idleCountlBL.text = self.idleText
            self.idleCountlBL.textColor = Alert_Color
            
            let stopImg: UIImage = UIImage(named: "red")!
            self.stoppedBtn.setBackgroundImage(stopImg, for: .normal)
            self.stoppedBtn.applyGradient(colours: [Stopped_Color])
            self.stoppedBtn.setTitle(String(self.stopped), for: .normal)
            self.stopCntLbl.text = self.stoppedText
            self.stopCntLbl.textColor = Stopped_Color
            
            let noDataImg: UIImage = UIImage(named: "black")!
            self.noDataBtn.setBackgroundImage(noDataImg, for: .normal)
            self.movingBtn.applyGradient(colours: [Unreach_Color])
            self.noDataBtn.setTitle(String(self.unreach), for: .normal)
            self.noDataLbl.text = self.noDataText
            self.noDataLbl.textColor = Unreach_Color
            
            let allDataImg: UIImage = UIImage(named: "all")!
            self.allBtn.setBackgroundImage(allDataImg, for: .normal)
            self.allBtn.applyGradient(colours: [Global_Blue_Color])
            let all_count = self.moving + self.idle +  self.stopped + self.unreach
            self.allBtn.setTitle(String(all_count), for: .normal)
            self.allDataLbl.text = self.allText
            self.allDataLbl.textColor = Global_Blue_Color
            
        }
       
    }
    

    @IBAction func DasboardBtnSelected(_ sender: UIButton) {
        
        NetworkManager().StopAllThreads()
        clearSelectedState()
        is_queue = false
        selectedPath = sender.tag
        segment_set.selectedSegmentIndex = 1
        self.Dashboard_View.isHidden = true
        self.Live_View.isHidden = false
        self.Map_View.isHidden = true
        
        guard let locationController = children[1] as? SuccessVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[1]))")
        }
        
        liveStatus = locationController
        
        guard let MaplocationController = children[2] as? MainMapVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[2]))")
        }
        mapStatus = MaplocationController
        print("\(sender.tag)")
        
        switch sender.tag {
        case 0:
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: true, Idle: false, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        case 1:
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: true, Stopped: false, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        case 2:
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: true, No_data: false, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        case 3:
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: true, All: false, select: true, vehicle_data: self.all_vehicals)
            })
        case 4:
            UIView.animate(withDuration: 0.0, animations: {
                showToast(controller: self, message: "Please Wait...", seconds: 1.5)
            }, completion: { _ in
                self.liveStatus?.Check(Moving: false, Idle: false, Stopped: false, No_data: false, All: true, select: true, vehicle_data: self.all_vehicals)
            })
        default:
            print("out of index selected error")
        }
    }
    
    
    
    func clearSelectedState()
    {
        [self.movingBtn, self.IdleBtn,self.stoppedBtn,self.noDataBtn,self.allBtn].forEach {
            $0?.layer.borderWidth = 0.0
        }
    }
    
}

extension DashboardVC:NetworkStatusListener {
    
    func SideMenu() {
        NetworkManager().StopAllThreads()
        btn_menu_pressed.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
  }
 
    func networkStatusDidChange(status: Reachability.Connection) {
        switch status {
        case .unavailable :
            NotificationBar(over: self, text: "No Internet Connection", style: .error).show()
            //showToast(controller: self, message: "No Network Connection , Please Connect to Internet", seconds: 3.0)
            debugPrint("ViewController: Network became unreachable")
        case .wifi:
            debugPrint("ViewController: Network reachable through WiFi")
            NotificationBar(over: self, text: "Back Online", style: .success).show()
            CallTrakingDataFromServer()
        case .cellular:
            debugPrint("ViewController: Network reachable through Cellular Data")
            CallTrakingDataFromServer()
        case .none:
            debugPrint("ViewController: Network reachable through WAN")
        }
    }

}
