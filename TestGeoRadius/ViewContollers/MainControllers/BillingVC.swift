//
//  BillingVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class BillingVC: UIViewController {
    
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var tbl_billing_reports: UITableView!
    @IBOutlet weak var leftSideBarBtn: UIButton!
    
    var item_name = [String]()
    var item_images = ["add_license_1", "renew_license_1", "invoice_1", "payments_1", "statement_1"]
    let cellSpacingHeight:CGFloat = 15.0
    var language = ""
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = getTargetName()
        setListTitles()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        SideMenu()
        self.tbl_billing_reports.register(UINib(nibName: "Report_Billing_Cell", bundle: nil), forCellReuseIdentifier: "Report_Billing_Cell")
        self.tbl_billing_reports.delegate = self
        self.tbl_billing_reports.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func setListTitles()
      {
          language = UserDefaults.standard.value(forKey: "language") as! String
          let bd = setLanguage(lang : language)
          item_name.append(bd.localizedString(forKey: "ADD_LICENSE", value: nil, table: nil))
          item_name.append(bd.localizedString(forKey: "RENEW_LICENSE", value: nil, table: nil))
          item_name.append(bd.localizedString(forKey: "INVOICES", value: nil, table: nil))
          item_name.append(bd.localizedString(forKey: "PAYMENTS", value: nil, table: nil))
          item_name.append(bd.localizedString(forKey: "STATEMENTS", value: nil, table: nil))
    }
      
    
    @IBAction func pressedNotificationBtn(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.navigationController?.isNavigationBarHidden = true
    }
}


extension BillingVC : UITableViewDelegate, UITableViewDataSource{
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_billing_reports.dequeueReusableCell(withIdentifier: "Report_Billing_Cell") as! Report_Billing_Cell
        cell.lbl_report_name.text = item_name[indexPath.section]
        cell.img_report.image = UIImage(named: item_images[indexPath.section])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_billing_reports.frame.size.height / 6
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddLicenseVC") as! AddLicenseVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RenewLicenseVC") as! RenewLicenseVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 4{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatementVC") as! StatementVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return item_name.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 0.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func SideMenu(){
        NetworkManager().StopAllThreads()
        leftSideBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
}
