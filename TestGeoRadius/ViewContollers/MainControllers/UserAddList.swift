//
//  UserAddList.swift
//  GeoTrack
//
//  Created by Georadius on 17/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UserAddList: UIViewController {
    
    @IBOutlet weak var btn_add_user: UIButton!
    @IBOutlet weak var tbl_user_add: UITableView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var leftSideBarBtn: UIButton!
    @IBOutlet weak var search_bar: UISearchBar!
    
    var searchActive : Bool = false
    var user_list : [UserListModel] = []
    var filterd_user_list : [UserListModel] = []
    var language = ""
    var bd:Bundle!
    var swipeActionsList = [String]()
    var contextActions = [UIContextualAction]()
    var progressView: ProgressView?
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        print("view will appear")
        self.navigationController?.isNavigationBarHidden = true
        self.progressView = ProgressView(message: "", theme: .dark, isModal: true)
        swipeActionsList = []
        swipeActionsList.append(LanguageHelperClass().editTxt)
        swipeActionsList.append(LanguageHelperClass().assignTxt)
        swipeActionsList.append(LanguageHelperClass().deleteTxt)
        swipeActionsList.append(LanguageHelperClass().passwordTxt)
        setSwipeActions()
        CallUserList()
    }
    
    func setSwipeActions()
    {
        
        if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String]{
            print("arr:\(arr), arr count:\(arr.count)")
          
            if !arr.contains(where: {$0 == "122"})
            {
                if let index = swipeActionsList.firstIndex(of: LanguageHelperClass().assignTxt) {
                    swipeActionsList.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            if !arr.contains(where: {$0 == "120"})
            {
                if let index = swipeActionsList.firstIndex(of: LanguageHelperClass().deleteTxt) {
                    swipeActionsList.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
            if !arr.contains(where: {$0 == "33"})
            {
                if let index = swipeActionsList.firstIndex(of: LanguageHelperClass().passwordTxt) {
                    swipeActionsList.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = getTargetName()
        language = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : language)
        
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        self.tbl_user_add.register(UINib(nibName: "UserAddCell", bundle: nil), forCellReuseIdentifier: "UserAddCell")
        SideMenu()

       // btn_add_user.layer.cornerRadius = btn_add_user.frame.size.width / 2
        btn_add_user.addTarget(self, action: #selector(pressed_add_user), for: .touchUpInside)
        tbl_user_add.delegate = self
        tbl_user_add.dataSource = self
            
    }
    
    
    func CallUserList(){
        
        self.progressView?.show()
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        let urlString = domain_name + SEARCH_USER_JSON + K_USER_NAME + user_name + K_HASH_KEY + hash_key + K_GROUP_ID + group_id
        
        print("dfklkd \(urlString)")
        if self.user_list.count > 0
        {
            self.user_list.removeAll()
        }
        
        NetworkManager().CallUserAddListFromServer(urlStirng: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    let user_name = val_data["username"] as! String
                    let email = val_data["email"] as! String
                    let phone = val_data["phone"] as! String
                    let user_id = val_data["userid"] as! String
                    
                    let newDetail = UserListModel(user_name: user_name, phone: phone, email: email, user_id: user_id)
                    self.user_list.append(newDetail)
                }
                
                self.tbl_user_add.isHidden = false
                self.tbl_user_add.delegate = self
                self.search_bar.delegate = self
                self.tbl_user_add.dataSource = self
                self.tbl_user_add.reloadData()
                
            }else{
                //  self.tbl_invoice.isHidden = true
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
            }
            self.progressView?.hide()
            // self.alert_view.removeFromSuperview()
        })
        
    }
    
    @objc func pressed_add_user(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserAddVC") as! UserAddVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notificationBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "IsNotification")
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension UserAddList : UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if(searchActive) {
              return filterd_user_list.count
          } else {
              return user_list.count
          }
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tbl_user_add.dequeueReusableCell(withIdentifier: "UserAddCell") as! UserAddCell
          if searchActive{
              cell.lbl_user_name.text = filterd_user_list[indexPath.row].user_name
              cell.lbl_phone.text = filterd_user_list[indexPath.row].phone
              cell.lbl_email.text = filterd_user_list[indexPath.row].email
          }else{
              cell.lbl_user_name.text = user_list[indexPath.row].user_name
              cell.lbl_phone.text = user_list[indexPath.row].phone
              cell.lbl_email.text = user_list[indexPath.row].email
          }
        return cell
      }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_user_add.frame.size.height / 4.5
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
       contextActions = []
        
        for action in swipeActionsList
        {
            if action == LanguageHelperClass().editTxt
            {
                let editAction = UIContextualAction(style: .destructive, title: action) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditUserInfoVC") as! EditUserInfoVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                editAction.backgroundColor = .systemYellow
                contextActions.append(editAction)
            }
            if action == LanguageHelperClass().assignTxt
            {
                let assignAction = UIContextualAction(style: .destructive, title: action) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "VehicleAssignment") as! VehicleAssignment
                    if self.searchActive {
                        vc.userID = self.filterd_user_list[indexPath.row].user_id
                    }
                    else
                    {
                        vc.userID = self.user_list[indexPath.row].user_id
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                assignAction.backgroundColor = .systemGreen
                contextActions.append(assignAction)
            }
            
            if action == LanguageHelperClass().deleteTxt
            {
                let deleteAction = UIContextualAction(style: .destructive, title: action) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeleteUserVC") as! DeleteUserVC
                    if self.searchActive {
                        vc.user_id = self.filterd_user_list[indexPath.row].user_id
                    }
                    else
                    {
                        vc.user_id = self.user_list[indexPath.row].user_id
                    }
                   // vc.user_id = self.user_list[indexPath.row].user_id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                deleteAction.backgroundColor = .systemRed
                contextActions.append(deleteAction)
            }
            
            if action == LanguageHelperClass().passwordTxt
            {
                let passwordAction = UIContextualAction(style: .destructive, title: action) { (action, view, handler) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPassword
                    if self.searchActive {
                        vc.user_id = self.filterd_user_list[indexPath.row].user_id
                    }
                    else
                    {
                        vc.user_id = self.user_list[indexPath.row].user_id
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                passwordAction.backgroundColor = .systemBlue
                contextActions.append(passwordAction)
            }
        }
        
        let configuration = UISwipeActionsConfiguration(actions: contextActions)
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
    
    
    func SideMenu(){
        NetworkManager().StopAllThreads()
        leftSideBarBtn.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        revealViewController()?.rearViewRevealWidth = 250
    }
}

extension UserAddList: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
       // self.tbl_show_live_track.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.search_bar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("in search bar users page")
        filterd_user_list = user_list.filter { $0.user_name.localizedCaseInsensitiveContains(searchText) }
        if searchText.count == 0
        {
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tbl_user_add.reloadData()
    }
}
