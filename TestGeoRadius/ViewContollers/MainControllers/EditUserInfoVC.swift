//
//  EditUserInfoVC.swift
//  GeoTrack
//
//  Created by Georadius on 22/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class EditUserInfoVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_company_name: UITextField!
    @IBOutlet weak var txt_companyTitle: UILabel!
    @IBOutlet weak var txt_user_address: UITextField!
    @IBOutlet weak var txt_userAddTitle: UILabel!
    @IBOutlet weak var txt_phone: UITextField!
    @IBOutlet weak var txt_phoneTitle: UILabel!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_emailTitle: UILabel!
    @IBOutlet weak var btn_back: UIButton!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
    super.viewDidLoad()
    setControlLanguage()
    view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
    self.btn_back.addTarget(self, action: #selector(pressed_back_btn), for: .touchUpInside)
    // Do any additional setup after loading the view.
    self.btn_submit.addTarget(self, action: #selector(UpdateDataonServer), for: .touchUpInside)
        
    }
    
    func setControlLanguage()
    {
        txt_companyTitle.text = LanguageHelperClass().companyNameText + ":"
        txt_emailTitle.text = LanguageHelperClass().emailText + ":"
        txt_phoneTitle.text = LanguageHelperClass().phoneText + ":"
        txt_userAddTitle.text = LanguageHelperClass().userAddText
        btn_submit.setTitle(LanguageHelperClass().submitTxt, for: .normal)
        titleLbl.text = LanguageHelperClass().editUserText
    }
           
    @objc func pressed_back_btn(){
         self.navigationController?.popViewController(animated: true)
    }

    
    @objc func UpdateDataonServer(){
        //https://track.gpsplatform.in/user_result.php?&action=update&user_name=premiumtech17@gmail.com&hash_key=VZPEKZMD&user_id=3093&email=&name=&phone=12456356&company_nameasr=&address=delhi&data_format=1

        if txt_email.text!.count < 1 || txt_user_address.text!.count < 1 || txt_company_name.text!.count < 1 || txt_phone.text!.count < 1{
            lbl_error.isHidden = false
            return
        }
        lbl_error.isHidden = true
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        let urlString = domain_name + Update_User + "user_name=" + user_name + "&hash_key=" + hash_key + "&user_id=" + user_id + "&email=" + txt_email.text! + "&name=&phone=" + txt_phone.text! + "&company_name=" + txt_company_name.text! + "&address=" + txt_user_address.text! + "&data_format=1"
        
        print("url string \(urlString)")
        
        NetworkManager().CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
                      if isNetwork && data != nil{
                          showToast(controller: self, message : "User Updated", seconds: 2.0)
                     
                      }else{
                          
                        showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
                          print("ERROR FOUND")
                      }
                      if r_error != nil{
                        showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
                      }
                      
                  })
        
    }
}
