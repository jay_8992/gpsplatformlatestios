//
//  ShowTrackingInfo.swift
//  TestGeoRadius
//
//  Created by Georadius on 29/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps
import Mapbox
import MapKit
import UIKit.UIGestureRecognizerSubclass
import EzPopup

// MARK: - State

private enum State {
    case closed
    case open
}


extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}




class ShowTrackingInfo: UIViewController, MGLMapViewDelegate , ParkingAlertDelegate{
    
    var locationManager:CLLocationManager!
    
    @IBOutlet weak var btn_zoom_in: UIButton!
    @IBOutlet weak var btn_zoom_out: UIButton!
    
    @IBOutlet weak var btn_navigate: UIButton!
    @IBOutlet weak var btn_lock: UIButton!
    @IBOutlet weak var lbl_header_name: UILabel!
    @IBOutlet weak var tbl_vehicle_data: UITableView!
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var view_show_history: UIView!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var btn_traffic: UIButton!
    @IBOutlet weak var showMap: GMSMapView!
    @IBOutlet weak var map_show: UIView!
    @IBOutlet weak var map_VIew: MGLMapView!
    @IBOutlet weak var btn_share: UIButton!
    @IBOutlet weak var btn_croxx: UIButton!
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var btn_done: UIButton!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var date_time_picker: UIDatePicker!
    @IBOutlet weak var view_date_and_time: UIView!
    @IBOutlet weak var menuBtn: UIButton!
    
    @IBOutlet weak var currentLocBtn: UIButton!
    var date_time : String!
    var mapView: MGLMapView!
    var rasterLayer: MGLRasterStyleLayer?
    var vehicals : Array<Any>?
    var latitude = [Double]()
    var longitude = [Double]()
    var start_date = [String]()
    var end_date = [String]()
    var index : Int!
    var registration = [String]()
    var vehicle_data = [NSAttributedString]()
    var data_heading = [String]()
    var device_serial = [String]()
    var vehicle_type_id = [String]()
    var vehicle_name = "car_green"
    var voice_no = [String]()
    var device_tag = [String]()
    var imageNamed : String = STOP_CAR
    var status = [Int]()
    var change_command_type : String!
    var device_id : String?
    let alert_view = AlertView.instanceFromNib()
    @IBOutlet var multiple_views: [UIView]!
    @IBOutlet var btn_view_change: [UIButton]!
    var containerViewController: TrackingStatusHistoryVC?
    var showReportTracking: ShowReportTracking?
    var showNotificationTracking: ShowNotificationTracking?
    var updateVehicle: UpdateVehicleVC?
    var iTemp:Int = 0
    var marker = GMSMarker()
    var rectangle = GMSPolyline()
    var timer = Timer()
    let path = GMSMutablePath()
    var direction = [Double]()
    var is_queue = false
    private var popupOffset: CGFloat = 300
    private var bottomViewHieght:CGFloat = 350
    private var bottomConstraint = NSLayoutConstraint()
    private var heightConstraint = NSLayoutConstraint()
    var items_heading = ["Speed: ", "Distance: ", "Coordinates: ", "Halt Time: ", "Location: ", "Fuel: ", "Temprature: ", "Odometer: ", "Driver Name: ", "Driver Mobile: "]
    var imageName = [String]()
    
    var speedVal : NSMutableAttributedString!
    var distanceVal : NSMutableAttributedString!
    var haltTime : NSMutableAttributedString!
    var driverName : NSMutableAttributedString!
    var driverNo : NSMutableAttributedString!
    var driverId = ""
    var coordinateVal : NSMutableAttributedString!
    var locationVal = ""
    var fuelVal : NSMutableAttributedString!
    var temperatureVal : NSMutableAttributedString!
    var odometerVal : NSMutableAttributedString!
    var lastUpdateTime = ""
    let emptyString : NSAttributedString = NSAttributedString(string: "")
    var isMenuListOpen = false
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var shareLbl: UILabel!
    @IBOutlet weak var shareLocLbl: UILabel!
    @IBOutlet weak var cutOnOffLbl: UILabel!
    @IBOutlet weak var parkingOnOffLbl: UILabel!
    @IBOutlet weak var nearVehicleLbl: UILabel!
    @IBOutlet weak var trafficSignalLbl: UILabel!
    @IBOutlet weak var parkingBtn: UIButton!
    var previousLocation:CLLocation! = nil
    var currentLocation:CLLocation!
    lazy var refreshControl = UIRefreshControl()
    
    //18/02/2020
    var kmText = ""
    var kmhText = ""
    var language : String!
    var bd:Bundle!
    var bottomViewState = "Close"
    
    var selectedMenuImages = ["tracking_page","map_history-1","trips_selected","alerts_selected","vehicle_edit"]
    var unselectedMenuImages = ["map","distance","trips","alert_bell","vehicle_edit_plane"]
    
    private var drawingTimer: Timer?
    private var polyline: GMSPolyline?
    var transaction1 : CATransaction?
    var transaction2 : CATransaction?
    var locationArr: [String] = []
    var distanceArr:[Double] = []
    
    var fromVC = ""
    var vehicleLat:Double!
    var vehicleLong:Double!
    var parkingStatus = "ON"
    var testValue: String = ""
    var vehicle_moving_status = 1

    var animatePolyline: AnimatePolyline?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        sideMenuView.isHidden = true
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        btn_traffic.isHidden = true

        btn_traffic.addTarget(self, action: #selector(btn_traffic_pressed), for: .touchUpInside)
       // btn_traffic.layer.cornerRadius = btn_traffic.frame.size.height / 10
       // btn_traffic.layer.borderColor = UIColor.darkGray.cgColor
       // btn_traffic.layer.borderWidth = 1
        
       // btn_share.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_share.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        
        
        //currentLocBtn.layer.cornerRadius = btn_share.frame.size.width / 2
        currentLocBtn.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        currentLocBtn.addTarget(self, action: #selector(showNearestVehicles), for: .touchUpInside)
        
       // btn_lock.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_lock.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_lock.addTarget(self, action: #selector(CallIgnitionData), for: .touchUpInside)
        
      //  btn_zoom_in.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_zoom_in.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_in.addTarget(self, action: #selector(pressed_zoom_in), for: .touchUpInside)
        
      //  btn_zoom_out.layer.cornerRadius = btn_share.frame.size.width / 2
        btn_zoom_out.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_zoom_out.addTarget(self, action: #selector(pressed_zoom_out), for: .touchUpInside)
        
        //btn_navigate.layer.cornerRadius = btn_navigate.frame.size.width / 2
       // btn_navigate.layer.cornerRadius = btn_navigate.frame.size.width / 2
        btn_navigate.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_navigate.addTarget(self, action: #selector(openMapForPlace), for: .touchUpInside)
        
        btn_croxx.addTarget(self, action: #selector(cross_pressed), for: .touchUpInside)
        btn_done.addTarget(self, action: #selector(CallPasswordData), for: .touchUpInside)
        view_date_and_time.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        btn_share.addTarget(self, action: #selector(ShareInfoToUser), for: .touchUpInside)
        date_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
       // mapView = MGLMapView(frame: view.bounds, styleURL: MGLStyle.lightStyleURL)
       // mapView.frame = CGRect(x: 0, y: self.mapView.frame.origin.y, width: self.view.frame.size.width, height: self.mapView.frame.size.height)
      //  mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      //  mapView.tintColor = .darkGray
      //  mapView.delegate = self
        
        // multiple_views[0].addSubview(map_VIew)
      //  map_show.addSubview(mapView)
       // mapView.setCenter(CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773), zoomLevel: 10, animated: true)
        let point = MyCustomPointAnnotation()
        point.coordinate = CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773)
        let myPlaces = point
       // mapView.addAnnotation(myPlaces)
        CallDataFromServerForIngnition(isDidLoad: false)
        

        // ------------------------------------------ Google Map ---------------------------------------- //
        self.showMap.animate(toLocation: CLLocationCoordinate2D(latitude: 28.6127, longitude: 77.2773))
        let camera = GMSCameraPosition.camera(withLatitude: 28.6127, longitude: 77.2773, zoom: 16)
        self.showMap!.camera = camera
        
        date_time = TodayToDate()
        self.tbl_vehicle_data.register(UINib(nibName: "Vehicle_Data_Cell", bundle: nil), forCellReuseIdentifier: "Vehicle_Data_Cell")
        
        //SetAllData()
        
    }
    
    @objc func refresh(sender:AnyObject) {
       // Code to refresh table view
    }
    
    @IBAction func pressed_back_button(_ sender: Any) {
        NetworkManager().StopAllThreads()
        if fromVC == "MapPage"
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainMapVC") as! MainMapVC
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
            vc.delegate = self
            self.navigationController?.popViewController(animated: true)
        }
        //  self.performSegue(withIdentifier: "status_seague", sender: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        is_queue = false
    }
    
    func sendParkingAlertStatus(status: String) {
        print("parking status:\(status)")
        if status == "1"
        {
            self.parkingStatus = "ON"
            DispatchQueue.main.async {
                self.parkingBtn.setImage(UIImage.init(named: "parking_act"), for: .normal)
            }
        }
        else
        {
            self.parkingStatus = "OFF"
            DispatchQueue.main.async {
                self.parkingBtn.setImage(UIImage.init(named: "park_inactive"), for: .normal)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        language = UserDefaults.standard.value(forKey: "language") as? String
        bd = setLanguage(lang : language)
        self.lbl_header_name.text = bd.localizedString(forKey: "TRACKING", value: nil, table: nil)
        setSideMenuTitles()
        setSwipeActions()
      //  btn_lock.setImage(UIImage(named: "lock"), for: .normal)
        view_show_history.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view_show_history.layer.shadowColor = UIColor.black.cgColor
        view_show_history.layer.shadowOpacity = 0.1
        view_show_history.layer.shadowRadius = 10
       // view_show_history.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 3), radius: 5, scale: true)
        layout()
        view_show_history.translatesAutoresizingMaskIntoConstraints = false
        overlayView.addGestureRecognizer(panRecognizer)
       // self.view.addSubview(alert_view)
        //NotificationCenter.default.addObserver(self, selector: #selector(GetTrackingData), name: Notification.Name("TrackingData"), object: nil)
        is_queue = true
       // DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) { // Change `2.0` to the desired number of seconds.
           // Code you want to be delayed
            self.CallDataRepeatedly()
      //  }
    }
    
    
    func setSwipeActions()
      {
          if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String]{
              print("arr:\(arr), arr count:\(arr.count)")
            
              if !arr.contains(where: {$0 == "112"})
              {
                btn_view_change[4].isHidden = true
                btn_view_change[0].frame = CGRect.init(x: view_header.frame.origin.x, y: btn_view_change[0].frame.origin.y, width: view_header.frame.size.width/4, height: btn_view_change[0].frame.size.height)
                btn_view_change[2].frame = CGRect.init(x: btn_view_change[0].frame.origin.x + btn_view_change[0].frame.size.width, y: btn_view_change[2].frame.origin.y, width: view_header.frame.size.width/4, height: btn_view_change[2].frame.size.height)
                btn_view_change[1].frame = CGRect.init(x: btn_view_change[2].frame.origin.x + btn_view_change[2].frame.size.width, y: btn_view_change[1].frame.origin.y, width: view_header.frame.size.width/4, height: btn_view_change[1].frame.size.height)
                btn_view_change[3].frame = CGRect.init(x: btn_view_change[1].frame.origin.x + btn_view_change[1].frame.size.width, y: btn_view_change[3].frame.origin.y, width: view_header.frame.size.width/4, height: btn_view_change[3].frame.size.height)
              }
                
              else
              {
                btn_view_change[4].isHidden = false
                btn_view_change[0].frame = CGRect.init(x: view_header.frame.origin.x, y: btn_view_change[0].frame.origin.y, width: view_header.frame.size.width/5, height: btn_view_change[0].frame.size.height)
                btn_view_change[2].frame = CGRect.init(x: btn_view_change[0].frame.origin.x + btn_view_change[0].frame.size.width, y: btn_view_change[2].frame.origin.y, width: view_header.frame.size.width/5, height: btn_view_change[2].frame.size.height)
                btn_view_change[1].frame = CGRect.init(x: btn_view_change[2].frame.origin.x + btn_view_change[2].frame.size.width, y: btn_view_change[1].frame.origin.y, width: view_header.frame.size.width/5, height: btn_view_change[1].frame.size.height)
                btn_view_change[3].frame = CGRect.init(x: btn_view_change[1].frame.origin.x + btn_view_change[1].frame.size.width, y: btn_view_change[3].frame.origin.y, width: view_header.frame.size.width/5, height: btn_view_change[3].frame.size.height)
                btn_view_change[4].frame = CGRect.init(x: btn_view_change[3].frame.origin.x + btn_view_change[3].frame.size.width, y: btn_view_change[4].frame.origin.y, width: view_header.frame.size.width/5, height: btn_view_change[4].frame.size.height)
              }
          }
        
      }
      
    
    func setSideMenuTitles ()
    {
        self.nearVehicleLbl.text =  self.bd.localizedString(forKey: "NEAREST_VEHICLE", value: nil, table: nil)
        self.shareLocLbl.text = self.bd.localizedString(forKey: "SHARE_LOCATION", value: nil, table: nil)
        self.shareLbl.text = self.bd.localizedString(forKey: "SHARE", value: nil, table: nil)
        self.trafficSignalLbl.text =  self.bd.localizedString(forKey: "TRAFFIC_SIGNAL", value: nil, table: nil)
    }
    
   
    func layout(){
        
        view_show_history.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        view_show_history.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        bottomConstraint = view_show_history.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: popupOffset)
        bottomConstraint.isActive = true
        heightConstraint = view_show_history.heightAnchor.constraint(equalToConstant: bottomViewHieght)
        heightConstraint.isActive = true
       
    }
    
    func updateLayout()
    {
        self.heightConstraint.constant = self.bottomViewHieght
        if self.bottomViewState == "Close"
        {
         self.bottomConstraint.constant = self.popupOffset
        }
    }
    
    private var currentState: State = .closed
    
    private var runningAnimators = [UIViewPropertyAnimator]()
    
    private var animationProgress = [CGFloat]()
    
    private lazy var panRecognizer: InstantPanGestureRecognizer = {
        let recognizer = InstantPanGestureRecognizer()
        recognizer.addTarget(self, action: #selector(popupViewPanned(recognizer:)))
        
        return recognizer
    }()
    
    private func animateTransitionIfNeeded(to state: State, duration: TimeInterval) {
        
        guard runningAnimators.isEmpty else { return }
        
        let transitionAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomViewState = "Open"
                self.bottomConstraint.constant = 0
                self.view_show_history.layer.cornerRadius = 10
                
            case .closed:
                self.bottomViewState = "Close"
                self.bottomConstraint.constant = self.popupOffset
                self.view_show_history.layer.cornerRadius = 0
                
            }
            self.view.layoutIfNeeded()
        })
        
        transitionAnimator.addCompletion { position in
            
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            }
            
            switch self.currentState {
            case .open:
                self.bottomConstraint.constant = 0

            case .closed:
                self.bottomConstraint.constant = self.popupOffset
                
            }
            
            self.runningAnimators.removeAll()
            
        }
        
        let inTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeIn, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        inTitleAnimator.scrubsLinearly = false
        
        let outTitleAnimator = UIViewPropertyAnimator(duration: duration, curve: .easeOut, animations: {
            switch state {
            case .open: break
            case .closed: break
            }
        })
        outTitleAnimator.scrubsLinearly = false
        
        transitionAnimator.startAnimation()
        inTitleAnimator.startAnimation()
        outTitleAnimator.startAnimation()
        
        runningAnimators.append(transitionAnimator)
        runningAnimators.append(inTitleAnimator)
        runningAnimators.append(outTitleAnimator)
        
    }
    
    @objc private func popupViewPanned(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
            
        case .began:
            
            animateTransitionIfNeeded(to: currentState.opposite, duration: 1)
            
            runningAnimators.forEach { $0.pauseAnimation() }
            
            animationProgress = runningAnimators.map { $0.fractionComplete }
            
        case .changed:
            
            let translation = recognizer.translation(in: view_show_history)
            var fraction = -translation.y / popupOffset
            
            if currentState == .open { fraction *= -1 }
            if runningAnimators[0].isReversed { fraction *= -1 }
            
            for (index, animator) in runningAnimators.enumerated() {
                animator.fractionComplete = fraction + animationProgress[index]
            }
            
        case .ended:
            
            let yVelocity = recognizer.velocity(in: view_show_history).y
            let shouldClose = yVelocity > 0
            
            if yVelocity == 0 {
                runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
                break
            }
            
            switch currentState {
            case .open:
                if !shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            case .closed:
                if shouldClose && !runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
                if !shouldClose && runningAnimators[0].isReversed { runningAnimators.forEach { $0.isReversed = !$0.isReversed } }
            }
            
            runningAnimators.forEach { $0.continueAnimation(withTimingParameters: nil, durationFactor: 0) }
            
        default:
            ()
        }
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        if identifier == "status_seague"{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrackingStatusHistoryVC") as! TrackingStatusHistoryVC
            vc.PlayOrPause()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //status_seague
        if segue.identifier == "status_seague"{
            containerViewController = segue.destination as? TrackingStatusHistoryVC
            containerViewController?.device_id = self.device_id
            containerViewController?.vehicle_type_id = self.vehicle_name
        }
        
        if segue.identifier == "report_seague"{
            showReportTracking = segue.destination as? ShowReportTracking
            showReportTracking?.device_id = self.device_id
        }
        
        if segue.identifier == "notification_seague"{
            showNotificationTracking = segue.destination as? ShowNotificationTracking
            showNotificationTracking?.device_id = self.device_id
        }
        //update_vehicle
    }
    
    @IBAction func openMenuItems(_ sender: UIButton) {
        sender.flash()
        if isMenuListOpen == false
        {
            isMenuListOpen = true
            
            self.viewSlideInFromBottomToTop(view: btn_navigate)
            self.viewSlideInFromBottomToTop(view: btn_lock)
            self.viewSlideInFromBottomToTop(view: btn_share)
            self.viewSlideInFromBottomToTop(view: btn_traffic)
            self.viewSlideInFromBottomToTop(view: currentLocBtn)
            self.viewSlideInFromBottomToTop(view: parkingBtn)
            self.viewSlideInFromBottomToTop(view: shareLbl)
            self.viewSlideInFromBottomToTop(view: shareLocLbl)
            self.viewSlideInFromBottomToTop(view: cutOnOffLbl)
            self.viewSlideInFromBottomToTop(view: parkingOnOffLbl)
            self.viewSlideInFromBottomToTop(view: nearVehicleLbl)
            self.viewSlideInFromBottomToTop(view: trafficSignalLbl)
            
            self.btn_navigate.isHidden = false
            self.btn_lock.isHidden = false
            self.btn_share.isHidden = false
            self.btn_traffic.isHidden = false
            self.currentLocBtn.isHidden = false
            self.parkingBtn.isHidden = false
            self.shareLbl.isHidden = false
            self.shareLocLbl.isHidden = false
            self.cutOnOffLbl.isHidden = false
            self.parkingOnOffLbl.isHidden = false
            self.nearVehicleLbl.isHidden = false
            self.trafficSignalLbl.isHidden = false
        }
            
        else
        {
            isMenuListOpen = false
            
            self.viewSlideInFromTopToBottom(view: btn_navigate)
            self.viewSlideInFromTopToBottom(view: btn_lock)
            self.viewSlideInFromTopToBottom(view: btn_share)
            self.viewSlideInFromTopToBottom(view: btn_traffic)
            self.viewSlideInFromTopToBottom(view: currentLocBtn)
            self.viewSlideInFromTopToBottom(view: parkingBtn)
            self.viewSlideInFromTopToBottom(view: shareLbl)
            self.viewSlideInFromTopToBottom(view: shareLocLbl)
            self.viewSlideInFromTopToBottom(view: cutOnOffLbl)
            self.viewSlideInFromTopToBottom(view: parkingOnOffLbl)
            self.viewSlideInFromTopToBottom(view: nearVehicleLbl)
            self.viewSlideInFromTopToBottom(view: trafficSignalLbl)
            

            self.btn_navigate.isHidden = true
            self.btn_lock.isHidden = true
            self.btn_share.isHidden = true
            self.btn_traffic.isHidden = true
            self.currentLocBtn.isHidden = true
            self.parkingBtn.isHidden = true
            self.shareLbl.isHidden = true
            self.shareLocLbl.isHidden = true
            self.cutOnOffLbl.isHidden = true
            self.parkingOnOffLbl.isHidden = true
            self.nearVehicleLbl.isHidden = true
            self.trafficSignalLbl.isHidden = true
            
        }
    }

 
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        view.layer.add(transition, forKey: kCATransition)
    }
    
    func viewSlideInFromBottomToTop(view: UIView) -> Void {
           let transition:CATransition = CATransition()
           transition.duration = 1.0
           transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
           transition.type = CATransitionType.push
           transition.subtype = CATransitionSubtype.fromRight
           view.layer.add(transition, forKey: kCATransition)
    }
    
    @IBAction func pressed_map(_ sender: Any) {
        is_queue = true
        CallDataRepeatedly()
        multiple_views[1].tag = 1
        btn_view_change[0].tag = 0
        self.lbl_header_name.text = self.bd.localizedString(forKey: "TRACKING", value: nil, table: nil)
        lbl_header_name.text = "Tracking"
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 0, viewTagValue: 1, selectedImgArr: selectedMenuImages, unselectedImgArr: unselectedMenuImages)
    }
    
    @IBAction func pressed_notification(_ sender: Any) {
        is_queue = false
        multiple_views[3].tag = 3
        btn_view_change[3].tag = 3
        self.lbl_header_name.text = self.bd.localizedString(forKey: "ALERTS", value: nil, table: nil)
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 3, viewTagValue: 3, selectedImgArr: selectedMenuImages, unselectedImgArr: unselectedMenuImages)
    }
    
    @IBAction func pressed_monitor(_ sender: Any) {
        is_queue = false
        multiple_views[4].tag = 4
        btn_view_change[4].tag = 4
        let titleText = "Update " + self.bd.localizedString(forKey: "VEHICLE", value: nil, table: nil)
        self.lbl_header_name.text = titleText
        guard let MaplocationController = children[3] as? UpdateVehicleVC else  {
            fatalError("Check storyboard for missing SuccessVC \(String(describing: children[3]))")
        }
        
        updateVehicle = MaplocationController
        updateVehicle!.SetDataOnFields(registration_no: registration[0], voice_no: voice_no[0], device_tag: device_tag[0], device_serial: device_serial[0], device_id: self.device_id!, vehicle_type_id: vehicle_type_id[0], driver_name: self.driverName.string, driver_no: self.driverNo.string, driver_id: self.driverId)
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 4, viewTagValue: 4, selectedImgArr: selectedMenuImages, unselectedImgArr: unselectedMenuImages)
    }
    
    
    @IBAction func presses_report(_ sender: Any) {
        is_queue = false
        multiple_views[0].tag = 0
        btn_view_change[1].tag = 1
        self.lbl_header_name.text = self.bd.localizedString(forKey: "TRIPS", value: nil, table: nil)
        //lbl_header_name.text = "Trips"
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 1, viewTagValue: 0, selectedImgArr: selectedMenuImages, unselectedImgArr: unselectedMenuImages)
    }
    
    
    @IBAction func pressed_status(_ sender: Any) {
        is_queue = false
        multiple_views[2].tag = 2
        btn_view_change[2].tag = 2
        self.lbl_header_name.text = self.bd.localizedString(forKey: "MAP_HISTORY", value: nil, table: nil)
       // lbl_header_name.text = "Map History"
        DidFunctionThings(buttonName: btn_view_change, viewName: multiple_views, buttonTagValue: 2, viewTagValue: 2, selectedImgArr: selectedMenuImages, unselectedImgArr: unselectedMenuImages)
    }
    
    
    @IBAction func select_map_style(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            showMap.mapType = .normal
            
        case 1:
            showMap.mapType = .hybrid
            
        default:
            showMap.mapType = .satellite
        }
    }
    
    
    @IBAction func date_time_done(_ sender: Any) {
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
            // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        view_date_and_time.isHidden = true
        self.view.addSubview(alert_view)
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        //print("lslklks \(domain_name) \(hash_key) \(user_name) \(device_id!) \(date_time!)")
        let urlString1 = domain_name + "/vehicle_result.php?action=temp_access&device_id=" + device_id!
        let urlString = urlString1 + "&phone_val=&email_val=&data_format=1&user_name=" + user_name + "&hash_key=" + hash_key + "&validity_date=" + date_time
        
        let encodedString = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: " ").inverted)
        
        NetworkManager().CallShareDataFromServer(urlString: encodedString!, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                let url = data!["tempurl"] as! String
                
                let items = [url];
                let activity = UIActivityViewController(activityItems: items, applicationActivities: nil);
                self.present(activity, animated: true, completion: nil)
                
            }else{
                showToast(controller: self, message : self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallDataFromServerForIngnition(isDidLoad: Bool)
    {
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
            //self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        is_queue = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString = domain_name + "/device_command_result.php?action=app_checkmobilize&data_format=1&device_id=" + self.device_id! + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallNotificatioSettingDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    let command_type = val_data["command_type"] as! String
                    if command_type == "1100"{
                        
                        self.btn_lock.setImage(UIImage(named: "lock_icon"), for: .normal)
                    }else{
                        self.btn_lock.setImage(UIImage(named: "unlock_icon"), for: .normal)
                    }
                    self.change_command_type = command_type
                }
                
            }else{
                if isDidLoad{
                    showToast(controller: self, message : self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
                }
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                if isDidLoad{
                    showToast(controller: self, message :self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
                }
            }
            
            self.is_queue = true
            //self.CallDataRepeatedly()
        })
        
    }
    
    @objc func pressed_zoom_in(){
        
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom + 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
        //et camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        //self.showMap.animate(to: camera)
    }
    
    @objc func pressed_zoom_out(){
        let loc : CLLocation = CLLocation(latitude: latitude[0], longitude: longitude[0])
        let zoom_in = self.showMap.camera.zoom - 2
        let camera = GMSCameraPosition.camera(withTarget: loc.coordinate, zoom: zoom_in)
        self.showMap!.camera = camera
    }
    
    @objc func openMapForPlace() {
        let latitude: CLLocationDegrees = self.latitude[0]
        let longitude: CLLocationDegrees = self.longitude[0]
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        // mapItem.name = "Place Name"
        mapItem.openInMaps(launchOptions: options)
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        date_time = dateFormatter.string(from: sender.date)
    }
    
    @objc func cross_pressed(){
        txt_password.text = ""
        viewSlideInFromBottomToTop(view: view_password)
        view_password.isHidden = true
    }
    
    @objc func CallIgnitionData(){
        self.viewSlideInFromTopToBottom(view: view_password)
        view_password.isHidden = false
    }
    
    @objc func showNearestVehicles(){
           
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "NearestVehicleVC") as! NearestVehicleVC
        // Init popup view controller with content is your content view controller
        contentVC.deviceId = self.device_id
        contentVC.coordinate = self.coordinateVal.string
        // let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
        let popupVC = PopupViewController(contentController: contentVC, position: .top(60.0), popupWidth: contentVC.view.frame.size.width-20, popupHeight:contentVC.view.frame.size.height/1.4)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
    }
    
    @IBAction func showParkingAlert(_ sender: Any) {
        
        if self.vehicle_moving_status == 0 || self.vehicle_moving_status == 4
        {
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SetParkingVC") as! SetParkingAlertVC
             // Init popup view controller with content is your content view controller
        contentVC.device_id = self.device_id
        contentVC.lat = self.vehicleLat
        contentVC.long = self.vehicleLong
        contentVC.parking_status = self.parkingStatus
        contentVC.vehicle_status = self.vehicle_moving_status
        contentVC.delegate = self
        let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
        let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize), popupWidth: contentVC.view.frame.size.width-20, popupHeight:contentVC.view.frame.size.height/3)
        popupVC.cornerRadius = 20
             // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
        }
        else
        {
            if self.vehicle_moving_status == 2 {
                showToast(controller: self, message: "Currently your vehicle is in moving condition. This feature is not available in this condition", seconds: 3.0)
            }
            else
            {
               showToast(controller: self, message: "Currently your vehicle is in Idle condition. This feature is not available in this condition", seconds: 3.0)
            }
        }
    }
    
    @objc func CallPasswordData(){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
            // self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        is_queue = false
        if txt_password.text!.count < 1{
            showToast(controller: self, message: "Password Can't be empty", seconds: 2.0)
            return
        }
        
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString = domain_name + Authenticate_Url_Username + user_name + Authenticate_Url_Password + txt_password.text! + "&user_app_id=" + device_token
        
        NetworkManager().CallWebService(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                // print(String(describing: data))
                let result = data?[K_Result] as! Int
                print(result)
                switch (result){
                case 0 :
                    if self.change_command_type == "1100"{
                        self.change_command_type = "1101"
                    }else{
                        self.change_command_type = "1100"
                    }
                    self.UpdateIgnitionData()
                    break
                case 2 :
                    showToast(controller: self, message: self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
                    break
                    
                default:
                    print("Default Case")
                }
            }else{
                showToast(controller: self, message: self.bd.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil), seconds: 2.0)
            }
            self.is_queue = true
            self.CallDataRepeatedly()
        })
    }
    
    func UpdateIgnitionData(){
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
            self.alert_view.removeFromSuperview()
            return
        }
        
        is_queue = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        
        let urlString1  =  domain_name + Update_Ignition + String(change_command_type)
        let urlString2 = urlString1 + "&data_format=1&vehicle=" + self.device_id! + "&user_name=" + user_name
        let urlString = urlString2 + "&hash_key=" + hash_key
  
        NetworkManager().CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.txt_password.text = ""
                self.view_password.isHidden = true
                showToast(controller: self, message : data!, seconds: 2.0)
                if self.btn_lock.currentImage == UIImage(named: "lock_icon") {
                    self.btn_lock.setImage(UIImage(named: "unlock_icon"), for: .normal)
                } else{
                    self.btn_lock.setImage(UIImage(named: "lock_icon"), for: .normal)
                }
            }else{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            self.is_queue = true
            self.CallDataRepeatedly()
        })
    }
    
    
    @objc func btn_traffic_pressed() {
        if showMap.isTrafficEnabled {
            showMap.isTrafficEnabled = false
            btn_traffic.setImage(UIImage(named: "traffic_off_new"), for: .normal)
        }else{
            showMap.isTrafficEnabled = true
            btn_traffic.setImage(UIImage(named: "traffic_on_new"), for: .normal)
        }
    }
    
    @objc func ShareInfoToUser() {
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareLocationVC") as! ShareLiveLocationVC
        contentVC.device_id = self.device_id
        // Init popup view controller with content is your content view controller
        let frameSize: CGPoint = CGPoint(x:0.0,y: 20.0)
        let popupVC = PopupViewController(contentController: contentVC, position: .center(frameSize), popupWidth: contentVC.view.frame.size.width-20, popupHeight:250)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
    }
    
    func CallDataRepeatedly(){
        
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: self.bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil), seconds: 1.5)
            self.tbl_vehicle_data.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        if self.vehicals!.count > 0
        {
            self.vehicals?.removeAll()
        }
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        
        let url = domain_name + "/tracking_api.php"
        let parameters : Dictionary<String, Any> = [
            "action" : "track",
            "user_name" : user_name,
            "hash_key" : hash_key,
            "device_id": self.device_id!
        ]
        print("tracking page URL: \(url), parameters:\(parameters)")
        NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as? Int ?? 100
                DispatchQueue.main.async {
                   dispatchGroup.leave()   // <<----
                }
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    print("live track data:\(c_data)")
                    self.vehicals = (c_data[VEHICALS] as! Array<Any>)
                    //self.SetAllData()
                    break
                    
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                //showToast(controller: self, message: "Please check your Internet Connection.", seconds: 0.3)
                print("ERROR FOUND")
            }
            
            if self.is_queue{
                DispatchQueue.main.asyncAfter(deadline: .now() + 20.0) {
                    [weak self] in
                    if self?.is_queue != nil && (self?.is_queue)! {
                        self!.CallDataRepeatedly()
                    }
                }
            }
        })
        
        
        dispatchGroup.enter()   // <<---

        let url_location = domain_name + "/tracking_api.php"
        
        let parameters_new : Dictionary<String, Any> = [
            "action" : "extra",
            "user_name" : user_name,
            "hash_key" : hash_key,
            "device_id": self.device_id!
        ]
        
        distanceVal = NSMutableAttributedString(string: "")
        locationArr.removeAll()
        distanceArr.removeAll()

        NetworkManager().CallNewTrackResult(urlString: url_location, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
            
            let result = data?[K_Result] as? Int ?? 100
            print("result:\(result)")
            dispatchGroup.leave()
            switch (result){
                
            case 0 :
                
                let c_data = data?[K_Data] as!  Dictionary<String,Any>
                
                if let loc_data = c_data["vehicle_data"] as? [Dictionary<String, Any>]
                {
                    self.locationArr = loc_data.map({$0 ["place"] as! String})
                    self.distanceArr = loc_data.map({$0 ["today_distance"] as? Double ?? 0.0})
                }
                
                print("loc arr:\(self.locationArr), distance arr:\(self.distanceArr)")
                let c_distance = String(self.distanceArr[0])
                let s_c_distance = NSMutableAttributedString(string: c_distance + " Km")
                self.distanceVal = s_c_distance
                
                let c_location = self.locationArr[0]
                self.locationVal = c_location
                
                break
            case 2 :
                let message = data?[K_Message] as! String
                print(message)
                break
            default:
                print("Default Case")
            }
            
        })
        
        dispatchGroup.notify(queue: .main) {
            print("both api done in tracking page")
            self.SetAllData()
        }
        
    }
    
    
    @objc func GetTrackingData(notification: Notification){
        let c_data = notification.userInfo
        vehicals = (c_data![VEHICALS] as! Array<Any>)
        SetAllData()
    }
    
    
    func SetAllData(){
        
        print("seting data")
        latitude.removeAll()
        longitude.removeAll()
        registration.removeAll()
        status.removeAll()
        direction.removeAll()
        vehicle_type_id.removeAll()
        
        speedVal = NSMutableAttributedString(string: "")
        haltTime = NSMutableAttributedString(string: "")
        driverName = NSMutableAttributedString(string: "")
        driverNo = NSMutableAttributedString(string: "")
        coordinateVal = NSMutableAttributedString(string: "")
        fuelVal = NSMutableAttributedString(string: "")
        temperatureVal = NSMutableAttributedString(string: "")
        odometerVal = NSMutableAttributedString(string: "")
        
        for (_, loc) in (vehicals?.enumerated())! {
            
            let val_data = loc as! Dictionary<String, Any>
            self.latitude.append(GetLatitudeFromData(Vehicals : val_data))
            self.longitude.append(GetLonitudeFromData(Vehicals: val_data))
            registration.append(GetRegistrationNumber(Vehicals: val_data))
            status.append(GetDeviceStatus(Vehicals: val_data))
            vehicle_type_id.append(GetVehicleTypeId(Vehicals: val_data))
            device_tag.append(val_data["tag"] as? String ?? "")
            device_serial.append(val_data["serial"] as? String ?? "")
            voice_no.append(val_data["voice_no"] as? String ?? "")
            let angle = Double(val_data["direction"] as? String ?? "")
            self.direction.append(angle!)
            
            
            vehicle_data.removeAll()
            data_heading.removeAll()
            imageName.removeAll()
            
            if let speedwidkey = val_data["speed"] as? String {
                let s_c_speed = NSMutableAttributedString(string: speedwidkey + " Km/h")
                speedVal = s_c_speed
                vehicle_data.append(s_c_speed)
                data_heading.append("Speed: ")
                imageName.append("speed")
            }
            
            let latitude = String(GetLatitudeFromData(Vehicals : val_data))
            let longitude = String(GetLonitudeFromData(Vehicals: val_data))
            let s_c_lat_long = NSMutableAttributedString(string: latitude + ", " + longitude)
            
            vehicleLat = GetLatitudeFromData(Vehicals : val_data)
            vehicleLong = GetLonitudeFromData(Vehicals: val_data)
            
            vehicle_data.append(s_c_lat_long)
            coordinateVal = s_c_lat_long
            data_heading.append("Coordinates: ")
            imageName.append("coordinate")
            
            if let halt_time = val_data["status_time"] as? String {
                if halt_time.count > 0{
                    let s_c_halt_time = NSMutableAttributedString(string: halt_time)
                    vehicle_data.append(s_c_halt_time)
                    haltTime = s_c_halt_time
                    data_heading.append("Halt Time: ")
                    imageName.append("halt_time")
                }
            }
       
            if let last_update_time = val_data["last_update"] as? String {
                  lastUpdateTime = last_update_time
            }
            
            if let fuel = val_data["fuel_level"] as? String {
                let s_c_fuel = NSMutableAttributedString(string: fuel)
                let fuel_val_perc = NSMutableAttributedString(string: fuel + " %")
                let fuel_val_ltr = NSMutableAttributedString(string: fuel + "Ltr")
                vehicle_data.append(s_c_fuel)
                data_heading.append("Fuel: ")
                if s_c_fuel.string == "0" {
                 fuelVal = fuel_val_perc
                }
                else
                {
                    if let fuel_tank_cap = val_data["fuel_tank_capacity"] as? String, let device_type_id = val_data["device_type_id"] as? String
                    {
                        let s_c_fuel_tank_cap = NSMutableAttributedString(string: fuel_tank_cap)
                        
                        if s_c_fuel_tank_cap.string == "0"
                        {
                            fuelVal = fuel_val_perc
                        }
                        else
                        {
                            if device_type_id == "133"
                            {
                                fuelVal = fuel_val_ltr
                            }
                            else
                            {
                                let fuel_val = Double(fuel)! * Double(fuel_tank_cap)!
                                let fuel_val_short = fuel_val.truncate(to: 3)
                                let final_fuel_val = String(fuel_val_short/100)
                                let final_fuel_ltr = NSMutableAttributedString(string: final_fuel_val + "Ltr")
                                fuelVal = final_fuel_ltr
                            }
                        }
                        
                    }
                }
                imageName.append("fuel")
            }
            
            if let temperature = val_data["temperature"] as? String{
                let s_c_temprature = NSMutableAttributedString(string: temperature)
                vehicle_data.append(s_c_temprature)
                data_heading.append("Temperature: ")
    
                if Double(temperature)! > 6000.0
                {
                    let temperature = Double(temperature)! - 6553.3
                    let temp_val = NSMutableAttributedString(string: String(temperature))
                    temperatureVal = temp_val
                }
                else
                {
                    temperatureVal = s_c_temprature
                }
                
                imageName.append("temperature")
            }
            
            if let gps_odometer = val_data["odo"] as? String {
                let s_c_gps_odometer = NSMutableAttributedString(string: gps_odometer)
                vehicle_data.append(s_c_gps_odometer)
                data_heading.append("Odometer: ")
                odometerVal = s_c_gps_odometer
                imageName.append("odometer")
            }
            
            if let driver_name = val_data["driver_name"] as? String{
                let s_c_driver_name = NSMutableAttributedString(string: driver_name)
                vehicle_data.append(s_c_driver_name)
                data_heading.append("Driver Name: ")
                driverName = s_c_driver_name
                imageName.append("driver_name")
            }
            
            if let driver_mobile = val_data["driver_mobile"] as? String{
                let s_c_driver_mobile = NSMutableAttributedString(string: driver_mobile)
                vehicle_data.append(s_c_driver_mobile)
                data_heading.append("Driver Mobile: ")
                driverNo = s_c_driver_mobile
                imageName.append("driver_mobile")
            }
            
            if let parking_mode_status = val_data["parking"] as? String {
                print("parking mode status:\(parking_mode_status)")
                if parking_mode_status == "1"
                {
                    self.parkingStatus = "ON"
                    DispatchQueue.main.async {
                        self.parkingBtn.setImage(UIImage.init(named: "parking_act"), for: .normal)
                    }
                }
                else
                {
                    self.parkingStatus = "OFF"
                    DispatchQueue.main.async {
                        self.parkingBtn.setImage(UIImage.init(named: "park_inactive"), for: .normal)
                    }
                }
            }
                       
            if let driver_id =  val_data ["driver_id"] as? String
            {
                self.driverId = driver_id
            }
            
            if haltTime.length <= 2 && fuelVal.string == "0 %" && temperatureVal.length <= 1 && odometerVal.length <= 1 && driverName.length < 1
            {
                print("in 1")
                bottomViewHieght = 200
                popupOffset = 150
            }
                
            else if (haltTime.length <= 2 && fuelVal.string == "0 %" && driverName.length > 1 && temperatureVal.length <= 1 && odometerVal.length <= 1) || (haltTime.length >= 2 && driverName.length < 1 && temperatureVal.length <= 1 && odometerVal.length <= 1) || (fuelVal.string != "0 %" && driverName.length < 1 && temperatureVal.length <= 1 && odometerVal.length <= 1) || (fuelVal.string != "0 %" && haltTime.length <= 2 && driverName.length < 1 && temperatureVal.length >= 1) || (fuelVal.string != "0 %" && haltTime.length <= 2 && driverName.length < 1 && odometerVal.length >= 1 )
            {
                print("in 2")
                bottomViewHieght = 250
                popupOffset = 200
            }
                
            else if ((haltTime.length >= 2 || fuelVal.string != "0 %") && driverName.length > 1 && ((odometerVal.length >= 1 && odometerVal.string !=  "0") || (temperatureVal.length >= 1 && temperatureVal.string != "0") ))
            {
                print("in 3 odometer val:\(String(describing: odometerVal)), temp val:\(String(describing: temperatureVal))")
                bottomViewHieght = 350
                popupOffset = 300
            }
                
            else
            {
                print("in 4")
                bottomViewHieght = 300
                popupOffset = 250
            }
            
            DispatchQueue.main.async {
                self.updateLayout()
                self.tbl_vehicle_data.delegate = self
                self.tbl_vehicle_data.dataSource = self
                self.tbl_vehicle_data.reloadData()
                self.alert_view.removeFromSuperview()
            }
        }
        
        
        SetDataOnMAp()
        playCar()
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    
}


class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if (self.state == UIGestureRecognizer.State.began) { return }
        super.touchesBegan(touches, with: event)
        self.state = UIGestureRecognizer.State.began
    }
    
}

extension ShowTrackingInfo {
    
    
    func SetDataOnMAp() {
        //------------------------Add New Annotations ---------------------------//
        let point = MyCustomPointAnnotation()
        point.coordinate = CLLocationCoordinate2D(latitude: self.latitude[0], longitude: longitude[0])
        var vehicle_name = "car"
        
        if vehicle_type_id[0] == "73"{
            vehicle_name = "tractor"
        }
        else if vehicle_type_id[0] == "74"{
            vehicle_name = "roadroller"
        }
        else if vehicle_type_id[0] == "75"{
            vehicle_name = "cementmixer"
        }
        else if vehicle_type_id[0] == "56"{
            vehicle_name = "pcr"
        }else if vehicle_type_id[0] == "1"{
            vehicle_name = "car"
        }else if vehicle_type_id[0] == "2"{
            vehicle_name = "truck"
        }else if vehicle_type_id[0] == "3"{
            vehicle_name = "bus"
        }else if vehicle_type_id[0] == "4"{
            vehicle_name = "van"
        }else if vehicle_type_id[0] == "5"{
            vehicle_name = "car"
        }else if vehicle_type_id[0] == "6"{
            vehicle_name = "bike"
        }else if vehicle_type_id[0] == "7"{
            vehicle_name = "scooty"
        }
        else if vehicle_type_id[0] == "58"{
        vehicle_name = "ambulance"
        }
        else{
        vehicle_name = "car"
        }
                
        if status[0] == 0 {
            imageNamed = vehicle_name + "_red_map"
            vehicle_moving_status = 0
        }else if status[0] == 1{
            imageNamed = vehicle_name + "_yellow_map"
            vehicle_moving_status = 1
        }else if status[0] == 2{
            imageNamed = vehicle_name + "_green_map"
            vehicle_moving_status = 2
        }else if status[0] == 4{
            imageNamed = vehicle_name + "_black_map"
            vehicle_moving_status = 4
        }
        print("selected image:\(imageNamed)")
    }
    
    // ---------------------------------------- Google Map ------------------------------------------//
    
    @objc func drawPathOnMap() {
        path.add(CLLocationCoordinate2DMake(latitude[index], longitude[index]))
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0
        
        let styles = [GMSStrokeStyle.solidColor(.clear),
                      GMSStrokeStyle.solidColor(.black)]

        let lengths: [NSNumber] = [10, 10]
        rectangle.spans = GMSStyleSpans(rectangle.path!, styles, lengths,GMSLengthKind.rhumb)
       // marker.position = destination
      //  marker.map = showMap
        rectangle.map = showMap
    }
    
    
    func playCar()
    {
       // print("index of location is :\(String(describing: index))")
        let loc : CLLocation = CLLocation(latitude:latitude[0] , longitude: longitude[0])
        updateMapFrame(newLocation: loc, zoom: self.showMap.camera.zoom)
        if previousLocation == nil
        {
            previousLocation = loc
        }
        marker.icon = UIImage(named: imageNamed)
        marker.setIconSize(scaledToSize: .init(width: 20, height: 40))
        marker.map = showMap
     
        animate(from: previousLocation.coordinate, to: loc.coordinate)
        print("current loc:\(loc.coordinate.latitude), previous loc:\(previousLocation.coordinate.latitude)")
      //  showLiveTracking(from: previousLocation.coordinate, to: loc.coordinate)
        previousLocation = loc
       // drawPathOnMap()
    }
    
   
    
    func showLiveTracking(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D)
    {
       //  marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
         marker.rotation =  CLLocationDegrees(getHeadingForDirection(fromCoordinate: source, toCoordinate: destination))
        //found bearing value by calculation when marker add
        marker.position = source
        //this can be old position to make car movement to new position
        marker.map = showMap
        //marker movement animation
        CATransaction.begin()
        marker.icon = UIImage(named: imageNamed)
        marker.setIconSize(scaledToSize: .init(width: 20, height: 40))
        CATransaction.setValue(Int(2.0), forKey: kCATransactionAnimationDuration)
    
        CATransaction.setCompletionBlock({() -> Void in
            self.marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            self.marker.rotation = self.direction[0]
            let bounds = GMSCoordinateBounds(path: self.path)
            self.showMap!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 50.0))
           // self.updateMapFrame(newLocation: loc1, zoom: self.showMap.camera.zoom)
           // let mapBearing = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: source, toCoordinate: destination))
          //  self.showMap.animate(toBearing: mapBearing)
            //New bearing value from backend after car movement is done
        })
        
        path.add(CLLocationCoordinate2DMake(latitude[0], longitude[0]))
        
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0
        rectangle.map = showMap

        let styles = [GMSStrokeStyle.solidColor(.clear),
                      GMSStrokeStyle.solidColor(.black)]

        let lengths: [NSNumber] = [10, 10]
        rectangle.spans = GMSStyleSpans(rectangle.path!, styles, lengths,GMSLengthKind.rhumb)
        marker.position = destination
        
       // marker.map = showMap
    
        //let cameraPos = GMSCameraPosition.init(target: destination, zoom: self.showMap.camera.zoom)
       // self.showMap.animate(to: cameraPos)
        CATransaction.commit()
    }
    
    
    func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance
    {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
    
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
    

    func bearing(fromPoint:CLLocationCoordinate2D, to point: CLLocationCoordinate2D) -> Double {
           func degreesToRadians(_ degrees: Double) -> Double { return degrees * Double.pi / 180.0 }
           func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / Double.pi }
           
           let fromLatitude = degreesToRadians(fromPoint.latitude)
           let fromLongitude = degreesToRadians(fromPoint.longitude)
           
           let toLatitude = degreesToRadians(point.latitude)
           let toLongitude = degreesToRadians(point.longitude)
           
           let differenceLongitude = toLongitude - fromLongitude
           
           let y = sin(differenceLongitude) * cos(toLatitude)
           let x = cos(fromLatitude) * sin(toLatitude) - sin(fromLatitude) * cos(toLatitude) * cos(differenceLongitude)
           let radiansBearing = atan2(y, x);
           let degree = radiansToDegrees(radiansBearing)
           return (degree >= 0) ? degree : (360 + degree)
       }
    
    
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        
        let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
        self.showMap.animate(to: camera)
    }
    
    func animate(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        
        // Keep Rotation Short
        CATransaction.begin()
        CATransaction.setAnimationDuration(1)
        marker.rotation =  CLLocationDegrees(getHeadingForDirection(fromCoordinate: source, toCoordinate: destination))
        // marker.rotation = direction[index]
        marker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
        CATransaction.commit()
        
      /*
        DispatchQueue.main.async {
            UIView.animate(withDuration: 3.0, delay: 1.0, options: [.allowAnimatedContent], animations: {
                // drawRoute(path: path, animated: true)
                let t = GMSGeometryInterpolate(source, destination, 1.0)
                self.path.add(t)
                
                self.rectangle = GMSPolyline(path: self.path)
                self.rectangle.strokeWidth = 5.0
                let styles = [GMSStrokeStyle.solidColor(.clear),
                              GMSStrokeStyle.solidColor(.black)]
                let lengths: [NSNumber] = [10, 10]
                self.rectangle.spans = GMSStyleSpans(self.rectangle.path!, styles, lengths,GMSLengthKind.rhumb)
                
                self.marker.position = destination
                //  marker.map = showMap
                self.rectangle.map = self.showMap
                // Center Map View
                // let camera = GMSCameraPosition.camera(withTarget: destination, zoom: self.showMap.camera.zoom)
                // self.showMap.animate(to: camera)
                let camera = GMSCameraUpdate.setTarget(destination)
                self.showMap.animate(with: camera)
            }, completion: { _ in
                 self.marker.rotation = self.direction[self.index]
            })
        }
        
     */
        
        // Movement
        CATransaction.begin()
        CATransaction.setAnimationDuration(3)
        CATransaction.setCompletionBlock({
            // you can do something here
            self.marker.rotation = self.direction[0]
            //self.drawRoute(path: self.path, animated: true)
        })
        
        let t = GMSGeometryInterpolate(source, destination, 1.0)
        path.add(t)
        
        rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0
        let styles = [GMSStrokeStyle.solidColor(.clear),
                      GMSStrokeStyle.solidColor(.black)]
        let lengths: [NSNumber] = [10, 10]
        rectangle.spans = GMSStyleSpans(rectangle.path!, styles, lengths,GMSLengthKind.rhumb)
        
        marker.position = destination
        //  marker.map = showMap
        rectangle.map = showMap
         
        // Center Map View
        // let camera = GMSCameraPosition.camera(withTarget: destination, zoom: self.showMap.camera.zoom)
        // self.showMap.animate(to: camera)
        let camera = GMSCameraUpdate.setTarget(destination)
        showMap.animate(with: camera)
        CATransaction.commit()
 
 }
    
    
    
    //10/04/20
    
    func animate(route: (GMSMutablePath), duration: TimeInterval, destination:CLLocationCoordinate2D, completion: (() -> Void)?) {
            guard route.count() > 0 else { return }
            var currentStep = 1
            let totalSteps = route.count()
            let stepDrawDuration = duration/TimeInterval(totalSteps)
            var previousSegment: GMSPolyline?
            
            drawingTimer = Timer.scheduledTimer(withTimeInterval: stepDrawDuration, repeats: true) { [weak self] timer in
                guard let self = self else {
                    // Invalidate animation if we can't retain self
                    timer.invalidate()
                    completion?()
                    return
                }
                
                if let previous = previousSegment {
                    // Remove last drawn segment if needed.
                    previousSegment?.map = nil
                    previousSegment = nil
                }
                
                guard currentStep < totalSteps else {
                    // If this is the last animation step...
                    let finalPolyline = GMSPolyline(path: route)
                    finalPolyline.map = self.showMap
                    // Assign the final polyline instance to the class property.
                    self.polyline = finalPolyline
                    timer.invalidate()
                    completion?()
                    return
                }
                
                // Animation step.
                // The current segment to draw consists of a coordinate array from 0 to the 'currentStep' taken from the route.
               // let subCoordinates = Array(route.prefix(upTo: currentStep))
              //  let currentSegment = MKPolyline(coordinates: subCoordinates, count: subCoordinates.count)
              //  self.mapView.addOverlay(currentSegment)
                
              //  previousSegment = currentSegment
                currentStep += 1
            }
        }
    
    
    //Extra code
    func drawRoute(path: GMSMutablePath, animated: Bool) {
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.strokeColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        polyline.map = showMap
        if(animated){
            self.animatePolylinePath(path: path)
        }
    }
    
    
    func animatePolylinePath(path: GMSMutablePath) {
        var pos: UInt = 0
        var animationPath = GMSMutablePath()
        let animationPolyline = GMSPolyline()
        //  self.timer = Timer.scheduledTimer(withTimeInterval: 0.003, repeats: true) { timer in
        pos += 1
        if(pos >= path.count()) {
            pos = 0
            animationPath = GMSMutablePath()
            animationPolyline.map = nil
        }
        
        animationPath.add(path.coordinate(at: pos))
        animationPolyline.path = animationPath
        animationPolyline.strokeColor = UIColor.yellow
        animationPolyline.strokeWidth = 3
        animationPolyline.map = self.showMap
    //  }
    }
    
    
    // 09/04/20 (Trying smooth vehicle movement)
    func layer(from path: GMSPath, from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) -> CAShapeLayer {
        let breizerPath = UIBezierPath()
        breizerPath.move(to: self.showMap.projection.point(for: source))
        for i in 1 ..< Int((path.count())){
            print(path.coordinate(at: UInt(i)))
            let coordinate: CLLocationCoordinate2D = path.coordinate(at: UInt(i))
            breizerPath.addLine(to: self.showMap.projection.point(for: coordinate))
        }
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = breizerPath.reversing().cgPath
        shapeLayer.strokeColor = UIColor.green.cgColor
        shapeLayer.lineWidth = 4.0
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.cornerRadius = 5
        return shapeLayer
    }

    
    func animatePath(_ layer: CAShapeLayer) {
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 6
        //pathAnimation.delegate = self
        pathAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        pathAnimation.fromValue = Int(0.0)
        pathAnimation.toValue = Int(1.0)
        pathAnimation.repeatCount = 100
        layer.add(pathAnimation, forKey: "strokeEnd")
    }
    
    
    
}


extension CLLocationCoordinate2D {
    
    func bearing(to point: CLLocationCoordinate2D) -> Double {
        func degreesToRadians(_ degrees: Double) -> Double { return degrees * Double.pi / 180.0 }
        func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / Double.pi }
        
        let fromLatitude = degreesToRadians(latitude)
        let fromLongitude = degreesToRadians(longitude)
        
        let toLatitude = degreesToRadians(point.latitude)
        let toLongitude = degreesToRadians(point.longitude)
        
        let differenceLongitude = toLongitude - fromLongitude
        
        let y = sin(differenceLongitude) * cos(toLatitude)
        let x = cos(fromLatitude) * sin(toLatitude) - sin(fromLatitude) * cos(toLatitude) * cos(differenceLongitude)
        let radiansBearing = atan2(y, x);
        let degree = radiansToDegrees(radiansBearing)
        return (degree >= 0) ? degree : (360 + degree)
    }
}

extension CLLocationCoordinate2D : Equatable{
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
    //distance in meters, as explained in CLLoactionDistance definition
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination=CLLocation(latitude:from.latitude,longitude:from.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }
}

extension ShowTrackingInfo: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let info_cell = tbl_vehicle_data.dequeueReusableCell(withIdentifier: "Vehicle_Data_Cell") as! Vehicle_Data_Cell
        info_cell.lbl_data.attributedText = speedVal ?? emptyString
        info_cell.lbl_distance.attributedText = distanceVal ?? emptyString
        info_cell.lbl_haltTime.attributedText = haltTime ?? emptyString
        info_cell.lbl_fuel.attributedText = fuelVal ?? emptyString
        
        let speedIcon = info_cell.img_speed.image?.imageWithColor(color: .systemRed)
        info_cell.img_speed.image = speedIcon
        
        let distanceIcon = info_cell.img_distance.image?.imageWithColor(color: .systemBlue)
        info_cell.img_distance.image = distanceIcon
        
        let purpleIcon = info_cell.img_coordinate.image?.imageWithColor(color: .systemPurple)
        info_cell.img_coordinate.image = purpleIcon
        
        let haltIcon = info_cell.img_haltTime.image?.imageWithColor(color: .systemYellow)
        info_cell.img_haltTime.image = haltIcon
        
        let locationIcon = info_cell.img_location.image?.imageWithColor(color: .systemGreen)
        info_cell.img_location.image = locationIcon
               
        if driverName.length > 1
        {
            info_cell.driverDetailStack.isHidden = false
            info_cell.lbl_driverName.attributedText = driverName
            info_cell.lbl_driverNo.attributedText = driverNo
        }
        else
        {
            info_cell.driverDetailStack.isHidden = true
        }
        
        if temperatureVal.length > 1 && odometerVal.length > 1
        {
            info_cell.lbl_odometer.attributedText = odometerVal
            info_cell.lbl_temperature.attributedText = temperatureVal
            info_cell.tempStack.isHidden = false
            info_cell.lbl_odometer.isHidden = false
            info_cell.img_odometer.isHidden = false
            info_cell.lbl_title_odometer.isHidden = false
            info_cell.lbl_temperature.isHidden = false
            info_cell.lbl_title_temp.isHidden = false
            info_cell.img_temp.isHidden = false
            
        }
        else if temperatureVal.length > 1 && odometerVal.length <= 1
        {
            info_cell.lbl_odometer.attributedText = odometerVal
            info_cell.lbl_temperature.attributedText = temperatureVal
            info_cell.tempStack.isHidden = false
            info_cell.lbl_odometer.isHidden = true
            info_cell.img_odometer.isHidden = true
            info_cell.lbl_title_odometer.isHidden = true
            info_cell.lbl_temperature.isHidden = false
            info_cell.lbl_title_temp.isHidden = false
            info_cell.img_temp.isHidden = false
        }
        else if temperatureVal.length <= 1 && odometerVal.length > 1
        {
            info_cell.lbl_odometer.attributedText = odometerVal
            info_cell.lbl_temperature.attributedText = temperatureVal
            info_cell.tempStack.isHidden = false
            info_cell.lbl_odometer.isHidden = false
            info_cell.img_odometer.isHidden = false
            info_cell.lbl_title_odometer.isHidden = false
            info_cell.lbl_temperature.isHidden = true
            info_cell.lbl_title_temp.isHidden = true
            info_cell.img_temp.isHidden = true
        }
        else if temperatureVal.length <= 1 && odometerVal.length <= 1
        {
            info_cell.tempStack.isHidden = true
        }
        
        if haltTime.length > 2 && fuelVal.string != "0 %"
        {
            info_cell.lbl_haltTime.attributedText = haltTime
            info_cell.lbl_fuel.attributedText = fuelVal
            info_cell.haltStackView.isHidden = false
            info_cell.lbl_haltTime.isHidden = false
            info_cell.img_haltTime.isHidden = false
            info_cell.lbl_title_haltTime.isHidden = false
            info_cell.lbl_fuel.isHidden = false
            info_cell.lbl_title_fuel.isHidden = false
            info_cell.img_fuel.isHidden = false
        }

        else if haltTime.length > 2 && fuelVal.string == "0 %"
        {
            info_cell.lbl_haltTime.attributedText = haltTime
            info_cell.lbl_fuel.attributedText = fuelVal
            info_cell.haltStackView.isHidden = false
            info_cell.lbl_haltTime.isHidden = false
            info_cell.img_haltTime.isHidden = false
            info_cell.lbl_title_haltTime.isHidden = false
            info_cell.lbl_fuel.isHidden = true
            info_cell.lbl_title_fuel.isHidden = true
            info_cell.img_fuel.isHidden = true
        }
        else if haltTime.length <= 2 && fuelVal.string !=  "0 %"
        {
            info_cell.lbl_haltTime.attributedText = haltTime
            info_cell.lbl_fuel.attributedText = fuelVal
            info_cell.haltStackView.isHidden = false
            info_cell.lbl_haltTime.isHidden = true
            info_cell.img_haltTime.isHidden = true
            info_cell.lbl_title_haltTime.isHidden = true
            info_cell.lbl_fuel.isHidden = false
            info_cell.lbl_title_fuel.isHidden = false
            info_cell.img_fuel.isHidden = false
        }
        else if haltTime.length <= 2 && fuelVal.string == "0 %"
        {
            info_cell.haltStackView.isHidden = true
        }
        
        let s_c_location = FixBoldBetweenText(firstString: locationVal, boldFontName: " on ", lastString: lastUpdateTime)
        
        info_cell.lbl_coordinate.attributedText = coordinateVal ?? emptyString
        
        info_cell.lbl_location.attributedText = s_c_location
        
        return info_cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if haltTime.length <= 2 && fuelVal.string == "0 %" && temperatureVal.length <= 1 && odometerVal.length <= 1 && driverName.length < 1
        {
            return 150.0
        }
        else if haltTime.length <= 2 && fuelVal.string == "0 %" && driverName.length > 1
        {
            return 190.0
        }
       
        return 290.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return tbl_vehicle_data.frame .size.height / 6.2
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = Vehicle_Data_Header.instanceFromNib()
        //view.view_content.layer.cornerRadius = view.view_content.frame.size.height / 18
        // view.view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        view.backgroundColor = UIColor.groupTableViewBackground
        view.lbl_registration.text = registration[0]
       // view.lbl_registration.addGestureRecognizer(panRecognizer)
        return view
    }
}


extension ShowTrackingInfo: IsShowWait{
    func isWait() -> Bool {
        return true
    }
}


 extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}

 extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension UIButton {
    
    func flash() {
    let flash = CABasicAnimation(keyPath: "opacity")
    flash.duration = 0.3
    flash.fromValue = 1
    flash.toValue = 0.1
    flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    flash.autoreverses = true
    flash.repeatCount = 2
    layer.add(flash, forKey: nil)
    }
}
