//
//  SuccessVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


protocol IsShowWait {
    func isWait() -> Bool
   
}

protocol SendingVehiclesActiveCount: class {
    func sendVehiclesData(data: Dictionary<String,Any>)
}


class SuccessVC: UIViewController {

    @IBOutlet weak var tbl_show_live_track: UITableView!
    @IBOutlet weak var sarch_bar_item: UISearchBar!
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var collection_item: UICollectionView!
    var vehicals : Array<Any>?
    var vehicleData : Dictionary<String, Any>?
     let alert_view = AlertView.instanceFromNib()
    var moving = false
    var idle = false
    var stopped = false
    var no_data = false
    var all = true
    var vehicle_name = "car"
    var filterdData : [VehicleDetail] = []
    var searchActive : Bool = false
    var data : [VehicleDetail] = []
    var currentDate = ""
    var moving_c = 00
    var stopped_c = 00
    var unreach_c = 00
    var idle_c = 00
    var vehicals_s : Array<Any>?
    var timer = Timer()
    var delegate : IsShowWait?
    var is_queue = false
    var isBackground = false
    lazy var refreshControl = UIRefreshControl()
    var kmText = ""
    var kmhText = ""
    var language = ""
    var bd:Bundle!
    
    //28/02/20
    var movingVehicleArr = [[String:Any]] ()
    var idleVehicleArr = [[String:Any]] ()
    var stoppedVehicleArr = [[String:Any]] ()
    var noDataVehicleArr = [[String:Any]] ()
    var allVehiclesDataArr = [[String:Any]] ()
    
    weak var countDelegate: SendingVehiclesActiveCount?
    
    var apiHitCount = 0
    
    var timeInterval = 40.0
    
    var all_vehicles_count = 0

    var locationArr: [String] = []
    var movVehLocArr = [String]()
    var idleVehLocArr = [String]()
    var stopVehLocArr = [String]()
    var noDtVehLocArr = [String]()
    
    var distanceArr: [Double] = []
    var movDistArr = [Double]()
    var idleDistArr = [Double]()
    var stopDistArr = [Double]()
    var noDistArr = [Double]()

    override func viewWillAppear(_ animated: Bool) {
        print("in success VC")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tbl_show_live_track.register(UINib(nibName: "TrackCell", bundle: nil), forCellReuseIdentifier: "TrackCell")
 
        NotificationCenter.default.addObserver(self, selector: #selector(AtBackground), name:
            UIApplication.didEnterBackgroundNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AtForGround), name:
            UIApplication.willEnterForegroundNotification, object: nil)
        let searchBarStyle = sarch_bar_item.value(forKey: "searchField") as? UITextField
        searchBarStyle?.clearButtonMode = .never
        
      //  refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
     //   self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
     //   tbl_show_live_track.addSubview(refreshControl)// not required when using UITableViewControl
        
        language = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : language)
        kmText =  bd.localizedString(forKey: "KM_TEXT", value: nil, table: nil)
        kmhText = bd.localizedString(forKey: "KMH_TEXT", value: nil, table: nil)
        sarch_bar_item.placeholder = bd.localizedString(forKey: "SEARCH", value: nil, table: nil)
        
       // sarch_bar_item.searchBarStyle = .minimal
        
      // tbl_show_live_track.reloadRows(at: tbl_show_live_track!.indexPathsForVisibleRows!, with: .none)

    }
    
    func animateTable() {
        tbl_show_live_track.reloadData()
        let cells = tbl_show_live_track.visibleCells
        let tableHeight: CGFloat = tbl_show_live_track.bounds.size.height
            
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
            
        var index = 0
            
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay:  0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
 
    @objc func refresh(sender:AnyObject) {
       // Code to refresh table view
        CallTrakingDataFromServer()
    }
    
    func CheckIsHidden(isHidden_V: Bool){
        if isHidden_V{
            is_queue = false
            NetworkManager().StopAllThreads()

        }
    }
    
    
    @objc func AtBackground(){
        isBackground = true
    }
    
   @objc func AtForGround(){
        isBackground = false
    }
    
    func StartThread(isStart: Bool, vehic: Array<Any>){
       is_queue = isStart
       CallTrakingDataFromServer()
}
    
    
    func SetAllDataToTable(vehicle_data: Array<Any> , locArr:[String] , distArr:[Double]){
        //print("setting data to table")
        print("moving vehicle arr count:\(movingVehicleArr.count) , data count:\(self.data.count) , vehicle count:\(vehicle_data.count)")
        
        self.data.removeAll()
        self.movingVehicleArr.removeAll()
        self.idleVehicleArr.removeAll()
        self.stoppedVehicleArr.removeAll()
        self.noDataVehicleArr.removeAll()
        self.allVehiclesDataArr.removeAll()
        self.movVehLocArr.removeAll()
        self.idleVehLocArr.removeAll()
        self.noDtVehLocArr.removeAll()
        self.stopVehLocArr.removeAll()
        self.movDistArr.removeAll()
        self.idleDistArr.removeAll()
        self.stopDistArr.removeAll()
        self.noDistArr.removeAll()
        
        if vehicle_data.count < 1{
            self.tbl_show_live_track.isHidden = true
            self.lbl_error.isHidden = false
            return
        }
        
        
        for i in 0..<vehicle_data.count
            
        {
            let val_data = vehicle_data[i] as! Dictionary<String,Any>
            
            let status = val_data["device_status"] as! Int
            
            if moving{
                if status == 2{
                    if self.moving_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.movingVehicleArr.append(val_data)
                        self.movVehLocArr.append(locArr[i])
                        self.movDistArr.append(distArr[i])
                        // SetAllData(val_data: val_data)
                    }
                    
                }
            }else if idle{
                
                if status == 1{
                    if self.idle_c < 1 {
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.idleVehicleArr.append(val_data)
                        self.idleVehLocArr.append(locArr[i])
                        self.idleDistArr.append(distArr[i])
                        // SetAllData(val_data: val_data)
                    }
                }
            }else if stopped{
                
                if status == 0{
                    if self.stopped_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.stoppedVehicleArr.append(val_data)
                        self.stopVehLocArr.append(locArr[i])
                        self.stopDistArr.append(distArr[i])
                        //SetAllData(val_data: val_data)
                    }
                }
            }else if no_data{
                if status == 4{
                    if self.unreach_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.noDataVehicleArr.append(val_data)
                        self.noDtVehLocArr.append(locArr[i])
                        self.noDistArr.append(distArr[i])
                        //SetAllData(val_data: val_data)
                    }
                }
            }else if all{
                DispatchQueue.main.async {
                    self.tbl_show_live_track.isHidden = false
                    self.lbl_error.isHidden = true
                }
                
                self.allVehiclesDataArr.append(val_data)
                //SetAllData(val_data: val_data)
            }
        }
        
        
    /*
        for val in vehicle_data {
            let val_data = val as! Dictionary<String, Any>
            let status = val_data["device_status"] as! Int
            
            if moving{
                if status == 2{
                    if self.moving_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.movingVehicleArr.append(val_data)
                       // SetAllData(val_data: val_data)
                    }
                    
                }
            }else if idle{
               
                if status == 1{
                    if self.idle_c < 1 {
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.idleVehicleArr.append(val_data)
                       // SetAllData(val_data: val_data)
                    }
                }
            }else if stopped{
               
                if status == 0{
                    if self.stopped_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.stoppedVehicleArr.append(val_data)
                        //SetAllData(val_data: val_data)
                    }
                }
            }else if no_data{
                if status == 4{
                    if self.unreach_c < 1{
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                        return
                    }else{
                        self.tbl_show_live_track.isHidden = false
                        self.lbl_error.isHidden = true
                        self.noDataVehicleArr.append(val_data)
                        //SetAllData(val_data: val_data)
                    }
                }
            }else if all{
                DispatchQueue.main.async {
                     self.tbl_show_live_track.isHidden = false
                     self.lbl_error.isHidden = true
                }
               
                self.allVehiclesDataArr.append(val_data)
                //SetAllData(val_data: val_data)
            }
        }
  */
        if moving
        {
            self.loadVehiclesDataBasedOnStatus(vehicleArr: self.movingVehicleArr, locArr: movVehLocArr, distArr: movDistArr)
        }
        else if idle
        {
            print("idle dist arr: \(idleDistArr)")
            self.loadVehiclesDataBasedOnStatus(vehicleArr: self.idleVehicleArr, locArr: idleVehLocArr, distArr: idleDistArr)
        }
        else if stopped
        {
            self.loadVehiclesDataBasedOnStatus(vehicleArr: self.stoppedVehicleArr, locArr: stopVehLocArr, distArr: stopDistArr)
        }
        else if no_data
        {
            self.loadVehiclesDataBasedOnStatus(vehicleArr: self.noDataVehicleArr, locArr: noDtVehLocArr, distArr: noDistArr)
        }
        else if all
        {
            self.loadVehiclesDataBasedOnStatus(vehicleArr: self.allVehiclesDataArr, locArr: locArr, distArr: distArr)
        }
    }
    
    func loadVehiclesDataBasedOnStatus(vehicleArr:[[String:Any]], locArr:[String], distArr:[Double])
    {
        if vehicleArr.count < 1
        {
            self.tbl_show_live_track.isHidden = true
            self.lbl_error.isHidden = false
            return
        }
        print("vehicle arr count:\(vehicleArr.count) , loc arr count:\(locArr.count)")
        if vehicleArr.count == locArr.count
        {
            for i in 0..<vehicleArr.count {
                //statements of outer loop
                setVehicleDataBasedOnVehicleStatus(val_data: vehicleArr[i], location: locArr[i], distance: distArr[i])
            }
        }
        else
        {
            for i in 0..<vehicleArr.count {
                //statements of outer loop
                setVehicleDataBasedOnVehicleStatus(val_data: vehicleArr[i], location: "", distance: 0.0)
            }
        }
        
        let contentOffset = self.tbl_show_live_track.contentOffset
        
        DispatchQueue.main.async {
            self.tbl_show_live_track.isHidden = false
            self.tbl_show_live_track.delegate = self
            self.tbl_show_live_track.dataSource = self
            self.tbl_show_live_track.reloadData()
            //self.animateTable()
            self.tbl_show_live_track.setContentOffset(contentOffset, animated: true)
            self.sarch_bar_item.delegate = self
        }
        
        filterdData = data
        UIView.animate(withDuration: 0.2, animations: {
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    func setVehicleDataBasedOnVehicleStatus(val_data:Dictionary<String, Any>, location:String, distance:Double)
    {
        let registration = GetRegistrationNumber(Vehicals: val_data)
        let latitude = GetLatitudeFromData(Vehicals : val_data)
        let longitude = GetLonitudeFromData(Vehicals: val_data)
       // let distance = val_data["today_distance"] as? Double ?? 0.0
        let status = val_data["device_status"] as! Int
        let speed = val_data["speed"] as! String
        print("location of vehicle:\(location)")
        let halt_time = val_data["status_time"] as! String //status_total_time
        let last_update = val_data["last_update"] as! String
        let ignition_status = val_data["ignition_status"] as! String
        let next_billing_date = val_data["next_billing_date"] as? String ?? ""
        
        let vehicle_type_id = GetVehicleTypeId(Vehicals: val_data)
        let digital_sensor_status = val_data["sensor_status"] as! String
        let device_id = GetDeviceID(Vehicals: val_data)
        let newDetail = VehicleDetail(registration: registration, speed: speed, location: location, distance: distance, status: status, lat: latitude, device_id: device_id, long: longitude, halt_time: halt_time, last_update: last_update, ignition_status: ignition_status, digital_sensor_status: digital_sensor_status, vehicle_type_id: vehicle_type_id, next_billing_date: next_billing_date)
        
        self.data.append(newDetail)
                       
    }
    
    
    func Check(Moving: Bool, Idle: Bool, Stopped: Bool, No_data: Bool, All: Bool, select: Bool, vehicle_data: Array<Any>){
        
        is_queue = true
        moving = Moving
        idle = Idle
        stopped = Stopped
        no_data = No_data
        all = All
        self.tbl_show_live_track.isHidden = true
        self.CallTrakingDataFromServer()
    }
    
    
    func SetAllData(val_data: Dictionary<String, Any>){
        
        let registration = GetRegistrationNumber(Vehicals: val_data)
        let latitude = GetLatitudeFromData(Vehicals : val_data)
        let longitude = GetLonitudeFromData(Vehicals: val_data)
        let distance = val_data["today_distance"] as? Double ?? 0.0
        let status = val_data["device_status"] as? Int ?? 0
        let speed = val_data["speed"] as? String ?? "0.0"
        let location = val_data["location"] as? String ?? ""
        let halt_time = val_data["status_time"] as? String ?? ""
        let last_update = val_data["last_update"] as? String ?? ""
        let ignition_status = val_data["ignition_status"] as? String ?? ""
        let next_billing_date = val_data["next_billing_date"] as? String ?? ""
        
        let vehicle_type_id = GetVehicleTypeId(Vehicals: val_data)
        let digital_sensor_status = val_data["sensor_status"] as? String ?? ""
        let device_id = GetDeviceID(Vehicals: val_data)
        let newDetail = VehicleDetail(registration: registration, speed: speed, location: location, distance: distance, status: status, lat: latitude, device_id: device_id, long: longitude, halt_time: halt_time, last_update: last_update, ignition_status: ignition_status, digital_sensor_status: digital_sensor_status, vehicle_type_id: vehicle_type_id, next_billing_date: next_billing_date)
        
        self.data.append(newDetail)
        
        let contentOffset = self.tbl_show_live_track.contentOffset
        
         DispatchQueue.main.async {
            self.tbl_show_live_track.isHidden = false
            self.tbl_show_live_track.delegate = self
            self.tbl_show_live_track.dataSource = self
            self.tbl_show_live_track.reloadData()
            self.tbl_show_live_track.setContentOffset(contentOffset, animated: true)
            self.sarch_bar_item.delegate = self
        }
     
        filterdData = data
        UIView.animate(withDuration: 0.2, animations: {
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    
    @objc func CallTrakingDataFromServer(){
        
        apiHitCount += 1
        print("api hit count:\(apiHitCount)")
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_show_live_track.isHidden = true
            self.lbl_error.isHidden = false
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(self.alert_view)
        self.alert_view.backgroundColor = UIColor.clear
        if !isBackground{
            self.vehicals?.removeAll()
            self.vehicals_s?.removeAll()
        }
       
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let url = domain_name + "/tracking_api.php"
        let parameters : Dictionary<String, Any> = [
                  "action" : "track",
                  "user_name" : user_name,
                  "hash_key" : hash_key,
                  
        ]
        
        print("success page URL: \(url)")
        
        NetworkManager().CallNewTrackResult(urlString: url, parameters: parameters, completionHandler:{data, r_error, isNetwork in
            
            if isNetwork{
          
                let result = data?[K_Result] as! Int
                DispatchQueue.main.async {
                dispatchGroup.leave()   // <<----
                }
                switch (result){
                    
                case 0 :
                    
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    self.vehicals = (c_data["vehicles"] as! Array<Any>)
                    self.moving_c = c_data["moving_count"] as? Int ?? 0
                    self.stopped_c =  c_data["stopped_count"] as? Int ?? 0
                    self.unreach_c =  c_data["unreachable_count"] as? Int ?? 0
                    self.idle_c =  c_data["idling_count"] as? Int ?? 0
                    self.all_vehicles_count = self.moving_c + self.idle_c +  self.stopped_c + self.unreach_c
                    self.currentDate = c_data["current_date"] as! String
                    print("current date:\(self.currentDate)")
                    self.vehicals_s = (c_data[VEHICALS] as! Array<Any>)
                    self.data.removeAll()
                    
                    if self.vehicals?.count == 0
                    {
                        DispatchQueue.main.async {
                            showToast(controller: self, message: "Reloading Data..", seconds: 1.0)
                        }
                    }
                        
                    else
                    {
                        
                    DispatchQueue.main.async {
                        self.countDelegate?.sendVehiclesData(data: c_data)
                        NotificationCenter.default.post(name: Notification.Name("TrackingData"), object: nil, userInfo: c_data)
                        NotificationCenter.default.post(name: Notification.Name("TrackingDataForDashboard"), object: nil, userInfo: c_data)
                       
                      }
                  }
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
            }else{
                
                //Added 3/12/2019
                
                if let vehicleCount = self.vehicals?.count
                {
                    if vehicleCount < 1
                    {
                        self.tbl_show_live_track.isHidden = true
                        self.lbl_error.isHidden = false
                    }
                    else
                    {
                        self.data.removeAll()
                        self.SetAllDataToTable(vehicle_data: self.vehicals!, locArr: [], distArr: [])
                    }
                }
                else
                {
                      self.tbl_show_live_track.isHidden = true
                      self.lbl_error.isHidden = false
                }
            }
         
            if self.all_vehicles_count > 1000
            {
                self.timeInterval = 70.0
            }
            else if self.all_vehicles_count > 500
            {
                self.timeInterval = 50.0
            }
            else if self.all_vehicles_count > 200
            {
                self.timeInterval = 30.0
            }
            
            if self.is_queue {
                DispatchQueue.main.asyncAfter(deadline: .now() + self.timeInterval) {
                [weak self] in
                if self?.is_queue != nil && (self?.is_queue)! {
                self!.CallTrakingDataFromServer()
                }
              }
            }
        })
        
       dispatchGroup.enter()   // <<---
        
        let url_location = domain_name + "/tracking_api.php"
        
        let parameters_new : Dictionary<String, Any> = [
                         "action" : "extra",
                         "user_name" : user_name,
                         "hash_key" : hash_key,
        ]
        
        locationArr.removeAll()
        distanceArr.removeAll()

        NetworkManager().CallNewTrackResult(urlString: url_location, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
            
            let result = data?[K_Result] as! Int
            print("result:\(result)")
            dispatchGroup.leave()
            switch (result){
                
            case 0 :
                
                   let c_data = data?[K_Data] as!  Dictionary<String,Any>
                
                   print("location data:\(c_data)")
                   
                   if let loc_data = c_data["vehicle_data"] as? [Dictionary<String, Any>]
                   {
                     self.locationArr = loc_data.map({$0 ["place"] as! String})
                     self.distanceArr = loc_data.map({$0 ["today_distance"] as? Double ?? 0.0})
                   }
                
                   print("loc arr:\(self.locationArr), distance arr:\(self.distanceArr)")

                if self.vehicals?.count == 0
                {
                    DispatchQueue.main.async {
                        showToast(controller: self, message: "Reloading Data..", seconds: 1.0)
                    }
                }
                    
                else
                {
                    DispatchQueue.main.async {
                      NotificationCenter.default.post(name: Notification.Name("TrackingDataForAlertCount"), object: nil, userInfo: c_data)
                    }
                }
                
                break
            case 2 :
                let message = data?[K_Message] as! String
                print(message)
                break
            default:
                print("Default Case")
            }
            
            self.alert_view.removeFromSuperview()
            
        })
        
        dispatchGroup.notify(queue: .main) {
            self.SetAllDataToTable(vehicle_data: self.vehicals!, locArr: self.locationArr, distArr: self.distanceArr)
            print("both api done")
        }
        
    }
    
    
    @objc func RepeatFunc(){
        self.vehicals?.removeAll()
        //data.removeAll()
        CallTrakingDataFromServer()
    }
 
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func pressed_back(_ sender: Any) {
         let side_bar = SideBar.instanceFromNib()
         self.view.addSubview(side_bar)
    }
    
    func getDate(dateStr:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateStr)!
    }
      
    
    func SetDataInCell(cell: TrackCell, indexPath: Int, data: [VehicleDetail]) -> UITableViewCell {
        
        let currentDate = self.getDate(dateStr: Date.getCurrentDate())
        
        let vehicleBillingDate = self.getDate(dateStr: data[indexPath].next_billing_date)
        
        let numOfDays = currentDate.daysBetweenDate(toDate: vehicleBillingDate)
        
        if currentDate > vehicleBillingDate
        {
            cell.lbl_regn_status.isHidden = false
            cell.lbl_regn_status.text = self.bd.localizedString(forKey: "EXPIRED", value: nil, table: nil)
            cell.lbl_regstration.frame.size.width = cell.lbl_regn_status.frame.origin.x - cell.lbl_regstration.frame.origin.x
            cell.lbl_regn_status.textColor = UIColor.red
            cell.view_background.backgroundColor = UIColor.lightGray
        }
       
       else if numOfDays < 7
        {
            cell.lbl_regn_status.isHidden = false
            cell.lbl_regn_status.text = self.bd.localizedString(forKey: "EXPIRING_SOON", value: nil, table: nil)
            cell.lbl_regstration.frame.size.width = cell.lbl_regn_status.frame.origin.x - cell.lbl_regstration.frame.origin.x
            cell.lbl_regn_status.textColor = UIColor.systemYellow
            cell.view_background.backgroundColor = UIColor.white
        }
            
        else
        {
            cell.lbl_regn_status.isHidden = true
            cell.lbl_regstration.frame.size.width = cell.view_content.frame.size.width - cell.lbl_regstration.frame.origin.x - 8
            cell.view_background.backgroundColor = UIColor.white
        }
        
        cell.lbl_regstration.text = data[indexPath].registration
        
        cell.lbl_speed.text = data[indexPath].speed + kmhText
        let sppedImg = cell.speedIcon.image?.imageWithColor(color: .red)
        cell.speedIcon.image = sppedImg
        
        cell.lbl_location.text = data[indexPath].location
        let locImg = cell.locIcon.image?.imageWithColor(color: .systemGreen)
        cell.locIcon.image = locImg
        
        cell.lbl_distance.text = String(data[indexPath].distance) + kmText
        let distanceIcon = cell.distanceIcon.image?.imageWithColor(color: Global_Blue_Color)
        cell.distanceIcon.image = distanceIcon
        
        if data[indexPath].halt_time.count > 0{
            cell.lbl_halt_time.text = data[indexPath].halt_time
        }else{
            cell.lbl_halt_time.text = "-----"
        }
        
        let haltIcon = cell.haltTimeIcon.image?.imageWithColor(color: .systemGreen)
        cell.haltTimeIcon.image = haltIcon
        
        
        if data[indexPath].ignition_status == "0"{
            cell.img_ignition.image = UIImage(named: "ignition_off")
        }else{
            cell.img_ignition.image = UIImage(named: "ignition_on")
        }
        
        let get_status = data[indexPath].digital_sensor_status.suffix(2)
        
        let get_ac_status = get_status.suffix(1)
        
        let get_door_status = get_status.prefix(1)
        
        if get_ac_status == "0"{
            cell.ing_ac.image = UIImage(named: "ac_off")
        }else{
            cell.ing_ac.image = UIImage(named: "ac_on")
        }
        
        if get_door_status == "0"{
            cell.img_door.image = UIImage(named: "door_close")
        }else{
            cell.img_door.image = UIImage(named: "door_open")
        }
        
        cell.lbl_last_update.text = data[indexPath].last_update
        let lastUpdateIcon = cell.lastUpdateIcon.image?.imageWithColor(color: .systemYellow)
        cell.lastUpdateIcon.image = lastUpdateIcon
              
        let lat = data[indexPath].lat.truncate(to: 6)
        let long = data[indexPath].long.truncate(to: 6)
       
        cell.lbl_coordinates.text = String(lat) + ", " + String(long)
        let coordinateIcon = cell.coordinatesIocn.image?.imageWithColor(color: .systemPurple)
        cell.coordinatesIocn.image = coordinateIcon
        
        if data[indexPath].vehicle_type_id == "73"{
            vehicle_name = "tractor"
        }
        else if data[indexPath].vehicle_type_id == "74"{
            vehicle_name = "roadroller"
        }
        else if data[indexPath].vehicle_type_id == "75"{
            vehicle_name = "cementmixer"
        }
        else if data[indexPath].vehicle_type_id == "58"{
            vehicle_name = "ambulance"
        }
        else if data[indexPath].vehicle_type_id == "56"{
            vehicle_name = "pcr"
        }else if data[indexPath].vehicle_type_id == "1"{
            vehicle_name = "car"
        }else if data[indexPath].vehicle_type_id == "2"{
            vehicle_name = "truck"
        }else if data[indexPath].vehicle_type_id == "3"{
            vehicle_name = "bus"
        }else if data[indexPath].vehicle_type_id == "4"{
            vehicle_name = "van"
        }else if data[indexPath].vehicle_type_id == "5"{
            vehicle_name = "car"
        }else if data[indexPath].vehicle_type_id == "6"{
            vehicle_name = "bike"
        }else if data[indexPath].vehicle_type_id == "7"{
            vehicle_name = "scooty"
        }else{
            vehicle_name = "car"
        }
        
        if data[indexPath].status == 0 {
            // cell.alert_color.backgroundColor = UIColor.red
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_red")
        }
        if data[indexPath].status == 1 {
            // cell.alert_color.backgroundColor = UIColor.yellow
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_yellow")
        }
        
        if data[indexPath].status == 2 {
            //  cell.alert_color.backgroundColor = UIColor.green
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_green")
        }
        if data[indexPath].status == 4 {
            //  cell.alert_color.backgroundColor = UIColor.black
            cell.img_vehicle.image = UIImage(named: vehicle_name + "_black")
        }
        
        return cell
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        print("view did disappear")
        super.viewWillDisappear(false)
      //  self.is_queue = false
       // StopAllThreads()
    }
 
}

extension SuccessVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filterdData.count
        } else {
            return data.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_show_live_track.dequeueReusableCell(withIdentifier: "TrackCell") as! TrackCell
        if searchActive{
               return SetDataInCell(cell: cell, indexPath: indexPath.row, data: filterdData)
        }else{
            if data.count > 0
            {
             return SetDataInCell(cell: cell, indexPath: indexPath.row, data: data)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
        {
            return tbl_show_live_track.frame.size.height / 3.0
        }
        return tbl_show_live_track.frame.size.height / 1.7
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.is_queue = false
        NetworkManager().StopAllThreads()
        var index_val : Int!
        var deviceID = [String]()
        
        for (index, loc) in (vehicals?.enumerated())! {
            let val_data = loc as! Dictionary<String, Any>
            print("reg no: \(GetRegistrationNumber(Vehicals: val_data))")
            deviceID.append(GetDeviceID(Vehicals: val_data))
            if(searchActive) {
                if filterdData[indexPath.row].registration == GetRegistrationNumber(Vehicals: val_data){
                        index_val = index
                }}
            else {
                    if data[indexPath.row].registration == GetRegistrationNumber(Vehicals: val_data){
                        index_val = index
                    }
               }
           }
      
        self.sarch_bar_item.endEditing(true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowTrackingInfo") as! ShowTrackingInfo
        vc.index = index_val
        vc.vehicals = vehicals_s
        vc.device_id = deviceID[index_val]
        print("index:\(index_val), device id:\(deviceID[index_val])")
        vc.fromVC = "LivePage"
        vc.vehicle_name = vehicle_name + "_green"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}



extension SuccessVC: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.sarch_bar_item.endEditing(true)
       // self.tbl_show_live_track.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.sarch_bar_item.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.sarch_bar_item.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("in search bar success page")
        filterdData = data.filter { $0.registration.localizedCaseInsensitiveContains(searchText) }
        if(filterdData.count == 0) {
            searchActive = false;
        } else {
            searchActive = true;
        }
      
        self.tbl_show_live_track.reloadData()
    }
}

