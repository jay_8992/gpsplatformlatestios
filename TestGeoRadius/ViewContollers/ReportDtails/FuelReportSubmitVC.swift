//
//  FuelReportSubmitVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FuelReportSubmitVC: UIViewController {
    
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_report: UITableView!
    let alert_view = AlertView.instanceFromNib()
    
    var fuel_level = [String]()
    var distance = [String]()
    var date_time = [String]()
    
    var cellSpacingHeight:CGFloat = 15.0
    var temperature_rpt_data : [TemperatureDataModel] = []
    var reportType = ""
    var fuel_type = [Int]()
    var unitTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbl_report.delegate = self
        tbl_report.dataSource = self
        
        
        self.tbl_report.register(UINib(nibName: "FuelCell", bundle: nil), forCellReuseIdentifier: "FuelCell")
        
        self.tbl_report.register(UINib(nibName: "TemperatureCell", bundle: nil), forCellReuseIdentifier: "tempCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //CallDataFromServer()
        
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
    }
    
    
    @objc func BottomToTop(){
        if date_time.count > 0{
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbl_report.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    
    func CallTempDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: String){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()   
            return
        }
        
        self.view.addSubview(alert_view)
        temperature_rpt_data.removeAll()
        reportType = "TemperatureRpt"
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Temperature_All + device_id + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        print("temp report url \(urlString)")
        NetworkManager().CallTempReport(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                print("temp data:\(data)")
                for val in data! {
                    let val_data = val as! Dictionary<String, Any>
                    let time = val_data["log_date_start"] as? String ?? ""
                    let temp = val_data["temperature"] as? Double ?? 0.0
                    let distance = val_data["distance"] as? Double ?? 0.0
                    let location = val_data["location"] as? String ?? "--"
                    let newData = TemperatureDataModel(time: time, temperature: temp, distance: distance, location: location)
                    self.temperature_rpt_data.append(newData)
                    
                }
                
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                
                if data != nil{
                    self.lbl_error.text = LanguageHelperClass.getSeverError()
                    self.tbl_report.isHidden = true
                }else{
                    showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
                
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_report.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    
    func CallDataFromServer(to_date: String, from_date: String, to_time: String, from_time: String, device_id: String){
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.view.addSubview(alert_view)
        fuel_level.removeAll()
        distance.removeAll()
        date_time.removeAll()
        fuel_type.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Fuel_All + device_id + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallFuelReport(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                print("data:\(data)")
                let fuelData = data! ["fuel_data"] as! Array<Any>
                self.unitTxt = data! ["fuel_unit"] as? String ?? "Ltr"
                for val in fuelData{
                    let val_data = val as! Dictionary<String, Any>
                    self.date_time.append(val_data["log_date_start"] as? String ?? "")
                    self.fuel_level.append(val_data["fuel_level"] as? String ?? "")
                    self.fuel_type.append(val_data["fuel_type"] as? Int ?? 0 )
                    let dist = String(val_data["distance"] as? Double ?? 0.0)
                    self.distance.append(dist)
                }
                
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                
                if data != nil{
                    self.lbl_error.text = LanguageHelperClass.getSeverError()
                    self.tbl_report.isHidden = true
                }
                
                if r_error != nil{
                    self.lbl_error.text = r_error
                    self.tbl_report.isHidden = true
                }
                
                else{
                    showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 0.3)
                    self.tbl_report.isHidden = true
                }
    
                print("ERROR FOUND")
            }
        
            self.alert_view.removeFromSuperview()
        })
    }
}


extension FuelReportSubmitVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if reportType == "TemperatureRpt"
        {
            let cell = tbl_report.dequeueReusableCell(withIdentifier: "tempCell") as! TemperatureRptCell
            cell.layer.cornerRadius = 12.0
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.5
            cell.lbl_dateTime.text = temperature_rpt_data[indexPath.section].time
            cell.lbl_temperature.text = String(temperature_rpt_data[indexPath.section].temperature) + "°C"
            cell.lbl_distance.text = String(temperature_rpt_data[indexPath.section].distance)
            cell.lbl_location.text = temperature_rpt_data[indexPath.section].location
            
            return cell
        }
        else
        {
            let cell = tbl_report.dequeueReusableCell(withIdentifier: "FuelCell") as! FuelCell
            cell.layer.cornerRadius = 12.0
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.5
            cell.lbl_fuel.attributedText = FixBoldBetweenText(firstString: fuel_level[indexPath.section] + unitTxt, boldFontName: " on ", lastString: date_time[indexPath.section] + LanguageHelperClass().hrsTxt)
            cell.lbl_distance.text = distance[indexPath.section] + LanguageHelperClass().kmTxt
            return cell
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if reportType == "TemperatureRpt"
        {
            return tbl_report.frame.size.height / 4.5
        }
        return tbl_report.frame.size.height / 6
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if reportType == "TemperatureRpt"
        {
            return temperature_rpt_data.count
        }
        return date_time.count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 3.0
        }
        return cellSpacingHeight
    }
    
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
}
