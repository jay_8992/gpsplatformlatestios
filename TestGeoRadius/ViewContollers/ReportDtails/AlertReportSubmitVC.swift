//
//  AlertReportSubmitVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit


class AlertReportSubmitVC: UIViewController {

    
    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var tbl_reports: UITableView!
    let alert_view = AlertView.instanceFromNib()
  
    var alert_data_registration = [String]()
    var alert_child = [NSArray]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbl_reports.register(UINib(nibName: "AllRepeortsCell", bundle: nil), forCellReuseIdentifier: "AllRepeortsCell")
        self.tbl_reports.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")

        //to resolve extra padding in top in grouped table view
        
       self.tbl_reports.contentInset = UIEdgeInsets.init(top: -30, left: 0, bottom: 0, right: 0)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //CallDataFromServer()
        if #available(iOS 11.0, *) {
                 self.tbl_reports.contentInsetAdjustmentBehavior = .never
            }
        let buttonTop = UIButton(frame: CGRect(x: self.view.frame.size.width - 70, y: self.view.frame.size.height - 100, width: 50, height: 50))
        buttonTop.backgroundColor = UIColor.darkGray
        buttonTop.layer.cornerRadius = buttonTop.frame.size.width / 2
        buttonTop.setImage(UIImage(named: "up"), for: .normal)
        buttonTop.addTarget(self, action: #selector(BottomToTop), for: .touchUpInside)
        self.view.addSubview(buttonTop)
        
    }
    
    @objc func BottomToTop(){
        if alert_data_registration.count > 0{
            self.tbl_reports.reloadData()
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbl_reports.scrollToRow(at: indexPath, at: .top, animated: true)
        }
       
    }
    
    func SetValues(device_id : [String], alert_type_id : [String], to_date: String, to_time: String, from_date: String, from_time: String){
        
        let device_ids = device_id.joined(separator: ",")
        let alert_type_ids = alert_type_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Report_All + device_ids + "&date_from=" + from_date + "&date_to=" + to_date + "&time_picker_from=" + from_time + "&time_picker_to=" + to_time + "&alert_type_id=" + alert_type_ids + "&user_name=" + user_name + "&hash_key=" + hash_key
       
        CallDataFromServer(urlString: urlString, device_id : device_id, alert_type_id : alert_type_id)
    }
    
    
    func CallDataFromServer(urlString: String, device_id : [String], alert_type_id : [String]){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_reports.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        alert_data_registration.removeAll()
        alert_child.removeAll()
        self.view.addSubview(alert_view)
        
        NetworkManager().CallReportDataFromServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
            self.tbl_reports.isHidden = false
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.alert_data_registration.append(GetRegistrationNumber(Vehicals: val_data))
                    self.alert_child.append(val_data["alertchild_data"] as! NSArray)
                }
                
                self.tbl_reports.delegate = self
                self.tbl_reports.dataSource = self
                self.tbl_reports.reloadData()
            }else{
                self.lbl_error.text = LanguageHelperClass.getSeverError()
                self.tbl_reports.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                self.lbl_error.text = r_error
                self.tbl_reports.isHidden = true
            }
            self.alert_view.removeFromSuperview()
        })
    }
  
}

extension AlertReportSubmitVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return alert_data_registration.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        let total_count = alert_child[section]
        if total_count.count < 1{
            return 1
        }else{
           return total_count.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_reports.dequeueReusableCell(withIdentifier: "AllRepeortsCell") as! AllRepeortsCell
        let no_cell = tbl_reports.dequeueReusableCell(withIdentifier: "NoDataCell") as! NoDataCell
        let total_count = alert_child[indexPath.section]
        
        if total_count.count < 1{
            return no_cell
        }else{
            let data_alert = alert_child[indexPath.section][indexPath.row] as! Dictionary<String, Any>
            cell.lbl_time.text = GetAlertTime(alert_child: data_alert)
            cell.lbl_alert.text = GetAlertTypeName(alert_child: data_alert)
            cell.lbl_location.text = GetAlertLocation(alert_child: data_alert)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
            return tbl_reports.frame.size.height / 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 50
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = HeaderViewReportAll.instanceFromNib()
        view.lbl_registration.text = alert_data_registration[section]
        return view
    }
    
}
