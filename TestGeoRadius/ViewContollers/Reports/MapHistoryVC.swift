//
//  MapHistoryVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps
import RSSelectionMenu

class MapHistoryVC: UIViewController {
    
    
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var txt_from_date: UITextField!
    @IBOutlet weak var view_table: UIView!
    @IBOutlet weak var view_flash: UIView!
    
    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var tbl_sort_item: UITableView!
    @IBOutlet weak var view_date_and_time_perent: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var txt_to_date: UITextField!
    @IBOutlet weak var view_date_time: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var view_map_report: UIView!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    
 //   var vehicle_type_id = [String]()
    var val_vehicle_type_id : String!
    var report_data_controller : MapHistoryReportVC?
  //  var filterdData : [String]!
  //  var vehicle_data = [String]()
   // var device_id = [String]()
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var isFromDate = true
    var alert_report_origin : CGFloat!
    var device_id_val : String!
    var alert_view = AlertView.instanceFromNib()
    var searchActive : Bool = false
    var searchData:[String]!
    let cellSpacingHeight : CGFloat = 15.0
    let searchBar = UISearchBar()
    
    var data : [MapVehicleModel] = []
    var filteredData1 : [MapVehicleModel] = []
    var language:String!
    var bd:Bundle!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var endDateGreaterTxt = ""
    var selectVehicleTxt = ""
    
    //24/03/20
    var regNoArr = [String]()
    var selectedDataArray = [String]()
    var simpleSelectedArray = [String]()
    var vehicle_type_id = [String]()
    var cellSelectionStyle: CellSelectionStyle = .tickmark
    var device_id_arr = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguagesInControls()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        self.tbl_sort_item.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_map_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        
        txt_to_date.text = TodayToDate()
        
        txt_from_date.text = TodayFromDate()
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_sort_item.tableHeaderView = searchBar
        
        SetReportView(viewName: view_map_report)
      
        SetOrigin()
        
        // Do any additional setup after loading the view.
    }
    
    func setLanguagesInControls()
    {
        language = UserDefaults.standard.value(forKey: "language") as? String
        bd = setLanguage(lang : language)
        self.titleLbl.text = bd.localizedString(forKey: "MAP_HISTORY", value: nil, table: nil)
        txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        endDateGreaterTxt = bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil)
        selectVehicleTxt = bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil)
        segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
        segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
        segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
        segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)
        let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        btn_submit.setTitle(submitTitle, for: .normal)
    }
    
    
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_map_report.isHidden || view_map_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_map_report.frame.origin.y =  self.alert_report_origin
            //self.view_alerts_reports.isHidden = true
        })
        //}
    }
    
    func SetOrigin(){
        
        view_height = view_date_time.frame.size.height
        
        self.view_date_time.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_date_time.frame.origin.y + 30.0
    }
    
    
    @IBAction func back_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_map_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_map_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_map_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date.resignFirstResponder()
        isFromDate = true
        view_date_and_time_perent.isHidden = false
    }
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date.resignFirstResponder()
        isFromDate = false
        view_date_and_time_perent.isHidden = false
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_sort_item.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        CallVehicleFromServer()
        view_table.isHidden = false
       
        /* UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
      */
    }
    
    @IBAction func day_select(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date.text = TodayToDate()
            txt_from_date.text = TodayFromDate()
            if !view_date_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_time.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date.text = YesterdayFromDate()
            txt_to_date.text = YesterdayToDate()
            
            if !view_date_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_time.isHidden = true
            }
            break
            
        case 2:
            txt_from_date.text = Date.getPastDate(days: -6, from: "Reports")
            txt_to_date.text = TodayToDate()
            
            if !view_date_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_time.isHidden = true
            }
            
            break
            
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_date_time.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_date_time.isHidden = false
            view_map_report.isHidden = true
            break
        default:
            break;
        }
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_and_time_perent.isHidden = true
    }
    
    @IBAction func pressed_done_table(_ sender: Any) {
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_to_date.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func submit_pressed(){
        
        if txt_to_date.text! <  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_to_date.text! ==  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        
        if txt_to_date.text!.count < 1 || txt_from_date.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        if txt_select_vehicle.text!.count < 1{
            showToast(controller: self, message: selectVehicleTxt, seconds: 0.6)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date.text!)
        let to_time = GetToTime(time : txt_to_date.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date.text!)
        let from_time = GetFromTime(time: txt_from_date.text!)
        
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_map_report.frame.origin.y = self.header_view.frame.size.height + 35
            //self.view_alerts_reports.isHidden = false
        })
        view_map_report.isHidden = false
        
        guard let locationController = children.first as? MapHistoryReportVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        print("vehicle id:\(String(describing: val_vehicle_type_id)), device id:\(String(describing: device_id_val))")
        //report_data_controller?.AddPolyLine()
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: device_id_val, val_vehicle_type_id: val_vehicle_type_id)
    }
    
}
extension MapHistoryVC{
    func CallVehicleFromServer(){
        
        self.view.addSubview(alert_view)
        data.removeAll()
        filteredData1.removeAll()
        self.vehicle_type_id.removeAll()
        self.device_id_arr.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        // print("dlkldfkl \(urlString)")
        NetworkManager().CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    let registration_no = v_name["registration_no"] as? String ?? ""
                    let device_id = v_name["device_id"] as? String ?? ""
                    let vehicle_type_id = GetVehicleTypeId(Vehicals: v_name)
                    self.vehicle_type_id.append(vehicle_type_id)
                    self.device_id_arr.append(device_id)
                    self.regNoArr.append(registration_no)
                           
                    let newData = MapVehicleModel(registration_no: registration_no, device_id: device_id, vehicle_type_id: vehicle_type_id)
                    self.data.append(newData)
                }
                
                // self.filterdData = self.vehicle_data
                
                self.filteredData1 = self.data
                self.tbl_sort_item.delegate = self
                self.tbl_sort_item.dataSource = self
               //self.searchBar.delegate = self
                self.tbl_sort_item.reloadData()
                
                UIView.animate(withDuration: 0.8, animations: {
                    self.showSingleSelectionMenu(style: .present)
                })
                
                
            }else{
                // showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort_item.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
}
extension MapHistoryVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl_sort_item.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        cell.img_check.isHidden = true
        cell.img_uncheck.isHidden = true
        
        if searchActive{
            cell.lbl_item_name.text = filteredData1[indexPath.section].registration_no
        }
        else{
            cell.lbl_item_name.text = data[indexPath.section].registration_no
        }
        
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if searchActive{
            txt_select_vehicle.text = filteredData1[indexPath.section].registration_no
            self.val_vehicle_type_id = filteredData1[indexPath.section].vehicle_type_id
            self.device_id_val =  filteredData1[indexPath.section].device_id
        }
        else
        {
            txt_select_vehicle.text = data[indexPath.section].registration_no
            self.val_vehicle_type_id = data[indexPath.section].vehicle_type_id
            self.device_id_val = data[indexPath.section].device_id
            
        }
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
            self.data.removeAll()
            self.searchBar.text = ""
            self.searchBar.endEditing(true)
        })
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return tbl_sort_item.frame.size.height / 8
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filteredData1.count
        } else {
            count = data.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData1 = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        print("search data:\(filteredData1)")
        if(filteredData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        
        self.tbl_sort_item.reloadData()
    }
    
    
    func showSingleSelectionMenu(style: PresentationStyle) {
        // Here you'll get cell configuration where you'll get array item for each index
        // Cell configuration following parameters.
        // 1. UITableViewCell   2. Item of type T  3.IndexPath
        let selectionMenu = RSSelectionMenu(dataSource: self.regNoArr) { (cell, item, indexPath) in
            cell.textLabel?.text = item
        }
        // set default selected items when menu present on screen.
        // here you'll get handler each time you select a row
        // 1. Selected Item  2. Index of Selected Item  3. Selected or Deselected  4. All Selected Items
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { [weak self] (text, index, isSelected, selectedItems) in
            // update your existing array with updated selected items, so when menu show menu next time, updated items will be default selected.
            self?.simpleSelectedArray = selectedItems
            let selectedIndex = self?.regNoArr.indices.filter {self!.regNoArr[$0] == text}
            print("selected index:\(String(describing: selectedIndex))")
            switch style {
            case .push:
               print("Push view")
            case .present:
                self?.txt_select_vehicle.text = text
                if selectedIndex?.count == 1
                {
                 self!.val_vehicle_type_id = self?.vehicle_type_id[selectedIndex![0]]
                 self!.device_id_val = self?.device_id_arr[selectedIndex![0]]
                }
                self?.tbl_sort_item.isHidden = true
               // print("vehicle type id:\(String(describing: self?.val_vehicle_type_id))")
            default:
                break
            }
             self?.tbl_sort_item.reloadData()
        }
        
        selectionMenu.showSearchBar { [weak self] (searchText) -> ([String]) in
            print("vehicle name arr:\(String(describing: self?.regNoArr)), vehicle id arr:\(String(describing: self?.vehicle_type_id)) ")
            return self?.regNoArr.filter({ $0.lowercased().starts(with: searchText.lowercased()) }) ?? []
        }
        
        selectionMenu.onDismiss = { [weak self] selectedItems in
            self?.selectedDataArray = selectedItems
            /// do some stuff when menu is dismssed
            self?.tbl_sort_item.isHidden = true
        }
        /// set cell selection style - Default is 'tickmark'
        /// (Optional)
        selectionMenu.cellSelectionStyle = self.cellSelectionStyle
        /// Customization
        /// set navigationBar title, attributes and colors
        selectionMenu.setNavigationBar(title: "Select Vehicle", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white], barTintColor: #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), tintColor: UIColor.white)
        
        // show menu as (push or present)
        selectionMenu.show(style: style, from: self)
    }
    
    
}
