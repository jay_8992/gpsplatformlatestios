//
//  CameraVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import AlamofireImage

class CameraVC: UIViewController {
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var view_date_time_picker: UIDatePicker!
    @IBOutlet weak var view_table: UIView!
    @IBOutlet weak var tbl_sort: UITableView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_todate_from_date: UIView!
    @IBOutlet weak var txt_from_date_time: UITextField!
    @IBOutlet weak var camera_report: UIView!
    @IBOutlet weak var image_view: UIView!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var btn_filter: UIButton!
    
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var img_show: UIImageView!
    @IBOutlet weak var txt_to_date_time: UITextField!
    
    var alert_view = AlertView.instanceFromNib()
    var indexPath_val = [Int]()
    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var isFromDate = true
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var vehicle_type_values = [String]()
    var remove_values = [String]()
    var indexPath_val_remove = [Int]()
    var alert_report_origin : CGFloat!
    var report_data_controller : CameraReportVC?
    
    var searchActive : Bool = false
    var searchData:[String]!
    let cellSpacingHeight : CGFloat = 15.0
    let searchBar = UISearchBar()
    
    var data : [FuelReportModel] = []
    var filteredData1 : [FuelReportModel] = []
    var device_id_val : String!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var bd:Bundle!
    var endDateGreaterTxt = ""
    var selectVehicleTxt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setControlLanguages()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        view_date_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        self.tbl_sort.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = camera_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        
        txt_to_date_time.text = TodayToDate()
        
        txt_from_date_time.text = TodayFromDate()
        
        SetReportView(viewName: camera_report)
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_sort.tableHeaderView = searchBar
        
        SetOrigin()
        // Do any additional setup after loading the view.
    }
    
    func setControlLanguages()
      {
          let lang = UserDefaults.standard.value(forKey: "language") as! String
          bd = setLanguage(lang : lang)
          txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
          endDateGreaterTxt = bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil)
          selectVehicleTxt = bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil)
          segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
          segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
          segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
          segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)
          let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
          btn_submit.setTitle(submitTitle, for: .normal)
          titleLbl.text = bd.localizedString(forKey: "CAMERA", value: nil, table: nil)

      }
      
      
    func SetOrigin(){
        
        view_height = view_todate_from_date.frame.size.height
        
        self.view_todate_from_date.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_todate_from_date.frame.origin.y + 30.0
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_time_perent.isHidden = true
    }
    @IBAction func back_pressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dat_select(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date_time.text = TodayToDate()
            txt_from_date_time.text = TodayFromDate()
            if !view_todate_from_date.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_from_date.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date_time.text = YesterdayFromDate()
            txt_to_date_time.text = YesterdayToDate()
            
            if !view_todate_from_date.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_from_date.isHidden = true
            }
            break
        case 2:
            txt_from_date_time.text = Date.getPastDate(days: -6, from: "Reports")
            txt_to_date_time.text = TodayToDate()
            if !view_todate_from_date.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_from_date.isHidden = true
            }
        break
            
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_todate_from_date.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_todate_from_date.isHidden = false
            camera_report.isHidden = true
            break
        default:
            break;
        }
    }
    @IBAction func pressed_filter(_ sender: Any) {
        
        if camera_report.isHidden || camera_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        
        
        UIView.animate(withDuration: 0.5, animations: {
            self.camera_report.frame.origin.y =  self.alert_report_origin
            //self.view_alerts_reports.isHidden = true
        })
        // }
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if camera_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.camera_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.camera_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_sort.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        CallVehicleFromServer()
        view_table.isHidden = false
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    }
    
    
    
    @IBAction func btn_done(_ sender: Any) {
        
        filteredData1.removeAll()
        data.removeAll()
       
    }
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date_time.resignFirstResponder()
        isFromDate = true
        view_date_time_perent.isHidden = false
    }
    
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date_time.resignFirstResponder()
        isFromDate = false
        view_date_time_perent.isHidden = false
    }
    
    
    
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date_time.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_to_date_time.text = dateFormatter.string(from: sender.date)
        }
        
        
    }
    @IBAction func hide_image_view(_ sender: Any) {
        image_view.isHidden = true
    }
    
    @objc func submit_pressed(){
        
        if txt_to_date_time.text! <  txt_from_date_time.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_to_date_time.text! ==  txt_from_date_time.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_select_vehicle.text!.count < 1{
            showToast(controller: self, message: selectVehicleTxt, seconds: 0.6)
            return
        }
        
        if txt_to_date_time.text!.count < 1 || txt_from_date_time.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date_time.text!)
        let to_time = GetToTime(time : txt_to_date_time.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date_time.text!)
        let from_time = GetFromTime(time: txt_from_date_time.text!)
    
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.camera_report.frame.origin.y = self.view_header.frame.size.height + 30
            //self.view_alerts_reports.isHidden = false
        })
        camera_report.isHidden = false
        guard let locationController = children.first as? CameraReportVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        report_data_controller!.delegate = self
        
        print("device id:\(String(describing: device_id_val))")
        
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: device_id_val)
        
    }
    
}
extension CameraVC{
    
    func CallVehicleFromServer(){
        
        self.view.addSubview(alert_view)
        //vehicle_data.removeAll()
        
        filteredData1.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                    
                    let registration_no = v_name["registration_no"] as! String
                    let device_id = v_name["device_id"] as! String
                    let newData = FuelReportModel(registration_no: registration_no, device_id: device_id)
                    self.data.append(newData)
                }
                // self.filterdData = self.vehicle_data
                self.filteredData1 = self.data
                self.tbl_sort.delegate = self
                self.tbl_sort.dataSource = self
                self.searchBar.delegate = self
                self.tbl_sort.reloadData()
            }else{
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
    
    
}
extension CameraVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        cell.img_check.isHidden = true
        cell.img_uncheck.isHidden = true

        
        if searchActive{
            cell.lbl_item_name.text = filteredData1[indexPath.section].registration_no
        }
        else{
            cell.lbl_item_name.text = data[indexPath.section].registration_no
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if searchActive{
            txt_select_vehicle.text = filteredData1[indexPath.section].registration_no
            self.device_id_val = filteredData1[indexPath.section].device_id
        }
        else
        {
            txt_select_vehicle.text = data[indexPath.section].registration_no
            self.device_id_val = data[indexPath.section].device_id
        }
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
            self.data.removeAll()
            self.searchBar.text = ""
            self.searchBar.endEditing(true)
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return tbl_sort_item.frame.size.height / 8
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filteredData1.count
        } else {
            count = data.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredData1 = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        
        print("search data:\(filteredData1)")
        
        if(filteredData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tbl_sort.reloadData()
    }
    
    
}
extension CameraVC: ViewImageData{
    func viewImage(urlString: String) {
        print("sdlskldsd \(urlString)")
        //let imageView = UIImageView(frame: frame)
        let url = URL(string: urlString)!
        
        img_show.af_setImage(withURL: url)
        image_view.isHidden = false
    }
    
    
}
