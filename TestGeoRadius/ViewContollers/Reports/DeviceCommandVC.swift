//
//  DeviceCommandVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 22/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class DeviceCommandVC: UIViewController {

    @IBOutlet weak var lbl_error: UILabel!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var tbl_report: UITableView!
    
    var command_response = [String]()
    var command_type = [String]()
    var command_status = [String]()
    var registration_number = [String]()
    var time = [String]()
    
      let alert_view = AlertView.instanceFromNib()
    
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var titleLbl: UILabel!
    var bd:Bundle!
    var noDataTxt : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
  
        self.tbl_report.register(UINib(nibName: "DeviceCommandCell", bundle: nil), forCellReuseIdentifier: "DeviceCommandCell")

        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        tbl_report.addSubview(refreshControl)
        
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : lang)
        titleLbl.text = bd.localizedString(forKey: "DEVICE_COMMAND", value: nil, table: nil)
        noDataTxt = bd.localizedString(forKey: "NO_DATA_TEXT", value: nil, table: nil)
        
        // Do any additional setup after loading the view.
     
        CallDataFromServer()
    }
    
    
    
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
         CallDataFromServer()
    }

    @IBAction func back_pressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func CallDataFromServer(){
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_report.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        self.command_type.removeAll()
        self.command_status.removeAll()
        self.command_response.removeAll()
        self.registration_number.removeAll()
        self.time.removeAll()
        
        self.view.addSubview(alert_view)
        
        let toDate = TodayToDate()
        let fromDate = TodayFromDate()
        
        let to_date = GetToDate(date : toDate)
        let to_time = GetToTime(time : toDate)
        
        
        let from_date = GetFromDate(date: fromDate)
        let from_time = GetFromTime(time: fromDate)
        
       
        //let alert_type_ids = alert_type_id.joined(separator: ",")
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        
        
        let urlString = domain_name + "/device_command_result.php?action=display&device_id=null"  +  "&date_from="  +  from_date + "&date_to="  + to_date  +  "&time_from="  +  from_time  +  "&time_to="  +  to_time  +  "&device_command=&user_name=" + user_name + "&hash_key=" + hash_key + "&data_format=1&userdevices=1&user_id=" + user_id
        
      
       // print("sldksldksld \(urlString)")
        
        NetworkManager().CallTripReport(urlString: urlString, key_val: "dc_data", completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                self.tbl_report.isHidden = false
                
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    self.command_type.append(GetCommandType(alert_child: val_data))
                    self.command_status.append(GetCommandStatus(alert_child: val_data))
                    self.command_response.append(GetCommandResponse(alert_child: val_data))
                    self.registration_number.append(GetRegistrationNumber(Vehicals: val_data))
                    self.time.append(val_data["command_time"] as! String)
                    
                    
                }
                self.tbl_report.delegate = self
                self.tbl_report.dataSource = self
                self.tbl_report.reloadData()
            }else{
                
                if !NetworkAvailability.isConnectedToNetwork() {
                    
                    showToast(controller: self, message:LanguageHelperClass.getInternetError() , seconds: 1.5)
                    self.tbl_report.isHidden = true
                    self.alert_view.removeFromSuperview()
                    return
                }
            }
            if r_error != nil{
                self.lbl_error.text = self.noDataTxt
                self.tbl_report.isHidden = true
            }
            self.refreshControl.endRefreshing()
            self.alert_view.removeFromSuperview()
        })
        
    }

}

extension DeviceCommandVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.command_response.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_report.dequeueReusableCell(withIdentifier: "DeviceCommandCell") as! DeviceCommandCell
        cell.lbl_status.text = command_status[indexPath.row]
        cell.lbl_response.text = command_response[indexPath.row]
        cell.lbl_command.attributedText = Fix3BoldBetweenText(firstString: self.command_type[indexPath.row], boldFontName: " to ", secondString: self.registration_number[indexPath.row], bold2FontName: " at ", lastString: time[indexPath.row])
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tbl_report.frame.size.height / 5
    }
    
}
