//
//  OverspeedVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import EzPopup

class OverspeedVC: UIViewController,DataDelegate {
    
    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var view_date_and_time: UIView!
    @IBOutlet weak var txt_from_date: UITextField!
    @IBOutlet weak var txt_to_date: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var view_flash: UIView!
    
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var view_overspeed_report: UIView!
    @IBOutlet weak var slider_speed: UISlider!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    
    @IBOutlet weak var tbl_sort: UITableView!
    @IBOutlet weak var view_table: UIView!
    var alert_report_origin : CGFloat!
    var isFromDate = true
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var remove_values = [String]()
    var indexPath_val = [Int]()
    var indexPath_val_remove = [Int]()
    var vehicle_type_values = [String]()
    var report_data_controller : OverspeedReportVC?
    var alert_view = AlertView.instanceFromNib()
    
    var searchData:[String]!
    let cellSpacingHeight : CGFloat = 15.0
    let searchBar = UISearchBar()
    var searchActive : Bool = false
    var filterdData1 : [FuelReportModel] = []
    var data : [FuelReportModel] = []
    var single_selection_index = [Int]()
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    var isSelectAll = false
    
    @IBOutlet weak var selectAllBtn: UIButton!
    var selectedText : String!
    var endDateGreaterTxt = ""
    var selectVehicleTxt = ""
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var titleLbl: UILabel!
    
    var selectedDeviceId = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setControlLanguages()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        slider_speed.addTarget(self, action: #selector(updateKmsLabel), for: .allEvents)
        slider_speed.setValue(60, animated: true)
        self.lbl_speed.text = "Speed: 60"
        self.tbl_sort.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_overspeed_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        
        txt_to_date.text = TodayToDate()
        
        txt_from_date.text = TodayFromDate()
        
        SetReportView(viewName: view_overspeed_report)
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_sort.tableHeaderView = searchBar
        SetOrigin()
        // Do any additional setup after loading the view.
    }
    
    
    func setControlLanguages()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
        segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
        segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
        segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)
        let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        btn_submit.setTitle(submitTitle, for: .normal)
        titleLbl.text = bd.localizedString(forKey: "OVERSPEED_REPORT", value: nil, table: nil)
        selectedText = bd.localizedString(forKey: "SELECTED_TEXT", value: nil, table: nil)
        endDateGreaterTxt = bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil)
        selectVehicleTxt = bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil)
    }
    
    
    func SetOrigin(){
        view_height = view_date_and_time.frame.size.height
        
        self.view_date_and_time.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_date_and_time.frame.origin.y + 30.0
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_sort.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        let senderTF: UITextField = sender as! UITextField
        senderTF.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
        
       /* CallVehicleFromServer()
        view_table.isHidden = false
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    */
    }
    
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
             print("device Ids in overspeed VC :\(deviceId)")
             selectedDeviceId = deviceId
             UIView.animate(withDuration: 0.8, animations: {
               if self.selectedDeviceId.count > 1{
                      let count = String(self.selectedDeviceId.count)
                      self.txt_select_vehicle.text = count + self.selectedText
                  }else if self.selectedDeviceId.count > 0{
                      self.txt_select_vehicle.text = self.selectedDeviceId[0]
                  }else{
                      self.txt_select_vehicle.text =  "0" + self.selectedText
                  }
                  //20/12/2019
              })

       }
         
    
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_time_perent.isHidden = true
    }
    @IBAction func pressed_back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_overspeed_report.isHidden || view_overspeed_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        //        if view_overspeed_report.frame.origin.y == self.alert_report_origin{
        //
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.view_overspeed_report.frame.origin.y = self.header_view.frame.size.height + 30
        //                //self.view_alerts_reports.isHidden = false
        //            })
        //        }else{
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_overspeed_report.frame.origin.y =  self.alert_report_origin
            //self.view_alerts_reports.isHidden = true
        })
        //}
    }
    
    @IBAction func tap_search(_ sender: Any) {
        
        txt_search.resignFirstResponder()
        if view_overspeed_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_overspeed_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_overspeed_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    
    @IBAction func select_day(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date.text = TodayToDate()
            txt_from_date.text = TodayFromDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            
            break
        case 1:
            txt_from_date.text = YesterdayFromDate()
            txt_to_date.text = YesterdayToDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            break
        case 2:
            txt_from_date.text = Date.getPastDate(days: -6, from: "Reports")
            txt_to_date.text = TodayToDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            
            break
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_date_and_time.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_date_and_time.isHidden = false
            break
        default:
            break;
        }
    }
    
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date.resignFirstResponder()
        isFromDate = true
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date.resignFirstResponder()
        isFromDate = false
        view_date_time_perent.isHidden = false
    }
    
    @objc func updateKmsLabel(sender: UISlider!) {
        let value = Int(sender.value)
        DispatchQueue.main.async {
            self.lbl_speed.text = "Speed: " + "\(value)"
            // print("Slider value = \(value)")
        }
    }
    
    @IBAction func pressed_done(_ sender: Any) {
    
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
            if self.selected_devie_id.count > 1{
                let count = String(self.selected_devie_id.count)
                self.txt_select_vehicle.text = count + self.selectedText
            }else if self.selected_devie_id.count > 0{
                self.txt_select_vehicle.text = self.selected_devie_id[0]
            }else{
                self.txt_select_vehicle.text =  "0" + self.selectedText
            }
            
            self.searchBar.endEditing(true)
            self.searchBar.text = ""
            self.selectAllBtn.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            self.isSelectAll = false
            self.vehicle_type_values.removeAll()
            self.remove_values.removeAll()
            self.indexPath_val_remove.removeAll()
            self.single_selection_index.removeAll()
            
            //20/12/2019
        })
        
        
        
        
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        if isFromDate{
            txt_from_date.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_to_date.text = dateFormatter.string(from: sender.date)
            
        }
        
        
    }
    
    @IBAction func selectAllTypes(_ sender: UIButton) {
        single_selection_index.removeAll()
        print(isSelectAll)
        
        if isSelectAll == true {
            isSelectAll = false
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            single_selection_index.removeAll()
            selected_devie_id.removeAll()
        }
        else{
            isSelectAll = true
            if (searchActive)
            {
                
                for (index,_) in filterdData1.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(filterdData1[index].device_id)
                }}
            else
            {
                for (index,_) in data.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(all_selected_device_id[index])
                }
            }
            sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
        }
        
        print("selected device id arr;\(selected_devie_id)")
        tbl_sort.reloadData()
    }
    @objc func submit_pressed(){
        
        
        if txt_to_date.text! <  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_to_date.text! ==  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        
        if selectedDeviceId.count < 1{
            showToast(controller: self, message: selectVehicleTxt, seconds: 0.6)
            return
        }
        if txt_to_date.text!.count < 1 || txt_from_date.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date.text!)
        let to_time = GetToTime(time : txt_to_date.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date.text!)
        let from_time = GetFromTime(time: txt_from_date.text!)
        
        //        var vehicle_val = [String]()
        //        for index in indexPath_val{
        //            vehicle_val.append(device_id[index])
        //
        //        }
        
        //        if !view_date_and_time.isHidden{
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.SetOrigin()
        //            })
        //            view_date_and_time.isHidden = true
        //        }
        
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_overspeed_report.frame.origin.y = self.header_view.frame.size.height + 30
            self.view_overspeed_report.isHidden = false
        })
        view_overspeed_report.isHidden = false
        guard let locationController = children.first as? OverspeedReportVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        let group_hour = lbl_speed.text
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: selectedDeviceId, group_hour: group_hour!)
        
    }
    
    
    
}
extension OverspeedVC{
    func CallVehicleFromServer(){
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        filterdData1.removeAll()
        self.selected_devie_id.removeAll()
        data.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                    
                    let registration_no = v_name["registration_no"] as! String
                    let device_id = v_name["device_id"] as! String
                    self.all_selected_device_id.append(device_id)
                    let newData = FuelReportModel(registration_no: registration_no, device_id: device_id)
                    self.data.append(newData)
                }
                
                self.filterdData1 = self.data
                // self.filterdData = self.vehicle_data
                self.tbl_sort.delegate = self
                self.tbl_sort.dataSource = self
                self.searchBar.delegate = self
                self.tbl_sort.reloadData()
            }else{
                if NetworkAvailability.isConnectedToNetwork() {
                    print("Internet connection available")
                }
                else{
                    //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                    self.tbl_sort.isHidden = true
                    self.alert_view.removeFromSuperview()
                    
                }
                
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
}
extension OverspeedVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        
        for val in single_selection_index{
            
            if indexPath.section == val{
                if cell.img_uncheck.isHidden{
                    cell.img_uncheck.isHidden = false
                    // cell.img_check.isHidden = true
                }else{
                    cell.img_uncheck.isHidden = true
                    cell.img_check.isHidden = false
                }
            }
        }
        
        if searchActive{
            
            cell.lbl_item_name.text = filterdData1[indexPath.section].registration_no
            
            
        }
        else{
            
            cell.lbl_item_name.text = data[indexPath.section].registration_no
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_sort.cellForRow(at: indexPath) as! VehicleCell
        
        if cell.img_uncheck.isHidden{
            
            if (searchActive)
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != filterdData1[indexPath.section].device_id}
            }
            else
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]
                }
                single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
            }
        }else{
            // cell.img_check.isHidden = true
            
            if (searchActive)
            {
                self.selected_devie_id.append(filterdData1[indexPath.section].device_id)
            }
            else
            {
                self.selected_devie_id.append(self.all_selected_device_id[indexPath.section])
            }
            self.single_selection_index.append(indexPath.section)
        }
        
        print("selected device id:\(self.selected_devie_id)")
        tbl_sort.reloadData()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filterdData1.count
        } else {
            count = data.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterdData1 = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        print("search data:\(filterdData1)")
        if(filterdData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tbl_sort.reloadData()
    }
}
