//
//  FleetUsageVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 16/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import EzPopup

class FleetUsageVC: UIViewController,DataDelegate {
  
    
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var view_table: UIView!
    @IBOutlet weak var btn_filter: UIButton!
    
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var lbl_header: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var view_fleet_report: UIView!
    @IBOutlet weak var tbl_sort_item: UITableView!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var txt_to_date: UITextField!
    @IBOutlet weak var txt_from_date: UITextField!
    @IBOutlet weak var view_todate_fromdate: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    
    var alert_view = AlertView.instanceFromNib()
    var report_data_controller : FleetReportSubmitVC?
    var indexPath_val = [Int]()
    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var isFromDate = true
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var vehicle_type_values = [String]()
    var remove_values = [String]()
    var indexPath_val_remove = [Int]()
    var alert_report_origin : CGFloat!
    var searchActive : Bool = false
    var searchData:[String]!
    let cellSpacingHeight : CGFloat = 15.0
    let searchBar = UISearchBar()
    
    var filterdData1 : [FuelReportModel] = []
    var data : [FuelReportModel] = []
    var single_selection_index = [Int]()
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    var isSelectAll = false
    @IBOutlet weak var selectAllBtn: UIButton!
    var language = ""
    var bd : Bundle!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var selectedText = ""
    var endDateGreaterTxt = ""
    var selectVehicleTxt = ""
    var selectedDeviceId = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setControlLanguages()
        view_header.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        self.tbl_sort_item.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_fleet_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        view_flash.backgroundColor = UIColor.clear
        txt_to_date.text = TodayToDate()
        
        txt_from_date.text = TodayFromDate()
        
        SetReportView(viewName: view_fleet_report)
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_sort_item.tableHeaderView = searchBar
        
        SetOrigin()
        // Do any additional setup after loading the view.
    }
    
    func setControlLanguages()
      {
        language = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : language)
        txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        selectedText = bd.localizedString(forKey: "SELECTED_TEXT", value: nil, table: nil)
        endDateGreaterTxt = bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil)
        selectVehicleTxt = bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil)
        segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
        segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
        segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
        segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)
        let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        btn_submit.setTitle(submitTitle, for: .normal)
        lbl_header.text = bd.localizedString(forKey: "FLEET_USAGE", value: nil, table: nil)
      }
      
      
      
    
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_fleet_report.isHidden || view_fleet_report.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
        }

        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_fleet_report.frame.origin.y =  self.alert_report_origin
            //self.view_alerts_reports.isHidden = true
        })
        //}
    }
    
    
    func SetOrigin(){
        
        view_height = view_todate_fromdate.frame.size.height
        
        self.view_todate_fromdate.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_todate_fromdate.frame.origin.y + 30.0
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_time_perent.isHidden = true
    }
    
    @IBAction func pressed_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_sort_item.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        let senderTF: UITextField = sender as! UITextField
        senderTF.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
        // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
        // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
        
     /*  CallVehicleFromServer()
        view_table.isHidden = false
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
     */
        
    }
    
    @IBAction func tap_search(_ sender: Any) {
        
        txt_search.resignFirstResponder()
      
        if view_fleet_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_fleet_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_fleet_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
     
    }
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
          print("device Ids :\(deviceId)")
          selectedDeviceId = deviceId
          UIView.animate(withDuration: 0.8, animations: {
            if self.selectedDeviceId.count >= 1{
                   let count = String(self.selectedDeviceId.count)
                   self.txt_select_vehicle.text = count + self.selectedText
               }else{
                   self.txt_select_vehicle.text =  "0" + self.selectedText
               }
               //20/12/2019
           })

    }
      
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date.resignFirstResponder()
        isFromDate = true
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date.resignFirstResponder()
        isFromDate = false
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func select_day(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date.text = TodayToDate()            
            txt_from_date.text = TodayFromDate()
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date.text = YesterdayFromDate()
            txt_to_date.text = YesterdayToDate()
            
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            break
        case 2:
            txt_from_date.text = Date.getPastDate(days: -6, from: "Reports")
            txt_to_date.text = TodayToDate()
            
            if !view_todate_fromdate.isHidden {
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            
            break
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_todate_fromdate.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_todate_fromdate.isHidden = false
            view_fleet_report.isHidden = true
            break
        default:
            break;
        }
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_to_date.text = dateFormatter.string(from: sender.date)
            
        }
        
        
    }
    
    @IBAction func pressed_done(_ sender: Any) {
        
        //
        //        for values in remove_values{
        //            if let index = vehicle_type_values.index(of: values) {
        //                vehicle_type_values.remove(at: index)
        //            }
        //        }
        //
        //        for values in indexPath_val_remove{
        //            if let index = indexPath_val.index(of: values) {
        //                indexPath_val.remove(at: index)
        //            }
        //        }
        
        print("selected device :\(self.selected_devie_id)")
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
            if self.selected_devie_id.count > 1{
                let count = String(self.selected_devie_id.count)
                self.txt_select_vehicle.text = count + self.selectedText
            }else if self.selected_devie_id.count > 0{
                self.txt_select_vehicle.text = self.selected_devie_id[0]
            }else{
                self.txt_select_vehicle.text =  "0" + self.selectedText
            }
            
            self.searchBar.endEditing(true)
            self.searchBar.text = ""
            self.selectAllBtn.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            self.isSelectAll = false
            self.vehicle_type_values.removeAll()
            self.remove_values.removeAll()
            self.indexPath_val_remove.removeAll()
            self.single_selection_index.removeAll()
            
            //20/12/2019
        })
    
    }
    
    @IBAction func selectAllTypes(_ sender: UIButton) {
        single_selection_index.removeAll()
        print(isSelectAll)
        
        if isSelectAll == true {
            isSelectAll = false
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            single_selection_index.removeAll()
            selected_devie_id.removeAll()
        }
        else{
            isSelectAll = true
            if (searchActive)
            {
                
                for (index,_) in filterdData1.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(filterdData1[index].device_id)
                }}
            else
            {
                for (index,_) in data.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(all_selected_device_id[index])
                }
            }
              sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
        }
        
        print("selected device id arr;\(selected_devie_id)")
        tbl_sort_item.reloadData()
    }
    
    @objc func submit_pressed(){
        
        if txt_to_date.text! <  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_to_date.text! ==  txt_from_date.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        
        if selectedDeviceId.count < 1{
            showToast(controller: self, message: selectVehicleTxt, seconds: 0.6)
            return
        }
        if txt_to_date.text!.count < 1 || txt_from_date.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date.text!)
        let to_time = GetToTime(time : txt_to_date.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date.text!)
        let from_time = GetFromTime(time: txt_from_date.text!)
        
        //        var vehicle_val = [String]()
        //        for index in indexPath_val{
        //            vehicle_val.append(device_id[index])
        //
        //        }
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        
        //        if !view_todate_fromdate.isHidden{
        //            UIView.animate(withDuration: 0.5, animations: {
        //                self.SetOrigin()
        //            })
        //            view_todate_fromdate.isHidden = true
        //        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view_fleet_report.frame.origin.y = self.view_header.frame.size.height + 35
            //self.view_alerts_reports.isHidden = false
        })
        view_fleet_report.isHidden = false
        guard let locationController = children.first as? FleetReportSubmitVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        
       // report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: selected_devie_id)
        
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: selectedDeviceId)
        
    }
    
}

extension FleetUsageVC{
    func CallVehicleFromServer(){
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        self.data.removeAll()
        self.selected_devie_id.removeAll()
        self.filterdData1.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                    
                    let registration_no = v_name["registration_no"] as! String
                    let device_id = v_name["device_id"] as! String
                    self.all_selected_device_id.append(device_id)
                    let newData = FuelReportModel(registration_no: registration_no, device_id: device_id)
                    self.data.append(newData)
                    
                }
                
                self.filterdData1 = self.data
                //self.filterdData = self.vehicle_data
                self.tbl_sort_item.delegate = self
                self.tbl_sort_item.dataSource = self
                self.searchBar.delegate = self
                //self.tbl_sort_item.reloadData()
                self.animateTable()
            }else{
                
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort_item.isHidden = true
                self.alert_view.removeFromSuperview()
                
                
                
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    func animateTable() {
        self.tbl_sort_item.reloadData()
        let cells = self.tbl_sort_item.visibleCells
        let tableHeight: CGFloat = self.tbl_sort_item.bounds.size.height
            
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
            
        var index = 0
            
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay:  0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
}

extension FleetUsageVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return filterdData.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort_item.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        
        print("index path arr :\(self.selected_devie_id)")
        
        //         isSelectAll = false
        
        for val in single_selection_index{
            
            if indexPath.section == val{
                if cell.img_uncheck.isHidden{
                    cell.img_uncheck.isHidden = false
                    // cell.img_check.isHidden = true
                }else{
                    cell.img_uncheck.isHidden = true
                    cell.img_check.isHidden = false
                }
            }
        }
        
        if searchActive{
            
            cell.lbl_item_name.text = filterdData1[indexPath.section].registration_no
            
            
        }
        else{
            
            cell.lbl_item_name.text = data[indexPath.section].registration_no
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_sort_item.cellForRow(at: indexPath) as! VehicleCell
        if  cell.img_uncheck.isHidden{
         
            
            if (searchActive)
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != filterdData1[indexPath.section].device_id}
            }
            else
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]
                }
                single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
            }
            
        }
            
        else{
            // cell.img_check.isHidden = true
            
            if (searchActive)
            {
                self.selected_devie_id.append(filterdData1[indexPath.section].device_id)
            }
            else
            {
                self.selected_devie_id.append(self.all_selected_device_id[indexPath.section])
            }
            self.single_selection_index.append(indexPath.section)
        }
        print("selected device id:\(self.selected_devie_id)")
        tbl_sort_item.reloadData()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filterdData1.count
        } else {
            count = data.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterdData1 = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        print("search data:\(filterdData1)")
        if(filterdData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tbl_sort_item.reloadData()
    }
}
