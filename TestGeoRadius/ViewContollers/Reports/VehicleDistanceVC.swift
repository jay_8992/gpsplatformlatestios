//
//  VehicleDistanceVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import EzPopup

class VehicleDistanceVC: UIViewController,DataDelegate {
    
    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var view_date_time_perent: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_vehicle_report: UIView!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var tbl_sort: UITableView!
    @IBOutlet weak var view_yable: UIView!
    @IBOutlet weak var lbl_group_time: UILabel!
    @IBOutlet weak var txt_to_date_and_time: UITextField!
    @IBOutlet weak var txt_from_date_time: UITextField!
    @IBOutlet weak var view_date_and_time: UIView!
    @IBOutlet weak var slider_distance: UISlider!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var selectAllBtn: UIButton!
    var alert_report_origin : CGFloat!
    var isFromDate = true
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var filterdData : [String]!
    var vehicle_data = [String]()
    var device_id = [String]()
    var remove_values = [String]()
    var indexPath_val = [Int]()
    var indexPath_val_remove = [Int]()
    var vehicle_type_values = [String]()
    var report_data_controller : VehicleDistanceReportVC?
    var alert_view = AlertView.instanceFromNib()
    var searchActive : Bool = false
    var searchData:[String]!
    let cellSpacingHeight : CGFloat = 15.0
    let searchBar = UISearchBar()
    var isSelectAll = false
    var single_selection_index = [Int]()
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    
    var filterdData1 : [RenewLicenseModel] = []
    var data1 : [RenewLicenseModel] = []
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    var bd:Bundle!
    var selectedText = ""
    var endDateGreaterTxt = ""
    var selectVehicleTxt = ""
    var group_mode = "0"
    
    @IBOutlet weak var groupingHoursView: UIView!
    var groupingHrsViewHeight : CGFloat = 0.0
    var selectedTimeInterval = ""
    var groupHour = "24"
    @IBOutlet weak var checkBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setControlLanguages()
        slider_distance.isHidden = true
        groupingHrsViewHeight = self.groupingHoursView.frame.size.height
        self.lbl_group_time.frame = CGRect.init(x: self.lbl_group_time.frame.origin.x , y: 8 , width: self.lbl_group_time.frame.size.width, height: self.lbl_group_time.frame.size.height)
        groupingHoursView.frame.size.height = self.lbl_group_time.frame.size.height + 10
        self.checkBtn.frame = CGRect.init(x: self.checkBtn.frame.origin.x, y: 4 , width: 20, height: 20)
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        slider_distance.addTarget(self, action: #selector(updateKmsLabel), for: .allEvents)
        slider_distance.setValue(24, animated: true)
       // self.lbl_group_time.text = "Grouping Hours: 24"
        self.tbl_sort.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        alert_report_origin = view_vehicle_report.frame.origin.y
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        
        txt_to_date_and_time.text = TodayToDate()
        
        txt_from_date_time.text = TodayFromDate()
        
        SetReportView(viewName: view_vehicle_report)
        
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_sort.tableHeaderView = searchBar
        
        SetOrigin()
    }
    
    func setControlLanguages()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : lang)
        txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        selectedText = bd.localizedString(forKey: "SELECTED_TEXT", value: nil, table: nil)
        endDateGreaterTxt = bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil)
        selectVehicleTxt = bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil)
        segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
        segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
        segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
        segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)
        let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        btn_submit.setTitle(submitTitle, for: .normal)
        titleLbl.text = bd.localizedString(forKey: "VEHICLE_DISTANCE", value: nil, table: nil)
    }
    
    
    func SetOrigin(){
        
        view_height = view_date_and_time.frame.size.height
        
        self.view_date_and_time.frame.size.height = 0
        
        button_origin_y = btn_submit.frame.origin.y
        
        btn_submit.frame.origin.y =  view_date_and_time.frame.origin.y + 30.0
    }
    
    
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_vehicle_report.isHidden || view_vehicle_report.frame.origin.y == self.alert_report_origin {
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
        }
        
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
    
        UIView.animate(withDuration: 0.5, animations: {
            self.view_vehicle_report.frame.origin.y =  self.alert_report_origin
        })
        //}
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_vehicle_report.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_vehicle_report.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_vehicle_report.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
    }
    
    @IBAction func btn_date_done(_ sender: Any) {
        view_date_time_perent.isHidden = true
    }
    
    @IBAction func back_pressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func groupingHrsSelection(_ sender: Any) {
        
        let sender: UIButton = sender as! UIButton
        if !sender.isSelected
           {
            print("selected")
            slider_distance.isHidden = false
            group_mode = "1"
            sender.isSelected = true
            sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
            UIView.animate(withDuration: 0.5, animations: {
            self.lbl_group_time.text = "Grouping Hours: " + self.groupHour
            self.groupingHoursView.frame.size.height = self.groupingHrsViewHeight
            })
           }
           else
           {
            print("not selected")
            slider_distance.isHidden = true
            group_mode = "0"
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            sender.isSelected = false;
            UIView.animate(withDuration: 0.5, animations: {
            self.lbl_group_time.text = "Select Grouping Hours"
            self.groupingHoursView.frame.size.height = self.lbl_group_time.frame.size.height + 10
            })
           }
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        if !NetworkAvailability.isConnectedToNetwork() {
            showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 1.5)
            self.tbl_sort.isHidden = true
            self.alert_view.removeFromSuperview()
            return
        }
        
        if self.selected_devie_id.count > 0
        {
            self.selected_devie_id.removeAll()
            self.single_selection_index.removeAll()
        }
        
        
        CallVehicleFromServer()
        view_yable.isHidden = false
        
        let senderTF: UITextField = sender as! UITextField
        senderTF.resignFirstResponder()
        let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsVC") as! SearchResultsVC
        contentVC.delegate = self
              // Init popup view controller with content is your content view controller
        let popupVC = PopupViewController(contentController: contentVC, position: .top(20), popupWidth: contentVC.view.frame.size.width, popupHeight: contentVC.view.frame.size.height-50)
        popupVC.cornerRadius = 20
              // show it by call present(_ , animated:) method from a current UIViewController
        present(popupVC, animated: true)
        
     /*
        UIView.animate(withDuration: 0.8, animations: {
            self.view_yable.frame.origin.y = 20
        })
   */
    }
    
    func sendDeviceData(sender: SearchResultsVC, deviceId: [String]) {
             print("device Ids :\(deviceId)")
              self.selected_devie_id = deviceId
             UIView.animate(withDuration: 0.8, animations: {
               if self.selected_devie_id.count >= 1{
                      let count = String(self.selected_devie_id.count)
                      self.txt_select_vehicle.text = count + self.selectedText
                  }else{
                      self.txt_select_vehicle.text =  "0" + self.selectedText
                  }
                  //20/12/2019
              })
       }
    
    @IBAction func day_select(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            txt_to_date_and_time.text = TodayToDate()
            txt_from_date_time.text = TodayFromDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            
            break
        case 1:
            
            txt_from_date_time.text = YesterdayFromDate()
            txt_to_date_and_time.text = YesterdayToDate()
            
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
            break
        case 2:
            txt_from_date_time.text = Date.getPastDate(days: -6, from: "Reports")
            txt_to_date_and_time.text = TodayToDate()
            if !view_date_and_time.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_date_and_time.isHidden = true
            }
          break
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_date_and_time.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
            })
            view_date_and_time.isHidden = false
            view_vehicle_report.isHidden = true
            break
        default:
            break;
        }
    }
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date_time.resignFirstResponder()
        isFromDate = true
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func tap_to_date(_ sender: Any) {
        txt_to_date_and_time.resignFirstResponder()
        isFromDate = false
        view_date_time_perent.isHidden = false
    }
    
    @IBAction func pressed_done(_ sender: Any) {
     
        print("selected device :\(self.selected_devie_id)")
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_yable.frame.origin.y = self.view_yable.frame.size.height + 20
            if self.selected_devie_id.count >= 1{
                let count = String(self.selected_devie_id.count)
                self.txt_select_vehicle.text = count + self.selectedText
            }else{
                self.txt_select_vehicle.text =  "0 " + self.selectedText
            }
            
            self.searchBar.endEditing(true)
            self.searchBar.text = ""
            self.selectAllBtn.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            self.isSelectAll = false
            self.vehicle_type_values.removeAll()
            self.remove_values.removeAll()
            self.indexPath_val_remove.removeAll()
            self.single_selection_index.removeAll()
            
            //20/12/2019
            
            
        })
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date_time.text = dateFormatter.string(from: sender.date)
        }else{
            txt_to_date_and_time.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @IBAction func selectAllVehicles(_ sender: UIButton) {
        
        selected_devie_id.removeAll()
        single_selection_index.removeAll()
        
        print(isSelectAll)
        
        if isSelectAll == true {
            isSelectAll = false
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            single_selection_index.removeAll()
            selected_devie_id.removeAll()
        }
        else{
            isSelectAll = true
            if (searchActive)
            {
                for (index,_) in filterdData1.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(filterdData1[index].device_id)
                }
            }
            else
            {
                for (index,_) in data1.enumerated(){
                    single_selection_index.append(index)
                    selected_devie_id.append(all_selected_device_id[index])
                }
            }
            sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
        }
        
        print("selected device id arr;\(selected_devie_id)")
        
        tbl_sort.reloadData()
    }
    
    
    @objc func submit_pressed(){
        
        if txt_to_date_and_time.text! <  txt_from_date_time.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        if txt_to_date_and_time.text! ==  txt_from_date_time.text!{
            showToast(controller: self, message: endDateGreaterTxt, seconds: 1.5)
            return
        }
        
        
        if self.selected_devie_id.count < 1{
            showToast(controller: self, message: selectVehicleTxt, seconds: 0.6)
            return
        }
        if txt_to_date_and_time.text!.count < 1 || txt_from_date_time.text!.count < 1{
            showToast(controller: self, message: "Fields can't be Empty", seconds: 0.3)
            return
        }
        
        let to_date = GetToDate(date : txt_to_date_and_time.text!)
        let to_time = GetToTime(time : txt_to_date_and_time.text!)
        
        
        let from_date = GetFromDate(date: txt_from_date_time.text!)
        let from_time = GetFromTime(time: txt_from_date_time.text!)
        
        var vehicle_val = [String]()
       
        print("vehicle data:\(selected_devie_id)")
        
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view_vehicle_report.frame.origin.y = self.header_view.frame.size.height + 30
            self.view_vehicle_report.isHidden = false
        })
        view_vehicle_report.isHidden = false
        guard let locationController = children.first as? VehicleDistanceReportVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        report_data_controller = locationController
        
       // let group_hour = lbl_group_time.text
        
        report_data_controller?.CallDataFromServer(to_date: to_date, from_date: from_date, to_time: to_time, from_time: from_time, device_id: selected_devie_id, group_hour: groupHour, group_mode: group_mode)
        
    }
    
    @objc func updateKmsLabel(sender: UISlider!) {
        let value = Int(sender.value)
        groupHour = "\(value)"
        DispatchQueue.main.async {
            self.lbl_group_time.text = "Grouping Hours: " + "\(value)"
            // print("Slider value = \(value)")
        }
    }
    
}
extension VehicleDistanceVC{
    func CallVehicleFromServer(){
        
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        indexPath_val.removeAll()
        self.data1.removeAll()
        self.filterdData1.removeAll()
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallVehicleData(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //self.tbl_reports.isHidden = false
                for name in data!{
                    let v_name = name as! Dictionary<String, Any>
                    self.vehicle_data.append(v_name["registration_no"] as! String)
                    self.device_id.append(v_name["device_id"] as! String)
                    //   self.all_selected_device_id.append(v_name["device_id"] as! String)
                    
                    let registration_no = v_name["registration_no"] as! String
                    let device_serial = v_name["device_serial"] as! String
                    let valid_date = v_name["date_display"] as! String
                    let device_id = v_name["device_id"] as! String
                    
                    self.all_selected_device_id.append(v_name["device_id"] as! String)
                    let newData = RenewLicenseModel(registration_no: registration_no, device_serial: device_serial, valid_date: valid_date, device_id: device_id)
                    self.data1.append(newData)
                }
                
                
                self.filterdData1 = self.data1
                print("filtered data:\(self.filterdData)")
                print("device data:\(self.device_id)")
                self.tbl_sort.delegate = self
                self.tbl_sort.dataSource = self
                self.searchBar.delegate = self
                self.tbl_sort.reloadData()
            }else{
                //showToast(controller: self, message: "Please Check Internet Connection.", seconds: 0.3)
                self.tbl_sort.isHidden = true
                print("ERROR FOUND")
            }
            
            if r_error != nil{
                //print("sdfsdlklk \(String(describing: r_error))")
            }
            self.alert_view.removeFromSuperview()
        })
        
        
    }
}

extension VehicleDistanceVC: UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_sort.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        print("index path arr :\(self.selected_devie_id)")
        for val in single_selection_index{
            
            if indexPath.section == val{
                if cell.img_uncheck.isHidden{
                    cell.img_uncheck.isHidden = false
                    // cell.img_check.isHidden = true
                }else{
                    cell.img_uncheck.isHidden = true
                    cell.img_check.isHidden = false
                }
            }
        }
        
        if searchActive{
            
            cell.lbl_item_name.text = filterdData1[indexPath.section].registration_no
        }
            
        else{
            
            cell.lbl_item_name.text = data1[indexPath.section].registration_no
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_sort.cellForRow(at: indexPath) as! VehicleCell
        if cell.img_uncheck.isHidden{
            
            if (searchActive)
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != filterdData1[indexPath.section].device_id}
            }
            else
            {
                self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]
                }
                single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
            }
        }
            
        else
        {
            if (searchActive)
            {
                self.selected_devie_id.append(filterdData1[indexPath.section].device_id)
            }
            else
            {
                self.selected_devie_id.append(self.all_selected_device_id[indexPath.section])
            }
            self.single_selection_index.append(indexPath.section)
        }
        print("selected device id:\(self.selected_devie_id)")
        tbl_sort.reloadData()
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return tbl_sort_item.frame.size.height / 8
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count : Int = 0
        if(searchActive) {
            count = filterdData1.count
        } else {
            count = data1.count
        }
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0
        {
            return 10.0
        }
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        //searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // let newData = vehicle_data[1..<vehicle_data.count]
        
        filterdData1 = data1.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
        
        print("search data:\(filterdData1)")
        
        if(filterdData1.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        tbl_sort.reloadData()
    }
    
}
