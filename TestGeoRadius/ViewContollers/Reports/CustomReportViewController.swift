//
//  CustomReportViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/09/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import AZExpandable
class CustomReportViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var endDateTF: UITextField!
    var viewModel: APIDemoViewModelProtocol?
    var datePicker : UIDatePicker!
    let toolBar = UIToolbar()
    var inputView1 : UIView!
    var from_date_time:String!
    var to_date_time:String!
    var selectedField:UITextField!
    var startDateTime : [String]!
    var endDateTime : [String]!
    
    private var expandableTable: ExpandableTable!
    private var expandedCell: ExpandedCellInfo?
   
    var alert_view = AlertView.instanceFromNib()
    var noDataLabel: UILabel!
    var searchActive : Bool = false
    var vehicle_list : [CustomReportVehiclesModel] = []
    var filterd_vehicle_list : [CustomReportVehiclesModel] = []
    var final_date : String!
    @IBOutlet weak var searchBar: UISearchBar!
    
   override func viewDidLoad() {
            super.viewDidLoad()
            self.tableView.isHidden = true
            searchBar.isHidden = true
            noDataLabel = UILabel(frame: CGRect(x: 0, y: self.tableView.frame.origin.y, width: tableView.bounds.size.width, height: 30))
            noDataLabel.translatesAutoresizingMaskIntoConstraints = false
            noDataLabel.numberOfLines = 0
            noDataLabel.text = "No Records Found"
            noDataLabel.textColor = UIColor.darkText
            noDataLabel.textAlignment = .center
            self.view.addSubview(noDataLabel)
            noDataLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            noDataLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            noDataLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            noDataLabel.isHidden = true
            tableView.register(CustomReportHeaderView.nib, forHeaderFooterViewReuseIdentifier: CustomReportHeaderView.identifier)
        }
        
    
        @IBAction func navBack(_ sender: Any) {
             self.navigationController?.popViewController(animated: true)
        }
        
    
        @IBAction func searchData(_ sender: Any) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let fromTime = dateFormatter.date(from: from_date_time)
            let toTime = dateFormatter.date(from: to_date_time)
            
            let fromTimeInMS = currentDateTimeInMiliseconds(date: fromTime!)
            let toTimeInMS = currentDateTimeInMiliseconds(date: toTime!)
            
            print("fromTime:\(fromTimeInMS) , endTime:\(toTimeInMS)")
       
            CallDataFromServer(from_time: fromTimeInMS, end_time: toTimeInMS)
        }
        
        
        func currentDateTimeInMiliseconds(date:Date) -> Int {
              let since1970 = date.timeIntervalSince1970
              return Int(since1970 * 1000)
        }
        
        
        @IBAction func startTFSelected(_ sender: Any) {
            startDateTF.resignFirstResponder()
            selectedField = startDateTF
            doDatePicker()
        }
        
        
        @IBAction func endDateTFSelected(_ sender: Any) {
            endDateTF.resignFirstResponder()
            selectedField = endDateTF
            doDatePicker()
        }
    
    
        func doDatePicker(){
               
               // DatePicker
            inputView1 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height-250, width: self.view.frame.size.width, height: 250))
            if #available(iOS 13.0, *) {
                inputView1.backgroundColor = UIColor.systemGroupedBackground
            } else {
                // Fallback on earlier versions
                inputView1.backgroundColor = UIColor.groupTableViewBackground
            }
            
       /*   let titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: 100, height: 30))
            if selectedField == startDateTF
            {
                titleLabel.text = "Start Date & Time:"
            }
            else
            {
                titleLabel.text = "To Date & Time:"
            }
     */
            let doneButton = UIButton(frame: CGRect(x: self.view.frame.size.width-80, y: 5, width: 70, height: 35))
            doneButton.setTitle("Done", for: .normal)
            doneButton.layer.cornerRadius = 10.0
            doneButton.backgroundColor = UIColor.darkGray
            doneButton.setTitleColor(.white, for: .normal)
            inputView1.addSubview(doneButton) // add Button to UIView
            doneButton.addTarget(self, action: #selector(ShareLiveLocationVC.doneClick), for: UIControl.Event.touchUpInside)
            
            let cancelButton = UIButton(frame: CGRect(x: self.view.frame.size.width-180, y:5, width: 70, height: 35))
            cancelButton.setTitle("Cancel", for: .normal)
            cancelButton.layer.cornerRadius = 10.0
            cancelButton.backgroundColor = UIColor.darkGray
            cancelButton.setTitleColor(.white, for: .normal)
            inputView1.addSubview(cancelButton) // add Button to UIView
            cancelButton.addTarget(self, action: #selector(ShareLiveLocationVC.cancelClick), for: UIControl.Event.touchUpInside)
            
            self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 50, width: self.view.frame.size.width, height: 150))
            self.datePicker?.backgroundColor = UIColor.white
            self.datePicker?.maximumDate = Date()
            self.datePicker?.datePickerMode = .dateAndTime
            
            inputView1.addSubview(self.datePicker)
            
            self.view.addSubview(inputView1)
               // ToolBar
           }

           @objc func doneClick() {
               let dateFormatter1 = DateFormatter()
               dateFormatter1.calendar = Calendar(identifier: .gregorian)
               dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
               if selectedField == startDateTF
               {
                from_date_time = dateFormatter1.string(from: datePicker.date)
                startDateTF.text = from_date_time
               }
               else
               {
                to_date_time = dateFormatter1.string(from: datePicker.date)
                endDateTF.text = to_date_time
               }
               self.datePicker.resignFirstResponder()
               inputView1.removeFromSuperview()
           }

           @objc func cancelClick() {
               inputView1.removeFromSuperview()
           }
           
        func CallDataFromServer(from_time: Int, end_time: Int) {
            print("call data")
            if !NetworkAvailability.isConnectedToNetwork() {
                showToast(controller: self, message: "Please Check Internet Connection.", seconds: 1.5)
                self.alert_view.removeFromSuperview()
                return
            }
            
            if startDateTF.text!.count < 1 || endDateTF.text!.count < 1{
                showToast(controller: self, message: "Start Date & End Date can't be Empty.", seconds: 1.5)
                alert_view.removeFromSuperview()
                return
            }
            
            if endDateTF.text! <  startDateTF.text!{
              showToast(controller: self, message: "End Date should be greater then Start Date..", seconds: 1.5)
              return
            }
            
            if endDateTF.text! ==  startDateTF.text!{
                showToast(controller: self, message: "End Date should be greater then Start Date..", seconds: 1.5)
                return
            }
            
            self.view.addSubview(alert_view)
            if vehicle_list.count > 0
            {
                vehicle_list.removeAll()
            }
            if filterd_vehicle_list.count > 0
            {
                filterd_vehicle_list.removeAll()
            }
            
            let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
            let url_graph = "http://service.doordrishti.co:8080/obdservice/public/rest/eventData/getFuelData"
            let parameters_new : Dictionary<String, Any> = [
                      "username" : user_name,
                      "password" : 2255225522 ,
                      "startTime" : from_time,
                      "endTime": end_time
            ]
                                          
             NetworkManager().CallCustomReportSearchResult(urlString: url_graph, parameters: parameters_new, completionHandler:{data, r_error, isNetwork in
                if isNetwork && data != nil {
                    print("data : \(String(describing: data))")
                    for val in data! {
                        let val_data = val as! Dictionary<String, Any>
                        if let startTime = val_data["starttime"] as? String
                        {
                            let dateVar = Date.init(timeIntervalSince1970: TimeInterval(Int(startTime)!)/1000)
                            let dateFormatter = DateFormatter()
                           // dateFormatter.dateFormat = "ccc MMM dd YYYY hh:mm:ss OOOO (zzzz)"
                            dateFormatter.dateFormat = "dd MMM YYYY"
                            let finalDate =  dateFormatter.string(from: dateVar)
                            self.final_date = finalDate
                        }
                        else
                        {
                            self.final_date = ""
                        }
                                                    
                        let newDetail = CustomReportVehiclesModel(registrationNo: val_data["vehicleno"] as? String ?? " ", date: self.final_date, distanceCovered: val_data["km"] as? String ?? "0.0", initialVolume: val_data["fuelVolumeInitial"] as? String ?? "0.0", finalVolume: val_data["fuelVolumeFinal"] as? String ?? "0.0", actualFuelConsumption: val_data["fuelConsumptionActual"] as? String ?? "0.0", refillingVolume: val_data["fuellingsVolume"] as? String ?? "0.0", drainingVolume: val_data["drainingsVolume"] as? String ?? "0.0", fuelEconomy: val_data["fueleconomy"] as? String ?? "0.0", fuelEconomyPerHour: val_data["fueleconomyperhour"] as? String ?? "0.0")
                        
                          self.vehicle_list.append(newDetail)
                    }
                    
                    self.tableView.delegate = self
                    self.tableView.dataSource = self
                    self.searchBar.delegate = self

                    if data!.count == 0
                    {
                        DispatchQueue.main.async
                            {
                                self.noDataLabel.isHidden = false
                                self.searchBar.isHidden = true
                                self.tableView.reloadData()
                        }
                    }
                        
                    else
                    {
                        DispatchQueue.main.async {
                            self.searchBar.isHidden = false
                            self.startDateTF.text = ""
                            self.endDateTF.text = ""
                            self.tableView.isHidden = false
                            self.noDataLabel.isHidden = true
                            self.tableView.reloadData()
                        }
                    }
                }else{
                    
                    if data != nil{
                        self.noDataLabel.text = LanguageHelperClass.getSeverError()
                    }else{
                        showToast(controller: self, message: "Please Check Your internet Connection.", seconds: 0.3)
                    }
                    self.searchBar.isHidden = true
                    self.tableView.isHidden = true
                    print("ERROR FOUND")
                }
                
                if r_error != nil {
                    self.noDataLabel.text = (r_error as! String)
                    self.searchBar.isHidden = true
                    self.tableView.isHidden = true
                }
                self.alert_view.removeFromSuperview()
            })
        }
    }

    extension CustomReportViewController: UITableViewDataSource,UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView,
                       cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomReportCell", for: indexPath) as! CustomReportTableViewCell
            
            if searchActive{
                cell.lbl_date.text = filterd_vehicle_list[indexPath.section].date
                cell.lbl_initial_volume.text = filterd_vehicle_list[indexPath.section].initialVolume
                cell.lbl_final_volume.text = filterd_vehicle_list[indexPath.section].finalVolume
                cell.lbl_fuel_consumption.text = filterd_vehicle_list[indexPath.section].actualFuelConsumption
                cell.lbl_drain_fuel.text = filterd_vehicle_list[indexPath.section].drainingVolume
                cell.lbl_refilling_volume.text = filterd_vehicle_list[indexPath.section].refillingVolume
                cell.lbl_distance_covered.text = filterd_vehicle_list[indexPath.section].distanceCovered
                cell.lbl_fuel_economy.text = filterd_vehicle_list[indexPath.section].fuelEconomy
                cell.lbl_fuel_per_hour.text = filterd_vehicle_list[indexPath.section].fuelEconomyPerHour

            }else{
                cell.lbl_date.text = vehicle_list[indexPath.section].date
                cell.lbl_initial_volume.text = vehicle_list[indexPath.section].initialVolume
                cell.lbl_final_volume.text = vehicle_list[indexPath.section].finalVolume
                cell.lbl_fuel_consumption.text = vehicle_list[indexPath.section].actualFuelConsumption
                cell.lbl_drain_fuel.text = vehicle_list[indexPath.section].drainingVolume
                cell.lbl_refilling_volume.text = vehicle_list[indexPath.section].refillingVolume
                cell.lbl_distance_covered.text = vehicle_list[indexPath.section].distanceCovered
                cell.lbl_fuel_economy.text = vehicle_list[indexPath.section].fuelEconomy
                cell.lbl_fuel_per_hour.text = vehicle_list[indexPath.section].fuelEconomyPerHour
            }
            return cell
        }

            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return UITableView.automaticDimension
        }
        
        
        func numberOfSections(in tableView: UITableView) -> Int {
           if(searchActive) {
                     return filterd_vehicle_list.count
                 } else {
                     return vehicle_list.count
                }
        }
        
        // Set the spacing between sections
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 30.0
        }
      
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
               let view = HeaderViewReportAll.instanceFromNib()
               view.backgroundColor = UIColor.darkGray
               view.lbl_registration.textColor = UIColor.white
               if searchActive
               {
                view.lbl_registration.text = filterd_vehicle_list[section].registrationNo
               }
               else
               {
                view.lbl_registration.text = vehicle_list[section].registrationNo
               }
               return view
           }
    }


    extension CustomReportViewController: CollapsibleTableViewHeaderDelegate {
        func toggleSection(_ header: CustomReportHeaderView, section: Int) {
            tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
        }
    }


    extension CustomReportViewController: UISearchBarDelegate {
        
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            searchActive = true;
        }
        
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            searchActive = false;
            self.searchBar.endEditing(true)
        }
        
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
            self.searchBar.endEditing(true)
        }
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchActive = false;
            self.searchBar.endEditing(true)
        }
        
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            filterd_vehicle_list = vehicle_list.filter { $0.registrationNo.localizedCaseInsensitiveContains(searchText) }
            if searchText.count == 0
            {
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tableView.reloadData()
        }
    }
