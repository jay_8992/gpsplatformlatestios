//
//  AlertReportVC.swift
//  TestGeoRadius
//
//  Created by Georadius on 14/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
//import DateTimePicker


class AlertReportVC: UIViewController{
    
    @IBOutlet weak var btn_filter: UIButton!
    @IBOutlet weak var view_flash: UIView!
    @IBOutlet weak var view_date_picker_perent: UIView!
    @IBOutlet weak var view_alerts_reports: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var date_and_time_picker: UIDatePicker!
    @IBOutlet weak var header_view: UIView!
    @IBOutlet weak var txt_select_vehicle: UITextField!
    @IBOutlet weak var txt_alert_type: UITextField!
    @IBOutlet weak var txt_to_date: UITextField!
    @IBOutlet weak var txt_from_date: UITextField!
    @IBOutlet weak var tbl_shot_item: UITableView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_table: UIView!
    let alert_view = AlertView.instanceFromNib()
    var report_data_controller : AlertReportSubmitVC?
    var indexPath_val = [Int]()
    var indexPath_val_remove = [Int]()
    
    var indexPath_val_alert = [Int]()
    var indexPath_val_remove_alert = [Int]()
    
    var alert_type_values = [String]()
    var vehicle_type_values = [String]()
    
    var alert_type_id = [String]()
    var device_id = [String]()
    
    var alert_report_origin : CGFloat!
    var view_height : CGFloat!
    var button_origin_y : CGFloat!
    var vehicle_data = [String]()
    var alert_data = [String]()
    var alert_type = [String]()
    var filterdData : [String]!
    var searchData:[String]!
    var isVehicleType = true
    var isFromDate = true
    var selected_values = [String]()
    var remove_values = [String]()
    @IBOutlet weak var view_todate_fromdate: UIView!
    let cellSpacingHeight : CGFloat = 15.0
    var searchActive : Bool = false
    let searchBar = UISearchBar()
    
    var filterdData1 : [FuelReportModel] = []
    var data : [FuelReportModel] = []
    var alertData:[AlertTypeModel] = []
    var alertFilteredData:[AlertTypeModel] = []
    var single_selection_index = [Int]()
    var all_selected_device_id = [String]()
    var selected_devie_id = [String]()
    var selected_alert_id = [String]()
    var isSelectAll = false
    
    @IBOutlet weak var selectAllBtn: UIButton!
    var selectedText : String!
    var allText : String!
    var checkInternetErr : String!
    var bd : Bundle!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var titleLbl: UILabel!
 //   let picker = DateTimePicker()

  
    override func viewDidLoad() {
        super.viewDidLoad()
        setControlLanguages()
        header_view.dropShadow(color: .gray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)
      
        self.tbl_shot_item.register(UINib(nibName: "VehicleCell", bundle: nil), forCellReuseIdentifier: "cell_vehicle")
        let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tbl_shot_item.frame
        tempImageView.contentMode = .scaleAspectFill
        self.tbl_shot_item.backgroundView = tempImageView
        
        date_and_time_picker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        btn_submit.addTarget(self, action: #selector(submit_pressed), for: .touchUpInside)
        self.view_todate_fromdate.isHidden = true
        alert_report_origin = view_alerts_reports.frame.origin.y
        view_alerts_reports.layer.cornerRadius = view_alerts_reports.frame.size.height / 29
      
        SetReportView(viewName: view_alerts_reports)
        txt_to_date.resignFirstResponder()
        view_flash.backgroundColor = UIColor.clear
     
        txt_to_date.text = TodayToDate()
     
        txt_from_date.text = TodayFromDate()
        
        SetTextFieldLeftSide(imageName: "search", txt_field: txt_search)
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height / 4
        SetOrigin()
        
        //adding search bar
        searchBar.frame =  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        searchBar.isTranslucent = true
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "Search"
        searchBar.showsSearchResultsButton = false
        self.tbl_shot_item.tableHeaderView = searchBar
        
        // Do any additional setup after loading the view.
        
    }
    
    
    func setControlLanguages()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        bd = setLanguage(lang : lang)
        txt_select_vehicle.placeholder = bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        txt_alert_type.placeholder = bd.localizedString(forKey: "SELECT_ALERT", value: nil, table: nil)
        segmentControl.setTitle(bd.localizedString(forKey: "TODAY", value: nil, table: nil), forSegmentAt: 0)
        segmentControl.setTitle(bd.localizedString(forKey: "YESTERDAY", value: nil, table: nil), forSegmentAt: 1)
        segmentControl.setTitle(bd.localizedString(forKey: "WEEKLY", value: nil, table: nil), forSegmentAt: 2)
        segmentControl.setTitle(bd.localizedString(forKey: "CUSTOM", value: nil, table: nil), forSegmentAt: 3)

        let submitTitle = bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        btn_submit.setTitle(submitTitle, for: .normal)
        titleLbl.text = bd.localizedString(forKey: "ALERT_REPORT", value: nil, table: nil)
        selectedText = bd.localizedString(forKey: "SELECTED_TEXT", value: nil, table: nil)
        allText = bd.localizedString(forKey: "ALL", value: nil, table: nil)
        checkInternetErr = bd.localizedString(forKey: "NO_INTERNET_CONNECTION", value: nil, table: nil)
    }
    
    
    @objc func submit_pressed(){
        
        if txt_to_date.text! <  txt_from_date.text!{
            showToast(controller: self, message: self.bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil), seconds: 1.5)
            return
        }
        
        if txt_to_date.text! ==  txt_from_date.text!{
            showToast(controller: self, message: self.bd.localizedString(forKey: "END_DATE_GREATER", value: nil, table: nil), seconds: 1.5)
            return
        }
        
        if txt_from_date.text!.count < 1 || txt_to_date.text!.count < 1{
            showToast(controller: self, message: self.bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil), seconds: 0.3)
            return
        }
        
        if selected_devie_id.count < 1 || selected_alert_id.count < 1{
            showToast(controller: self, message: self.bd.localizedString(forKey: "PLZ_SELECT_VEHICLE", value: nil, table: nil), seconds: 0.5)
            return
        }
    
        let to_date = GetToDate(date : txt_to_date.text!)
        let to_time = GetToTime(time : txt_to_date.text!)
        
        //        let from_date_time = txt_from_date.text
        //        let date_time = from_date_time?.components(separatedBy: " ")
        
        let from_date = GetFromDate(date: txt_from_date.text!)
        let from_time = GetFromTime(time: txt_from_date.text!)
        
    
        btn_filter.setImage(UIImage(named: "filterdown"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_alerts_reports.frame.origin.y = self.header_view.frame.size.height + 30
            //self.view_alerts_reports.isHidden = false
        })
        
        view_alerts_reports.isHidden = false
        guard let locationController = children.first as? AlertReportSubmitVC else  {
            fatalError("Check storyboard for missing LocationTableViewController")
        }
        
        print("selected devices id:\(self.selected_devie_id) , selected alerts id:\(self.selected_alert_id)")
        
        report_data_controller = locationController
        
        report_data_controller?.SetValues(device_id : self.selected_devie_id, alert_type_id: self.selected_alert_id, to_date: to_date, to_time: to_time, from_date: from_date, from_time: from_time)
    }
    
    
    @IBAction func pressed_date_done(_ sender: Any) {
        view_date_picker_perent.isHidden = true
    }
    
    func SetOrigin() {
        view_height = view_todate_fromdate.frame.size.height
        self.view_todate_fromdate.frame.size.height = 0
        button_origin_y = btn_submit.frame.origin.y
        btn_submit.frame.origin.y =  view_todate_fromdate.frame.origin.y + 30.0
    }
    
    @IBAction func pressed_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_select_vehicle(_ sender: Any) {
        txt_select_vehicle.resignFirstResponder()
        // indexPath_val.removeAll()
        selected_values.removeAll()
        CallVehicleFromServer()
        view_table.isHidden = false
        isVehicleType = true
        
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    }
    
    @IBAction func tap_alert_type(_ sender: Any) {
        
        txt_alert_type.resignFirstResponder()
        //indexPath_val.removeAll()
        selected_values.removeAll()
        CallAlertTypeData()
        view_table.isHidden = false
        isVehicleType = false
        //self.txt_vehicle_type.isEnabled = true
        UIView.animate(withDuration: 0.8, animations: {
            self.view_table.frame.origin.y = 20
        })
    }
    
    @IBAction func tap_from_date(_ sender: Any) {
        txt_from_date.resignFirstResponder()
        isFromDate = true
        //showDateTimePicker(sender: txt_from_date)
        view_date_picker_perent.isHidden = false
    }
    
    
    @IBAction func to_date_tap(_ sender: Any) {
        txt_to_date.resignFirstResponder()
        isFromDate = false
        view_date_picker_perent.isHidden = false
    }
    
    @IBAction func pressed_done_table(_ sender: Any) {
        
        print("selected device id  list:\(selected_devie_id)")
        print("selected alert id list :\(selected_alert_id)")
        
        UIView.animate(withDuration: 0.8, animations: {
            
            self.searchBar.endEditing(true)
            
            self.searchBar.text = ""
            
            self.selectAllBtn.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            
            self.isSelectAll = false
            
            self.view_table.frame.origin.y = self.view_table.frame.size.height + 20
            
            if self.isVehicleType{
                
                if self.selected_devie_id.count > 1{
                    let count = String(self.selected_devie_id.count)
                    self.txt_select_vehicle.text = count + self.selectedText
                }else if self.selected_devie_id.count == 1 {
                    self.txt_select_vehicle.text = "1" + self.selectedText
                }
                else{
                    self.txt_select_vehicle.text =  "0" + self.selectedText
                }
            }else{
                
                if self.alert_type_values.count == self.alert_data.count + 1{
                    self.txt_alert_type.text = "All"
                }else if self.selected_alert_id.count > 1{
                    let count = String(self.selected_alert_id.count)
                    self.txt_alert_type.text = count + self.selectedText
                }else if self.alert_type_values.count > 0{
                    self.txt_alert_type.text = self.alert_type_values[0]
                }else if self.selected_alert_id.count == 1
                {
                    self.txt_alert_type.text =  "1" + self.selectedText
                }
                else{
                    self.txt_alert_type.text =  "0" + self.selectedText
                }
            }
            
            self.single_selection_index.removeAll()
            self.remove_values.removeAll()
            self.indexPath_val_remove.removeAll()
            self.indexPath_val_remove_alert.removeAll()
            
        })
    }
    
    @IBAction func select_day(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            
            txt_to_date.text = TodayToDate()
            txt_from_date.text = TodayFromDate()
            
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            break
        case 1:
            txt_from_date.text = YesterdayFromDate()
            txt_to_date.text = YesterdayToDate()
            
            if !view_todate_fromdate.isHidden{
                UIView.animate(withDuration: 0.5, animations: {
                    self.SetOrigin()
                })
                view_todate_fromdate.isHidden = true
            }
            break
        case 2:
                txt_from_date.text = Date.getPastDate(days: -6, from: "Reports")
                txt_to_date.text = TodayToDate()
                
                if !view_todate_fromdate.isHidden {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.SetOrigin()
                    })
                    view_todate_fromdate.isHidden = true
                }
                      
            break
        case 3:
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_todate_fromdate.frame.size.height = self.view_height
                self.btn_submit.frame.origin.y = self.button_origin_y
                
            })
            
            view_todate_fromdate.isHidden = false
            view_alerts_reports.isHidden = true
            break
        default:
            break;
        }
    }
    @IBAction func pressed_filter(_ sender: Any) {
        
        if view_alerts_reports.isHidden || view_alerts_reports.frame.origin.y == self.alert_report_origin{
            UIView.animate(withDuration: 0.5, animations: {
                self.view_flash.backgroundColor = UIColor.lightGray
            }, completion: { _ in
                self.view_flash.backgroundColor = UIColor.clear
                return
            })
            
        }
        btn_filter.setImage(UIImage(named: "filter"), for: .normal)
        UIView.animate(withDuration: 0.5, animations: {
            self.view_alerts_reports.frame.origin.y =  self.alert_report_origin
            //self.view_alerts_reports.isHidden = true
        })
        // }
    }
    
    @IBAction func tap_search(_ sender: Any) {
        txt_search.resignFirstResponder()
        if view_alerts_reports.frame.origin.y == self.alert_report_origin{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_alerts_reports.frame.origin.y = self.txt_search.frame.origin.y + 35
                //self.view_alerts_reports.isHidden = false
            })
        }else{
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view_alerts_reports.frame.origin.y =  self.alert_report_origin
                //self.view_alerts_reports.isHidden = true
            })
        }
        
    }
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if isFromDate{
            txt_from_date.text = dateFormatter.string(from: sender.date)
            
        }else{
            txt_to_date.text = dateFormatter.string(from: sender.date)
            
        }
    }
    
    @IBAction func SelectAllType(_ sender: UIButton) {
        
        // selected_devie_id.removeAll()
        single_selection_index.removeAll()
        print(isSelectAll)
        
        if isSelectAll == true {
            isSelectAll = false
            sender.setBackgroundImage(UIImage(named: "uncheck"), for: .normal)
            single_selection_index.removeAll()
            selected_devie_id.removeAll()
            selected_alert_id.removeAll()
        }
        else{
            isSelectAll = true
            if (searchActive)
            {
                if (isVehicleType)
                {
                    for (index,_) in filterdData1.enumerated(){
                        single_selection_index.append(index)
                        selected_devie_id.append(filterdData1[index].device_id)
                    }
                }
                else
                {
                    for (index,_) in alertFilteredData.enumerated(){
                        single_selection_index.append(index)
                        selected_alert_id.append(filterdData1[index].device_id)
                    }
                }
            }
                
            else
            {
                if (isVehicleType)
                {
                    for (index,_) in data.enumerated(){
                        single_selection_index.append(index)
                        selected_devie_id.append(all_selected_device_id[index])
                    }
                }
                else
                {
                    for (index,_) in alertData.enumerated(){
                        single_selection_index.append(index)
                        selected_alert_id.append(alert_type_id[index])
                    }
                }
            }
            sender.setBackgroundImage(UIImage(named: "check"), for: .normal)
        }
        
        print("selected device id arr;\(selected_devie_id)")
        
        tbl_shot_item.reloadData()
        
        
    }
    
    func CallAlertTypeData(){
        self.view.addSubview(alert_view)
        self.alertData.removeAll()
        self.alertFilteredData.removeAll()
        self.selected_alert_id.removeAll()
        self.alert_type_id.removeAll()
        //  alert_data.append("All")
        //  alert_type_id.append("")
        
        // self.data.removeAll()
        print("alert data count  :\(self.alertData.count)")
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let vehicle_type_url = domain_name + Alert_Type + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallTrackResult(urlString: vehicle_type_url, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    for val in c_data{
                        let v_val = val as! Dictionary<String, Any>
                        self.alert_data.append(v_val["alert_type_name"] as! String)
                        self.alert_type_id.append(v_val["alert_type_id"] as! String)
                        
                        //23.12.2019
                        let alertTypeName = v_val["alert_type_name"] as! String
                        let alert_id = v_val["alert_type_id"] as! String
                        
                        let newData = AlertTypeModel(alert_type_name: alertTypeName, alert_type_id: alert_id)
                        self.alertData.append(newData)
                        
                    }
                    
                    print("alert arr :\(self.alert_type_id)")
                    
                    
                    self.alertFilteredData = self.alertData
                    self.tbl_shot_item.delegate = self
                    self.tbl_shot_item.dataSource = self
                    self.searchBar.delegate = self
                    self.tbl_shot_item.reloadData()
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 0.3)
                self.tbl_shot_item.isHidden = true
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    func CallVehicleFromServer(){
        
        print("getting vehicles list")
        self.view.addSubview(alert_view)
        vehicle_data.removeAll()
        device_id.removeAll()
        data.removeAll()
        selected_devie_id.removeAll()
        //vehicle_data.append("Select All")
        
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        
        let urlString = domain_name + Vehicle_Edit + "user_name=" + user_name + "&hash_key=" + hash_key
        
        NetworkManager().CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["return_json"] as! Array<Any>
                    
                    for name in val{
                        let v_name = name as! Dictionary<String, Any>
                        self.vehicle_data.append(v_name["registration_no"] as! String)
                        self.device_id.append(v_name["device_id"] as! String)
                        
                        //23.12.2019
                        let registration_no = v_name["registration_no"] as! String
                        let device_id = v_name["device_id"] as! String
                        self.all_selected_device_id.append(device_id)
                        let newData = FuelReportModel(registration_no: registration_no, device_id: device_id)
                        self.data.append(newData)
                    }
                    
                    // self.filterdData = self.vehicle_data
                    self.filterdData1 = self.data
                    self.tbl_shot_item.delegate = self
                    self.tbl_shot_item.dataSource = self
                    self.searchBar.delegate = self
                    self.tbl_shot_item.reloadData()
                   // self.animateTable()
                    
                    break
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    break
                default:
                    print("Default Case")
                }
                
            }else{
                showToast(controller: self, message: LanguageHelperClass.getInternetError(), seconds: 0.3)
                self.tbl_shot_item.isHidden = true
                print("ERROR FOUND")
            }
            self.alert_view.removeFromSuperview()
        })
    }
    
    
 func animateTable() {
       self.tbl_shot_item.reloadData()
       let cells = self.tbl_shot_item.visibleCells
       let tableHeight: CGFloat = self.tbl_shot_item.bounds.size.height
           
       for i in cells {
           let cell: UITableViewCell = i as UITableViewCell
           cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
       }
           
       var index = 0
           
       for a in cells {
           let cell: UITableViewCell = a as UITableViewCell
           UIView.animate(withDuration: 1.5, delay:  0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
               cell.transform = CGAffineTransform(translationX: 0, y: 0);
           }, completion: nil)
           index += 1
       }
   }
    
}



extension AlertReportVC : UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tbl_shot_item.dequeueReusableCell(withIdentifier: "cell_vehicle") as! VehicleCell
        let cell = tbl_shot_item.dequeueReusableCell(withIdentifier: "cell_vehicle", for: indexPath) as! VehicleCell
        print("index path arr :\(self.selected_devie_id), vehicle type:\(isVehicleType)")
        if searchActive && filterdData1.count > 0 {
            if isVehicleType
            {
                cell.lbl_item_name.text = filterdData1[indexPath.section].registration_no
                if self.selected_devie_id.contains(filterdData1[indexPath.section].device_id)
                {
                     if cell.img_uncheck.isHidden{
                        cell.img_uncheck.isHidden = false
                    }
                     else {
                    cell.img_uncheck.isHidden = true
                    cell.img_check.isHidden = false
                    }
                }
                else
                {
                    cell.img_uncheck.isHidden  = false
                }
            }
            else
            {
                cell.lbl_item_name.text = alertFilteredData [indexPath.section].alert_type_name
            }
           
        }
            
        else  {
            if isVehicleType
            {
                cell.lbl_item_name.text = data[indexPath.section].registration_no
            }
            else
            {
                cell.lbl_item_name.text = alertData [indexPath.section].alert_type_name
            }
            
            for val in single_selection_index{
                if indexPath.section == val{
                    if cell.img_uncheck.isHidden{
                        cell.img_uncheck.isHidden = false
                        // cell.img_check.isHidden = true
                    }else{
                        cell.img_uncheck.isHidden = true
                        cell.img_check.isHidden = false
                    }
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tbl_shot_item.cellForRow(at: indexPath) as! VehicleCell
        isSelectAll = false
        print("single selection arr:\(self.single_selection_index.count)")
        
        if cell.img_uncheck.isHidden{
             print("btn uncheck hidden")
            if isVehicleType {
                if (searchActive)
                {
                    self.selected_devie_id = selected_devie_id.filter(){$0 != filterdData1[indexPath.section].device_id}
                }
                else
                {
                    self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]
                }
                    single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
                }
                
            }
                
            else
            {
                single_selection_index = single_selection_index.filter(){$0 != indexPath.section}
                
                if (searchActive)
                {
                    self.selected_devie_id = selected_devie_id.filter(){$0 != alertFilteredData[indexPath.section].alert_type_id}
                }
                else
                {
                    self.selected_devie_id = selected_devie_id.filter(){$0 != all_selected_device_id[indexPath.section]}
                }
            }
        }
            
        else
        {
            print("btn uncheck not hidden")
            if isVehicleType {
                if (searchActive)
                {
                    self.selected_devie_id.append(filterdData1[indexPath.section].device_id)
                }
                else
                {
                    self.selected_devie_id.append(self.all_selected_device_id[indexPath.section])
                    print("selected device id:\(self.selected_devie_id)")
                }
                self.single_selection_index.append(indexPath.section)
            }
                
            else
            {
                if (searchActive)
                {
                    self.selected_alert_id.append(alertFilteredData[indexPath.section].alert_type_id)
                }
                else
                {
                    self.selected_alert_id.append(self.alert_type_id[indexPath.section])
                }
                self.single_selection_index.append(indexPath.section)
            }
        }
        
        print("selected device id:\(self.selected_devie_id)")
        self.tbl_shot_item.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count : Int = 0
        
        if(searchActive) {
            if (isVehicleType)
            {
                count = filterdData1.count
            }
            else
            {
                count = alertFilteredData.count
            }
        } else {
            if (isVehicleType)
            {
                count = data.count
            }
            else
            {
                count = alertData.count
            }
        }
        
        return count
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if isVehicleType
        {
            filterdData1 = data.filter { $0.registration_no.localizedCaseInsensitiveContains(searchText) }
            print("search data:\(filterdData1)")
            if(filterdData1.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tbl_shot_item.reloadData()
        }
        else
        {
            print("alert type data:\(alertData.map{$0.alert_type_name})")
            alertFilteredData = alertData.filter { $0.alert_type_name.localizedCaseInsensitiveContains(searchText) }
            print("search data:\(alertFilteredData)")
            if(alertFilteredData.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            self.tbl_shot_item.reloadData()
        }
    }
}
