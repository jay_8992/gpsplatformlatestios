//
//  SideBar.swift
//  TestGeoRadius
//
//  Created by Georadius on 27/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

protocol CallAnotherViewController {
    func CallViewController(val : String)
}

class SideBar: UIView {
    @IBOutlet weak var tbl_side_bar: UITableView!
    
    let num_section = 6
    var isExpend = false
    var extend_call : Int!
    let expend_col = [0, 0, 2, 3, 0]
    let names = ["", "Dashboard", "Feedback", "About", "Service Provider", "Logout"]
    var p_delegate:CallAnotherViewController?
    
    
    override func draw(_ rect: CGRect) {
       tbl_side_bar.delegate = self
       tbl_side_bar.dataSource = self
       let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tbl_side_bar.frame
        self.tbl_side_bar.backgroundView = tempImageView;
       self.tbl_side_bar.register(UINib(nibName: SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SIDEBAR_CELL)
       self.tbl_side_bar.register(UINib(nibName: SUB_SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SUB_SIDEBAR_CELL)
       showSideBar()
    }
 
    @IBAction func pressed_hide_side_bar(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.tbl_side_bar.frame.origin.x = -self.tbl_side_bar.frame.size.width
            }, completion: { (true) in
              
                self.removeFromSuperview()
                })
        
    }
//-----------------------------------------Intance Of NIB-------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "SideBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
//--------------------------------------------------------------------------------------------------------//
    
    func showSideBar(){
        UIView.animate(withDuration: 0.5, animations: {
            self.tbl_side_bar.frame.origin.x = 0
           
        })
    }
    
    @objc func expend_sideBar_pressed(sender : UIButton){
//        extend_call = sender.tag
//        if isExpend{
//            isExpend = false
//        }else{
//            isExpend = true
//        }
//
//        tbl_side_bar.reloadData()
    }

    @objc func tappedOnSectionHeader(sender : UIButton){
        p_delegate?.CallViewController(val: "Hello")
        if sender.tag == 2{
            
        }
       print(sender.tag)
    }
}

extension SideBar : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3{
            if isExpend && section == extend_call{
                return 3
            }else{
              return 0
            }
        }
        
        if section == 2{
            if isExpend && section == extend_call{
                return 2
            }else{
                return 0
            }
        }
        
        if section == 0{
             return 1
        }
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbl_side_bar.dequeueReusableCell(withIdentifier: SIDEBAR_CELL) as! SideBarCell
        let sub_cell = tbl_side_bar.dequeueReusableCell(withIdentifier: SUB_SIDEBAR_CELL) as! SubSideBarCell

        if indexPath.section > 0{
            return sub_cell
        }else{
             return cell
        }
       
    }
  //--------------------------------------------------------------------------------------------------------//
// ---------------------------------------------Section Work------------------------------------------------//
 //---------------------------------------------------------------------------------------------------------//
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return names.count
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view : UIView = UIView(frame: CGRect(x : 0, y : 0, width : tbl_side_bar.frame.size.width, height: tbl_side_bar.frame.size.height / 12))
        let name : UILabel = UILabel(frame: CGRect(x : tbl_side_bar.frame.size.width / 5.3, y : 0, width : tbl_side_bar.frame.size.width / 1.8, height: tbl_side_bar.frame.size.height / 12))
        
//        if section == num_section-1{
//            name.text = "LOGOUT"
//            name.font = name.font.withSize(8)
//
//        }
//        if section == 0{
//            name.text = "Abhishek"
//        }else{
//             name.text = names[section]
//        }
        
       // if section > 0{
            name.text = names[section]
        
       
        name.font = name.font.withSize(15)
        
        name.textAlignment = .left
        let originY = name.frame.size.height / 2.3
        let originX = tbl_side_bar.frame.size.width / 1.2
        let expend_sideBar : UIButton = UIButton(frame: CGRect(x : originX, y : originY, width : 10, height : 10))
        
        
       // let origImage = UIImage(named: Extend);
      //  let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        // let tempImageView = UIImageView(image: UIImage(named: "backImg3.jpg"))
//        expend_sideBar.setImage(UIImage(named: "backImg3.jpg"), for: .normal)
//        expend_sideBar.tintColor = UIColor.clear
        
        expend_sideBar.addTarget(self, action: #selector(expend_sideBar_pressed), for: .touchUpInside)
        expend_sideBar.tag = section
        
        let tappedButton : UIButton = UIButton(frame: CGRect(x : tbl_side_bar.frame.size.width / 5.3, y : 0, width : tbl_side_bar.frame.size.width / 1.8, height: tbl_side_bar.frame.size.height / 12))
        tappedButton.addTarget(self, action: #selector(tappedOnSectionHeader), for: .touchUpInside)
        tappedButton.backgroundColor = UIColor.clear
        tappedButton.tag = section
        
//        for check_col in 1..<num_section{
//            if section == expend_col[check_col]{
//                view.addSubview(expend_sideBar)
//
//            }
//        }
        view.addSubview(tappedButton)
        view.addSubview(name)
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section > 0{
           return tbl_side_bar.frame.size.height / 12.5
        }else{
            return 0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section > 0{
            return tbl_side_bar.frame.size.height / 12.5
        }else{
             return tbl_side_bar.frame.size.height / 4.5
        }

    }
    
}


