//
//  SideBarViewController.swift
//  TestGeoRadius
//
//  Created by Georadius on 15/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case Profile = 0
    case DashBoard
    case Geofancing
//    case MaintananceRecord
    case Settings
    case Feedback
    case About
    case Service
//    case Geofancing
//    case Notification_Setting
    case Logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class SideBarViewController: UIViewController {
    
    let alert_view = AlertView.instanceFromNib()
    
    var names = [""]
    
    let sideMenuTitleArr = ["","Home","Geofencing","Settings","Feedback","About","Service Provider","Logout"]
    
    var image_names = ["", "home_new","geofence","settings_new", "feedback_new", "about_new", "service_provider_new", "logout_new"]
    
    @IBOutlet weak var tbl_side_menu: UITableView!
    var dashboardViewController: UIViewController!
    var feebackViewController: UIViewController!
    var aboutviewVontroller: UIViewController!
    var serviceproviderViewController: UIViewController!
    var profileViewController: UIViewController!
    var serviceViewController: UIViewController!
    var geofancing: UIViewController!
    var notificationSetting: UIViewController!
    var settingsViewController: UIViewController!
    var maintenanceViewController:UIViewController!
    let cellSpacingHeight:CGFloat = 10.0
    
    let logoutView = MessagewithButtonView.instanceFromNib()
    var language = ""
    var bd:Bundle!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        logoutView.btn_yes.addTarget(self, action: #selector(pressed_yes_logout), for: .touchUpInside)
        logoutView.btn_no.addTarget(self, action: #selector(pressed_no_logout), for: .touchUpInside)
        self.tbl_side_menu.register(UINib(nibName: SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SIDEBAR_CELL)
        self.tbl_side_menu.register(UINib(nibName: SUB_SIDEBAR_CELL, bundle: nil), forCellReuseIdentifier: SUB_SIDEBAR_CELL)
        let tempImageView = UIImageView(image: UIImage(named: "back_img.png"))
        tempImageView.frame = self.tbl_side_menu.frame
        tempImageView.contentMode = .scaleAspectFill
        self.tbl_side_menu.backgroundView = tempImageView
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        self.profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.profileViewController = UINavigationController(rootViewController: profileViewController)
             
        self.dashboardViewController = storyboard.instantiateViewController(withIdentifier: "CheckLoginVC") as! CheckLoginVC
        self.dashboardViewController = UINavigationController(rootViewController: dashboardViewController)
        
        self.feebackViewController = storyboard.instantiateViewController(withIdentifier: "FeedBackVC") as! FeedBackVC
        self.feebackViewController = UINavigationController(rootViewController: feebackViewController)
        
        self.aboutviewVontroller = storyboard.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
        self.aboutviewVontroller = UINavigationController(rootViewController: aboutviewVontroller)
        
        self.serviceViewController = storyboard.instantiateViewController(withIdentifier: "ServiceProvider") as! ServiceProvider
        self.serviceViewController = UINavigationController(rootViewController: serviceViewController)
        
        self.geofancing = storyboard.instantiateViewController(withIdentifier: "GeofencingVC") as! GeofencingViewController
        self.geofancing = UINavigationController(rootViewController: geofancing)
        
        self.maintenanceViewController = storyboard.instantiateViewController(withIdentifier: "MaintenanceVC") as! MaintenanceRecordVC
        self.maintenanceViewController = UINavigationController(rootViewController: maintenanceViewController)
              
        self.notificationSetting = storyboard.instantiateViewController(withIdentifier: "NotificationSettingVC") as! NotificationSettingVC
        self.notificationSetting = UINavigationController(rootViewController: notificationSetting)
        
        self.settingsViewController = storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsViewController
        self.settingsViewController = UINavigationController(rootViewController: settingsViewController)

        tbl_side_menu.delegate = self
        tbl_side_menu.dataSource = self
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        print("in side view controller")
        super.viewWillAppear(animated)
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        if names.count > 0
        {
            names = []
        }
        setListTitles()
        tbl_side_menu.reloadData()
    }
    
      func setListTitles()
      {
          let language = UserDefaults.standard.value(forKey: "language") as! String
          print("language:\(language)")
          let bd = setLanguage(lang : language)
          names.append("")
          names.append(bd.localizedString(forKey: "HOME", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "GEOFENCING", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "SETTINGS", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "FEEDBACK", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "ABOUT", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "SERVICE_PROVIDER", value: nil, table: nil))
          names.append(bd.localizedString(forKey: "LOGOUT", value: nil, table: nil))
      
          self.setSwipeActions()
      }
      
    func setSwipeActions()
    {
        if let arr = UserDefaults.standard.array(forKey: "MenuPermArr") as? [String] {
            
            if !arr.contains(where: {$0 == "46"})
            {
                if let index = names.firstIndex(of: LanguageHelperClass().geofencingTxt) {
                    names.remove(at: index)
                    image_names.remove(at: index)
                } else {
                    // not found
                    print("arr not found")
                }
            }}
    }
    
    /*
     * This way we instantiate ViewController when needed, not on SideMenu initialization (viewDidLoad), because we don't want it in memory until we go to that view
     */
    func instantiateDynamicallyCreatedViewControllers() {
        let serviceViewController = ServiceProvider()
        serviceViewController.view.backgroundColor = UIColor.orange
        self.serviceViewController = UINavigationController(rootViewController: serviceViewController)
    }
    
    /*
     * This way we deallocate ViewController after changing to another ViewController, so we don't keep it in memory all the time
     */
    func deallocateDynamicallyCreatedViewControllers() {
        self.serviceViewController = nil
    }
    
    @objc func pressed_yes_logout(){
         LogoutFromServer()
    }
    
    @objc func pressed_no_logout(){
        logoutView.removeFromSuperview()
    }
    
    func changeViewController(menu: LeftMenu) {
       // deallocateDynamicallyCreatedViewControllers()
        
        switch menu {
            
        case .Profile:
            self.revealViewController().pushFrontViewController(self.profileViewController, animated: true)

            break
        case .DashBoard:
            self.revealViewController().pushFrontViewController(self.dashboardViewController, animated: true)
          
            break
        case .About:
            self.revealViewController().pushFrontViewController(self.aboutviewVontroller, animated: true)
            break
        case .Feedback:
            self.revealViewController().pushFrontViewController(self.feebackViewController, animated: true)
            break
        
        case .Service:
           // instantiateDynamicallyCreatedViewControllers()
            self.revealViewController().pushFrontViewController(self.serviceViewController, animated: true)
            break
//        case .Geofancing:
//            instantiateDynamicallyCreatedViewControllers()
//            self.revealViewController().pushFrontViewController(self.geofancing, animated: true)
//            break
//
        case .Settings:

            self.revealViewController().pushFrontViewController(self.settingsViewController, animated: true)
            
        case .Logout:
            logoutView.frame.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            UIApplication.shared.keyWindow!.addSubview(logoutView)
            
        case .Geofancing:
                // instantiateDynamicallyCreatedViewControllers()
                self.revealViewController().pushFrontViewController(self.geofancing, animated: true)
                
   //     case .MaintananceRecord:
   //           instantiateDynamicallyCreatedViewControllers()
   //         self.revealViewController().pushFrontViewController(self.maintenanceViewController, animated: true)
                
        }
        
    }
    
    func LogoutFromServer(){
        print("Logout")
        
        UIApplication.shared.keyWindow!.addSubview(alert_view)
        
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let device_token = UserDefaults.standard.value(forKey: "DEVICE_TOKEN") as? String ?? ""
        
        let urlString = domain_name + "/login_result.php?action=logoutApp&user_id=" + user_id + "&user_app_id=" + device_token
        
        NetworkManager().CallUpdateDataOnServer(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                //showToast(controller: self, message : "Logout", seconds: 2.0)
            }else{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
                print("ERROR FOUND")
            }
            if r_error != nil{
                showToast(controller: self, message : LanguageHelperClass.getSeverError(), seconds: 2.0)
            }
            
            self.alert_view.removeFromSuperview()
            SaveLoginKey(key : "")
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDel.window?.rootViewController = loginVC
        })
    }

}

extension SideBarViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
           return self.names.count
    }
       
       // There is just one row in every section
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 1
       }
       
       // Set the spacing between sections
       func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
           if section == 0
           {
               return 3.0
           }
           return cellSpacingHeight
       }
       
       // Make the background color show through
       func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView = UIView()
           headerView.backgroundColor = UIColor.clear
           return headerView
       }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbl_side_menu.dequeueReusableCell(withIdentifier: SIDEBAR_CELL) as! SideBarCell
        let sub_cell = tbl_side_menu.dequeueReusableCell(withIdentifier: SUB_SIDEBAR_CELL) as! SubSideBarCell
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let phone = UserDefaults.standard.value(forKey: PHONE) as! String
        if indexPath.section > 0 {
            sub_cell.layer.cornerRadius = 10.0
            sub_cell.layer.borderWidth = 0.5
            sub_cell.layer.borderColor = UIColor.lightGray.cgColor
            sub_cell.lbl_name.text = names[indexPath.section]
            sub_cell.img_item.image = UIImage(named: image_names[indexPath.section])
            return sub_cell
        }else{
            cell.layer.cornerRadius = 12.0
            cell.layer.borderWidth = 0.5
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.lbl_name.text = user_name.firstUppercased
            cell.lbl_contact.text = phone
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return tbl_side_menu.frame.size.height / 4.5
        } else {
            return tbl_side_menu.frame.size.height / 10
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.section) {
            self.changeViewController(menu: menu)
            
            switch menu {
            case .Profile: break
            case .DashBoard: break
            case .About: break
            case .Feedback: break
            case .Service: break
            case .Settings: break
            case .Logout:
                break
            case .Geofancing:
                break
//            case .MaintananceRecord:
//                break
            
            }
        }
    }
}
