//
//  GeofenceDeviceModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 07/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class GeofenceDeviceModel
    {
    var registrationNo : String!
    var IMEINo : String!
    var voiceNo : String!
    var vehicleType : String!
    var billingValidity : String!
    var device_tag : String
    var device_id : String
    
    init(registrationNo: String, IMEINo: String, voiceNo: String, vehicleType: String, billingValidity:String,device_tag:String,device_id:String) {
        self.registrationNo = registrationNo
        self.IMEINo = IMEINo
        self.voiceNo = voiceNo
        self.vehicleType = vehicleType
        self.billingValidity = billingValidity
        self.device_tag = device_tag
        self.device_id = device_id
    }
}
