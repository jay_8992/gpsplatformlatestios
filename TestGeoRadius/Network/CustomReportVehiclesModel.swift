//
//  CustomReportVehiclesModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 17/09/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class CustomReportVehiclesModel
    {
    var registrationNo : String!
    var date : String!
    var distanceCovered : String!
    var initialVolume : String!
    var finalVolume : String!
    var actualFuelConsumption : String
    var refillingVolume : String
    var drainingVolume : String
    var fuelEconomy:String
    var fuelEconomyPerHour:String
   
    init(registrationNo: String,date: String,distanceCovered: String,initialVolume: String,finalVolume: String, actualFuelConsumption:String,refillingVolume:String,drainingVolume:String,fuelEconomy:String,fuelEconomyPerHour:String) {
        self.registrationNo = registrationNo
        self.date = date
        self.distanceCovered = distanceCovered
        self.initialVolume = initialVolume
        self.finalVolume = finalVolume
        self.actualFuelConsumption = actualFuelConsumption
        self.refillingVolume = refillingVolume
        self.drainingVolume =  drainingVolume
        self.fuelEconomy = fuelEconomy
        self.fuelEconomyPerHour = fuelEconomyPerHour
    }
}
