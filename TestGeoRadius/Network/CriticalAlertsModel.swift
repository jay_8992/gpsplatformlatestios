//
//  CriticalAlertsModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class CriticalAlertsModel
    {
    var alert_id : String!
    var alert_time : String!
    var registration_no : String!
    var alert_type_name : String!
   
    init(alert_id: String, alert_time: String, registration_no: String, alert_type_name:String) {
        self.alert_id = alert_id
        self.alert_time = alert_time
        self.registration_no = registration_no
        self.alert_type_name = alert_type_name
    }
}
