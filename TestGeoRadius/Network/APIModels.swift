//
//  APIDemoModels.swift
//  APIManagerDemo
//
//  Created by Pavle Pesic on 4/11/19.
//  Copyright (c) 2019 Pavle Pesic. All rights reserved.
//

import UIKit

enum APIDemo {
    
    struct User: Codable {
        let fullName: String
        let email: String
        let isPrimeUser: Bool
        let paymentMethods: [String]
    }
    
    struct UsersList: Decodable
    {
        let data : [DataModal]
    }
    
    struct DataModal: Decodable {
        
        let username: String
        let email : String
        let phone: String
        let userid : String
    }
    
    
    struct VehicleList: Decodable {
        
        let data : VehicleData
        
    }
    
    struct VehicleData: Decodable
    {
        
        let return_json: [JSONData]
    
    }
    
    struct JSONData: Decodable {
        let registration_no: String?
        let serial_no: String?
        let voice_no: String?
        let vehicle_type_name: String?
        let next_billing_date: String?
        let device_tag : String?
        let device_id : String?
        let vehicle_type_id : String?
        let fleet_driver_name : String?
        let driver_phone_number : String?
        let driver_id : String?
    }
    
    
    struct CriricalAlertsList : Decodable {
        
        let data : [CriticalAlertData]
    }
    
    struct CriticalAlertData : Decodable {
           let alert_id: String?
           let alert_time: String?
           let registration_no: String?
           let alert_type_name: String?
          
       }
    
    struct VehicleDistance : Decodable {
           
           let data : [DistanceData]
       }
       
       struct DistanceData : Decodable {
              let device_id: String?
              let registration_number: String?
              let total_distance: String?
             
          }
       
  // CustomReportData
    struct CustomReportData : Decodable
    {
        let vehicleno : String?
        let starttime : Int?
        let fuelVolumeInitial : Double?
        let fuelVolumeFinal : Double?
        let drainingsVolume : Double?
        let fuelConsumptionActual : Double?
        let km : Double?
    }
    
}
