
//  MultipleLanguageSupoort.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation
import UIKit

class LanguageHelperClass {
    
    var bd:Bundle!
    var language:String!
    var recurringPeriod = ""
    var serverErrorText = ""
    var editUserText = ""
    var userAddText = ""
    var companyNameText = ""
    var emailText = ""
    var phoneText = ""
    var submitTxt = ""
    var vehicleAssignTxt = ""
    var grpVehicleTxt = ""
    var userVehicleTxt = ""
    var assignTxt = ""
    var unAssignTxt = ""
    var deleteTxt = ""
    var deleteUserTxt = ""
    var deleteAlertTxt = ""
    var resetPasswordTxt = ""
    var passwordTxt = ""
    var confirmPswdTxt = ""
    var re_enterTxt = ""
    var addUserTxt = ""
    var regNoTxt = ""
    var voiceNoTxt = ""
    var vehicleTypeTxt = ""
    var billValText = ""
    var groupNameTxt = ""
    var userNameTxt = ""
    var deviceSerialTxt = ""
    var deviceTagTxt = ""
    var vehicleTxt = ""
    var vehicleEditTxt = ""
    var editTxt = ""
    var resetTxt = ""
    var mondayTxt = ""
    var tuesdayTxt = ""
    var wednesdayTxt = ""
    var thursdayTxt = ""
    var fridayTxt = ""
    var saturdayTxt = ""
    var sundayTxt = ""
    var monTxt = ""
    var tueTxt = ""
    var wedTxt = ""
    var thuTxt = ""
    var friTxt = ""
    var satTxt = ""
    var sunTxt = ""
    
    var startTimeTxt = ""
    var endTimeTxt = ""
    var saveTxt = ""
    var activeDaysTxt = ""
    var activeTimeTxt = ""
    var notificationsTxt = ""
    var notifTitleTxt = ""
    var notifSettingTxt = ""
    
    var dashboardTxt = ""
    var changeLangTxt = ""
    var settingsTxt = ""
    
    var selDefPageTxt = ""
    var dashPageTxt = ""
    var livePageTxt = ""
    var mapPageTxt = ""
    var selDefLangTxt = ""
    
    var dateFromTxt = ""
    var dateToTxt = ""
    
    var createGeofenceTxt = ""
    var geofencingTxt = ""
    var geofenceCheckTxt = ""
    var geofenceNameTxt = ""
    var overstayTxt = ""
    var restrictionTxt = ""
    var fromDateTimeTxt = ""
    var toDateTimeTxt = ""
    var selectDateTimeTxt = ""
    var ok_DoneTxt = ""
    var cancelTxt = ""
    var searchTxt = ""
    
    var activityText = ""
    var reminderTypeTxt = ""
    var alertNotifTxt = ""
    var addMaintenanceTxt = ""
    var maintenanceSubText = ""
    var selectVehicleTxt = ""
    var hrsTxt = ""
    var kmTxt = ""
    var kmhTxt = ""
    var distanceTxt = ""
    var timeTxt = ""
    var tripTxt = ""
    
    var alertsTxt = ""
    var fleetUSageTxt = ""
    var overspeedTxt = ""
    var mapHistoryTxt = ""
    var tripsTxt = ""
    var vehicleDistanceTxt = ""
    var fuelTxt = ""
    var demindCommandTxt = ""
    var cameraTxt = ""
    
    var homeTxt = ""
    var feedbackTxt = ""
    var aboutTxt = ""
    var serviceProviderTxt = ""
    var logoutTxt = ""
    
    var reportsTxt = ""
    var billingsTxt = ""
    var userTxt = ""
    
    var temperatureTxt = ""
    var customRptTxt = ""
    
    init() {
        self.language = UserDefaults.standard.value(forKey: "language") as? String
        self.bd = setLanguage(lang : self.language)
        self.recurringPeriod = self.bd.localizedString(forKey: "RECURING_PERIOD", value: nil, table: nil)
        self.editUserText = self.bd.localizedString(forKey: "EDIT_USER_INFO", value: nil, table: nil)
        self.userAddText = self.bd.localizedString(forKey: "USER_ADDRESS", value: nil, table: nil)
        self.companyNameText = self.bd.localizedString(forKey: "COMPANY_NAME", value: nil, table: nil)
        self.emailText = self.bd.localizedString(forKey: "EMAIL", value: nil, table: nil)
        self.phoneText = self.bd.localizedString(forKey: "PHONE", value: nil, table: nil)
        self.submitTxt = self.bd.localizedString(forKey: "SUBMIT", value: nil, table: nil)
        self.vehicleAssignTxt = self.bd.localizedString(forKey: "VEHICLE_ASSIGNMENT", value: nil, table: nil)
        self.grpVehicleTxt = self.bd.localizedString(forKey: "GROUP_VEHICLE_LIST", value: nil, table: nil)
        self.userVehicleTxt = self.bd.localizedString(forKey: "USER_VEHICLE_LIST", value: nil, table: nil)
        self.assignTxt = self.bd.localizedString(forKey: "ASSIGN", value: nil, table: nil)
        self.unAssignTxt = self.bd.localizedString(forKey: "UN_ASSIGN", value: nil, table: nil)
        self.deleteTxt = self.bd.localizedString(forKey: "DELETE", value: nil, table: nil)
        self.deleteUserTxt = self.bd.localizedString(forKey: "DELETE_USER", value: nil, table: nil)
        self.deleteAlertTxt = self.bd.localizedString(forKey: "DELETE_ALERT", value: nil, table: nil)
        self.resetPasswordTxt = self.bd.localizedString(forKey: "RESET_PASSWORD", value: nil, table: nil)
        self.passwordTxt = self.bd.localizedString(forKey: "PASSWORD", value: nil, table: nil)
        self.confirmPswdTxt = self.bd.localizedString(forKey: "CONFIRM_PASSWORD", value: nil, table: nil)
        self.re_enterTxt = self.bd.localizedString(forKey: "RE_ENTER", value: nil, table: nil)
        self.addUserTxt = self.bd.localizedString(forKey: "ADD_USER", value: nil, table: nil)
        self.regNoTxt = self.bd.localizedString(forKey: "REGISTRATION_NO", value: nil, table: nil)
        self.voiceNoTxt = self.bd.localizedString(forKey: "VOICE_NO", value: nil, table: nil)
        self.vehicleTypeTxt = self.bd.localizedString(forKey: "VEHICLE_TYPE", value: nil, table: nil)
        self.billValText = self.bd.localizedString(forKey: "BILLING_VALIDITY", value: nil, table: nil)
        self.groupNameTxt = self.bd.localizedString(forKey: "GROUP_NAME", value: nil, table: nil)
        self.userNameTxt = self.bd.localizedString(forKey: "USER_NAME", value: nil, table: nil)
        self.vehicleTxt = self.bd.localizedString(forKey: "VEHICLE", value: nil, table: nil)
        self.deviceSerialTxt = self.bd.localizedString(forKey: "DEVICE_SERIAL", value: nil, table: nil)
        self.deviceTagTxt = self.bd.localizedString(forKey: "DEVICE_TAG", value: nil, table: nil)
        self.vehicleEditTxt = self.bd.localizedString(forKey: "VEHICLE_EDIT", value: nil, table: nil)
        self.editTxt = self.bd.localizedString(forKey: "EDIT_VEHICLE", value: nil, table: nil)
        self.resetTxt = self.bd.localizedString(forKey: "RESET", value: nil, table: nil)
        self.mondayTxt = self.bd.localizedString(forKey: "MONDAY", value: nil, table: nil)
        self.tuesdayTxt = self.bd.localizedString(forKey: "TUESDAY", value: nil, table: nil)
        self.wednesdayTxt = self.bd.localizedString(forKey: "WEDNESDAY", value: nil, table: nil)
        self.thursdayTxt = self.bd.localizedString(forKey: "THURSDAY", value: nil, table: nil)
        self.fridayTxt = self.bd.localizedString(forKey: "FRIDAY", value: nil, table: nil)
        self.saturdayTxt = self.bd.localizedString(forKey: "SATURDAY", value: nil, table: nil)
        self.sundayTxt = self.bd.localizedString(forKey: "SUNDAY", value: nil, table: nil)
        self.monTxt = self.bd.localizedString(forKey: "MON", value: nil, table: nil)
        self.tueTxt = self.bd.localizedString(forKey: "TUE", value: nil, table: nil)
        self.wedTxt = self.bd.localizedString(forKey: "WED", value: nil, table: nil)
        self.thuTxt = self.bd.localizedString(forKey: "THU", value: nil, table: nil)
        self.friTxt = self.bd.localizedString(forKey: "FRI", value: nil, table: nil)
        self.satTxt = self.bd.localizedString(forKey: "SAT", value: nil, table: nil)
        self.sunTxt = self.bd.localizedString(forKey: "SUN", value: nil, table: nil)
        self.startTimeTxt =  self.bd.localizedString(forKey: "ACTIVE_TIME_START", value: nil, table: nil)
        self.endTimeTxt =  self.bd.localizedString(forKey: "ACTIVE_TIME_END", value: nil, table: nil)
        self.saveTxt =  self.bd.localizedString(forKey: "SAVE", value: nil, table: nil)
        self.activeDaysTxt = self.bd.localizedString(forKey: "ACTIVE_DAYS", value: nil, table: nil)
        self.activeTimeTxt = self.bd.localizedString(forKey: "ACTIVE_TIME", value: nil, table: nil)
        self.notificationsTxt = self.bd.localizedString(forKey: "NOTIFICATIONS", value: nil, table: nil)
        self.notifTitleTxt = self.bd.localizedString(forKey: "NOTIFICATION_TEXT", value: nil, table: nil)
        self.notifSettingTxt = self.bd.localizedString(forKey: "NOTIFICATION_SETTING", value: nil, table: nil)
        self.dashboardTxt = self.bd.localizedString(forKey: "DASHBOARD", value: nil, table: nil)
        self.changeLangTxt = self.bd.localizedString(forKey: "CHANGE_LANGUAGE", value: nil, table: nil)
        self.settingsTxt = self.bd.localizedString(forKey: "SETTINGS", value: nil, table: nil)
        self.selDefPageTxt = self.bd.localizedString(forKey: "SELECT_DEFAULT_PAGE", value: nil, table: nil)
        self.dashPageTxt = self.bd.localizedString(forKey: "DASHBOARD_PAGE", value: nil, table: nil)
        self.livePageTxt = self.bd.localizedString(forKey: "LIVE_PAGE", value: nil, table: nil)
        self.mapPageTxt = self.bd.localizedString(forKey: "MAP_PAGE", value: nil, table: nil)
        self.selDefLangTxt = self.bd.localizedString(forKey: "SELECT_DEFAULT_LANG", value: nil, table: nil)
        self.dateFromTxt = self.bd.localizedString(forKey: "DATE_FROM", value: nil, table: nil)
        self.dateToTxt = self.bd.localizedString(forKey: "DATE_TO", value: nil, table: nil)
        
        self.createGeofenceTxt = self.bd.localizedString(forKey: "GEOFENCE_CREATE", value: nil, table: nil)
        self.geofencingTxt = self.bd.localizedString(forKey: "GEOFENCING", value: nil, table: nil)
        self.geofenceNameTxt = self.bd.localizedString(forKey: "GEOFENCE_NAME", value: nil, table: nil)
        self.geofenceCheckTxt = self.bd.localizedString(forKey: "GEOFENCE_CHECK", value: nil, table: nil)
        self.overstayTxt = self.bd.localizedString(forKey: "OVERSTAY_TEXT", value: nil, table: nil)
        self.restrictionTxt = self.bd.localizedString(forKey: "DATE_RISTRICTION", value: nil, table: nil)
        self.fromDateTimeTxt = self.bd.localizedString(forKey: "FROM_DATE_TIME", value: nil, table: nil)
        self.toDateTimeTxt = self.bd.localizedString(forKey: "TO_DATE_TIME", value: nil, table: nil)
        self.selectDateTimeTxt = self.bd.localizedString(forKey: "SELECT_DATE_TIME", value: nil, table: nil)
        self.ok_DoneTxt = self.bd.localizedString(forKey: "OK_SUBMIT", value: nil, table: nil)
        self.cancelTxt = self.bd.localizedString(forKey: "CANCEL", value: nil, table: nil)
        self.searchTxt = self.bd.localizedString(forKey: "SEARCH", value: nil, table: nil)

        self.activityText = self.bd.localizedString(forKey: "ACTIVITY_NAME", value: nil, table: nil)
        self.reminderTypeTxt = self.bd.localizedString(forKey: "REMINDER_TYPE", value: nil, table: nil)
        self.alertNotifTxt = self.bd.localizedString(forKey: "ALERT_NOTIF_CUTOFF", value: nil, table: nil)
        self.addMaintenanceTxt = self.bd.localizedString(forKey: "ADD_MAINTENANCE_RECORD", value: nil, table: nil)
        self.maintenanceSubText = self.bd.localizedString(forKey: "ADD_MAINTENANCE_SUBMIT", value: nil, table: nil)
        self.selectVehicleTxt = self.bd.localizedString(forKey: "SELECT_VEHICLE", value: nil, table: nil)
        self.hrsTxt = self.bd.localizedString(forKey: "HRS_TEXT", value: nil, table: nil)
        self.kmTxt = self.bd.localizedString(forKey: "KM_TEXT", value: nil, table: nil)
        self.kmhTxt = self.bd.localizedString(forKey: "KMH_TEXT", value: nil, table: nil)
        self.distanceTxt = self.bd.localizedString(forKey: "DISTANCE", value: nil, table: nil)
        self.timeTxt = self.bd.localizedString(forKey: "TIME", value: nil, table: nil)
        self.tripTxt = self.bd.localizedString(forKey: "TRIP", value: nil, table: nil)

        self.alertsTxt = self.bd.localizedString(forKey: "ALERTS", value: nil, table: nil)
        self.fleetUSageTxt = self.bd.localizedString(forKey: "FLEET_USAGE", value: nil, table: nil)
        self.overspeedTxt = self.bd.localizedString(forKey: "OVERSPEED", value: nil, table: nil)
        self.mapHistoryTxt = self.bd.localizedString(forKey: "MAP_HISTORY", value: nil, table: nil)
        self.tripsTxt = self.bd.localizedString(forKey: "TRIPS", value: nil, table: nil)
        self.vehicleDistanceTxt = self.bd.localizedString(forKey: "VEHICLE_DISTANCE", value: nil, table: nil)
        self.fuelTxt =  self.bd.localizedString(forKey: "FUEL", value: nil, table: nil)
        self.demindCommandTxt = self.bd.localizedString(forKey: "DEVICE_COMMAND", value: nil, table: nil)
        self.cameraTxt = self.bd.localizedString(forKey: "CAMERA", value: nil, table: nil)
        
        self.homeTxt = self.bd.localizedString(forKey: "HOME", value: nil, table: nil)
        self.feedbackTxt = self.bd.localizedString(forKey: "FEEDBACK", value: nil, table: nil)
        self.aboutTxt = self.bd.localizedString(forKey: "ABOUT", value: nil, table: nil)
        self.serviceProviderTxt = self.bd.localizedString(forKey: "SERVICE_PROVIDER", value: nil, table: nil)
        self.logoutTxt = self.bd.localizedString(forKey: "LOGOUT", value: nil, table: nil)
        
        self.reportsTxt = self.bd.localizedString(forKey: "REPORTS", value: nil, table: nil)
        self.billingsTxt = self.bd.localizedString(forKey: "BILLING", value: nil, table: nil)
        self.userTxt = self.bd.localizedString(forKey: "USER", value: nil, table: nil)
        self.temperatureTxt = self.bd.localizedString(forKey: "TEMPERATURE", value: nil, table: nil)
        self.customRptTxt = self.bd.localizedString(forKey: "CUSTOM_REPORT", value: nil, table: nil)


}
    
    static func getSeverError() -> String
    {
        let language1 = UserDefaults.standard.value(forKey: "language") as! String
        let bundle = setLanguage(lang : language1)
        return bundle.localizedString(forKey: "ERROR_TEXT", value: nil, table: nil)
    }


   static func getInternetError() -> String
    {
        let language1 = UserDefaults.standard.value(forKey: "language") as! String
        let bundle = setLanguage(lang : language1)
        return bundle.localizedString(forKey: "NO_INTERNET_CONNNECTION", value: nil, table: nil)
    }
    
}


func getLanguageType() -> String {
    return UserDefaults.standard.value(forKey: "language") as! String
}


   
   
   
