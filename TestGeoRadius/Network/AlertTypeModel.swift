//
//  AlertTypeModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 23/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class AlertTypeModel{
    
    var alert_type_name : String!
    var alert_type_id : String!
    
    init(alert_type_name: String, alert_type_id: String) {
        self.alert_type_name = alert_type_name
        self.alert_type_id = alert_type_id
    }
    
    
}
