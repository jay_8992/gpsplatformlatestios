//
//  MapVehicleModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class MapVehicleModel{
    
    var registration_no : String!
    var device_id : String!
    var vehicle_type_id: String!
   
    init(registration_no: String, device_id: String, vehicle_type_id:String){
        self.registration_no = registration_no
        self.device_id = device_id
        self.vehicle_type_id = vehicle_type_id
    }
    
    
}
