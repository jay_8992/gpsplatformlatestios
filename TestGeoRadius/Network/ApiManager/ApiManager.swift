//
//  ApiManager.swift
//  Helbiz
//
//  Created by Pavle Pesic on 4/28/18.
//  Copyright © 2018 Pavle Pesic. All rights reserved.
//

import Alamofire

class APIManager {
    
    // MARK: - Vars & Lets
    
    private let sessionManager: SessionManager
    private let retrier: APIManagerRetrier
    static let networkEnviroment: NetworkEnvironment = .dev
    
    // MARK: - Public methods
    
    
    func call<T>(type: EndPointType, url:String, params: Parameters? = nil, handler: @escaping (Swift.Result<T, AlertMessage>) -> Void) where T: Decodable {
        let url1 = URL(string: url)!
        var urlRequest = URLRequest(url: url1)
        urlRequest.timeoutInterval = 60
        print("url request:\(urlRequest)")
        self.sessionManager.request(urlRequest).validate().responseJSON { (data) in
            do {
                guard let jsonData = data.data else {
                    throw AlertMessage(title: "\(Error.self)", body: "No data")
                }
                print("json data:\(jsonData)")
                let result = try JSONDecoder().decode(T.self, from: jsonData)
                print("result:\(result)")
                handler(.success(result))
                self.resetNumberOfRetries()
            } catch {
                if let error = error as? AlertMessage {
                    return handler(.failure(error))
                }
                
                handler(.failure(self.parseApiError(data: data.data)))
            }

        }
    }
    
    // MARK: - Private methods
    
    private func resetNumberOfRetries() {
        self.retrier.numberOfRetries = 0
    }
    
    private func parseApiError(data: Data?) -> AlertMessage {
        let decoder = JSONDecoder()
        if let jsonData = data, let error = try? decoder.decode(NetworkError.self, from: jsonData) {
            return AlertMessage(title: Constants.errorAlertTitle, body: error.key ?? error.message)
        }
        return AlertMessage(title: Constants.errorAlertTitle, body: Constants.genericErrorMessage)
    }
    
    // MARK: - Initialization
    
    init(sessionManager: SessionManager, retrier: APIManagerRetrier) {
        self.sessionManager = sessionManager
        self.retrier = retrier
        self.sessionManager.retrier = self.retrier
    }
    
}
