//
//  CoordinatesModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 24/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class CoordinatesModel:CustomStringConvertible {
    
    let latitude: Double
    let longitude: Double
    
    init(latitude: Double, longitude: Double){
        self.latitude = latitude
        self.longitude = longitude
    }

    var description: String {
        return "lat: \(latitude), long: \(longitude)"
    }
}
