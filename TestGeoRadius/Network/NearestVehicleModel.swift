//
//  NearestVehicleModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class NearestVehicleModel{
    
    var vehicle_no : String!
    var vehicle_duration: String!
    var driver_name: String!
    var driver_no: String!
    
    init(vehicle_no: String, vehicle_duration: String, driver_name:String, driver_no:String){
        self.vehicle_no = vehicle_no
        self.vehicle_duration = vehicle_duration
        self.driver_name = driver_name
        self.driver_no = driver_no
    }
}
