//
//  VehicleTypeModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 24/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class VehicleTypeModel{
    
    var vehicle_type_name : String!
    var vehicle_type_id: String!
   
    init(vehicle_type_name: String, vehicle_type_id: String){
        self.vehicle_type_name = vehicle_type_name
        self.vehicle_type_id = vehicle_type_id
    }
}
