//
//  ConnectionManager.swift
//  TestGeoRadius
//
//  Created by Georadius on 23/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

/*
public protocol NetworkStatusListener : class {
    func networkStatusDidChange(status: Reachability.Connection)
}

class ConnectionManager {

static let sharedInstance = ConnectionManager()
private var reachability : Reachability!
var listeners = [NetworkStatusListener]()

    func addListener(listener: NetworkStatusListener){
        listeners.append(listener)
    }
    
    func removeListener(listener: NetworkStatusListener){
        listeners = listeners.filter{ $0 !== listener}
    }
    
func observeReachability() {
    do {
           try  self.reachability = Reachability()
       }
   catch(let error) {
       print("Error occured while starting reachability notifications : \(error.localizedDescription)")
   }
    NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
    do {
        try self.reachability.startNotifier()
    }
    catch(let error) {
        print("Error occured while starting reachability notifications : \(error.localizedDescription)")
    }
}
    
    

@objc func reachabilityChanged(note: Notification) {
    let reachability = note.object as! Reachability
    switch reachability.connection {
    case .cellular:
        print("Network available via Cellular Data.")
        break
    case .wifi:
        print("Network available via WiFi.")
        break
    case .none:
        print("Network is not available.")
        break
    case .unavailable:
        print("Network is  unavailable.")
        break
    }
    for listener in listeners {
        listener.networkStatusDidChange(status: reachability.connection)
       }
    
  }
}
*/
