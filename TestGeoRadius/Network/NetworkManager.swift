//
//  NetworkManager.swift
//  TestGeoRadius
//
//  Created by Georadius on 02/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

//
//  ServiceNework.swift
//  Test
//
//  Created by Georadius on 14/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation
import Alamofire

class ServiceManager {
    
    //  private lazy var networkManager = NetworkManager()
    
    lazy var sessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpCookieStorage = nil
        configuration.httpCookieAcceptPolicy = HTTPCookie.AcceptPolicy.never
        let manager = SessionManager(configuration: configuration)
        manager.retrier = self
        return manager
    }()
    
    static var instance: ServiceManager?
    
    static func sharedInstance() -> ServiceManager {
        if self.instance == nil {
            self.instance = ServiceManager()
        }
        return self.instance!
    }
    
    func CallTrackResult(urlString : String?, completionHandler: @escaping (_ responseObject: Dictionary<String,Any>?, _ error: Error?, _ isNetwork : Bool) -> ()) {
        let url = URL(string: urlString!)!
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = 30
        Alamofire.request(urlRequest)
            .response { response in
                do {
                    let myData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableLeaves)
                    completionHandler(myData as? Dictionary<String, Any>, response.error, true)
                } catch _ {
                    print("error:\(response.error)")
                    completionHandler(nil, nil, false)
                }
        }
    }
    
    func CallWebService(urlString : String?, completionHandler: @escaping (_ responseObject: Dictionary<String,Any>?, _ error: Error?, _ isNetwork : Bool) -> ()) {
        
        // let urlString = Base_Url + value!
        Alamofire.request(urlString!, encoding: JSONEncoding.default).responseJSON { response in
            
            if (NetworkReachabilityManager()?.isReachable)!{
                // print("Network is Open")
                print("response:\(response)")
                
                if let response_d = response.result.value as? Dictionary<String, Any>
                {
                    completionHandler(response_d, response.error, true)
                }
                else
                {
                    completionHandler(nil, response.error, false)
                }}
            else{
                completionHandler(nil, nil, false)
            }
        }
    }
    
    
    func CallDashboardData(urlString : String?, completionHandler: @escaping (_ responseObject: Dictionary<String,Any>?, _ error: Error?, _ isNetwork : Bool) -> ()) {
        let url = URL(string: urlString!)!
        var urlRequest = URLRequest(url: url)
        urlRequest.timeoutInterval = 30
        Alamofire.request(urlRequest)
            .response { response in
                do {
                    let myData = try JSONSerialization.jsonObject(with: response.data!, options: .mutableLeaves)
                    print("response time in network page :\(response.timeline.totalDuration)")
                    completionHandler(myData as? Dictionary<String, Any>, response.error, true)
                } catch _ {
                    print("error:\(String(describing: response.error))")
                    completionHandler(nil, nil, false)
                }
        }
    }
    
    func GetMenuReportsFromServer (urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                print("response data:\(String(describing: data))")
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    completionHandler(c_data as Array<Any>, nil, true)
                    break
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                completionHandler(nil, nil, false)
                print("ERROR FOUND :\(String(describing: r_error))")
            }
            
        })
    }
    
    func CallReportDataFromServer(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String, Any>
                    let alert_data = c_data["alert_data"] as! Array<Any>
                    completionHandler(alert_data as Array<Any>, nil, true)
                    
                    break
                    
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                completionHandler(nil, nil, false)
                //print("ERROR FOUND")
            }
            
        })
    }
    
    
    func CallVehicleData(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    if let c_data = data?[K_Data] as? Dictionary<String,Any>
                    {
                        let val = c_data["return_json"] as! Array<Any>
                        completionHandler(val as Array<Any>, nil, true)
                        break
                    }
                    
                    if let c_data = data?[K_Data] as? Array<Any>
                    {
                        completionHandler(c_data as Array<Any>, nil, true)
                    }
                    
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
            }else{
                //print("ERROR FOUND")
            }
            
        })
    }
    
    func CallFleetReport(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool, _ idle: String?, _ Moving: String?, _ Stopped: String?) -> ()){
        
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["fleet_data"] as! Array<Any>
                    let idle = c_data["maxValueIdle"] as! String
                    let moving = c_data["maxValueMov"] as! String
                    let stop = c_data["maxValueStop"] as! String
                    completionHandler(val as Array<Any>, nil, true, idle, moving, stop)
                    
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true, nil, nil, nil)
                    break
                default:
                    completionHandler(nil, "No Result Found", true, nil, nil, nil)
                    print("Default Case")
                }
                
            }else{
                
                //  print("ERROR FOUND")
            }
            
        })
    }
    
    func CallFuelReport(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let val = c_data["fuel_data"] as! Array<Any>
                    completionHandler(val as Array<Any>, nil, true)
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                // print("ERROR FOUND")
            }
            
        })
    }
    
    func CallTripReport(urlString: String, key_val: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let array = Array(c_data.keys)
                    
                    var isVal = false
                    for match_key in array{
                        if match_key == key_val{
                            isVal = true
                        }
                    }
                    
                    if isVal{
                        let val = c_data[key_val] as! Array<Any>
                        completionHandler(val as Array<Any>, nil, true)
                    }else{
                        completionHandler(nil, "No Result Found", true)
                    }
                    
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                // print("ERROR FOUND")
            }
            
        })
    }
    
    
    
    func CallOverSpeedReport(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let ps = c_data["parent_data"] as! Array<Any>
                    completionHandler(ps, nil, true)
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
            }
            else{
                //  print("ERROR FOUND")
            }
            
        })
    }
    
    
    func CallUpdateDataOnServer(urlString: String, completionHandler: @escaping (_ responseObject: String?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let result_data = data!["message"] as! String
                    if result_data.count > 0{
                        completionHandler(result_data, nil, true)
                    }else{
                        completionHandler("Update Successfully", nil, true)
                    }
                    break
                case 97 :
                    let result_data = data!["message"] as! String
                    if result_data.count > 0{
                        completionHandler(result_data, nil, true)
                    }
                    break
                case  _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
            }else{
                completionHandler("Something Went Wrong", nil, true)
            }
        })
    }
    
    func CallNotificatioSettingDataFromServer(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    completionHandler(c_data as Array<Any>, nil, true)
                    break
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                completionHandler(nil, nil, false)
            }
        })
    }
    
    func CallShareDataFromServer(urlString: String, completionHandler: @escaping (_ responseObject: Dictionary<String, Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                let result = data?[K_Result] as! Int
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String, Any>
                    completionHandler(c_data as Dictionary<String, Any>, nil, true)
                    break
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
            }else{
                completionHandler(nil, nil, false)
            }
        })
    }
    
    func CallUserAddListFromServer(urlStirng: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetworking: Bool) -> () ){
        
        CallTrackResult(urlString: urlStirng, completionHandler: {data, r_error, isNetwork in
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                case 0 :
                    let c_data = data?[K_Data] as! Array<Any>
                    completionHandler(c_data as Array<Any>, nil, true)
                    break
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                completionHandler(nil, nil, false)
                // print("ERROR FOUND")
            }
        })
        
    }
    
    func CallForgotPasswordAPI(urlString: String, completionHandler: @escaping (_ responseObject: String?, _ error: String?, _ isNetwork: Bool) -> ()){
        
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Message] as! String
                    
                    completionHandler(c_data as String, nil, true)
                    
                    break
                    
                case _ where result > 0 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(message as String, nil, true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
                
            }else{
                completionHandler(nil, nil, false)
                //print("ERROR FOUND")
            }
            
        })
        
    }
    
    
    func CallRepeatAPI(urlString: String, completionHandler: @escaping (_ responseObject: Array<Any>?, _ error: String?, _ isNetwork: Bool) -> ()){
        CallTrackResult(urlString: urlString, completionHandler: {data, r_error, isNetwork in
            
            if isNetwork{
                
                let result = data?[K_Result] as! Int
                
                switch (result){
                    
                case 0 :
                    let c_data = data?[K_Data] as! Dictionary<String,Any>
                    let vehicals_s = (c_data[VEHICALS] as! Array<Any>)
                    
                    completionHandler(vehicals_s as Array<Any>, nil, true)
                    break
                    
                case 2 :
                    let message = data?[K_Message] as! String
                    print(message)
                    completionHandler(nil, "No Result Found", true)
                    break
                default:
                    completionHandler(nil, "No Result Found", true)
                    print("Default Case")
                }
            }else{
                completionHandler(nil, nil, false)
                //print("ERROR FOUND")
            }
            
            
        })
        
    }
    
    
    func CallApiWithParameters(url: String, parameters: Dictionary<String, Any>, completion:@escaping(_ responseObject: String?, _ error: String?, _ isNetwork: Bool)->Void){
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default).responseString{ response in
            
            if (NetworkReachabilityManager()?.isReachable)!{
                // print("Network is Open")
                
                let response_d = response.result.value
                
                completion(response_d, "", true)
            }else{
                completion(nil, nil, false)
            }
        }
        
    }
    
    
    func uploadImageData(inputUrl:String,parameters:[String:Any],imageName: String?,imageFile : UIImage?,completion:@escaping(_ responseObject: Dictionary<String, Any>?, _ error: String?, _ isNetwork: Bool)->Void) {
        
        
        
        let imageData = imageFile!.jpegData(compressionQuality: 0.75)
        //imageFile.pngData()
        
        
        // CREATE AND SEND REQUEST ----------
        //let imageData = UIPNGRepresentation(image)!
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            //        for (key, value) in parameters {
            //            multipartFormData.append(value.data(using: .utf8)!, withName: key)
            //        }
            
            for key in parameters.keys{
                let name = String(key)
                if let val = parameters[name] as? String{
                    multipartFormData.append(val.data(using: .utf8)!, withName: name)
                }
            }
            
            multipartFormData.append(imageData!, withName: imageName!, fileName: "swift_file.jpeg", mimeType: "image/jpeg")
        }, to:inputUrl)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        completion(JSON as? Dictionary<String, Any>, nil, true)
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                completion(nil,  "Something Went Wrong", true)
                print(encodingError)
            }
            
        }
        
    }
    
    func StopAllThreads1(){
    
    Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
        tasks.forEach{ $0.cancel() }
    }
    
 
}

}



extension ServiceManager: RequestRetrier {
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        let error = error as NSError
        switch error.code {
        case -1009:
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] _ in
                    alert?.dismiss(animated: true, completion: nil)
                }))
                //UIApplication.topMostViewController?.present(alert, animated: true, completion: nil)
            }
        default: break
        }
        print("-- Error code: \(error.code)")
        print("-- Error descriptiom: \(error.localizedDescription)")
    }
}




