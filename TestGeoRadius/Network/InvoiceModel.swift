//
//  InvoiceModel.swift
//  GeoTrack
//
//  Created by Georadius on 31/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class InvoiceModel{

    var date : String!
    var invoice_status : String!
    var note :  String!
    var total_amount : String!
    init(date: String, invoice_status: String, note: String, total_amount: String){
        self.date = date
        self.invoice_status = invoice_status
        self.note = note
        self.total_amount = total_amount
    }
}
