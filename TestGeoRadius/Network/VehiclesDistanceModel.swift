//
//  VehiclesDistanceModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation
class VehiclesDistanceModel
    {
    var device_id : String!
    var registration_number : String!
    var total_distance : String!
   
    init(device_id: String, registration_number: String, total_distance: String) {
        self.device_id = device_id
        self.registration_number = registration_number
        self.total_distance = total_distance
    }
}
