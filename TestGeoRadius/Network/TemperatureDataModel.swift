//
//  TemperatureDataModel.swift
//  TestGeoRadius
//
//  Created by Georadius on 31/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

class TemperatureDataModel{
    
    var time : String!
    var temperature : Double!
    var distance : Double!
    var location : String!

   
    init(time: String, temperature: Double, distance:Double, location:String){
        self.time = time
        self.temperature = temperature
        self.distance = distance
        self.location = location

    }
    
}
