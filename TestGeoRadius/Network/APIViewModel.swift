//
//  APIDemoViewModel.swift
//  APIManagerDemo
//
//  Created by Pavle Pesic on 4/11/19.
//  Copyright (c) 2019 Pavle Pesic. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

protocol APIDemoViewModelProtocol {
    var alertMessage: Dynamic<AlertMessage> { get set }
    var isLoaderHidden: Dynamic<Bool> { get set }
    var user: Dynamic<APIDemo.UsersList?> { get set }
    var vehicle: Dynamic<APIDemo.VehicleList?> { get set }
    var criticalAlerts: Dynamic<APIDemo.CriricalAlertsList?> { get set }
    var vehicleDistance: Dynamic<APIDemo.VehicleDistance?> { get set }
    var customData:Dynamic<APIDemo.CustomReportData?> { get set }
    
    func getUser()
    func getAllUsers()
    func getAllVehiclesList()
    func getCriticalAlertsList()
    func getVehiclesDistance(startDate:String, endDate:String)
    func getCustomReport(startDate:String, endDate:String, startTime: String, endTime:String)
}

class APIDemoViewModel: NSObject, APIDemoViewModelProtocol {
    
    // MARK: - APIDemoViewModelProtocol
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var user: Dynamic<APIDemo.UsersList?> = Dynamic(nil)
    var vehicle: Dynamic<APIDemo.VehicleList?> = Dynamic(nil)
    var criticalAlerts: Dynamic<APIDemo.CriricalAlertsList?> = Dynamic(nil)
    var vehicleDistance: Dynamic<APIDemo.VehicleDistance?> = Dynamic(nil)
    var customData: Dynamic<APIDemo.CustomReportData?> = Dynamic(nil)

    // MARK: - Vars & Lets
    
    private let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())
    
    // MARK: - Public methods
    
    func getAllUsers()
    {
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        let urlString = domain_name + SEARCH_USER_JSON + K_USER_NAME + user_name + K_HASH_KEY + hash_key + K_GROUP_ID + group_id
        
        NetworkManager().CallUserAddListFromServer(urlStirng: urlString, completionHandler: {data, r_error, isNetwork in
            if isNetwork && data != nil{
                print("all data:\(data!)")
                for val in data!{
                    let val_data = val as! Dictionary<String, Any>
                    let user_name = val_data["username"] as! String
                    let email = val_data["email"] as! String
                    let phone = val_data["phone"] as! String
                    let user_id = val_data["userid"] as! String
                    
                    let newDetail = UserListModel(user_name: user_name, phone: phone, email: email, user_id: user_id)
                    //  self.user_list.append(newDetail)
                }
                
            }else{
                //  self.tbl_invoice.isHidden = true
                print("ERROR FOUND")
            }
            if r_error != nil{
                // showToast(controller: self, message : "Something went wrong.", seconds: 2.0)
            }
            
            // self.alert_view.removeFromSuperview()
        })
        
    }
    
    
    func getUser() {
        self.isLoaderHidden.value = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let group_id = UserDefaults.standard.value(forKey: GROUP_ID) as! String
        let urlString = domain_name + SEARCH_USER_JSON + K_USER_NAME + user_name + K_HASH_KEY + hash_key + K_GROUP_ID + group_id
        
        print("url String \(urlString)")
        self.apiManager.call(type: RequestItemsType.getUser, url: urlString) { (res: Swift.Result<APIDemo.UsersList, AlertMessage>) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let user):
                self.user.value = user
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }
    
    
    
    func getAllVehiclesList() {
        self.isLoaderHidden.value = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString = domain_name + SEARCH_VEHICLE_JSON + K_USER_NAME + user_name + K_HASH_KEY + hash_key
        
        print(" vehicle list url String \(urlString)")
        self.apiManager.call(type: RequestItemsType.getVehicle, url: urlString) { (res: Swift.Result<APIDemo.VehicleList, AlertMessage>) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let user):
                self.vehicle.value = user
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }
    
    func getCriticalAlertsList() {
        self.isLoaderHidden.value = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String
        let urlString = domain_name + CRITICAL_ALERTS_LIST + K_USER_ID + user_id + K_USER_NAME + user_name + K_HASH_KEY + hash_key + "&data_format=1"
        self.apiManager.call(type: RequestItemsType.getVehicle, url: urlString) { (res: Swift.Result<APIDemo.CriricalAlertsList, AlertMessage>) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let user):
                self.criticalAlerts.value = user
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
        print("vehicle list url String \(urlString)")
    }
    
    
    func getVehiclesDistance(startDate:String, endDate:String) {
           self.isLoaderHidden.value = false
           let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
           let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
           let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
           let user_id = UserDefaults.standard.value(forKey: USER_ID) as! String

           let urlString = domain_name + TOTAL_VEHICLE_DISTANCE + K_START_DATE + startDate + K_END_DATE + endDate + K_USER_NAME + user_name + K_HASH_KEY + hash_key + K_USER_ID + user_id
        
           print("api for distance :\(urlString)")
        
           self.apiManager.call(type: RequestItemsType.getVehicle, url: urlString) { (res: Swift.Result<APIDemo.VehicleDistance, AlertMessage>) in
               self.isLoaderHidden.value = true
               switch res {
               case .success(let user):
                   self.vehicleDistance.value = user
                   break
               case .failure(let message):
                   self.alertMessage.value = message
                   break
               }
           }
                 
           print("vehicle list url String \(urlString)")
       }
       
    
    func getAllGeofenceDeviceData(deviceId:[String])
    {
        self.isLoaderHidden.value = false
        let device_ids = deviceId.joined(separator: ",")
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
        let urlString = domain_name + Geofence_Devices_Search + "device_id=" + device_ids + "&geofence_name=" + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        print(" vehicle list url String \(urlString)")
        
        self.apiManager.call(type: RequestItemsType.getVehicle, url: urlString) { (res: Swift.Result<APIDemo.VehicleList, AlertMessage>) in
            self.isLoaderHidden.value = true
            switch res {
            case .success(let user):
                self.vehicle.value = user
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }
    
    func getCustomReport(startDate:String, endDate:String, startTime: String, endTime:String)
    {
        self.isLoaderHidden.value = false
        let domain_name = UserDefaults.standard.value(forKey: DOMAIN_NAME) as! String
        let hash_key = UserDefaults.standard.value(forKey: LoginKey) as! String
        let user_name = UserDefaults.standard.value(forKey: USER_NAME) as! String
        
      //  let urlString = domain_name + Custom_Report_Search + "date_from=" + startDate + "&date_to=" + endDate + "time_picker_from=" + startTime + "time_picker_to" + endDate + "&user_name=" + user_name + "&hash_key=" + hash_key
        
        let urlString = "http://service.doordrishti.co:8080/obdservice/public/rest/eventData/getFuelData?"
        let parameters = "username=" + user_name + "&password=" + "2255225522"
        let dateParams = "&startTime=" + "1600128000000" + "&endTime=" + "1600214399000"
        let finalUrl = urlString + parameters + dateParams
    
        self.apiManager.call(type: RequestItemsType.getCustomData, url: finalUrl) { (res: Swift.Result<APIDemo.CustomReportData, AlertMessage>) in
                   self.isLoaderHidden.value = true
                   switch res {
                   case .success(let user):
                       self.customData.value = user
                       break
                   case .failure(let message):
                       self.alertMessage.value = message
                       break
                   }
            }
    }
    
    // MARK: - Init
    override init() {
        super.init()
    }
    
}
