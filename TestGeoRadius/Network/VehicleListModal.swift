//
//  VehicleListModal.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import Foundation

class VehicleListModal
    {
    var registrationNo : String!
    var IMEINo : String!
    var voiceNo : String!
    var vehicleType : String!
    var billingValidity : String!
    var device_tag : String
    var device_id : String
    var vehicle_type_id : String
    var driver_name : String
    var driver_mobile : String
    var driver_id : String
    
    init(registrationNo: String, IMEINo: String, voiceNo: String, vehicleType: String, billingValidity:String,device_tag:String,device_id:String, vehicle_type_id:String,driver_name:String,driver_mobile:String,driver_id:String ) {
        self.registrationNo = registrationNo
        self.IMEINo = IMEINo
        self.voiceNo = voiceNo
        self.vehicleType = vehicleType
        self.billingValidity = billingValidity
        self.device_tag = device_tag
        self.device_id = device_id
        self.vehicle_type_id =  vehicle_type_id
        self.driver_name = driver_name
        self.driver_mobile = driver_mobile
        self.driver_id = driver_id
    }
}
