//
//  FuelCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FuelCell: UITableViewCell {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_date_time: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_dist_title: UILabel!
    @IBOutlet weak var lbl_fuel: UILabel!
    @IBOutlet weak var lbl_fuel_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTitleLblText()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }

    func setTitleLblText()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        lbl_fuel_title.text = bd.localizedString(forKey: "FUEL_LEVEL", value: nil, table: nil)
        lbl_dist_title.text = bd.localizedString(forKey: "DISTANCE", value: nil, table: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
