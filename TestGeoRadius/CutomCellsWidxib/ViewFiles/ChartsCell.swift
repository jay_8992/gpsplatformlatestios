//
//  ChartsCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 17/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit
import Charts

class ChartsCell: UICollectionViewCell {

    @IBOutlet weak var chartsView: BarChartView!
    @IBOutlet weak var view_content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setChart(dataPoints: [String], values: [Double]) {
        chartsView.noDataText = "No chart data available."
       var dataEntries: [BarChartDataEntry] = []
      for i in 0..<dataPoints.count {
          let dataEntry = BarChartDataEntry(x: Double(i), y: Double(values[i]))
          dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(entries: dataEntries, label: "Vehicle Data")
        let chartData = BarChartData(dataSet: chartDataSet)
        chartsView.data = chartData
    }

}
