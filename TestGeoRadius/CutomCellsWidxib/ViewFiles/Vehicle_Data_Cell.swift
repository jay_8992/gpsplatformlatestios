//
//  Vehicle_Data_Cell.swift
//  TestGeoRadius
//
//  Created by Georadius on 03/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class Vehicle_Data_Cell: UITableViewCell {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var img_heading: UIImageView!
    @IBOutlet weak var img_speed: UIImageView!
    @IBOutlet weak var lbl_speedTitle: UILabel!
    @IBOutlet weak var lbl_data: UILabel!
    @IBOutlet weak var img_distance: UIImageView!
    @IBOutlet weak var lbl_distTitle: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_haltTime: UILabel!
    @IBOutlet weak var lbl_fuel: UILabel!
    @IBOutlet weak var img_driver: UIImageView!
    @IBOutlet weak var lbl_driverName: UILabel!
    @IBOutlet weak var lbl_driverTitle: UILabel!
    @IBOutlet weak var img_coordinate: UIImageView!
    @IBOutlet weak var lbl_coordinate: UILabel!
    @IBOutlet weak var lbl_coordTitle: UILabel!
    @IBOutlet weak var img_phone: UIImageView!
    @IBOutlet weak var lbl_driverNo: UILabel!
    @IBOutlet weak var lbl_driverNoTitle: UILabel!
    @IBOutlet weak var lbl_temperature: UILabel!
    @IBOutlet weak var lbl_odometer: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_locTitle: UILabel!
    @IBOutlet weak var img_haltTime: UIImageView!
    @IBOutlet weak var lbl_title_haltTime: UILabel!
    @IBOutlet weak var img_fuel: UIImageView!
    @IBOutlet weak var lbl_title_fuel: UILabel!
    @IBOutlet weak var img_temp: UIImageView!
    @IBOutlet weak var lbl_title_temp: UILabel!
    @IBOutlet weak var img_odometer: UIImageView!
    @IBOutlet weak var img_location: UIImageView!
    @IBOutlet weak var lbl_title_odometer: UILabel!
    @IBOutlet weak var speedStack: UIStackView!
    @IBOutlet weak var haltStackView: UIStackView!
    @IBOutlet weak var driverDetailStack: UIStackView!
    @IBOutlet weak var coordinateStack: UIStackView!
    @IBOutlet weak var tempStack: UIStackView!
    @IBOutlet weak var locationStack: UIStackView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleLblText()
    }
    
    func setTitleLblText()
      {
          let lang = UserDefaults.standard.value(forKey: "language") as! String
          let bd = setLanguage(lang : lang)
          lbl_speedTitle.text = bd.localizedString(forKey: "SPEED", value: nil, table: nil)
          lbl_distTitle.text = bd.localizedString(forKey: "DISTANCE", value: nil, table: nil)
          lbl_title_haltTime.text = bd.localizedString(forKey: "HALT_TIME", value: nil, table: nil)
          lbl_title_fuel.text = bd.localizedString(forKey: "FUEL_LEVEL", value: nil, table: nil)
          lbl_coordTitle.text = bd.localizedString(forKey: "COORDINATES", value: nil, table: nil)
          lbl_driverTitle.text = bd.localizedString(forKey: "DRIVER_NAME", value: nil, table: nil) + ":"
          lbl_driverNoTitle.text = bd.localizedString(forKey: "DRIVER_NO", value: nil, table: nil) + ":"
          lbl_title_odometer.text = bd.localizedString(forKey: "ODOMETER", value: nil, table: nil)
          lbl_title_temp.text = bd.localizedString(forKey: "TEMPERATURE", value: nil, table: nil) + ":"
          lbl_locTitle.text = bd.localizedString(forKey: "LOCATION", value: nil, table: nil)
      }
      

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
