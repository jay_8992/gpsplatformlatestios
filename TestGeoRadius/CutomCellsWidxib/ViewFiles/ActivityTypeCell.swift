//
//  ActivityTypeCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class ActivityTypeCell: UITableViewCell {

    @IBOutlet weak var iconView: UIButton!
    @IBOutlet weak var activityTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
