//
//  NotificationSettingCell.swift
//  GeoTrack
//
//  Created by Georadius on 14/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class NotificationSettingCell: UITableViewCell {

    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_notification_name: UILabel!
    @IBOutlet weak var switch_setting: UISwitch!
    @IBOutlet var lbl_days: [UILabel]!
    @IBOutlet weak var view_status: UIView!
    @IBOutlet weak var view_content: UIView!
    @IBOutlet var titleLabels: Array<UILabel>!

    override func awakeFromNib() {
        super.awakeFromNib()
        setLanguageInControls()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        SetDaysLableSize()
        // Initialization code
    }
    
    func setLanguageInControls()
    {
        if titleLabels.count == 3
        {
            titleLabels[0].text = LanguageHelperClass().notificationsTxt
            titleLabels[1].text = LanguageHelperClass().activeTimeTxt
            titleLabels[2].text = LanguageHelperClass().activeDaysTxt
        }
        
        for index in 0...6
        {
            switch index {
            case 0:
                lbl_days[index].text = LanguageHelperClass().monTxt
            case 1:
                lbl_days[index].text = LanguageHelperClass().tueTxt
            case 2:
                lbl_days[index].text = LanguageHelperClass().wedTxt
            case 3:
                lbl_days[index].text = LanguageHelperClass().thuTxt
            case 4:
                lbl_days[index].text = LanguageHelperClass().friTxt
            case 5:
                lbl_days[index].text = LanguageHelperClass().satTxt
            case 6:
                lbl_days[index].text = LanguageHelperClass().sunTxt
            default:
                print("done")
            }
        }
    }

    func SetDaysLableSize(){
        view_status.frame.size.width = view_status.frame.size.height
        view_status.layer.cornerRadius = view_status.frame.size.height / 7
        view_status.layer.borderColor = UIColor.black.cgColor
        view_status.layer.borderWidth = 0.5
    
        switch_setting.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        for index in 0...6{
            lbl_days[index].frame.size.width = lbl_days[index].frame.size.height
            lbl_days[index].layer.cornerRadius = 10
            lbl_days[index].dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        }
}
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
