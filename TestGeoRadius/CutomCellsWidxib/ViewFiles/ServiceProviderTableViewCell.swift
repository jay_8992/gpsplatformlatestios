//
//  ServiceProviderTableViewCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 02/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class ServiceProviderTableViewCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
