//
//  DateTimeView.swift
//  TestGeoRadius
//
//  Created by Georadius on 31/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import Foundation

import UIKit

protocol DateTimeDataDelegate: class {
    func sendData(time:String,timeType:String)
}

class DateTimeView: UIView {
    
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var fromTimeTF: UITextField!
    @IBOutlet weak var toTimeTF: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    var datePicker : UIDatePicker!
    let toolBar = UIToolbar()
    var titleLabel : UILabel!
    var inputView1 : UIView!
    var doneButton:UIButton!
    var dissmissButton:UIButton!
    weak var delegate: DateTimeDataDelegate?

    class func createMyClassView() -> DateTimeView {
        let myClassNib = UINib(nibName: "DateTimeView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! DateTimeView
    }
    
    
    func doDatePicker(sender:UIView,title:String){
        
        // DatePicker
        let language = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : language)
        
        inputView1 = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 230))
        if #available(iOS 13.0, *) {
            inputView1.backgroundColor = UIColor.systemGroupedBackground
        } else {
           inputView1.backgroundColor = UIColor.groupTableViewBackground
        }
        
        titleLabel = UILabel(frame: CGRect(x: 20, y: 0, width: 200, height: 30))
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
        titleLabel.text = title
        
        doneButton = UIButton(frame: CGRect(x: self.frame.size.width-80, y: 185, width: 70, height: 40))
        doneButton.setTitle(bd.localizedString(forKey: "OK", value: nil, table: nil), for: .normal)
        doneButton.layer.cornerRadius = 10.0
        doneButton.backgroundColor = UIColor.darkGray
        doneButton.setTitleColor(.white, for: .normal)// add Button to UIView
        doneButton.addTarget(self, action: #selector(doneClick), for: UIControl.Event.touchUpInside)
        
        dissmissButton = UIButton(frame: CGRect(x: self.frame.size.width-160, y:185, width: 70, height: 40))
        dissmissButton.setTitle(bd.localizedString(forKey: "CANCEL", value: nil, table: nil), for: .normal)
        dissmissButton.layer.cornerRadius = 10.0
        dissmissButton.backgroundColor = UIColor.darkGray
        dissmissButton.setTitleColor(.white, for: .normal)
        // add Button to UIView
        dissmissButton.addTarget(self, action: #selector(cancelClick), for: UIControl.Event.touchUpInside)
        
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 30, width: self.frame.size.width, height: 150))
        self.datePicker?.backgroundColor = UIColor.lightGray
        self.datePicker?.maximumDate = Date()
        self.datePicker?.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        inputView1.addSubview(titleLabel)
        inputView1.addSubview(doneButton)
        inputView1.addSubview(dissmissButton)
        inputView1.addSubview(self.datePicker)
        
        self.addSubview(inputView1)
        // ToolBar
    }
    
  
    @objc func doneClick() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTxt = dateFormatter1.string(from: self.datePicker.date)

        if titleLabel.text == "From Time:"
        {
            delegate?.sendData(time: dateTxt, timeType: "fromTime")
        }
        else
        {
             delegate?.sendData(time: dateTxt, timeType: "toTime")
        }
        self.datePicker.resignFirstResponder()
        inputView1.removeFromSuperview()
    }
    
    @objc func cancelClick() {
        self.datePicker.resignFirstResponder()
        inputView1.removeFromSuperview()
    }
    
    
}
