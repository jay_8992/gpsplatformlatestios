//
//  TrackCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 25/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class TrackCell: UITableViewCell {
    @IBOutlet weak var lbl_regstration: UILabel!
    @IBOutlet weak var lbl_regn_status: UILabel!
    @IBOutlet weak var ing_ac: UIImageView!
    @IBOutlet weak var img_door: UIImageView!
    @IBOutlet weak var img_ignition: UIImageView!
    @IBOutlet weak var lbl_last_update: UILabel!
    @IBOutlet weak var lbl_coordinates: UILabel!
    @IBOutlet weak var lbl_halt_time: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var img_vehicle: UIImageView!
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var alert_color: UILabel!
    @IBOutlet weak var speedIcon: UIImageView!
    @IBOutlet weak var distanceIcon: UIImageView!
    @IBOutlet weak var haltTimeIcon: UIImageView!
    @IBOutlet weak var coordinatesIocn: UIImageView!
    @IBOutlet weak var lastUpdateIcon: UIImageView!
    @IBOutlet weak var locIcon: UIImageView!
    @IBOutlet weak var view_background: UIView!
    @IBOutlet weak var lbl_speedTitle: UILabel!
    @IBOutlet weak var lbl_distanceTitle: UILabel!
    @IBOutlet weak var lbl_haltTimeTitle: UILabel!
    @IBOutlet weak var lbl_coordinateTitle: UILabel!
    @IBOutlet weak var lbl_updateTitle: UILabel!
    @IBOutlet weak var lbl_locTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //view_content.layer.cornerRadius = view_content.frame.size.height / 18
        //view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        //alert_color.layer.cornerRadius = 7
        //alert_color.layer.masksToBounds = true
        setTitleLblText()
        
    }
    
    
    func setTitleLblText()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        lbl_speedTitle.text = bd.localizedString(forKey: "SPEED", value: nil, table: nil)
        lbl_distanceTitle.text = bd.localizedString(forKey: "DISTANCE", value: nil, table: nil)
        lbl_haltTimeTitle.text = bd.localizedString(forKey: "HALT_TIME", value: nil, table: nil)
        lbl_coordinateTitle.text = bd.localizedString(forKey: "COORDINATES", value: nil, table: nil)
        lbl_locTitle.text = bd.localizedString(forKey: "LOCATION", value: nil, table: nil)
        lbl_updateTitle.text = bd.localizedString(forKey: "LAST_UPDATE", value: nil, table: nil)
        
    }
    
    override func layoutSubviews() {
        view_content.layer.cornerRadius = 12.0
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.contentView.backgroundColor = .white
        // Configure the view for the selected state
    }
    
}
