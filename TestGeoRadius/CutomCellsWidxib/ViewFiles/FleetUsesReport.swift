//
//  FleetUsesReport.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FleetUsesReport: UITableViewCell {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_idle: UILabel!
    @IBOutlet weak var lbl_moving: UILabel!
    @IBOutlet weak var lbl_stopped: UILabel!
    @IBOutlet weak var lbl_vehicleName: UILabel!
    @IBOutlet weak var lbl_idle_title: UILabel!
    @IBOutlet weak var lbl_moving_title: UILabel!
    @IBOutlet weak var lbl_stopped_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTitleLblText()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }
    
    
    func setTitleLblText()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        lbl_idle_title.text = bd.localizedString(forKey: "IDLE", value: nil, table: nil)
        lbl_moving_title.text = bd.localizedString(forKey: "MOVING", value: nil, table: nil)
        lbl_stopped_title.text = bd.localizedString(forKey: "STOPPED", value: nil, table: nil)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
