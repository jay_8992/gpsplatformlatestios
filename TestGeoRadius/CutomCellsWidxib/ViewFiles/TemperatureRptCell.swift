//
//  TemperatureRptCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 31/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class TemperatureRptCell: UITableViewCell {

    @IBOutlet var lbl_arr: [UILabel]!
    @IBOutlet weak var lbl_temperature: UILabel!
    @IBOutlet weak var lbl_dateTime: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_location: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
