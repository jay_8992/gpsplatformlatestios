//
//  InvoiceCell.swift
//  GeoTrack
//
//  Created by Georadius on 31/07/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class InvoiceCell: UITableViewCell {
    @IBOutlet weak var view_background: UIView!
    
    @IBOutlet weak var lbl_success: UILabel!
    @IBOutlet weak var lbl_rupee: UILabel!
    @IBOutlet weak var lbl_data: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //view_background.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        //view_background.layer.cornerRadius = 12.0
        //lbl_data.sizeToFit()
       // lbl_date.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
