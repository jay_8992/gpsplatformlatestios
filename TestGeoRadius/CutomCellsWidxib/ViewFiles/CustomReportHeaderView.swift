//
//  CustomReportHeaderView.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/09/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(_ header: CustomReportHeaderView, section: Int)
}

class CustomReportHeaderView: UITableViewHeaderFooterView {

    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    @IBOutlet weak var content_view: UIView!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
   static var nib:UINib {
       return UINib(nibName: identifier, bundle: nil)
   }
   
   static var identifier: String {
       return String(describing: self)
   }
   
   override func awakeFromNib() {
       super.awakeFromNib()
      // contentView.backgroundColor =  colorLiteral(red: 0.9294117647, green: 0.937254902, blue: 0.9490196078, alpha: 1)
       addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomReportHeaderView.tapHeader(_:))))

   }
   @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
       guard let cell = gestureRecognizer.view as? CustomReportHeaderView else {
           return
       }
       
       delegate?.toggleSection(self, section: cell.section)
   }
   
   func setCollapsed(_ collapsed: Bool) {
      // arrowLbl.rotate(collapsed ? 0.0 : .pi / 2)
   }
    
    
    
}
