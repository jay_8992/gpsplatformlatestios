//
//  CameraReportCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class CameraReportCell: UITableViewCell {

    @IBOutlet weak var btn_view: UIButton!
    @IBOutlet weak var lbl_location: UILabel!
    @IBOutlet weak var lbl_speed: UILabel!
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_date_title: UILabel!
    @IBOutlet weak var lbl_loc_title: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setTitleLblText()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
    }
    
    
    func setTitleLblText()
    {
       let lang = UserDefaults.standard.value(forKey: "language") as! String
       let bd = setLanguage(lang : lang)
       lbl_date_title.text = bd.localizedString(forKey: "DATE", value: nil, table: nil)
       lbl_loc_title.text = bd.localizedString(forKey: "LOCATION", value: nil, table: nil)
   }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
