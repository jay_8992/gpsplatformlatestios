//
//  FleetUsesTimeCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class FleetUsesTimeCell: UITableViewCell {

    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_stopped: UILabel!
    @IBOutlet weak var lbl_moving: UILabel!
    @IBOutlet weak var lbl_idle: UILabel!
    @IBOutlet weak var view_stopped: UIView!
    @IBOutlet weak var view_moving: UIView!
    @IBOutlet weak var view_idle: UIView!
    @IBOutlet weak var lbl_idle_title: UILabel!
    @IBOutlet weak var lbl_moving_title: UILabel!
    @IBOutlet weak var lbl_stopped_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleLblText()
        // Initialization code
    }
    
    func setTitleLblText()
     {
         let lang = UserDefaults.standard.value(forKey: "language") as! String
         let bd = setLanguage(lang : lang)
         lbl_idle_title.text = bd.localizedString(forKey: "IDLE", value: nil, table: nil)
         lbl_moving_title.text = bd.localizedString(forKey: "MOVING", value: nil, table: nil)
         lbl_stopped_title.text = bd.localizedString(forKey: "STOPPED", value: nil, table: nil)
     }
     
     

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       // view_idle.backgroundColor = Idle_Color
       // view_moving.backgroundColor = Moving_Color
       // view_stopped.backgroundColor = Stopped_Color
        // Configure the view for the selected state
    }
    
}
