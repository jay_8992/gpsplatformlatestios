//
//  VehicleDIstanceHeaderView.swift
//  TestGeoRadius
//
//  Created by Georadius on 21/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleDIstanceHeaderView: UIView {

    @IBOutlet weak var lbl_end_date: UILabel!
    @IBOutlet weak var lbl_start_date: UILabel!
    @IBOutlet weak var lbl_from_title: UILabel!
    @IBOutlet weak var lbl_to_title: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func draw(_ rect: CGRect) {
          // Drawing code
          setTitleLblText()
    }
    
    class func instanceFromNib() -> VehicleDIstanceHeaderView {
        let myClassNib = UINib(nibName: "VehicleDIstanceHeaderView", bundle: nil)
        return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! VehicleDIstanceHeaderView
    }
    
    func setTitleLblText()
     {
         let lang = UserDefaults.standard.value(forKey: "language") as! String
         let bd = setLanguage(lang : lang)
         lbl_from_title.text = bd.localizedString(forKey: "FROM", value: nil, table: nil)
         lbl_to_title.text = bd.localizedString(forKey: "TO", value: nil, table: nil)
     }
     

}
