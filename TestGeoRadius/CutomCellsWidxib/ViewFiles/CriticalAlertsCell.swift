//
//  CriticalAlertsCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 19/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class CriticalAlertsCell: UITableViewCell {

    @IBOutlet weak var lbl_registration_no: UILabel!
    @IBOutlet weak var lbl_alert_type: UILabel!
    @IBOutlet weak var lbl_alert_time: UILabel!
    @IBOutlet weak var content_view: UIView!
    @IBOutlet var lbl_arr: [UILabel]!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
