//
//  VehicleCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/04/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleCell: UITableViewCell {

    @IBOutlet weak var lbl_item_name: UILabel!
    @IBOutlet weak var img_check: UIImageView!
    @IBOutlet weak var img_uncheck: UIImageView!
    @IBOutlet weak var content_view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if #available(iOS 11.0, *) {
            self.clipsToBounds = true
            content_view.layer.cornerRadius = 15.0
            content_view.dropShadow(color: .lightGray, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        }
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.img_uncheck.isHidden = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
