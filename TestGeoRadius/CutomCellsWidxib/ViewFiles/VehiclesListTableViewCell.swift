//
//  VehiclesListTableViewCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehiclesListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var vehicleStatusStack: UIStackView!
    @IBOutlet weak var regnStatusLbl: UILabel!
    @IBOutlet weak var regnNoTitle: UILabel!
    @IBOutlet weak var registrationNoLbl: UILabel!
    @IBOutlet weak var IMEINoLbl: UILabel!
    @IBOutlet weak var imeiTilteLbl: UILabel!
    @IBOutlet weak var voiceNoLbl: UILabel!
    @IBOutlet weak var voiceTitleLbl: UILabel!
    @IBOutlet weak var vehicleTypeLbl: UILabel!
    @IBOutlet weak var vehTypTitleLbl: UILabel!
    @IBOutlet weak var billingValidityLbl: UILabel!
    @IBOutlet weak var bilValTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setLanguageInControls()
        // Initialization code
    }

    
    func setLanguageInControls()
    {
        regnNoTitle.text = LanguageHelperClass().regNoTxt
        voiceTitleLbl.text = LanguageHelperClass().voiceNoTxt
        vehTypTitleLbl.text = LanguageHelperClass().vehicleTypeTxt
        bilValTitleLbl.text = LanguageHelperClass().billValText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
