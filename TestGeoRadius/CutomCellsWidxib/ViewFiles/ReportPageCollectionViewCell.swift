//
//  ReportPageCollectionViewCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 11/12/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class ReportPageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
}
