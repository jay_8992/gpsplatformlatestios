//
//  NearestVehiclesTableViewCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 01/02/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class NearestVehiclesTableViewCell: UITableViewCell {

    @IBOutlet weak var vehicleLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var phoneNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
