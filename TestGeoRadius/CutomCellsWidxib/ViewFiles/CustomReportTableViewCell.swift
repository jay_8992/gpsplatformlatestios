//
//  CustomReportTableViewCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 09/09/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class CustomReportTableViewCell: UITableViewCell {

    @IBOutlet var header_labels: [UILabel]!
    @IBOutlet weak var lbl_vehicle_name: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_distance_covered: UILabel!
    @IBOutlet weak var lbl_max_speed: UILabel!
    @IBOutlet weak var lbl_initial_volume: UILabel!
    @IBOutlet weak var lbl_final_volume: UILabel!
    @IBOutlet weak var lbl_fuel_consumption: UILabel!
    @IBOutlet weak var lbl_refilling_volume: UILabel!
    @IBOutlet weak var lbl_drain_fuel: UILabel!
    @IBOutlet weak var lbl_mileage: UILabel!
    @IBOutlet weak var lbl_fuel_economy: UILabel!
    @IBOutlet weak var lbl_fuel_per_hour: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
