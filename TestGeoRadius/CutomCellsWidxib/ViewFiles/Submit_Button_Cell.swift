//
//  Submit_Button_Cell.swift
//  TestGeoRadius
//
//  Created by Georadius on 04/06/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class Submit_Button_Cell: UITableViewCell {

    @IBOutlet weak var btn_submit: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
