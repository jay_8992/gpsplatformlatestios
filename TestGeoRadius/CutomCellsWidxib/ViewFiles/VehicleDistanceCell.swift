//
//  VehicleDistanceCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/05/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class VehicleDistanceCell: UITableViewCell {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_ang_speed: UILabel!
    @IBOutlet weak var lbl_speed_title: UILabel!
    @IBOutlet weak var lbl_registration: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_distance_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleLblText()
        view_content.layer.cornerRadius = view_content.frame.size.height / 18
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        // Initialization code
    }

    func setTitleLblText()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        lbl_distance_title.text = bd.localizedString(forKey: "DISTANCE", value: nil, table: nil)
        lbl_speed_title.text = bd.localizedString(forKey: "AVERAGE", value: nil, table: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
