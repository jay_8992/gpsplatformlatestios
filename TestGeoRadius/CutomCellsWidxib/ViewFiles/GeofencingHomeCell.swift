//
//  GeofencingHomeCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 06/01/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class GeofencingHomeCell: UITableViewCell {
    
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var geofenceStatusLbl: UILabel!
    @IBOutlet weak var vehicleIcon: UIImageView!
    @IBOutlet weak var vehicleTitleLbl: UILabel!
    @IBOutlet weak var dateFrmLbl: UILabel!
    @IBOutlet weak var dateToLbl: UILabel!
    @IBOutlet var titleLables: Array<UILabel>!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleLblText()
        // Initialization code
    }
    
    func setTitleLblText()
    {
        for index in 0...2
        {
            switch index {
            case 0:
                titleLables[index].text = LanguageHelperClass().vehicleTxt
            case 1:
                titleLables[index].text = LanguageHelperClass().dateFromTxt
            case 2:
                titleLables[index].text = LanguageHelperClass().dateToTxt
            default:
                print("done")
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
