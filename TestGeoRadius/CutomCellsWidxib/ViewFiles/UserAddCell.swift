//
//  UserAddCell.swift
//  GeoTrack
//
//  Created by Georadius on 17/10/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit

class UserAddCell: UITableViewCell {
    
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var lbl_phone: UILabel!
    @IBOutlet weak var lbl_phoneTitle: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_emailTitle: UILabel!
    @IBOutlet weak var lbl_user_name: UILabel!
    @IBOutlet weak var lbl_userTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleLblText()
        view_content.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        view_content.layer.cornerRadius = 12.0
        // Initialization code
    }
    
    func setTitleLblText()
    {
        let lang = UserDefaults.standard.value(forKey: "language") as! String
        let bd = setLanguage(lang : lang)
        lbl_phoneTitle.text = bd.localizedString(forKey: "PHONE_NO", value: nil, table: nil)
        lbl_emailTitle.text = bd.localizedString(forKey: "EMAIL", value: nil, table: nil)
        lbl_userTitle.text = bd.localizedString(forKey: "USERNAME", value: nil, table: nil)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
