//
//  DistanceRptCell.swift
//  TestGeoRadius
//
//  Created by Georadius on 20/03/20.
//  Copyright © 2020 Georadius. All rights reserved.
//

import UIKit

class DistanceRptCell: UITableViewCell {
    @IBOutlet weak var lbl_reg_no: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
