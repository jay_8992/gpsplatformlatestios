//
//  AppDelegate.swift
//  TestGeoRadius
//
//  Created by Georadius on 18/03/19.
//  Copyright © 2019 Georadius. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import UserNotifications
import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var shortcutHandled: Bool!
    var shortcutIdentifier: String?
    var reachability: Reachability?
    
      override init() {
           super.init()
           UIFont.overrideInitialize()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //Check if the app is not in the foreground right now
        debugPrint("Received: \(userInfo)")
        if (UIApplication.shared.applicationState != .active) {
          openNotificationsPage()
        }
    }
   
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                              didReceive response: UNNotificationResponse,
                                              withCompletionHandler completionHandler: @escaping () -> Void) {
           
           defer {
               completionHandler()
           }
              let userInfo = response.notification.request.content.userInfo
              print("userinfo : \(userInfo)")
              if let aps = userInfo[AnyHashable("aps")] as? NSDictionary, let device_id = aps["device_id"] as? NSNumber , let alert = aps["alert"] as? String
              {
                 UserDefaults.standard.set(device_id.stringValue, forKey: "deviceID")
                 UserDefaults.standard.set(alert, forKey: "alert")
                 print("action identifier is :\(response.actionIdentifier)")
                 switch response.actionIdentifier {
                 case "VIEW":
                 print("view")
                 UserDefaults.standard.set(true, forKey: "ParkingAlertClicked")
               //  self.openParkingPage()
                 case UNNotificationDismissActionIdentifier:
                 print("second")
                 default:
                 UserDefaults.standard.set(true, forKey: "ParkingAlertClicked")
                 print("default")
               }
           }
       }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map{ String(format: "%02.2hhx", $0)}.joined()
        print("Token : \(token)")
        UserDefaults.standard.set(token, forKey: "DEVICE_TOKEN")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error Notification : \(error)")
    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool  {
        handleHomeActions()
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        Thread.sleep(forTimeInterval: 0.5)
        UserDefaults.standard.set(false, forKey: "IsNotification")
        if UserDefaults.standard.value(forKey: LoginKey) == nil{
            SaveLoginKey(key: "")
        }
       
        GMSServices.provideAPIKey("AIzaSyBhlhs52SR0drBuhvDykz7_HED9LkGwabA")
        GMSPlacesClient.provideAPIKey("AIzaSyBhlhs52SR0drBuhvDykz7_HED9LkGwabA")
        IQKeyboardManager.shared.enable = true
       // IQKeyboardManager.shared.enableAutoToolbar = true
       // IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (granted, error) in
            print("Permission granted: \(granted)")
            
        }
        application.registerForRemoteNotifications ()
        return true
    }
    
    func application(_ application: UIApplication,
                     performActionFor shortcutItem: UIApplicationShortcutItem,
                     completionHandler: @escaping (Bool) -> Void) {
        shortcutIdentifier = shortcutItem.type
        shortcutHandled = true
        let openShortcutItem = handleHomeActions()
        completionHandler(openShortcutItem)
    }
    
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        ReachabilityManager.shared.stopMonitoring()

    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        ReachabilityManager.shared.startMonitoring()

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func handleHomeActions() -> Bool {
        if shortcutHandled == true {
            shortcutHandled = false
            if shortcutIdentifier == "app.georadius.home" {
                // Handle action accordingly
                openNotificationsPage()
            }
            return true
        }
        return false
    }
    
    
    func openNotificationsPage()
    {
        UserDefaults.standard.set(true, forKey: "IsNotification")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        DropDown.startListeningToKeyboard()
        let sw = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        self.window?.rootViewController = sw
        
        let cv = storyboard.instantiateViewController(withIdentifier: "CheckLoginVC") as! CheckLoginVC
        let navigationController = UINavigationController(rootViewController: cv)
        navigationController.setNavigationBarHidden(true, animated: false)
        sw.pushFrontViewController(navigationController, animated: true)
    }
    
}



